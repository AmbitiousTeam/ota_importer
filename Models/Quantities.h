#ifndef __MODEL_OTA_HOTELRATEPLANNOTIFRQ_QUANTITIES_H__
#define __MODEL_OTA_HOTELRATEPLANNOTIFRQ_QUANTITIES_H__

#include "../Models/Entity.h"

class Model_OTA_HotelRatePlanNotifRQ_Quantities : public Model_Entity {
public:
  void setMinBillableGuests(int);
  int getMinBillableGuests(void);
  
  Model_OTA_HotelRatePlanNotifRQ_Quantities();
  ~Model_OTA_HotelRatePlanNotifRQ_Quantities();
protected:
private:
  int _iMinBillableGuests;
};

#endif
