#ifndef __MODEL_OTA_HOTELRATEPLANNOTIFRQ_DOW_RESTRICTIONS_H__
#define __MODEL_OTA_HOTELRATEPLANNOTIFRQ_DOW_RESTRICTIONS_H__

#include "Entity.h"
#include "ArrivalDaysOfWeek.h"
#include "DepartureDaysOfWeek.h"

class Model_OTA_HotelRatePlanNotifRQ_DOW_Restrictions : public Model_Entity {
public:
  void setArrivalDaysOfWeek(Model_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek&);
  Model_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek* getArrivalDaysOfWeek();
  
  void setDepartureDaysOfWeek(Model_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek&);
  Model_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek* getDepartureDaysOfWeek();
  
  Model_OTA_HotelRatePlanNotifRQ_DOW_Restrictions();
  ~Model_OTA_HotelRatePlanNotifRQ_DOW_Restrictions();
protected:
private:
  
  Model_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek* _poArrivalDaysOfWeek = NULL;
  Model_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek* _poDepartureDaysOfWeek = NULL;
};

#endif
