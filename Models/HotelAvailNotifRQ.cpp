#include <string>

#include "HotelAvailNotifRQ.h"

void Model_OTA_HotelAvailNotifRQ::setAvailStatusMessages(Model_OTA_HotelAvailNotifRQ_AvailStatusMessages& oAvailStatusMessages) {
  _poAvailStatusMessages = &oAvailStatusMessages;
}

Model_OTA_HotelAvailNotifRQ_AvailStatusMessages* Model_OTA_HotelAvailNotifRQ::getAvailStatusMessages() {
  return _poAvailStatusMessages;
}

void Model_OTA_HotelAvailNotifRQ::setXmlNsXsd(std::string sXmlNsXsd) {
  _sXmlNsXsd = sXmlNsXsd;
}

std::string Model_OTA_HotelAvailNotifRQ::getXmlNsXsd() {
  return _sXmlNsXsd;
}

void Model_OTA_HotelAvailNotifRQ::setXmlNsXsi(std::string sXmlNsXsi) {
  _sXmlNsXsi = sXmlNsXsi;
}

std::string Model_OTA_HotelAvailNotifRQ::getXmlNsXsi() {
  return _sXmlNsXsi;
}

Model_OTA_HotelAvailNotifRQ::Model_OTA_HotelAvailNotifRQ() : _sXmlNsXsd(""), _sXmlNsXsi("") {
}

Model_OTA_HotelAvailNotifRQ::~Model_OTA_HotelAvailNotifRQ() {
  delete this->_poAvailStatusMessages;
}
