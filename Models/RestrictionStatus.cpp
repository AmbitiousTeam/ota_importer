#include "RestrictionStatus.h"

void Model_OTA_HotelAvailNotifRQ_RestrictionStatus::setStatus(std::string sStatus) {
  _sStatus = sStatus;
}

std::string Model_OTA_HotelAvailNotifRQ_RestrictionStatus::getStatus() {
  return _sStatus;
}

void Model_OTA_HotelAvailNotifRQ_RestrictionStatus::setRestriction(std::string sRestriction) {
  _sRestriction = sRestriction;
}

std::string Model_OTA_HotelAvailNotifRQ_RestrictionStatus::getRestriction() {
  return _sRestriction;
}

Model_OTA_HotelAvailNotifRQ_RestrictionStatus::Model_OTA_HotelAvailNotifRQ_RestrictionStatus() {
  
}

Model_OTA_HotelAvailNotifRQ_RestrictionStatus::~Model_OTA_HotelAvailNotifRQ_RestrictionStatus() {
  
}
