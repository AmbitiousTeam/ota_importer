#include "Rate.h"

void Model_OTA_HotelRatePlanNotifRQ_Rates_Rate::setAdditionalGuestAmounts(Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts& oAdditionalGuestAmounts) {
  _oAdditionalGuestAmounts = &oAdditionalGuestAmounts;
}

Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts* Model_OTA_HotelRatePlanNotifRQ_Rates_Rate::getAdditionalGuestAmounts() {
  return _oAdditionalGuestAmounts;
}

void Model_OTA_HotelRatePlanNotifRQ_Rates_Rate::setBaseByGuestAmts(Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts& oBaseByGuestAmts) {
  _oBaseByGuestAmts = &oBaseByGuestAmts;
}

Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts* Model_OTA_HotelRatePlanNotifRQ_Rates_Rate::getBaseByGuestAmts() {
  return _oBaseByGuestAmts;
}

void Model_OTA_HotelRatePlanNotifRQ_Rates_Rate::setNoOffersAllowed(bool bNoOffersAllowed) {
  _bNoOffersAllowed = bNoOffersAllowed;
}

bool Model_OTA_HotelRatePlanNotifRQ_Rates_Rate::getNoOffersAllowed() {
  return _bNoOffersAllowed;
}

Model_OTA_HotelRatePlanNotifRQ_Rates_Rate::Model_OTA_HotelRatePlanNotifRQ_Rates_Rate() {

}

Model_OTA_HotelRatePlanNotifRQ_Rates_Rate::~Model_OTA_HotelRatePlanNotifRQ_Rates_Rate() {

}
