#ifndef __MODEL_OTA_HOTELRATEPLANNOTIFRQ_RATEPLANS_H__
#define __MODEL_OTA_HOTELRATEPLANNOTIFRQ_RATEPLANS_H__

#include <string>
#include "Collection.h"
#include "RatePlan.h"
#include "Supplements.h"
#include "Offers.h"
#include "BookingRules.h"

class Model_OTA_HotelRatePlanNotifRQ_RatePlans : public Model_Collection<Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan> {
public:
  void setHotelCode(std::string);
  std::string getHotelCode();
  
  void setIdCon(std::string);
  std::string getIdCon();
  
  void setDelta(std::string);
  void setDelta(int);
  void setDelta(bool);
  bool getDelta();
  
  void setHotelName(std::string);
  std::string getHotelName();
  
  void setMultiMarkup(std::string);
  void setMultiMarkup(int);
  void setMultiMarkup(bool);
  bool getMultiMarkup();
  
  void setMultiMarkupPriority(int);
  void setMultiMarkupPriority(std::string);
  int getMultiMarkupPriority();
  
  void setOffers(Model_OTA_HotelRatePlanNotifRQ_Offers&);
  Model_OTA_HotelRatePlanNotifRQ_Offers* getOffers();
  
  void setSupplements(Model_OTA_HotelRatePlanNotifRQ_Supplements&);
  Model_OTA_HotelRatePlanNotifRQ_Supplements* getSupplements();
  
  void setBookingRules(Model_OTA_HotelRatePlanNotifRQ_BookingRules&);
  Model_OTA_HotelRatePlanNotifRQ_BookingRules* getBookingRules();
  
  Model_OTA_HotelRatePlanNotifRQ_RatePlans();
  ~Model_OTA_HotelRatePlanNotifRQ_RatePlans();
  
protected:
  
private:
  
  std::string _sHotelCode;
  std::string _sIdCon;
  std::string _sHotelName;

  bool _bMultiMarkup;
  bool _bDelta;
  
  int _iMultiMarkupPriority;
  Model_OTA_HotelRatePlanNotifRQ_Offers *_oOffers = NULL;
  Model_OTA_HotelRatePlanNotifRQ_Supplements *_oSupplements = NULL;
  Model_OTA_HotelRatePlanNotifRQ_BookingRules *_oBookingRules = NULL;
};

#endif
