#include "OfferDescription.h"

void Model_OTA_HotelRatePlanNotifRQ_OfferDescription::setText(std::string sText) {
  _sText = sText;
}

std::string Model_OTA_HotelRatePlanNotifRQ_OfferDescription::getText() {
  return _sText;
}

Model_OTA_HotelRatePlanNotifRQ_OfferDescription::Model_OTA_HotelRatePlanNotifRQ_OfferDescription() {

}

Model_OTA_HotelRatePlanNotifRQ_OfferDescription::~Model_OTA_HotelRatePlanNotifRQ_OfferDescription() {

}
