#ifndef __MODEL_OTA_HOTELRATEPLANNOTIFRQ_DATERESTRICTIONS_H__
#define __MODEL_OTA_HOTELRATEPLANNOTIFRQ_DATERESTRICTIONS_H__

#include "Collection.h"
#include "DateRestriction.h"

class Model_OTA_HotelRatePlanNotifRQ_DateRestrictions : public Model_Collection<Model_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction> {
public:
  
  Model_OTA_HotelRatePlanNotifRQ_DateRestrictions();
  ~Model_OTA_HotelRatePlanNotifRQ_DateRestrictions();
protected:
private:
};

#endif