#ifndef __MODEL_OTA_HOTELRATEPLANNOTIFRQ_SUPPLEMENTS_SUPPLEMENT_H__
#define __MODEL_OTA_HOTELRATEPLANNOTIFRQ_SUPPLEMENTS_SUPPLEMENT_H__

#include <string>
#include "Entity.h"
#include "Date.h"

class Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement : public Model_Entity, public Model_Date {
public:
  void setInvCode(std::string);
  std::string getInvCode(void);
  
  void setInvType(std::string);
  std::string getInvType(void);
  
  void setRph(std::string);
  std::string getRph(void);
  
  void setAddToBasicRateIndicator(bool);
  bool getAddToBasicRateIndicator(void);
  
  void setAdditionalGuestNumber(int);
  int getAdditionalGuestNumber(void);
  
  void setMandatoryIndicator(bool);
  bool getMandatoryIndicator(void);
  
  void setPercent(double);
  double getPercent(void);
  
  void setSingleUseIndicator(bool);
  bool getSingleUseIndicator(void);
  
  void setSupplementType(std::string);
  std::string getSupplementType(void);
  
  void setAgeQualifyingCode(int);
  int getAgeQualifyingCode(void);
  
  void setMinAge(unsigned int);
  unsigned int getMinAge(void);
  
  void setMaxAge(unsigned int);
  unsigned int getMaxAge(void);
  
  void setAmount(double);
  double getAmount(void);
  
  void setCurrencyCode(std::string);
  std::string getCurrencyCode(void);
  
  void setDuration(std::string);
  std::string getDuration(void);
  
  void setChargeType(int);
  int getChargeType(void);
  
  Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement();
  ~Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement();
protected:
private:
  bool _bAddToBasicRateIndicator;
  bool _bMandatoryIndicator;
  bool _bSingleUseIndicator;
  
  double _dPercent;
  double _dAmount;
  
  int _iAdditionalGuestNumber;
  int _iAgeQualifyingCode;
  int _iChargeType;
  
  unsigned int _iMinAge;
  unsigned int _iMaxAge;
  
  std::string _sSupplementType;
  std::string _sInvCode;
  std::string _sInvType;
  std::string _sRph;
  std::string _sCurrencyCode;
  std::string _sDuration;
};

#endif
