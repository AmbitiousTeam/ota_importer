#include "CompatibleOffer.h"

void Model_OTA_HotelRatePlanNotifRQ_CompatibleOffers_CompatibleOffer::setOfferRph(std::string sOfferRph) {
  _sOfferRph = sOfferRph;
}

std::string Model_OTA_HotelRatePlanNotifRQ_CompatibleOffers_CompatibleOffer::getOfferRph() {
  return _sOfferRph;
}

void Model_OTA_HotelRatePlanNotifRQ_CompatibleOffers_CompatibleOffer::setIncompatibleOfferIndicator(bool bIncompatibleOfferIndicator) {
  _bIncompatibleOfferIndicator = bIncompatibleOfferIndicator;
}

bool Model_OTA_HotelRatePlanNotifRQ_CompatibleOffers_CompatibleOffer::getIncompatibleOfferIndicator() {
  return _bIncompatibleOfferIndicator;
}

Model_OTA_HotelRatePlanNotifRQ_CompatibleOffers_CompatibleOffer::Model_OTA_HotelRatePlanNotifRQ_CompatibleOffers_CompatibleOffer() {

}

Model_OTA_HotelRatePlanNotifRQ_CompatibleOffers_CompatibleOffer::~Model_OTA_HotelRatePlanNotifRQ_CompatibleOffers_CompatibleOffer() {

}
