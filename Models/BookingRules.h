#ifndef __MODEL_OTA_HOTELRATEPLANNOTIFRQ_BOOKINGRULES_H__
#define __MODEL_OTA_HOTELRATEPLANNOTIFRQ_BOOKINGRULES_H__

#include "Collection.h"
#include "BookingRule.h"

class Model_OTA_HotelRatePlanNotifRQ_BookingRules : 
  public Model_Collection<Model_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule> {
  public:
    Model_OTA_HotelRatePlanNotifRQ_BookingRules();
    ~Model_OTA_HotelRatePlanNotifRQ_BookingRules();
  protected:
  private:
};

#endif
