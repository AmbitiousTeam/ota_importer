#include "Quantities.h"

void Model_OTA_HotelRatePlanNotifRQ_Quantities::setMinBillableGuests(int iMinBillableGuests) {
  _iMinBillableGuests = iMinBillableGuests;
}

int Model_OTA_HotelRatePlanNotifRQ_Quantities::getMinBillableGuests(void) {
  return _iMinBillableGuests;
}

Model_OTA_HotelRatePlanNotifRQ_Quantities::Model_OTA_HotelRatePlanNotifRQ_Quantities() {
  setMinBillableGuests(0);
}

Model_OTA_HotelRatePlanNotifRQ_Quantities::~Model_OTA_HotelRatePlanNotifRQ_Quantities() {

}
