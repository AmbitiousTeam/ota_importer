#include "Deadline.h"

void Model_OTA_HotelRatePlanNotifRQ_Deadline::setOffsetDropTime(std::string sOffsetDropTime) {
  _sOffsetDropTime = sOffsetDropTime;
}

std::string Model_OTA_HotelRatePlanNotifRQ_Deadline::getOffsetDropTime() {
  return _sOffsetDropTime;
}

void Model_OTA_HotelRatePlanNotifRQ_Deadline::setOffsetTimeUnit(std::string sOffsetTimeUnit) {
  _sOffsetTimeUnit = sOffsetTimeUnit;
}

std::string Model_OTA_HotelRatePlanNotifRQ_Deadline::getOffsetTimeUnit() {
  return _sOffsetDropTime;
}

void Model_OTA_HotelRatePlanNotifRQ_Deadline::setOffsetUnitMultiplier(int iOffsetUnitMultiplier) {
  _iOffsetUnitMultiplier = iOffsetUnitMultiplier;
}

int Model_OTA_HotelRatePlanNotifRQ_Deadline::getOffsetUnitMultiplier() {
  return _iOffsetUnitMultiplier;
}

Model_OTA_HotelRatePlanNotifRQ_Deadline::Model_OTA_HotelRatePlanNotifRQ_Deadline() {

}

Model_OTA_HotelRatePlanNotifRQ_Deadline::~Model_OTA_HotelRatePlanNotifRQ_Deadline() {

}
