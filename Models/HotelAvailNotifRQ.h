#ifndef __MODEL_OTA_HOTELAVAILNOTIFRQ_H__
#define __MODEL_OTA_HOTELAVAILNOTIFRQ_H__

#include <string>
#include "AvailStatusMessages.h"

class Model_OTA_HotelAvailNotifRQ {
public:
  void setXmlNsXsd(std::string);
  std::string getXmlNsXsd();
  
  void setXmlNsXsi(std::string);
  std::string getXmlNsXsi();
  
  void setAvailStatusMessages(Model_OTA_HotelAvailNotifRQ_AvailStatusMessages&);
  Model_OTA_HotelAvailNotifRQ_AvailStatusMessages* getAvailStatusMessages();
  
  Model_OTA_HotelAvailNotifRQ();
  ~Model_OTA_HotelAvailNotifRQ();
  
protected:
private:
  std::string _sXmlNsXsd;
  std::string _sXmlNsXsi;
  Model_OTA_HotelAvailNotifRQ_AvailStatusMessages* _poAvailStatusMessages = NULL;
};

#endif
