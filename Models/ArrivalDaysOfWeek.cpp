#include "ArrivalDaysOfWeek.h"

void Model_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::setMon(bool bMon) {
  _bMon = bMon;
}

bool Model_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::getMon() {
  return _bMon;
}

void Model_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::setTue(bool bTue) {
  _bTue = bTue;
}

bool Model_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::getTue() {
  return _bTue;
}

void Model_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::setWed(bool bWed) {
  _bWed = bWed;
}

bool Model_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::getWed() {
  return _bWed;
}

void Model_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::setThu(bool bThu) {
  _bThu = bThu;
}

bool Model_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::getThu() {
  return _bThu;
}

void Model_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::setFri(bool bFri) {
  _bFri = bFri;
}

bool Model_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::getFri() {
  return _bFri;
}

void Model_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::setSat(bool bSat) {
  _bSat = bSat;
}

bool Model_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::getSat()
{
  return _bSat;
}

void Model_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::setSun(bool bSun) {
  _bSun = bSun;
}

bool Model_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::getSun() {
  return _bSun;
}

Model_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::Model_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek() {

}

Model_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::~Model_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek() {

}
