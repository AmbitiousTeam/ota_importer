#ifndef __MODEL_COLLECTION_H__
#define __MODEL_COLLECTION_H__

#include <iostream>
#include <string>
#include <stdexcept>

#include "boost/any.hpp"
#include "Node.h"

#include "../App/Defines.cpp"

template<typename T>class Model_Collection
{
  public:
    /** Member **/
    static const std::string SORT_ASC;
    static const std::string SORT_DESC;

    /** Methods **/
    T* get(const unsigned int);
    T* get(const std::string);
    unsigned int add(T&);
    unsigned int add(T&, unsigned int);
    std::string add(T&, std::string);
    unsigned int set(T&, unsigned int);
    unsigned int set(T&, std::string);
    bool remove(unsigned int);
    bool remove(std::string);
    bool replace(unsigned int, T&);
    bool replace(std::string, T&);
    
    bool reset();
    bool checkKeyExists(unsigned int);
    bool checkKeyExists(std::string);
    bool deleteNode(Model_Node<T>*);
    
    void setFirstNode(Model_Node<T>*);
    Model_Node<T>* getFirstNode(void);
    void setLastNode(Model_Node<T>*);
    Model_Node<T>* getLastNode(void);
    Model_Node<T>* getActualNode(void);
    
    T* getData();
    bool sort(std::string);
    bool swap(Model_Node<T>*, Model_Node<T>*);
    bool inject(Model_Node<T>*, T&, bool, std::string);
    bool isEqual(std::string, std::string);
    bool isLower(std::string, std::string);
    bool isLowerOrEqual(std::string, std::string);

    int getCount() const;
    unsigned int countStringKeysInCollection(void);
    unsigned int countIntKeysInCollection(void);
    
    T* operator[] (const unsigned int);
    const T* operator[] (const unsigned int) const;
    Model_Node<T>* operator=(const Model_Collection<T>*);
    
    Model_Node<T>& operator++ ();
    //     Model_Node<T>& operator++ (int);
//     T* operator++ (int);
    T* operator++ (int);
//     Model_Node<T>& operator++ (int);
    Model_Node<T>& operator-- ();
    Model_Node<T>& operator-- (int);

    Model_Collection();
    ~Model_Collection();
    
  protected:
    /** Member **/
    /** Methods **/
  private:
    /** Member **/
    unsigned int _iCount = 0;
    Model_Node<T>* _pNode = NULL;
    Model_Node<T>* _pFirstNode = NULL;
    Model_Node<T>* _pLastNode = NULL;
    /** Methods **/
    Model_Node<T>* _search(unsigned int);
    Model_Node<T>* _search(std::string);
};

template<typename T>const std::string Model_Collection<T>::SORT_ASC = "ASC";
template<typename T>const std::string Model_Collection<T>::SORT_DESC = "DESC";

template<typename T>unsigned int Model_Collection<T>::add(T& mData) {
  Model_Node<T>* pNode = new Model_Node<T>;
  
  if (_pNode) {
    pNode->setPrev(_pNode);
    pNode->setNext(_pNode->getNext());
  } else if(_pFirstNode) {
    pNode->setPrev(_pFirstNode);
    pNode->setNext(_pFirstNode->getNext());
  } else {
    pNode->setPrev(NULL);
    pNode->setNext(NULL);
  }
  
  pNode->setKey(_iCount);
  pNode->setData(mData);
  
  if (_pNode) {
    _pNode->setNext(pNode);
  } else if (_pFirstNode) {
    _pFirstNode->setNext(pNode);
  }
  
  if (NULL == _pFirstNode) {
    _pFirstNode = pNode;
  }
  if (NULL == _pLastNode) {
    Model_Collection::setLastNode(pNode);
  }
  _pNode = pNode;
  _iCount++;
  return pNode->getIntKey();
}

template<typename T>unsigned int Model_Collection<T>::add(T& mData, unsigned int iKey) {
  Model_Node<T>* pNode = new Model_Node<T>;
  
  if (_pNode) {
    pNode->setPrev(_pNode);
    pNode->setNext(_pNode->getNext());
  } else if(_pFirstNode) {
    pNode->setPrev(_pFirstNode);
    pNode->setNext(_pFirstNode->getNext());
  } else {
    pNode->setPrev(NULL);
    pNode->setNext(NULL);
  }
  
  if (iKey >= 0) {
    pNode->setKey(iKey);
  } else {
    pNode->setKey(_iCount);
  }
  pNode->setData(mData);
  
  if (_pNode) {
    _pNode->setNext(pNode);
  } else if (_pFirstNode) {
    _pFirstNode->setNext(pNode);
  }
  
  if (NULL == _pFirstNode) {
    _pFirstNode = pNode;
  }
//   if (0 == _pLastNode) {
    Model_Collection::setLastNode(pNode);
//   }
  _pNode = pNode;
  _iCount++;
  return pNode->getIntKey();
}

template<typename T>std::string Model_Collection<T>::add(T& mData, std::string sKey) {
  Model_Node<T>* pNode = new Model_Node<T>;
  
  if (_pNode) {
    pNode->setPrev(_pNode);
    pNode->setNext(_pNode->getNext());
  } else if(_pFirstNode) {
    pNode->setPrev(_pFirstNode);
    pNode->setNext(_pFirstNode->getNext());
  } else {
    pNode->setPrev(NULL);
    pNode->setNext(NULL);
  }
  
  pNode->setKey(sKey);
  pNode->setData(mData);
  
  if (_pNode) {
    _pNode->setNext(pNode);
  } else if (_pFirstNode) {
    _pFirstNode->setNext(pNode);
  }
  
  if (! _pFirstNode) {
    _pFirstNode = pNode;
  }
  Model_Collection::setLastNode(pNode);
  _pNode = pNode;
  _iCount++;
  return pNode->getStringKey();
}

template<typename T>T* Model_Collection<T>::get(unsigned int iIndex) {
  Model_Node<T>* pNode = Model_Collection::_search(iIndex);
  if (pNode) {
    return pNode->getData();
  } else {
    return NULL;
  }
}

template<typename T>T* Model_Collection<T>::get(std::string sIndex) {
  Model_Node<T>* pNode = Model_Collection::_search(sIndex);
  if (pNode) {
    return pNode->getData();
  } else {
    return NULL;
  }
}

template<typename T>Model_Node<T>* Model_Collection<T>::_search(unsigned int iKey) {
  Model_Node<T>* pNode = Model_Collection::getFirstNode();
  while (pNode) {
    unsigned int iTempKey = pNode->getIntKey();
    if (iKey == iTempKey) {
      break;
    }
    pNode = pNode->getNext();
  }
  return pNode;
}

template<typename T>Model_Node<T>* Model_Collection<T>::_search(std::string sKey) {
  Model_Node<T>* pNode = Model_Collection::getFirstNode();
  while (pNode) {
    std::string sStringKey = pNode->getStringKey();
    if (sKey == sStringKey) {
      break;
    }
    pNode = pNode->getNext();
  }
  return pNode;
}

template<typename T>unsigned int Model_Collection<T>::set(T& mData, unsigned int iIndex = -1) {
  if (-1 == iIndex) {
    add(mData);
  } else {
    replace(iIndex, mData);
  }
  return true;
}

template<typename T>unsigned int Model_Collection<T>::set(T& mData, std::string sKey) {
  if (0 < sKey.length()) {
    Model_Node<T>* pNode = _search(sKey);
    if (pNode) {
      pNode->setData(mData);
    } else {
      add(mData, sKey);
    }
  } else {
    add(mData);
  }
  return true;
}

template<typename T>bool Model_Collection<T>::remove(unsigned int iIndex) {
  if (0 < iIndex 
//     && iIndex <= _iCount
  ) {
    Model_Node<T>* pActualNode = _search(iIndex);
    Model_Node<T>* pPrevNode = pActualNode->getPrev();
    Model_Node<T>* pNextNode = pActualNode->getNext();
    pPrevNode->setNext(pNextNode);
    pNextNode->setPrev(pPrevNode);
    
    delete pActualNode;
  } else {
    return false;
  }
  
  return true;
}

template<typename T>bool Model_Collection<T>::replace(unsigned int iIndex, T& mData) {
  if (0 < iIndex 
//     && iIndex <= _iCount
  ) {
    Model_Node<T>* pAcutalNode = _search(iIndex);
    if (pAcutalNode) {
      pAcutalNode->setData(mData);
    } else {
      add(mData, iIndex);
    }
  } else {
    add(mData, iIndex);
  }
  return true;
}

template<typename T>bool Model_Collection<T>::checkKeyExists(unsigned int iKey) {
  Model_Node<T>* pNode = Model_Collection::getFirstNode();
  while (pNode) {
    if (iKey == pNode->getIntKey()
    ) {
      return true;
    }
    pNode = pNode->getNext();
  }
  return false;
}

template<typename T>bool Model_Collection<T>::checkKeyExists(std::string sKey) {
  Model_Node<T>* pNode = Model_Collection::getFirstNode();
  while (pNode) {
    if (sKey == pNode->getStringKey()) {
      return true;
    }
    pNode = pNode->getNext();
  }
  return false;
}

template<typename T>bool Model_Collection<T>::reset() {
  if (_pFirstNode) {
    _pNode = _pFirstNode;
    return true;
  }
  return false;
}

template<typename T>bool Model_Collection<T>::deleteNode(Model_Node<T>* pNode) {
  
}

template<typename T>void Model_Collection<T>::setFirstNode(Model_Node<T>* pFirstNode) {
//   if (pFirstNode == 0x30) {
//     std::cout << "TEST!" << std::endl;
//   }
  _pFirstNode = pFirstNode;
}

template<typename T>Model_Node<T>* Model_Collection<T>::getFirstNode() {
  if (_pFirstNode) {
    return _pFirstNode;
  }
  return NULL;
}

template<typename T>void Model_Collection<T>::setLastNode(Model_Node<T>* pLastNode) {
  _pLastNode = pLastNode;
}

template<typename T>Model_Node<T>* Model_Collection<T>::getLastNode() {
  return _pLastNode;
}

template<typename T>Model_Node<T>* Model_Collection<T>::getActualNode() {
  if (_pNode) {
    return _pNode;
  } else if (_pFirstNode == _pLastNode) {
    return _pFirstNode;
  }
}

template<typename T>int Model_Collection<T>::getCount() const {
  return _iCount;
}


template<typename T>T* Model_Collection<T>::operator[] (const unsigned int iIndex)
{
  Model_Node<T>* pNode = _search(iIndex);
  
  if (pNode) {
    return pNode->getData();
  }
  
  // index wurde nicht gefunden
  // wenn der index kleiner gleich dem aktuellen maxCount, gültig, also setzen
  if (iIndex <= _iCount) {
    T mData;
    add(mData, iIndex);
    Model_Node<T>* pNode = _search(iIndex);
    return pNode->getData();
  }
  return _pFirstNode->getData();
}

template<typename T>const T* Model_Collection<T>::operator[] (const unsigned int iIndex) const
{
  Model_Node<T>* pNode = _search(iIndex);
  
  if (pNode) {
    return pNode->getData();
  }
  
  // index wurde nicht gefunden
  // wenn der index kleiner gleich dem aktuellen maxCount, gültig, also setzen
  if (iIndex <= _iCount) {
    T mData;
    add(mData, iIndex);
    Model_Node<T>* pNode = _search(iIndex);
    return pNode->getData();
  }
  return _pFirstNode->getData();
}

// ++Object
template<typename T>Model_Node<T>& Model_Collection<T>::operator++ () {
  if (_pNode) {
    _pNode = _pNode->getNext();
  }
  return _pNode;
}

// Object++
// template<typename T>Model_Node<T>* Model_Collection<T>::operator++ (int iIndex) {
//   Model_Node<T>* pTmp = _pNode;
//   if (_pNode) {
//     _pNode = _pNode->getNext();
//   }
//   return pTmp;
// }

// Object++
// template<typename T>Model_Node<T>& Model_Collection<T>::operator++ (int iIndex) {
//   Model_Node<T>* pTmp = _pNode;
//   if (_pNode) {
//     _pNode = _pNode->getNext();
//   }
//   return pTmp;
// }

// Object++
// template<typename T>T* Model_Collection<T>::operator++ (int iIndex) {
//   Model_Node<T>* pTmp = _pNode;
//   if (_pNode) {
//     _pNode = _pNode->getNext();
//   }
//   return pTmp->getData();
// }

template<typename T>T* Model_Collection<T>::operator++ (int iIndex) {
  Model_Node<T>* pTmp = _pNode;
  if (_pNode) {
    _pNode = _pNode->getNext();
  }
  
  if (pTmp) {
    return pTmp->getData();
  }
  return NULL;
}

template<typename T>Model_Node<T>& Model_Collection<T>::operator-- () {
  if (_pNode) {
    _pNode = _pNode->getPrev();
  }
  return _pNode;
}

template<typename T>Model_Node<T>& Model_Collection<T>::operator-- (int iIndex) {
  Model_Node<T>* pTmp = _pNode;
  if (_pNode) {
    _pNode = _pNode->getPrev();
  }
  return pTmp;
}

template<typename T>Model_Node<T>* Model_Collection<T>::operator=(const Model_Collection<T>* mData) {
  if (DEBUG_MODE) {std::cout << "Rufe operator= auf!" << std::endl;}
  return *this;
}

template<typename T>T* Model_Collection<T>::getData() {
  if (_pNode) {
    return _pNode->getData();
  }
}

template<typename T>bool Model_Collection<T>::sort(std::string sOrder = SORT_ASC) {
  _pNode = getFirstNode();
  Model_Node<T>* pTempNode = NULL;
  
  // sammeln welche keys hier vorhanden sind, sind string und int keys gesetzt haben string keys vorrang
  unsigned int iStringKeyCount = countStringKeysInCollection();
//   unsigned int iIntKeyCount = countIntKeysInCollection();
  
  while (_pNode) {

    if (!pTempNode) {
      pTempNode = _pNode;
    }

    if (sOrder == SORT_ASC
      && _pNode != pTempNode
      && ((0 < iStringKeyCount 
          && _pNode->getStringKey() < pTempNode->getStringKey())
        || ( 0 == iStringKeyCount
           && _pNode->getIntKey() < pTempNode->getIntKey()))
    ) {
      swap(pTempNode, _pNode);
      _pNode = getFirstNode();
      pTempNode = NULL;
    } else if (sOrder == SORT_DESC
      && _pNode != pTempNode
      && ((0 < iStringKeyCount 
          && _pNode->getStringKey() > pTempNode->getStringKey())
      || (0 == iStringKeyCount 
         && _pNode->getIntKey() > pTempNode->getIntKey()))
    ) {
      swap(pTempNode, _pNode);
      _pNode = getFirstNode();
      pTempNode = NULL;
    } else {
      pTempNode = _pNode;
      _pNode = _pNode->getNext();
    }
  }
  // nach dem sortieren wieder auf den anfang setzen
  reset();
}

template<typename T>unsigned int Model_Collection<T>::countStringKeysInCollection(void) {
  unsigned int iCount = 0;
  Model_Node<T>* pNode = _pFirstNode;
  
  while (pNode) {
    if (0 < pNode->getStringKey().length()) {
      iCount++;
    }
    pNode = pNode->getNext();
  }
  return iCount;
}

template<typename T>unsigned int Model_Collection<T>::countIntKeysInCollection(void) {
  unsigned int iCount = 0;
  Model_Node<T>* pNode = _pFirstNode;
  
  while (pNode) {
    iCount++;
    pNode = pNode->getNext();
  }
  return iCount;
}

template<typename T>bool Model_Collection<T>::isEqual(std::string sString1, std::string sString2) {
  return sString1 == sString2;
}

template<typename T>bool Model_Collection<T>::isLower(std::string sString1, std::string sString2) {
  return sString1 < sString2;
}

template<typename T>bool Model_Collection<T>::isLowerOrEqual(std::string sString1, std::string sString2) {
  return sString1 <= sString2;
}

template<typename T>bool Model_Collection<T>::swap(Model_Node<T>* pNode1, Model_Node<T>* pNode2) {
  Model_Node<T>* pNodeTemp1 = pNode1;
  Model_Node<T>* pNodeTemp2 = pNode2;
  Model_Node<T>* pNodeTemp1Prev = pNode1->getPrev();
  Model_Node<T>* pNodeTemp1Next = pNode1->getNext();
  Model_Node<T>* pNodeTemp2Prev = pNode2->getPrev();
  Model_Node<T>* pNodeTemp2Next = pNode2->getNext();

  // 9 2 6
  pNode1 = pNode2;

  // 3 9 2
  pNode2 = pNodeTemp1;

  // 1 3 2
  if (pNodeTemp1Prev) {
    pNodeTemp1Prev->setNext(pNode1);
  } else {
    setFirstNode(pNode1);
  }

  // 3 2 6
  pNode1->setPrev(pNodeTemp1Prev);

  // 3 2 9
  pNode1->setNext(pNode2);

  // 2 9 2
  pNode2->setPrev(pNode1);

  // 2 9 6
  pNode2->setNext(pNodeTemp2Next);

  // 9 6 10
  if (pNodeTemp2Next) {
    pNodeTemp2Next->setPrev(pNode2);
  } else {
    setLastNode(pNode2);
  }

  return true;
}

template<typename T>bool Model_Collection<T>::inject(Model_Node<T>* pRefNode, T& mData, bool bAfter = true, std::string sKey = "") {
  int iActualKey = add(mData);
  Model_Node<T>* pNewNode = _search(iActualKey);
  
  Model_Node<T>* pRefNodePrev = pRefNode->getPrev();
  Model_Node<T>* pRefNodeNext = pRefNode->getNext();
  Model_Node<T>* pNewNodePrev = pNewNode->getPrev();
  Model_Node<T>* pNewNodeNext = pNewNode->getNext();
  
  // wenn danach und das folgende war leer, letzten knoten neu setzen
//   if (!pNewNodeNext) {
//     setLastNode(pRefNodeNext);
//   }
  
  // wenn es nicht nur einen knoten gibt (firstnode != lastnode)
//   if (pRefNodeNext) {
//     pRefNodeNext->setPrev(pNewNode);
// //     pRefNodeNext->setNext(pNewNodeNext);
//   }
  
  pNewNode->setNext(pRefNodeNext);
  pNewNode->setPrev(pRefNode);
  pRefNode->setNext(pNewNode);
  
  if (pNewNode->getNext()) {
    pNewNode->getNext()->setPrev(pNewNode);
    // wenn die aktuelle node nach der nächsten eingefügt wurde
    if (pNewNode->getNext()->getNext() == pNewNode) {
      pNewNode->getNext()->setNext(pNewNodeNext);
    }
  }
  
//   if (pNewNodeNext) {
//     pNewNodeNext->setPrev(pRefNodeNext);
//   }
  pNewNode->setKey(sKey);
  return true;
}

/**
 * CTOR
 */
template<typename T>Model_Collection<T>::Model_Collection() {
  this->_iCount = 0;
}

/**
 * DTOR
 */
// template<typename T>Model_Collection<T>::~Model_Collection() {
// //   std::cout << "DTOR von Collection!" << std::endl;
//   
// //   Model_Node<T>* pNode = getFirstNode();
// //   Model_Node<T>* pTempNode = pNode;
// //   while (pTempNode) {
// //     pNode = pTempNode;
// //     pTempNode = pNode->getNext();
// //     delete pNode;
// //   }
// //   // nur wenn es auch ein folge node gab, diese auch freigeben!
// //   if (pTempNode) {
// //     delete pTempNode;
// //   }
// }

template<typename T>Model_Collection<T>::~Model_Collection() {
  
}
#endif
