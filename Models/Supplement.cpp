#include "Supplement.h"

void Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setAdditionalGuestNumber(int iAdditionalGuestNumber) {
  _iAdditionalGuestNumber = iAdditionalGuestNumber;
}

int Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::getAdditionalGuestNumber(void) {
  return _iAdditionalGuestNumber;
}

void Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setAddToBasicRateIndicator(bool bAddToBasicRateIndicator) {
  _bAddToBasicRateIndicator = bAddToBasicRateIndicator;
}

bool Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::getAddToBasicRateIndicator(void) {
  return _bAddToBasicRateIndicator;
}

void Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setAgeQualifyingCode(int iAgeQualifyingCode) {
  _iAgeQualifyingCode = iAgeQualifyingCode;
}

int Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::getAgeQualifyingCode(void) {
  return _iAgeQualifyingCode;
}

void Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setAmount(double dAmount) {
  _dAmount = dAmount;
}

double Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::getAmount(void) {
  return _dAmount;
}

void Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setCurrencyCode(std::string sCurrencyCode) {
  _sCurrencyCode = sCurrencyCode;
}

std::string Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::getCurrencyCode(void) {
  return _sCurrencyCode;
}

void Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setDuration(std::string sDuration) {
  _sDuration = sDuration;
}

std::string Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::getDuration(void) {
  return _sDuration;
}

void Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setInvCode(std::string sInvCode) {
  _sInvCode = sInvCode;
}

std::string Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::getInvCode(void) {
  return _sInvCode;
}

void Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setInvType(std::string sInvType) {
  _sInvType = sInvType;
}

std::string Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::getInvType(void) {
  return _sInvType;
}

void Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setMandatoryIndicator(bool bMandatoryIndicator) {
  _bMandatoryIndicator = bMandatoryIndicator;
}

bool Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::getMandatoryIndicator(void) {
  return _bMandatoryIndicator;
}

void Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setMaxAge(unsigned int iMaxAge) {
  _iMaxAge = iMaxAge;
}

unsigned int Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::getMaxAge(void) {
  return _iMaxAge;
}

void Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setMinAge(unsigned int iMinAge) {
  _iMinAge = iMinAge;
}

unsigned int Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::getMinAge(void) {
  return _iMinAge;
}

void Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setPercent(double dPercent) {
  _dPercent = dPercent;
}

double Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::getPercent(void) {
  return _dPercent;
}

void Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setRph(std::string sRph) {
  _sRph = sRph;
}

std::string Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::getRph(void) {
  return _sRph;
}

void Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setChargeType(int iChargeType) {
  _iChargeType = iChargeType;
}

int Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::getChargeType(void) {
  return _iChargeType;
}

void Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setSingleUseIndicator(bool bSingleUseIndicator) {
  _bSingleUseIndicator = bSingleUseIndicator;
}

bool Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::getSingleUseIndicator() {
  return _bSingleUseIndicator;
}

void Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setSupplementType(std::string sSupplementType) {
  _sSupplementType = sSupplementType;
}

std::string Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::getSupplementType() {
  return _sSupplementType;
}

Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement() {
  setAdditionalGuestNumber(0);
  setAddToBasicRateIndicator(true);
  setAgeQualifyingCode(0);
  setAmount(0.00);
  setChargeType(0);
  setCurrencyCode("");
  setDuration("");
  setInvCode("");
  setInvType("");
  setMandatoryIndicator(false);
  setMaxAge(0);
  setMinAge(0);
  setPercent(0.00);
  setRph("");
  setSingleUseIndicator(false);
  setSupplementType("");
}

Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::~Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement() {
  
}
