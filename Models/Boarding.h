#ifndef __MODEL_BOARDING_H__
#define __MODEL_BOARDING_H__

#include "Entity.h"
#include "Supplements.h"
#include "SellableProducts.h"

class Model_Boarding : public Model_Entity {
public:
  void setSupplements(Model_OTA_HotelRatePlanNotifRQ_Supplements&);
  Model_OTA_HotelRatePlanNotifRQ_Supplements* getSupplements(void);
  
  void setSellableProducts(Model_OTA_HotelRatePlanNotifRQ_SellableProducts&);
  Model_OTA_HotelRatePlanNotifRQ_SellableProducts* getSellableProducts(void);
  
protected:
private:
  Model_OTA_HotelRatePlanNotifRQ_Supplements* _poSupplements = NULL;
  Model_OTA_HotelRatePlanNotifRQ_SellableProducts* _poSellableProducts = NULL;
};

#endif
