#include "DepartureDaysOfWeek.h"

void Model_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::setMon(bool bMon) {
  _bMon = bMon;
}

bool Model_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::getMon() {
  return _bMon;
}

void Model_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::setTue(bool bTue) {
  _bTue = bTue;
}

bool Model_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::getTue() {
  return _bTue;
}

void Model_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::setWed(bool bWed) {
  _bWed = bWed;
}

bool Model_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::getWed() {
  return _bWed;
}

void Model_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::setThu(bool bThu) {
  _bThu = bThu;
}

bool Model_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::getThu() {
  return _bThu;
}

void Model_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::setFri(bool bFri) {
  _bFri = bFri;
}

bool Model_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::getFri() {
  return _bFri;
}

void Model_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::setSat(bool bSat) {
  _bSat = bSat;
}

bool Model_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::getSat()
{
  return _bSat;
}

void Model_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::setSun(bool bSun) {
  _bSun = bSun;
}

bool Model_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::getSun() {
  return _bSun;
}

Model_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::Model_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek() {

}

Model_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::~Model_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek() {

}
