#include "TravellerConstellation.h"

void Model_TravellerConstellation::setAdditionalAdults(int iAdditionalAdults) {
  _iAdditionalAdults = iAdditionalAdults;
}

int Model_TravellerConstellation::getAdditionalAdults() {
  return _iAdditionalAdults;
}

void Model_TravellerConstellation::setAdditionalChildren(int iAdditionalChildren) {
  _iAdditionalChildren = iAdditionalChildren;
}

int Model_TravellerConstellation::getAdditionalChildren() {
  return _iAdditionalChildren;
}

void Model_TravellerConstellation::setAdditionalInfants(int iAdditionalInfantCount) {
  _iAdditionalInfantCount = iAdditionalInfantCount;
}

int Model_TravellerConstellation::getAdditionalInfants() {
  return _iAdditionalInfantCount;
}

void Model_TravellerConstellation::setAdultCount(int iAdultCount) {
  _iAdultCount = iAdultCount;
}

int Model_TravellerConstellation::getAdultCount() {
  return _iAdultCount;
}

void Model_TravellerConstellation::setChildCount(int iChildCount) {
  _iChildCount = iChildCount;
}

int Model_TravellerConstellation::getChildCount() {
  return _iChildCount;
}

void Model_TravellerConstellation::setInfantCount(int iInfantCount) {
  _iInfantCount = iInfantCount;
}

int Model_TravellerConstellation::getInfantCount() {
  return _iInfantCount;
}

void Model_TravellerConstellation::setPersonCount(int iPersonCount) {
  _iPersonCount = iPersonCount;
}

int Model_TravellerConstellation::getPersonCount() {
  return _iPersonCount;
}

void Model_TravellerConstellation::setStandardCapacity(int iStandardCapacity) {
  _iStandardCapacity = iStandardCapacity;
}

int Model_TravellerConstellation::getStandardCapacity() {
  return _iStandardCapacity;
}

void Model_TravellerConstellation::setAgeGroup(Model_AgeGroup& oAgeGroup) {
  _oAgeGroup = &oAgeGroup;
}

Model_AgeGroup* Model_TravellerConstellation::getAgeGroup(void) {
  return _oAgeGroup;
}

Model_TravellerConstellation::Model_TravellerConstellation() {
  setPersonCount(0);
  setAdultCount(0);
  setChildCount(0);
  setInfantCount(0);
  setAdditionalAdults(0);
  setAdditionalChildren(0);
  setAdditionalInfants(0);
  setStandardCapacity(0);
}

Model_TravellerConstellation::~Model_TravellerConstellation() {
  delete this->_oAgeGroup;
}
