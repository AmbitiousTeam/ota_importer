#ifndef __MODEL_OTAHOTELRATEPLANNORIFRQ_BOOKINGRULES_BOOKINGRULE_H__
#define __MODEL_OTAHOTELRATEPLANNORIFRQ_BOOKINGRULES_BOOKINGRULE_H__

#include <string>
#include "Entity.h"
#include "Date.h"
#include "DOW_Restrictions.h"
#include "LengthsOfStay.h"
#include "CancelPenalty.h"
#include "RestrictionStatus.h"

class Model_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule : public Model_Entity, public Model_Date {
public:
  
  void setDOWRestrictions(Model_OTA_HotelRatePlanNotifRQ_DOW_Restrictions&);
  Model_OTA_HotelRatePlanNotifRQ_DOW_Restrictions* getDOWRestrictions();
  
  void setLengthsOfStay(Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay&);
  Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay* getLengthsOfStay();
  
  void setCancelPenalty(Model_OTA_HotelRatePlanNotifRQ_CancelPenalty&);
  Model_OTA_HotelRatePlanNotifRQ_CancelPenalty* getCancelPenalty();
  
  void setRestrictionStatus(Model_OTA_HotelAvailNotifRQ_RestrictionStatus&);
  Model_OTA_HotelAvailNotifRQ_RestrictionStatus* getRestrictionStatus();
  
  Model_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule();
  ~Model_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule();
    
protected:
private:
  
  Model_OTA_HotelRatePlanNotifRQ_DOW_Restrictions* _poDOWRestrictions = NULL;
  Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay* _poLengthsOfStay = NULL;
  Model_OTA_HotelRatePlanNotifRQ_CancelPenalty* _poCancelPenalty = NULL;
  Model_OTA_HotelAvailNotifRQ_RestrictionStatus* _poRestrictionStatus = NULL;
};

#endif
