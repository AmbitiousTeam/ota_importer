#ifndef __MODEL_DATE_H__
#define __MODEL_DATE_H__

#include <string>

class Model_Date {
public:
  void setStart(std::string);
  std::string getStart();
  
  void setEnd(std::string);
  std::string getEnd();
  
  Model_Date();
  ~Model_Date();
protected:
private:
  std::string _sStart;
  std::string _sEnd;
};

#endif