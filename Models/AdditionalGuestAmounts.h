#ifndef __MODEL_OTA_HOTELRATEPLANNOTIFRQ_ADDITIONALGUESTAMOUNTS_H__
#define __MODEL_OTA_HOTELRATEPLANNOTIFRQ_ADDITIONALGUESTAMOUNTS_H__

#include "Collection.h"
#include "AdditionalGuestAmount.h"

class Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts : public Model_Collection<Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount> {
public:
  
  Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts();
  ~Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts();
protected:
private:
};

#endif