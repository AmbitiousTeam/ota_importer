#ifndef __MODEL_OTA_HOTELRATEPLANNOTIFRQ_AMOUNTPERCENT_H__
#define __MODEL_OTA_HOTELRATEPLANNOTIFRQ_AMOUNTPERCENT_H__

#include <string>
#include "Entity.h"

class Model_OTA_HotelRatePlanNotifRQ_AmountPercent : public Model_Entity {
public:
  void setCurrencyCode(std::string);
  std::string getCurrencyCode();
  
  void setPercent(float);
  float getPercent();
  
  void setAmount(float);
  float getAmount();
  
  Model_OTA_HotelRatePlanNotifRQ_AmountPercent();
  ~Model_OTA_HotelRatePlanNotifRQ_AmountPercent();
protected:
private:
  std::string _sCurrencyCode;
  float _fPercent;
  float _fAmount;
};

#endif