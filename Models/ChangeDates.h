#ifndef __MODEL_CHANGEDATES_H__
#define __MODEL_CHANGEDATES_H__

#include "Date.h"
#include "Collection.h"

class Model_ChangeDates : public Model_Collection<Model_Date> {
public:
  
  Model_ChangeDates();
  ~Model_ChangeDates();
protected:
private:
};

#endif