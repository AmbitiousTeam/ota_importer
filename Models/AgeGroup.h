#ifndef __MODEL_AGEGROUP_H__
#define __MODEL_AGEGROUP_H__

#include "Entity.h"

class Model_AgeGroup : public Model_Entity {
public:
  
  void setInfantMinAge(unsigned int);
  unsigned int getInfantMinAge(void);
  
  void setInfantMaxAge(unsigned int);
  unsigned int getInfantMaxAge(void);
  
  void setChildrenMinAge(unsigned int);
  unsigned int getChildrenMinAge(void);
  
  void setChildrenMaxAge(unsigned int);
  unsigned int getChildrenMaxAge(void);
  
  void setAdultMinAge(unsigned int);
  unsigned int getAdultMinAge(void);
  
  void setAdultMaxAge(unsigned int);
  unsigned int getAdultMaxAge(void);
  
  Model_AgeGroup();
  ~Model_AgeGroup();
protected:
private:
  unsigned int _iInfantMinAge;
  unsigned int _iInfantMaxAge;
  unsigned int _iChildrenMinAge;
  unsigned int _iChildrenMaxAge;
  unsigned int _iAdultMinAge;
  unsigned int _iAdultMaxAge;
};

#endif
