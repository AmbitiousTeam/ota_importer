#ifndef __MODEL_OTA_HOTELRATEPLANNOTIFRQ_OCCUPANCIES_H__
#define __MODEL_OTA_HOTELRATEPLANNOTIFRQ_OCCUPANCIES_H__

#include "Collection.h"
#include "Occupancy.h"

class Model_OTA_HotelRatePlanNotifRQ_Occupancies: public Model_Collection<Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy> {
public:
  
  Model_OTA_HotelRatePlanNotifRQ_Occupancies();
  ~Model_OTA_HotelRatePlanNotifRQ_Occupancies();
protected:
private:
};

#endif