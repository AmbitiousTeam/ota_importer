#include "SellableProduct.h"

void Model_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct::setGuestRoom(Model_OTA_HotelRatePlanNotifRQ_GuestRoom& oGuestRoom) {
  _oGuestRoom = &oGuestRoom;
}

Model_OTA_HotelRatePlanNotifRQ_GuestRoom* Model_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct::getGuestRoom() {
  return _oGuestRoom;
}

void Model_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct::setInvType(std::string sInvType) {
  _sInvType = sInvType;
}

std::string Model_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct::getInvType() {
  return _sInvType;
}

void Model_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct::setInvCode(std::string sInvCode) {
  _sInvCode = sInvCode;
}

std::string Model_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct::getInvCode() {
  return _sInvCode;
}

Model_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct::Model_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct() {

}

Model_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct::~Model_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct() {
  delete this->_oGuestRoom;
}
