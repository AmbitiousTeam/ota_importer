#include "OfferRule.h"

void Model_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule::setDateRestrictions(Model_OTA_HotelRatePlanNotifRQ_DateRestrictions& oDateRestrictions) {
  _oDateRestrictions = &oDateRestrictions;
}

Model_OTA_HotelRatePlanNotifRQ_DateRestrictions* Model_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule::getDateRestrictions() {
  return _oDateRestrictions;
}

void Model_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule::setLengthsOfStay(Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay& oLengthsOfStay) {
  _oLenghtsOfStay = &oLengthsOfStay;
}

Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay* Model_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule::getLengthsOfStay() {
  return _oLenghtsOfStay;
}

Model_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule::Model_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule() {
  
}

Model_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule::~Model_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule() {

}

