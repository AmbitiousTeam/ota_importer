#include "Boarding.h"

void Model_Boarding::setSupplements(Model_OTA_HotelRatePlanNotifRQ_Supplements& oSupplements) {
  _poSupplements = &oSupplements;
}

Model_OTA_HotelRatePlanNotifRQ_Supplements* Model_Boarding::getSupplements(void) {
  return _poSupplements;
}

void Model_Boarding::setSellableProducts(Model_OTA_HotelRatePlanNotifRQ_SellableProducts& oSellableProducts) {
  _poSellableProducts = &oSellableProducts;
}

Model_OTA_HotelRatePlanNotifRQ_SellableProducts* Model_Boarding::getSellableProducts(void) {
  return _poSellableProducts;
}
