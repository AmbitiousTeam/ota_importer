#ifndef __MODEL_OTA_HOTELRATEPLANNOTIFRQ_OFFERRULES_OFFERRULE_H__
#define __MODEL_OTA_HOTELRATEPLANNOTIFRQ_OFFERRULES_OFFERRULE_H__

#include "DateRestrictions.h"
#include "LengthsOfStay.h"

class Model_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule {
public:
  void setDateRestrictions(Model_OTA_HotelRatePlanNotifRQ_DateRestrictions&);
  Model_OTA_HotelRatePlanNotifRQ_DateRestrictions* getDateRestrictions();
  
  void setLengthsOfStay(Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay&);
  Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay* getLengthsOfStay();
  
  Model_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule();
  ~Model_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule();
protected:
private:
  Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay *_oLenghtsOfStay = NULL;
  Model_OTA_HotelRatePlanNotifRQ_DateRestrictions *_oDateRestrictions = NULL;
};

#endif
