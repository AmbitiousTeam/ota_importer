#include <iostream>
#include "RatePlan.h"

const std::string Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::RATEPLANNOTIFTYPEOVERLAY = "Overlay";

void Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::setRatePlanCode(const std::string sRatePlanCode) {
  _sRatePlanCode = sRatePlanCode;
}

std::string Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::getRatePlanCode() {
  return _sRatePlanCode;
}

void Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::setRatePlanNotifType(const std::string sRatePlanNotifType) {
  _sRatePlanNotifType = sRatePlanNotifType;
}

std::string Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::getRatePlanNotifType() {
  return _sRatePlanNotifType;
}

void Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::setCurrencyCode(const std::string sCurrencyCode) {
  _sCurrencyCode = sCurrencyCode;
}

std::string Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::getCurrencyCode() {
  return _sCurrencyCode;
}


void Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::setPromotionCode(std::string sPromotionCode) {
  _sPromotionCode = sPromotionCode;
}

std::string Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::getPromotionCode(void) {
  return _sPromotionCode;
}

void Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::setChargeType(int iChargeType) {
  _iChargeType = iChargeType;
}

int Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::getChargeType(void) {
  return _iChargeType;
}

void Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::setRates(Model_OTA_HotelRatePlanNotifRQ_Rates& oRates) {
  _oRates = &oRates;
}

Model_OTA_HotelRatePlanNotifRQ_Rates* Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::getRates() {
  return _oRates;
}

void Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::setSellableProducts(Model_OTA_HotelRatePlanNotifRQ_SellableProducts& oSellableProducts) {
  _oSellableProducts = &oSellableProducts;
}

Model_OTA_HotelRatePlanNotifRQ_SellableProducts* Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::getSellableProducts() {
  return _oSellableProducts;
}

void Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::setOffers(Model_OTA_HotelRatePlanNotifRQ_Offers& oOffers) {
  _oOffers = &oOffers;
}

Model_OTA_HotelRatePlanNotifRQ_Offers* Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::getOffers() {
  return _oOffers;
}

void Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::setBookingRules(Model_OTA_HotelRatePlanNotifRQ_BookingRules& oBookingRules) {
  _oBookingRules = &oBookingRules;
}

Model_OTA_HotelRatePlanNotifRQ_BookingRules* Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::getBookingRules() {
  return _oBookingRules;
}

void Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::setSupplements(Model_OTA_HotelRatePlanNotifRQ_Supplements& oSupplements) {
  _oSupplements = &oSupplements;
}

Model_OTA_HotelRatePlanNotifRQ_Supplements* Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::getSupplements() {
  return _oSupplements;
}

Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan() {
  setCurrencyCode("");
  setRatePlanCode("");
  setRatePlanNotifType("");
  setPromotionCode("");
  setChargeType(0);
  _oBookingRules = NULL;
  _oOffers = NULL;
  _oRates = NULL;
  _oSellableProducts = NULL;
  _oSupplements = NULL;
}

Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::~Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan() {
//   if (DEBUG_MODE) {std::cout << "DTOR von RatePlan!" << std::endl;}
}
