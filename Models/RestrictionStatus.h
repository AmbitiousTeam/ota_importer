#ifndef __MODEL_OTA_HOTELAVAILNOTIFRQ_RESTRICTIONSTATUS_H__
#define __MODEL_OTA_HOTELAVAILNOTIFRQ_RESTRICTIONSTATUS_H__

#include <string>
#include "Entity.h"

class Model_OTA_HotelAvailNotifRQ_RestrictionStatus : public Model_Entity {
public:
  void setStatus(std::string);
  std::string getStatus();
  
  void setRestriction(std::string);
  std::string getRestriction();
  
  Model_OTA_HotelAvailNotifRQ_RestrictionStatus();
  ~Model_OTA_HotelAvailNotifRQ_RestrictionStatus();
protected:
private:
  std::string _sRestriction;
  std::string _sStatus;
};

#endif