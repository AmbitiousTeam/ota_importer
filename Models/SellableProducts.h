#ifndef __MODEL_OTAHOTELRATEPLANNOTIFRQ_SELLABLEPRODUCTS_H__
#define __MODEL_OTAHOTELRATEPLANNOTIFRQ_SELLABLEPRODUCTS_H__

#include "Collection.h"
#include "SellableProduct.h"

class Model_OTA_HotelRatePlanNotifRQ_SellableProducts : public Model_Collection<Model_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct> {
public:
  
  Model_OTA_HotelRatePlanNotifRQ_SellableProducts();
  ~Model_OTA_HotelRatePlanNotifRQ_SellableProducts();
protected:
private:
};

#endif