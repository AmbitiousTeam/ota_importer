#ifndef __MODEL_OTA_HOTELRATEPLANNOTIFRQ_INVENTORIES_H__
#define __MODEL_OTA_HOTELRATEPLANNOTIFRQ_INVENTORIES_H__

#include "Collection.h"
#include "Inventory.h"

class Model_OTA_HotelRatePlanNotifRQ_Inventories : public Model_Collection<Model_OTA_HotelRatePlanNotifRQ_Inventories_Inventory> {
public:
  
  Model_OTA_HotelRatePlanNotifRQ_Inventories();
  ~Model_OTA_HotelRatePlanNotifRQ_Inventories();
protected:
private:
};

#endif