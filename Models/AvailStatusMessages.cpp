#include "../Tools/Converter.h"

#include "AvailStatusMessages.h"

void Model_OTA_HotelAvailNotifRQ_AvailStatusMessages::setIdCon(std::string sIdCon) {
  _sIdCon = sIdCon;
}

std::string Model_OTA_HotelAvailNotifRQ_AvailStatusMessages::getIdCon() const {
  return _sIdCon;
}

bool Model_OTA_HotelAvailNotifRQ_AvailStatusMessages::getDelta() {
  return _bDelta;
}

void Model_OTA_HotelAvailNotifRQ_AvailStatusMessages::setDelta(bool bDelta) {
  _bDelta = bDelta;
}

void Model_OTA_HotelAvailNotifRQ_AvailStatusMessages::setDelta(std::string sDelta) {
  _bDelta = Converter::convertMixedToBoolean(sDelta);
}

void Model_OTA_HotelAvailNotifRQ_AvailStatusMessages::setHotelCode(std::string sHotelCode) {
  _sHotelCode = sHotelCode;
}

std::string Model_OTA_HotelAvailNotifRQ_AvailStatusMessages::getHotelCode() const {
  return _sHotelCode;
}

Model_OTA_HotelAvailNotifRQ_AvailStatusMessages::Model_OTA_HotelAvailNotifRQ_AvailStatusMessages() {

}

Model_OTA_HotelAvailNotifRQ_AvailStatusMessages::~Model_OTA_HotelAvailNotifRQ_AvailStatusMessages() {

}
