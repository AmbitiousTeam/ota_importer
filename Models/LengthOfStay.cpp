#include "LengthOfStay.h"

const std::string Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay::MIN_MAX_MESSAGETYPE_MINLOS = "MinLOS";
const std::string Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay::MIN_MAX_MESSAGETYPE_MAXLOS = "MaxLOS";
const std::string Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay::MIN_MAX_MESSAGETYPE_FIXEDLOS = "FixedLOS";

const std::string Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay::TIMEUNIT = "Day";

void Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay::setTime(std::string sTime) {
  _sTime = sTime;
}

std::string Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay::getTime() {
  return _sTime;
}

void Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay::setTimeUnit(std::string sTimeUnit) {
  _sTimeUnit = sTimeUnit;
}

std::string Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay::getTimeUnit() {
  return _sTimeUnit;
}

void Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay::setMinMaxMessageType(std::string sMinMaxMessageType) {
  _sMinMaxMessageType = sMinMaxMessageType;
}

std::string Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay::getMinMaxMessageType() {
  return _sMinMaxMessageType;
}

Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay::Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay() {

}

Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay::~Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay() {

}
