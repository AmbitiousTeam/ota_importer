#ifndef __MODEL_OTA_HOTELRATEPLANNOTIFRQ_H__
#define __MODEL_OTA_HOTELRATEPLANNOTIFRQ_H__

#include "Entity.h"
#include "RatePlans.h"
#include "Offers.h"
#include "TPAExtensions.h"

// #include "Model.h"

// typedef Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan RatePlan;

class Model_OTA_HotelRatePlanNotifRQ : public Model_Entity {
public:
  void setXmlNs(std::string);
  std::string getXmlNs();
  
  void setXmlNsXsd(std::string);
  std::string getXmlNsXsd();
  
  void setXmlNsXsi(std::string);
  std::string getXmlNsXsi();
  
  void setVersion(std::string);
  std::string getVersion();
  
  void setXsiSchemaLocation(std::string);
  std::string getXsiSchemaLocation();
  
  void setRatePlans(Model_OTA_HotelRatePlanNotifRQ_RatePlans&);
  Model_OTA_HotelRatePlanNotifRQ_RatePlans* getRatePlans();
  
  void setOffers(Model_OTA_HotelRatePlanNotifRQ_Offers&);
  Model_OTA_HotelRatePlanNotifRQ_Offers* getOffers();
  
  void setTPAExtensions(Model_OTA_HotelRatePlanNotifRQ_TPAExtensions&);
  Model_OTA_HotelRatePlanNotifRQ_TPAExtensions* getTPAExtensions();
  
  void setEchoToken(std::string);
  std::string getEchoToken(void);
  
  void setCorrelationId(std::string);
  std::string getCorrelationId(void);
  
  Model_OTA_HotelRatePlanNotifRQ();
  ~Model_OTA_HotelRatePlanNotifRQ();
  
protected:
private:
  std::string _sXmlNs;
  std::string _sXmlNsXsd;
  std::string _sXmlNsXsi;
  std::string _sVersion;
  std::string _sXsiSchemaLocation;
  std::string _sEchoToken;
  std::string _sCorrelationId;
  
  Model_OTA_HotelRatePlanNotifRQ_Offers* _poOffers = NULL;
  Model_OTA_HotelRatePlanNotifRQ_RatePlans* _poRatePlans = NULL;
  Model_OTA_HotelRatePlanNotifRQ_TPAExtensions* _poTPAExtensions = NULL;
};

#endif
