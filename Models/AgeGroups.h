#ifndef __MODEL_AGEGROUPS_H__
#define __MODEL_AGEGROUPS_H__

#include "Collection.h"
#include "AgeGroup.h"

class Model_AgeGroups : public Model_Collection<Model_AgeGroup> {
public:
  
  void setDefaultAgeGroup(Model_AgeGroup&);
  Model_AgeGroup* getDefaultAgeGroup(void);
  
  Model_AgeGroups();
  ~Model_AgeGroups();
protected:
private:
  
  Model_AgeGroup* _poDefaultAgeGroup = NULL;
};

#endif
