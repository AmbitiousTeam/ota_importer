#ifndef __MODEL_OTA_HOTELRATEPLANNOTIFRQ_SUPPLEMENTS_H__
#define __MODEL_OTA_HOTELRATEPLANNOTIFRQ_SUPPLEMENTS_H__

#include "Collection.h"
#include "Supplement.h"

class Model_OTA_HotelRatePlanNotifRQ_Supplements : 
  public Model_Collection<Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement> {
public:
  Model_OTA_HotelRatePlanNotifRQ_Supplements();
  ~Model_OTA_HotelRatePlanNotifRQ_Supplements();
protected:
private:
};

#endif