#ifndef __MODEL_OTA_HOTELRATEPLANNOTIFRQ_DEADLINE_H__
#define __MODEL_OTA_HOTELRATEPLANNOTIFRQ_DEADLINE_H__

#include <string>
#include "Entity.h"

class Model_OTA_HotelRatePlanNotifRQ_Deadline : public Model_Entity {
public:
  void setOffsetUnitMultiplier(int);
  int getOffsetUnitMultiplier();
  
  void setOffsetTimeUnit(std::string);
  std::string getOffsetTimeUnit();
  
  void setOffsetDropTime(std::string);
  std::string getOffsetDropTime();
  
  Model_OTA_HotelRatePlanNotifRQ_Deadline();
  ~Model_OTA_HotelRatePlanNotifRQ_Deadline();
protected:
private:
  int _iOffsetUnitMultiplier;
  std::string _sOffsetTimeUnit;
  std::string _sOffsetDropTime;
};

#endif