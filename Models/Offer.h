#ifndef __MODEL_OTAHOTELRATEPLANNOTIFRQ_OFFERS_OFFER_H__
#define __MODEL_OTAHOTELRATEPLANNOTIFRQ_OFFERS_OFFER_H__

#include <string>
#include "Entity.h"
#include "Model.h"
#include "OfferRules.h"
#include "Discount.h"
#include "CompatibleOffers.h"
#include "OfferDescription.h"
#include "Inventories.h"

class Model_OTA_HotelRatePlanNotifRQ_Offers_Offer : public Model_Entity {
public:
  
  void setApplicationOrder(int);
  int getApplicationOrder();
  
  void setOfferCode(std::string);
  std::string getOfferCode();
  
  void setRph(std::string);
  std::string getRph();
  
  void setOfferRules(Model_OTA_HotelRatePlanNotifRQ_OfferRules*);
  Model_OTA_HotelRatePlanNotifRQ_OfferRules* getOfferRules();
  
  void setDiscount(Model_OTA_HotelRatePlanNotifRQ_Discount*);
  Model_OTA_HotelRatePlanNotifRQ_Discount* getDiscount();
  
  void setCompatibleOffers(Model_OTA_HotelRatePlanNotifRQ_CompatibleOffers*);
  Model_OTA_HotelRatePlanNotifRQ_CompatibleOffers* getCompatibleOffers();
  
  void setOfferDescription(Model_OTA_HotelRatePlanNotifRQ_OfferDescription*);
  Model_OTA_HotelRatePlanNotifRQ_OfferDescription* getOfferDescription();
  
  void setInventories(Model_OTA_HotelRatePlanNotifRQ_Inventories*);
  Model_OTA_HotelRatePlanNotifRQ_Inventories* getInventories();
  
  Model_OTA_HotelRatePlanNotifRQ_Offers_Offer();
  ~Model_OTA_HotelRatePlanNotifRQ_Offers_Offer();
protected:
private:
  int _iApplicationOrder;
  std::string _sOfferCode;
  std::string _sRph;

  Model_OTA_HotelRatePlanNotifRQ_Discount *_oDiscount = NULL;
  Model_OTA_HotelRatePlanNotifRQ_OfferRules *_oOfferRules = NULL;
  Model_OTA_HotelRatePlanNotifRQ_Inventories *_oInventories = NULL;
  Model_OTA_HotelRatePlanNotifRQ_CompatibleOffers *_oCompatibleOffers = NULL;
  Model_OTA_HotelRatePlanNotifRQ_OfferDescription *_oOfferDescription = NULL;
};

#endif
