#ifndef __MODEL_OTA_HOTELRATEPLANNOTIFRQ_OCCUPANCIES_OCCUPANCY_H__
#define __MODEL_OTA_HOTELRATEPLANNOTIFRQ_OCCUPANCIES_OCCUPANCY_H__

#include <string>
#include "Entity.h"

class Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy {
public:
  
  static const int AGE_QUALIFYING_CODE_ADULT;
  static const int AGE_QUALIFYING_CODE_CHILDREN;
  static const int AGE_QUALIFYING_CODE_INFANT;
  
  void setMinOccupancy(int);
  int getMinOccupancy();
  
  void setMaxOccupancy(int);
  int getMaxOccupancy();
  
  void setAgeQualifyingCode(int);
  int getAgeQualifyingCode();
  
  void setMinAge(int);
  int getMinAge();
  
  void setMaxAge(int);
  int getMaxAge();
  
  void setInfantsAreCounted(bool);
  bool getInfantsAreCounted();
  
  void setAgeTimeUnit(std::string);
  std::string getAgeTimeUnit();
  
  Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy();
  ~Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy();
protected:
private:
  
  int _iMinOccupancy;
  int _iMaxOccupancy;
  int _iAgeQualifyingCode;
  int _iMinAge;
  int _iMaxAge;
  bool _bInfantsAreCounted;
  
  std::string _sAgeTimeUnit;
};

#endif
