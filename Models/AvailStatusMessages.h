#ifndef __MODEL_OTA_HOTELAVAILNOTIFRQ_AVAILSTATUSMESSAGES_H__
#define __MODEL_OTA_HOTELAVAILNOTIFRQ_AVAILSTATUSMESSAGES_H__

#include <string>
#include "Collection.h"

#include "AvailStatusMessage.h"

class Model_OTA_HotelAvailNotifRQ_AvailStatusMessages : 
  public Model_Collection<Model_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage> {
public:
  void setHotelCode(std::string sHotelCode);
  std::string getHotelCode() const;
  void setIdCon(std::string sIdCon);
  std::string getIdCon() const;
  void setDelta(bool bDelta);
  void setDelta(std::string sDelta);
  bool getDelta();
  
  Model_OTA_HotelAvailNotifRQ_AvailStatusMessages();
  ~Model_OTA_HotelAvailNotifRQ_AvailStatusMessages();
protected:
private:
  bool _bDelta;
  std::string _sIdCon;
  std::string _sDelta;
  std::string _sHotelCode;
};
#endif