message("Compile Database Models")

set(DbModelsSrc 
  Abstract.cpp
)

add_library(DbModels SHARED ${DbModelsSrc})

install(TARGETS DbModels DESTINATION lib)
