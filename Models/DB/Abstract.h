#ifndef __MODEL_DB_ABSTRACT_H__
#define __MODEL_DB_ABSTRACT_H__
#include <string>
#include <iostream>

#include "../Models/Collection.h"
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>

class Model_DB_Abstract {
public:
    
  /**
    * Bereitet die Verbindungsdaten auf. hier werden die daten aus der config ausgelesen und an die member übergeben
    * 
    * @param Model_Collection<std::string>* oConfig
    * 
    * @return bool
    */
  bool connect(void);
  
  //
  /// brief stellt eine übergebene Anfrage an das bereits instanzierte statement
  /// return gibt den inhalt des members rResultSet als antwort zurück
  //
  sql::ResultSet *query(std::string sQueryString);
  
  void setHostName(std::string sHostName);
  std::string getHostName(void);
  
  void setUserName(std::string sUserName);
  std::string getUserName(void);
  
  void setPassword(std::string sPassword);
  std::string getPassword(void);
  
  void setDatabaseName(std::string sDatabaseName);
  std::string getDatabaseName(void);
  
  void setPort(int iPort);
  int getPort(void);
  
  Model_DB_Abstract(Model_Collection<std::string>* oConfig);
  ~Model_DB_Abstract();

protected:
  void setTableName(std::string sTableName);
  std::string getTableName(void);
  
private:
  void _setDriver(sql::Driver *oDriver);
  sql::Driver *_getDriver(void);
  
  void _setConnection(sql::Connection *oConnection);
  sql::Connection *_getConnection(void);
  
  void _setStatement(sql::Statement *oStatement);
  sql::Statement *_getStatement(void);
  
  void _setResultSet(sql::ResultSet *rResultSet);
  sql::ResultSet *_getResultSet(void);
  
  bool _prepareConnection(Model_Collection<std::string>* oConfig);
  
  sql::Driver *_oDriver;
  sql::Connection *_oConnection;
  sql::Statement *_oStatement;
  sql::ResultSet *_rResultSet;
  
  std::string _sTableName;
  std::string _sHostName;
  std::string _sUserName;
  std::string _sPassword;
  std::string _sDatabaseName;
  int _iPort;
};

#endif // __MODEL_DB_ABSTRACT_H__