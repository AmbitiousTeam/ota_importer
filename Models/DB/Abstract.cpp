#include "Abstract.h"
#include <sstream>

using namespace std;

Model_DB_Abstract::Model_DB_Abstract(Model_Collection<std::string>* oConfig) {
    try {
        if (this->_prepareConnection(oConfig)) {
            this->connect();
        }
    } catch (sql::SQLException &oException) {
        cout << "# ERR: SQLException in " << __FILE__;
        cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
        cout << "# ERR: " << oException.what();
        cout << " (MySQL error code: " << oException.getErrorCode();
        cout << ", SQLState: " << oException.getSQLState() << " )" << endl;
    }
}

bool Model_DB_Abstract::connect(void) {
    this->_setDriver(get_driver_instance());
    std::stringstream oStringStream;
    oStringStream << this->getHostName() << ":" << this->getPort();
    this->_setConnection(this->_getDriver()->connect(oStringStream.str(), this->getUserName(), this->getPassword()));
    this->_getConnection()->setSchema(this->getDatabaseName());
    
    return (this->_getConnection());
}

bool Model_DB_Abstract::_prepareConnection(Model_Collection<std::string>* oConfig) {
    bool bReturn = false;
    if (oConfig) {
        this->setHostName(oConfig->get("databaseHostName")->c_str());
        this->setUserName(oConfig->get("databaseUser")->c_str());
        this->setPassword(oConfig->get("databasePassword")->c_str());
        this->setDatabaseName(oConfig->get("databaseName")->c_str());
        this->setPort(atoi(oConfig->get("databasePort")->c_str()));
        bReturn = true;
    }
    return bReturn;
}

sql::ResultSet *Model_DB_Abstract::query(string sQueryString) {
    this->_setStatement(this->_getConnection()->createStatement());
    this->_setResultSet(this->_getStatement()->executeQuery(sQueryString));
    
    return this->_rResultSet;
}

void Model_DB_Abstract::_setDriver(sql::Driver* oDriver) {
    this->_oDriver = oDriver;
}

sql::Driver* Model_DB_Abstract::_getDriver(void) {
    return this->_oDriver;
}

void Model_DB_Abstract::_setConnection(sql::Connection* oConnection) {
    this->_oConnection = oConnection;
}

sql::Connection* Model_DB_Abstract::_getConnection(void) {
    return this->_oConnection;
}

void Model_DB_Abstract::_setResultSet(sql::ResultSet* rResultSet) {
    this->_rResultSet = rResultSet;
}

sql::ResultSet* Model_DB_Abstract::_getResultSet(void) {
    return this->_rResultSet;
}

void Model_DB_Abstract::_setStatement(sql::Statement* oStatement) {
    this->_oStatement = oStatement;
}

sql::Statement* Model_DB_Abstract::_getStatement(void) {
    return this->_oStatement;
}

void Model_DB_Abstract::setTableName(std::string sTableName) {
    this->_sTableName = sTableName;
}

std::string Model_DB_Abstract::getTableName(void) {
    return this->_sTableName;
}

void Model_DB_Abstract::setDatabaseName(string sDatabaseName) {
    this->_sDatabaseName = sDatabaseName;
}

string Model_DB_Abstract::getDatabaseName(void) {
    return this->_sDatabaseName;
}

void Model_DB_Abstract::setHostName(string sHostName) {
    this->_sHostName = sHostName;
}

string Model_DB_Abstract::getHostName(void) {
    return this->_sHostName;
}

void Model_DB_Abstract::setPassword(string sPassword) {
    this->_sPassword = sPassword;
}

string Model_DB_Abstract::getPassword(void) {
    return this->_sPassword;
}

void Model_DB_Abstract::setUserName(string sUserName) {
    this->_sUserName = sUserName;
}

string Model_DB_Abstract::getUserName(void) {
    return this->_sUserName;
}

void Model_DB_Abstract::setPort(int iPort) {
    this->_iPort = iPort;
}

int Model_DB_Abstract::getPort(void) {
    return this->_iPort;
}

Model_DB_Abstract::~Model_DB_Abstract() {
    delete this->_oConnection;
    delete this->_oStatement;
    delete this->_rResultSet;
}
