#include "BaseByGuestAmt.h"

void Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt::setAgeQualifyingCode(unsigned int iAgeQualifyingCode) {
  _iAgeQualifyingCode = iAgeQualifyingCode;
}

unsigned int Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt::getAgeQualifyingCode() {
  return _iAgeQualifyingCode;
}

void Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt::setAmountAfterTax(double fAmountAfterTax) {
  _fAmountAfterTax = fAmountAfterTax;
}

double Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt::getAmountAfterTax() {
  return _fAmountAfterTax;
}

void Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt::setCurrencyCode(std::string sCurrencyCode) {
  _sCurrencyCode = sCurrencyCode;
}

std::string Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt::getCurrencyCode() {
  return _sCurrencyCode;
}

void Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt::setNumberOfGuests(unsigned int iNumberOfGuests) {
  _iNumberOfGuests = iNumberOfGuests;
}

unsigned int Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt::getNumberOfGuests() {
  return _iNumberOfGuests;
}

void Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt::setMinAge(int iMinAge) {
  _iMinAge = iMinAge;
}

int Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt::getMinAge(void) {
  return _iMinAge;
}

void Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt::setMaxAge(int iMaxAge) {
  _iMaxAge = iMaxAge;
}

int Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt::getMaxAge(void) {
  return _iMaxAge;
}

Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt::Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt() {
  setAgeQualifyingCode(0);
  setAmountAfterTax(0.00);
  setNumberOfGuests(0);
  setCurrencyCode("");
  setMinAge(0);
  setMaxAge(0);
}

Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt::~Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt() {
  
}
