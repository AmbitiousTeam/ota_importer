#include "Discount.h"

void Model_OTA_HotelRatePlanNotifRQ_Discount::setAmount(double dAmount) {
  _dAmount = dAmount;
}

double Model_OTA_HotelRatePlanNotifRQ_Discount::getAmount() {
  return _dAmount;
}

void Model_OTA_HotelRatePlanNotifRQ_Discount::setPercent(double dPercent) {
  _dPercent = dPercent;
}

double Model_OTA_HotelRatePlanNotifRQ_Discount::getPercent() {
  return _dPercent;
}

void Model_OTA_HotelRatePlanNotifRQ_Discount::setNightsRequired(int iNightsRequired) {
  _iNightsRequired = iNightsRequired;
}

int Model_OTA_HotelRatePlanNotifRQ_Discount::getNightsRequired() {
  return _iNightsRequired;
}

void Model_OTA_HotelRatePlanNotifRQ_Discount::setNightsDiscounted(int iNightsDiscounted) {
  _iNightsDiscounted = iNightsDiscounted;
}

int Model_OTA_HotelRatePlanNotifRQ_Discount::getNightsDiscounted() {
  return _iNightsDiscounted;
}

void Model_OTA_HotelRatePlanNotifRQ_Discount::setChargeUnitCode(int iChargeUnitCode) {
  _iChargeUnitCode = iChargeUnitCode;
}

int Model_OTA_HotelRatePlanNotifRQ_Discount::getChargeUnitCode(void) {
  return _iChargeUnitCode;
}

void Model_OTA_HotelRatePlanNotifRQ_Discount::setCurrencyCode(std::string sCurrencyCode) {
  _sCurrencyCode = sCurrencyCode;
}

std::string Model_OTA_HotelRatePlanNotifRQ_Discount::getCurrencyCode(void) {
  return _sCurrencyCode;
}

void Model_OTA_HotelRatePlanNotifRQ_Discount::setDiscountLastBookingDays(bool bDiscountLastBookingDays) {
  _bDiscountLastBookingDays = bDiscountLastBookingDays;
}

bool Model_OTA_HotelRatePlanNotifRQ_Discount::getDiscountLastBookingDays(void) {
  return _bDiscountLastBookingDays;
}

Model_OTA_HotelRatePlanNotifRQ_Discount::Model_OTA_HotelRatePlanNotifRQ_Discount() {
  setAmount(0);
  setChargeUnitCode(0);
  setCurrencyCode("");
  setNightsDiscounted(0);
  setNightsRequired(0);
  setPercent(0);
  setDiscountLastBookingDays(false);
}

Model_OTA_HotelRatePlanNotifRQ_Discount::~Model_OTA_HotelRatePlanNotifRQ_Discount() {

}
