#include "AvailStatusMessage.h"

void Model_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage::setRestrictionStatus(Model_OTA_HotelAvailNotifRQ_RestrictionStatus& oRestrictionStatus) {
  _poRestrictionStatus = &oRestrictionStatus;
}

Model_OTA_HotelAvailNotifRQ_RestrictionStatus* Model_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage::getRestrictionStatus() {
  return _poRestrictionStatus;
}

void Model_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage::setStatusApplicationControl(Model_OTA_HotelAvailNotifRQ_StatusApplicationControl& oStatusApplicationControl) {
  _poStatusApplicationControl = &oStatusApplicationControl;
}

Model_OTA_HotelAvailNotifRQ_StatusApplicationControl* Model_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage::getStatusApplicationControl() {
  return _poStatusApplicationControl;
}

Model_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage::Model_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage() {
  
}

Model_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage::~Model_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage() {
  delete this->_poRestrictionStatus;
  delete this->_poStatusApplicationControl;
}
