#include "Inventory.h"

void Model_OTA_HotelRatePlanNotifRQ_Inventories_Inventory::setInvCode(std::string sInvCode) {
  _sInvCode = sInvCode;
}

std::string Model_OTA_HotelRatePlanNotifRQ_Inventories_Inventory::getInvCode(void) {
  return _sInvCode;
}

void Model_OTA_HotelRatePlanNotifRQ_Inventories_Inventory::setAppliesToIndicator(bool bAppliesToIndicator) {
  _bAppliesToIndicator = bAppliesToIndicator;
}

bool Model_OTA_HotelRatePlanNotifRQ_Inventories_Inventory::getAppliesToIndicator(void) {
  return _bAppliesToIndicator;
}

void Model_OTA_HotelRatePlanNotifRQ_Inventories_Inventory::setInvType(std::string sInvType) {
  _sInvType = sInvType;
}

std::string Model_OTA_HotelRatePlanNotifRQ_Inventories_Inventory::getInvType(void) {
  return _sInvType;
}

Model_OTA_HotelRatePlanNotifRQ_Inventories_Inventory::Model_OTA_HotelRatePlanNotifRQ_Inventories_Inventory() {
  setInvCode("");
  setInvType("");
  setAppliesToIndicator(false);
}

Model_OTA_HotelRatePlanNotifRQ_Inventories_Inventory::~Model_OTA_HotelRatePlanNotifRQ_Inventories_Inventory() {

}
