#ifndef __MODEL_OTA_HOTELRATEPLANNOTIFRQ_LENGTHSOFSTAY_H__
#define __MODEL_OTA_HOTELRATEPLANNOTIFRQ_LENGTHSOFSTAY_H__

#include "LengthOfStay.h"
#include "Collection.h"

class Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay : 
  public Model_Collection<Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay> {
public:
  Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay();
  ~Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay();
protected:
private:
  
};

#endif