#include "AgeGroups.h"

void Model_AgeGroups::setDefaultAgeGroup(Model_AgeGroup& oDefaultAgeGroup) {
  _poDefaultAgeGroup = &oDefaultAgeGroup;
}

Model_AgeGroup* Model_AgeGroups::getDefaultAgeGroup(void) {
  return _poDefaultAgeGroup;
}

Model_AgeGroups::Model_AgeGroups() {

}

Model_AgeGroups::~Model_AgeGroups() {
  if (DEBUG_MODE) {std::cout << "DTOR von Model_AgeGroups!" << std::endl;};
  delete this->_poDefaultAgeGroup;
}
