#ifndef __MODEL_OTA_HOTELRATEPLANNOTIFRQ_COMPATIBLEOFFERS_COMPATIBLEOFFER_H__
#define __MODEL_OTA_HOTELRATEPLANNOTIFRQ_COMPATIBLEOFFERS_COMPATIBLEOFFER_H__

#include <string>

class Model_OTA_HotelRatePlanNotifRQ_CompatibleOffers_CompatibleOffer {
public:
  void setIncompatibleOfferIndicator(bool);
  bool getIncompatibleOfferIndicator();
  
  void setOfferRph(std::string);
  std::string getOfferRph();
  
  Model_OTA_HotelRatePlanNotifRQ_CompatibleOffers_CompatibleOffer();
  ~Model_OTA_HotelRatePlanNotifRQ_CompatibleOffers_CompatibleOffer();
protected:
private:
  bool _bIncompatibleOfferIndicator;
  std::string _sOfferRph;
};

#endif