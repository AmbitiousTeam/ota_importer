#ifndef __MODEL_OTA_HOTELRATEPLANNOTIFRQ_BASEBYGUESTAMTS_BASEGUESTAMT_H__
#define __MODEL_OTA_HOTELRATEPLANNOTIFRQ_BASEBYGUESTAMTS_BASEGUESTAMT_H__

#include <string>
#include "Entity.h"

class Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt : public Model_Entity {
public:
  void setAmountAfterTax(double);
  double getAmountAfterTax();
  
  void setCurrencyCode(std::string);
  std::string getCurrencyCode();
  
  void setNumberOfGuests(unsigned int);
  unsigned int getNumberOfGuests();
  
  void setAgeQualifyingCode(unsigned int);
  unsigned int getAgeQualifyingCode();
  
  void setMinAge(int);
  int getMinAge(void);
  
  void setMaxAge(int);
  int getMaxAge(void);
  
  Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt();
  ~Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt();
protected:
private:
  std::string _sCurrencyCode;
  unsigned int _iNumberOfGuests;
  unsigned int _iAgeQualifyingCode;
  unsigned int _iMinAge;
  unsigned int _iMaxAge;
  
  double _fAmountAfterTax;
};

#endif
