#ifndef __MODEL_OTA_HOTELRATEPLANNOTIFRQ_GUESTROOM_H__
#define __MODEL_OTA_HOTELRATEPLANNOTIFRQ_GUESTROOM_H__

#include "Entity.h"
#include "Quantities.h"
#include "Occupancies.h"
#include "AdditionalGuestAmounts.h"

class Model_OTA_HotelRatePlanNotifRQ_GuestRoom : public Model_Entity {
public:
  void setOccupancies(Model_OTA_HotelRatePlanNotifRQ_Occupancies&);
  Model_OTA_HotelRatePlanNotifRQ_Occupancies* getOccupancies();
  
  void setAdditionalGuestAmounts(Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts&);
  Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts* getAdditionalGuestAmounts();
  
  void setQuantities(Model_OTA_HotelRatePlanNotifRQ_Quantities&);
  Model_OTA_HotelRatePlanNotifRQ_Quantities* getQuantities(void);
  
  Model_OTA_HotelRatePlanNotifRQ_GuestRoom();
  ~Model_OTA_HotelRatePlanNotifRQ_GuestRoom();
protected:
private:
  Model_OTA_HotelRatePlanNotifRQ_Quantities* _poQuantities = NULL;
  Model_OTA_HotelRatePlanNotifRQ_Occupancies* _poOccupancies = NULL;
  Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts* _poAdditionalGuestAmounts = NULL;
};

#endif
