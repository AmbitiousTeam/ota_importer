#include "StatusApplicationControl.h"

void Model_OTA_HotelAvailNotifRQ_StatusApplicationControl::setInvType(std::string sInvType) {
  _sInvType = sInvType;
}

std::string Model_OTA_HotelAvailNotifRQ_StatusApplicationControl::getInvType() {
  return _sInvType;
}

void Model_OTA_HotelAvailNotifRQ_StatusApplicationControl::setInvCode(std::string sInvCode) {
  _sInvCode = sInvCode;
}

std::string Model_OTA_HotelAvailNotifRQ_StatusApplicationControl::getInvCode() {
  return _sInvCode;
}

Model_OTA_HotelAvailNotifRQ_StatusApplicationControl::Model_OTA_HotelAvailNotifRQ_StatusApplicationControl() {
  
}

Model_OTA_HotelAvailNotifRQ_StatusApplicationControl::~Model_OTA_HotelAvailNotifRQ_StatusApplicationControl() {
  
}