#ifndef __MODEL_OTA_HOTELRATEPLANNOTIFRQ_LENGTHSOFSTAY_LENGTHOFSTAY_H__
#define __MODEL_OTA_HOTELRATEPLANNOTIFRQ_LENGTHSOFSTAY_LENGTHOFSTAY_H__

#include <string>
#include "Entity.h"

class Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay : public Model_Entity {
public:
  static const std::string MIN_MAX_MESSAGETYPE_MINLOS;
  static const std::string MIN_MAX_MESSAGETYPE_MAXLOS;
  static const std::string MIN_MAX_MESSAGETYPE_FIXEDLOS;
  
  static const std::string TIMEUNIT;
  
  void setTime(std::string);
  std::string getTime();
  
  void setTimeUnit(std::string);
  std::string getTimeUnit();
  
  void setMinMaxMessageType(std::string);
  std::string getMinMaxMessageType();
  
  Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay();
  ~Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay();
protected:
private:
  std::string _sTime;
  std::string _sTimeUnit;
  std::string _sMinMaxMessageType;
};

#endif