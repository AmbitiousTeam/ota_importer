#ifndef __MODEL_OTA_HOTELRATEPLANNOTIFRQ_DATESTRICTIONS_DATERESTRICTION_H__
#define __MODEL_OTA_HOTELRATEPLANNOTIFRQ_DATESTRICTIONS_DATERESTRICTION_H__

#include <string>
#include "Entity.h"
#include "Date.h"

class Model_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction : public Model_Entity, public Model_Date {
public:
  static const std::string RESTRICTION_TYPE_STAY;
  static const std::string RESTRICTION_TYPE_BOOKING;
  static const std::string RESTRICTION_TYPE_ARRIVAL;
  
  void setRestrictionType(std::string);
  std::string getRestrictionType();
  
  Model_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction();
  ~Model_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction();
protected:
private:
  std::string _sRestrictionType;
};

#endif