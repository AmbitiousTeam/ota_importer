#include "DateRestriction.h"

const std::string Model_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction::RESTRICTION_TYPE_STAY = "STAY";
const std::string Model_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction::RESTRICTION_TYPE_BOOKING = "BOOKING";
const std::string Model_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction::RESTRICTION_TYPE_ARRIVAL = "ARRIVAL";

void Model_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction::setRestrictionType(std::string sRestrictionType) {
  _sRestrictionType = sRestrictionType;
}

std::string Model_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction::getRestrictionType() {
  return _sRestrictionType;
}

Model_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction::Model_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction() {

}

Model_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction::~Model_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction() {

}
