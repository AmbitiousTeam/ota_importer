#ifndef __MODEL_OTA_HOTELRATEPLANNOTIFRQ_OFFERS_H__
#define __MODEL_OTA_HOTELRATEPLANNOTIFRQ_OFFERS_H__

#include "Collection.h"
#include "Offer.h"

class Model_OTA_HotelRatePlanNotifRQ_Offers : public Model_Collection<Model_OTA_HotelRatePlanNotifRQ_Offers_Offer> {
public:
  Model_OTA_HotelRatePlanNotifRQ_Offers();
  ~Model_OTA_HotelRatePlanNotifRQ_Offers();
protected:
private:
};

#endif