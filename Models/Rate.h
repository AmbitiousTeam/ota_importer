#ifndef __MODEL_OTA_HOTELRATEPLANNOTIFRQ_RATES_RATE_H__
#define __MODEL_OTA_HOTELRATEPLANNOTIFRQ_RATES_RATE_H__

#include <string>

#include "Entity.h"
#include "Date.h"
#include "BaseByGuestAmts.h"
#include "AdditionalGuestAmounts.h"

class Model_OTA_HotelRatePlanNotifRQ_Rates_Rate : public Model_Entity, public Model_Date {
public:
  
  bool getNoOffersAllowed();
  void setNoOffersAllowed(bool);
//   void setNoOffersAllowed(int);
//   void setNoOffersAllowed(std::string);
  
  Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts* getBaseByGuestAmts();
  void setBaseByGuestAmts(Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts&);
  
  Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts* getAdditionalGuestAmounts();
  void setAdditionalGuestAmounts(Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts&);
  
  Model_OTA_HotelRatePlanNotifRQ_Rates_Rate();
  ~Model_OTA_HotelRatePlanNotifRQ_Rates_Rate();
protected:

private:
  std::string _InvTypeCode;
  bool _bNoOffersAllowed;
  
  Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts* _oBaseByGuestAmts = NULL;
  Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts* _oAdditionalGuestAmounts = NULL;
};

#endif
