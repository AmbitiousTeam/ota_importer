#include "GuestRoom.h"

void Model_OTA_HotelRatePlanNotifRQ_GuestRoom::setAdditionalGuestAmounts(Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts& oAdditionalGuestAmounts) {
  _poAdditionalGuestAmounts = &oAdditionalGuestAmounts;
}

Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts* Model_OTA_HotelRatePlanNotifRQ_GuestRoom::getAdditionalGuestAmounts() {
  return _poAdditionalGuestAmounts;
}

void Model_OTA_HotelRatePlanNotifRQ_GuestRoom::setOccupancies(Model_OTA_HotelRatePlanNotifRQ_Occupancies& oOccupancies) {
  _poOccupancies = &oOccupancies;
}

Model_OTA_HotelRatePlanNotifRQ_Occupancies* Model_OTA_HotelRatePlanNotifRQ_GuestRoom::getOccupancies() {
  return _poOccupancies;
}

void Model_OTA_HotelRatePlanNotifRQ_GuestRoom::setQuantities(Model_OTA_HotelRatePlanNotifRQ_Quantities& oQuantities) {
  _poQuantities = &oQuantities;
}

Model_OTA_HotelRatePlanNotifRQ_Quantities* Model_OTA_HotelRatePlanNotifRQ_GuestRoom::getQuantities(void) {
  return _poQuantities;
}

Model_OTA_HotelRatePlanNotifRQ_GuestRoom::Model_OTA_HotelRatePlanNotifRQ_GuestRoom() {
}

Model_OTA_HotelRatePlanNotifRQ_GuestRoom::~Model_OTA_HotelRatePlanNotifRQ_GuestRoom() {
  delete this->_poAdditionalGuestAmounts;
  delete this->_poOccupancies;
  delete this->_poQuantities;
}
