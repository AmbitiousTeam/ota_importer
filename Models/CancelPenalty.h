#ifndef __MODEL_OTA_HOTELRATEPLANNOTIFRQ_CANCALPENALTY_H__
#define __MODEL_OTA_HOTELRATEPLANNOTIFRQ_CANCALPENALTY_H__

#include <string>
#include "Entity.h"
#include "Date.h"
#include "Deadline.h"
#include "AmountPercent.h"

class Model_OTA_HotelRatePlanNotifRQ_CancelPenalty : public Model_Entity, public Model_Date {
public:
  
  void setDeadline(Model_OTA_HotelRatePlanNotifRQ_Deadline&);
  Model_OTA_HotelRatePlanNotifRQ_Deadline* getDeadline();
  
  void setAmountPercent(Model_OTA_HotelRatePlanNotifRQ_AmountPercent&);
  Model_OTA_HotelRatePlanNotifRQ_AmountPercent* getAmountPercent();
  
  Model_OTA_HotelRatePlanNotifRQ_CancelPenalty();
  ~Model_OTA_HotelRatePlanNotifRQ_CancelPenalty();
protected:
private:
  Model_OTA_HotelRatePlanNotifRQ_Deadline* _poDeadline = NULL;
  Model_OTA_HotelRatePlanNotifRQ_AmountPercent* _poAmountPercent = NULL;
};

#endif
