#include "CancelPenalty.h"

void Model_OTA_HotelRatePlanNotifRQ_CancelPenalty::setDeadline(Model_OTA_HotelRatePlanNotifRQ_Deadline& oDeadline) {
  _poDeadline = &oDeadline;
}

Model_OTA_HotelRatePlanNotifRQ_Deadline* Model_OTA_HotelRatePlanNotifRQ_CancelPenalty::getDeadline() {
  return _poDeadline;
}

void Model_OTA_HotelRatePlanNotifRQ_CancelPenalty::setAmountPercent(Model_OTA_HotelRatePlanNotifRQ_AmountPercent& oAmountPercent) {
  _poAmountPercent = &oAmountPercent;
}

Model_OTA_HotelRatePlanNotifRQ_AmountPercent* Model_OTA_HotelRatePlanNotifRQ_CancelPenalty::getAmountPercent() {
  return _poAmountPercent;
}

Model_OTA_HotelRatePlanNotifRQ_CancelPenalty::Model_OTA_HotelRatePlanNotifRQ_CancelPenalty() {

}

Model_OTA_HotelRatePlanNotifRQ_CancelPenalty::~Model_OTA_HotelRatePlanNotifRQ_CancelPenalty() {
  delete this->_poAmountPercent;
  delete this->_poDeadline;
}
