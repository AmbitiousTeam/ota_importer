#ifndef __MODEL_OTA_HOTELRATEPLANNOTIFRQ_COMPATIBLEOFFERS_H__
#define __MODEL_OTA_HOTELRATEPLANNOTIFRQ_COMPATIBLEOFFERS_H__

#include "Collection.h"
#include "CompatibleOffer.h"

class Model_OTA_HotelRatePlanNotifRQ_CompatibleOffers : public Model_Collection<Model_OTA_HotelRatePlanNotifRQ_CompatibleOffers_CompatibleOffer> {
public:
  
  Model_OTA_HotelRatePlanNotifRQ_CompatibleOffers();
  ~Model_OTA_HotelRatePlanNotifRQ_CompatibleOffers();
protected:
private:
};

#endif