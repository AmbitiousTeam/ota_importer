#ifndef __MODEL_OTA_HOTELRATEPLANNOTIFRQ_OFFERDESCRIPTION_H__
#define __MODEL_OTA_HOTELRATEPLANNOTIFRQ_OFFERDESCRIPTION_H__

#include <string>

class Model_OTA_HotelRatePlanNotifRQ_OfferDescription {
public:
  void setText(std::string);
  std::string getText();
  
  Model_OTA_HotelRatePlanNotifRQ_OfferDescription();
  ~Model_OTA_HotelRatePlanNotifRQ_OfferDescription();
protected:
private:
  std::string _sText;
};

#endif