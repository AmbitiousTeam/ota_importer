#ifndef __MODEL_OTA_HOTELAVAILNOTIFRQ_STATUSAPPLICATIONCONTROL_H__
#define __MODEL_OTA_HOTELAVAILNOTIFRQ_STATUSAPPLICATIONCONTROL_H__

#include "Entity.h"
#include "Date.h"
#include <string>

class Model_OTA_HotelAvailNotifRQ_StatusApplicationControl : public Model_Date {
public:
  void setInvType(std::string);
  std::string getInvType();
  void setInvCode(std::string);
  std::string getInvCode();
  
  Model_OTA_HotelAvailNotifRQ_StatusApplicationControl();
  ~Model_OTA_HotelAvailNotifRQ_StatusApplicationControl();
protected:
private:
  std::string _sInvType;
  std::string _sInvCode;
};

#endif