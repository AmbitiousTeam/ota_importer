#include "Offer.h"

void Model_OTA_HotelRatePlanNotifRQ_Offers_Offer::setApplicationOrder(int iApplicationOrder) {
  _iApplicationOrder = iApplicationOrder;
}

int Model_OTA_HotelRatePlanNotifRQ_Offers_Offer::getApplicationOrder() {
  return _iApplicationOrder;
}

void Model_OTA_HotelRatePlanNotifRQ_Offers_Offer::setOfferCode(std::string sOfferCode) {
  _sOfferCode = sOfferCode;
}

std::string Model_OTA_HotelRatePlanNotifRQ_Offers_Offer::getOfferCode() {
  return _sOfferCode;
}

void Model_OTA_HotelRatePlanNotifRQ_Offers_Offer::setRph(std::string sRph) {
  _sRph = sRph;
}

std::string Model_OTA_HotelRatePlanNotifRQ_Offers_Offer::getRph() {
  return _sRph;
}

void Model_OTA_HotelRatePlanNotifRQ_Offers_Offer::setCompatibleOffers(Model_OTA_HotelRatePlanNotifRQ_CompatibleOffers* oCompatibleOffers) {
  _oCompatibleOffers = oCompatibleOffers;
}

Model_OTA_HotelRatePlanNotifRQ_CompatibleOffers* Model_OTA_HotelRatePlanNotifRQ_Offers_Offer::getCompatibleOffers() {
  return _oCompatibleOffers;
}

void Model_OTA_HotelRatePlanNotifRQ_Offers_Offer::setOfferRules(Model_OTA_HotelRatePlanNotifRQ_OfferRules* oOfferRules) {
  _oOfferRules = oOfferRules;
}

Model_OTA_HotelRatePlanNotifRQ_OfferRules* Model_OTA_HotelRatePlanNotifRQ_Offers_Offer::getOfferRules() {
  return _oOfferRules;
}

void Model_OTA_HotelRatePlanNotifRQ_Offers_Offer::setDiscount(Model_OTA_HotelRatePlanNotifRQ_Discount* oDiscount) {
  _oDiscount = oDiscount;
}

Model_OTA_HotelRatePlanNotifRQ_Discount* Model_OTA_HotelRatePlanNotifRQ_Offers_Offer::getDiscount() {
  return _oDiscount;
}

void Model_OTA_HotelRatePlanNotifRQ_Offers_Offer::setOfferDescription(Model_OTA_HotelRatePlanNotifRQ_OfferDescription* oOfferDescription) {
  _oOfferDescription = oOfferDescription;
}

Model_OTA_HotelRatePlanNotifRQ_OfferDescription* Model_OTA_HotelRatePlanNotifRQ_Offers_Offer::getOfferDescription() {
  return _oOfferDescription;
}

void Model_OTA_HotelRatePlanNotifRQ_Offers_Offer::setInventories(Model_OTA_HotelRatePlanNotifRQ_Inventories* oInventories) {
  _oInventories = oInventories;
}

Model_OTA_HotelRatePlanNotifRQ_Inventories* Model_OTA_HotelRatePlanNotifRQ_Offers_Offer::getInventories() {
  return _oInventories;
}

Model_OTA_HotelRatePlanNotifRQ_Offers_Offer::Model_OTA_HotelRatePlanNotifRQ_Offers_Offer() {

}

Model_OTA_HotelRatePlanNotifRQ_Offers_Offer::~Model_OTA_HotelRatePlanNotifRQ_Offers_Offer() {

}
