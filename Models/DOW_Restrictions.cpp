#include "DOW_Restrictions.h"

void Model_OTA_HotelRatePlanNotifRQ_DOW_Restrictions::setDepartureDaysOfWeek(Model_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek& oDepartureDaysOfWeek) {
  _poDepartureDaysOfWeek = &oDepartureDaysOfWeek;
}

Model_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek* Model_OTA_HotelRatePlanNotifRQ_DOW_Restrictions::getDepartureDaysOfWeek() {
  return _poDepartureDaysOfWeek;
}

void Model_OTA_HotelRatePlanNotifRQ_DOW_Restrictions::setArrivalDaysOfWeek(Model_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek& oArrivalDaysOfWeek) {
  _poArrivalDaysOfWeek = &oArrivalDaysOfWeek;
}

Model_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek* Model_OTA_HotelRatePlanNotifRQ_DOW_Restrictions::getArrivalDaysOfWeek() {
  return _poArrivalDaysOfWeek;
}

Model_OTA_HotelRatePlanNotifRQ_DOW_Restrictions::Model_OTA_HotelRatePlanNotifRQ_DOW_Restrictions() {

}

Model_OTA_HotelRatePlanNotifRQ_DOW_Restrictions::~Model_OTA_HotelRatePlanNotifRQ_DOW_Restrictions() {
  delete this->_poArrivalDaysOfWeek;
  delete this->_poDepartureDaysOfWeek;
}
