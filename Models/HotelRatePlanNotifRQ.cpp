#include "HotelRatePlanNotifRQ.h"

void Model_OTA_HotelRatePlanNotifRQ::setXmlNs(std::string sXmlNs) {
  _sXmlNs = sXmlNs;
}

std::string Model_OTA_HotelRatePlanNotifRQ::getXmlNs() {
  return _sXmlNs;
}

void Model_OTA_HotelRatePlanNotifRQ::setXmlNsXsd(std::string sXmlNsXsd) {
  _sXmlNsXsd = sXmlNsXsd;
}

std::string Model_OTA_HotelRatePlanNotifRQ::getXmlNsXsd() {
  return _sXmlNsXsd;
}

void Model_OTA_HotelRatePlanNotifRQ::setXmlNsXsi(std::string sXmlNsXsi) {
  _sXmlNsXsi = sXmlNsXsi;
}

std::string Model_OTA_HotelRatePlanNotifRQ::getXmlNsXsi() {
  return _sXmlNsXsi;
}

void Model_OTA_HotelRatePlanNotifRQ::setXsiSchemaLocation(std::string sXsiSchemaLocation) {
  _sXsiSchemaLocation = sXsiSchemaLocation;
}

std::string Model_OTA_HotelRatePlanNotifRQ::getXsiSchemaLocation() {
  return _sXsiSchemaLocation;
}

void Model_OTA_HotelRatePlanNotifRQ::setVersion(std::string sVersion) {
  _sVersion = sVersion;
}

std::string Model_OTA_HotelRatePlanNotifRQ::getVersion() {
  return _sVersion;
}

void Model_OTA_HotelRatePlanNotifRQ::setRatePlans (Model_OTA_HotelRatePlanNotifRQ_RatePlans& oRatePlans) {
  _poRatePlans = &oRatePlans;
}

Model_OTA_HotelRatePlanNotifRQ_RatePlans* Model_OTA_HotelRatePlanNotifRQ::getRatePlans() {
  return _poRatePlans;
}

void Model_OTA_HotelRatePlanNotifRQ::setOffers(Model_OTA_HotelRatePlanNotifRQ_Offers& oOffers) {
  _poOffers = &oOffers;
}

Model_OTA_HotelRatePlanNotifRQ_Offers* Model_OTA_HotelRatePlanNotifRQ::getOffers() {
  return _poOffers;
}

void Model_OTA_HotelRatePlanNotifRQ::setTPAExtensions(Model_OTA_HotelRatePlanNotifRQ_TPAExtensions& oTPAExtensions) {
  _poTPAExtensions = &oTPAExtensions;
}

Model_OTA_HotelRatePlanNotifRQ_TPAExtensions* Model_OTA_HotelRatePlanNotifRQ::getTPAExtensions() {
  return _poTPAExtensions;
}

void Model_OTA_HotelRatePlanNotifRQ::setCorrelationId(std::string sCorrelationId) {
  _sCorrelationId = sCorrelationId;
}

std::string Model_OTA_HotelRatePlanNotifRQ::getCorrelationId(void) {
  return _sCorrelationId;
}

void Model_OTA_HotelRatePlanNotifRQ::setEchoToken(std::string sEchoToken) {
  _sEchoToken = sEchoToken;
}

std::string Model_OTA_HotelRatePlanNotifRQ::getEchoToken(void) {
  return _sEchoToken;
}

Model_OTA_HotelRatePlanNotifRQ::Model_OTA_HotelRatePlanNotifRQ() {
  
}

Model_OTA_HotelRatePlanNotifRQ::~Model_OTA_HotelRatePlanNotifRQ() {
}
