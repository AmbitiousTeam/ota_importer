#ifndef __MODEL_NODE_H__
#define __MODEL_NODE_H__

#include <iostream>
#include <string>
#include "../App/Defines.cpp"
#include "Entity.h"

template<class T> class Model_Node {
public:
  
  void setNext(Model_Node*);
  
  Model_Node* getNext(void);
  
  void setPrev(Model_Node*);
  
  Model_Node* getPrev(void);
  
  void setData(T&);
  
  T* getData(void);
  
  std::string getStringKey();
  
  int getIntKey();
  
  void setKey(std::string);
  
  void setKey(int);
  
//   Model_Node* add(Model_Node*);
  
//   Model_Node* inject(Model_Node*, T&);
  
  Model_Node();
  
  ~Model_Node();

private:
  int _iKey;
  std::string _sKey;
  T* _mData = NULL;
  Model_Node *_pNext = NULL;
  Model_Node *_pPrev = NULL;
};

template<typename T>void Model_Node<T>::setNext(Model_Node* pNextNode) {
  _pNext = pNextNode;
}

template<typename T>Model_Node<T>* Model_Node<T>::getNext() {
  return _pNext;
}

template<typename T>void Model_Node<T>::setPrev(Model_Node* pPrevNode) {
  _pPrev = pPrevNode;
}

template<typename T>Model_Node<T>* Model_Node<T>::getPrev() {
  return _pPrev;
}

template<typename T>void Model_Node<T>::setData(T& mData) {
  _mData = &mData;
}

template<typename T>T* Model_Node<T>::getData() {
  return _mData;
}

template<typename T>void Model_Node<T>::setKey(std::string sKey) {
  _sKey = sKey;
}

template<typename T>void Model_Node<T>::setKey(int iKey) {
  _iKey = iKey;
}

template<typename T>std::string Model_Node<T>::getStringKey() {
  return _sKey;
}

template<typename T>int Model_Node<T>::getIntKey() {
  return _iKey;
}

template<typename T>Model_Node<T>::Model_Node() {
  _sKey = "";
  _iKey = 0;
}

template<typename T>Model_Node<T>::~Model_Node() {
  if (DEBUG_MODE) {std::cout << "DTOR Node!" << std::endl;}
}

#endif
