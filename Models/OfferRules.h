#ifndef __MODEL_OTA_HOTELRATEPLANNOTIFRQ_OFFERRULES_H__
#define __MODEL_OTA_HOTELRATEPLANNOTIFRQ_OFFERRULES_H__

#include "Collection.h"
#include "OfferRule.h"

class Model_OTA_HotelRatePlanNotifRQ_OfferRules : public Model_Collection<Model_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule> {
public:
  
  Model_OTA_HotelRatePlanNotifRQ_OfferRules();
  ~Model_OTA_HotelRatePlanNotifRQ_OfferRules();
protected:
private:
};

#endif