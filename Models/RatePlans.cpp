#include <iostream>
#include <string>

#include "RatePlans.h"
#include "../Tools/Converter.h"

void Model_OTA_HotelRatePlanNotifRQ_RatePlans::setHotelCode(std::string sHotelCode) {
  _sHotelCode = sHotelCode;
}

std::string Model_OTA_HotelRatePlanNotifRQ_RatePlans::getHotelCode() {
  return _sHotelCode;
}

void Model_OTA_HotelRatePlanNotifRQ_RatePlans::setHotelName(std::string sHotelName) {
  _sHotelName = sHotelName;
}

std::string Model_OTA_HotelRatePlanNotifRQ_RatePlans::getHotelName() {
  return _sHotelName;
}

void Model_OTA_HotelRatePlanNotifRQ_RatePlans::setIdCon(std::string sIdCon) {
  _sIdCon = sIdCon;
}

std::string Model_OTA_HotelRatePlanNotifRQ_RatePlans::getIdCon() {
  return _sIdCon;
}

void Model_OTA_HotelRatePlanNotifRQ_RatePlans::setMultiMarkupPriority(int iMultiMarkupPriority) {
  _iMultiMarkupPriority = iMultiMarkupPriority;
}

void Model_OTA_HotelRatePlanNotifRQ_RatePlans::setMultiMarkupPriority(std::string sMultiMarkupPriority) {
 _iMultiMarkupPriority = Converter::convertMixedToInt(sMultiMarkupPriority);
}

int Model_OTA_HotelRatePlanNotifRQ_RatePlans::getMultiMarkupPriority() {
  return _iMultiMarkupPriority;
}

void Model_OTA_HotelRatePlanNotifRQ_RatePlans::setDelta (std::string sDelta) {
  _bDelta = Converter::convertMixedToBoolean(sDelta);
}

void Model_OTA_HotelRatePlanNotifRQ_RatePlans::setDelta (int iDelta) {
  _bDelta = Converter::convertMixedToBoolean(iDelta);
}

void Model_OTA_HotelRatePlanNotifRQ_RatePlans::setDelta (bool bDelta) {
  _bDelta = Converter::convertMixedToBoolean(bDelta);
}

bool Model_OTA_HotelRatePlanNotifRQ_RatePlans::getDelta() {
  return _bDelta;
}

void Model_OTA_HotelRatePlanNotifRQ_RatePlans::setMultiMarkup (std::string sMultiMarkup) {
  _bMultiMarkup = Converter::convertMixedToBoolean(sMultiMarkup);
}

void Model_OTA_HotelRatePlanNotifRQ_RatePlans::setMultiMarkup (int iMultiMarkup) {
  _bMultiMarkup = Converter::convertMixedToBoolean(iMultiMarkup);
}

void Model_OTA_HotelRatePlanNotifRQ_RatePlans::setMultiMarkup (bool bMultiMarkup) {
  _bMultiMarkup = Converter::convertMixedToBoolean(bMultiMarkup);
}

bool Model_OTA_HotelRatePlanNotifRQ_RatePlans::getMultiMarkup() {
  return _bMultiMarkup;
}

Model_OTA_HotelRatePlanNotifRQ_BookingRules* Model_OTA_HotelRatePlanNotifRQ_RatePlans::getBookingRules() {
  return _oBookingRules;
}

void Model_OTA_HotelRatePlanNotifRQ_RatePlans::setBookingRules(Model_OTA_HotelRatePlanNotifRQ_BookingRules& oBookingRules) {
  _oBookingRules = &oBookingRules;
}

Model_OTA_HotelRatePlanNotifRQ_Offers* Model_OTA_HotelRatePlanNotifRQ_RatePlans::getOffers() {
  return _oOffers;
}

void Model_OTA_HotelRatePlanNotifRQ_RatePlans::setOffers(Model_OTA_HotelRatePlanNotifRQ_Offers& oOffers) {
  _oOffers = &oOffers;
}

Model_OTA_HotelRatePlanNotifRQ_Supplements* Model_OTA_HotelRatePlanNotifRQ_RatePlans::getSupplements() {
  return _oSupplements;
}

void Model_OTA_HotelRatePlanNotifRQ_RatePlans::setSupplements(Model_OTA_HotelRatePlanNotifRQ_Supplements& oSupplements) {
  _oSupplements = &oSupplements;
}

Model_OTA_HotelRatePlanNotifRQ_RatePlans::Model_OTA_HotelRatePlanNotifRQ_RatePlans() {
}

Model_OTA_HotelRatePlanNotifRQ_RatePlans::~Model_OTA_HotelRatePlanNotifRQ_RatePlans() {
  delete this->_oBookingRules;
  delete this->_oOffers;
  delete this->_oSupplements;
}
