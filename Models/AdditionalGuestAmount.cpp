#include "AdditionalGuestAmount.h"

void Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::setMinAge(int iMinAge) {
  _iMinAge = iMinAge;
}

int Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::getMinAge() {
  return _iMinAge;
}

void Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::setMaxAge(int iMaxAge) {
  _iMaxAge = iMaxAge;
}

int Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::getMaxAge() {
  return _iMaxAge;
}

void Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::setCurrencyCode(std::string sCurrencyCode) {
  _sCurrencyCode = sCurrencyCode;
}

std::string Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::getCurrencyCode() {
  return _sCurrencyCode;
}

void Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::setAdditionalGuestsNumber(int iAdditionalGuestsNumber) {
  _iAdditionalGuestNumber = iAdditionalGuestsNumber;
}

int Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::getAdditionalGuestNumber() {
  return _iAdditionalGuestNumber;
}

void Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::setAmountAfterTax(float fAmountAfterTax) {
  _fAmountAfterTax = fAmountAfterTax;
}

float Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::getAmountAfterTax() {
  return _fAmountAfterTax;
}

void Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::setAgeQualifyingCode(int iAgeQualifyingCode) {
  _iAgeQualifyingCode = iAgeQualifyingCode;
}

int Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::getAgeQualifyingCode() {
  return _iAdditionalGuestNumber;
}

void Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::setMaxAdditionalGuests(int iMaxAdditionalGuests) {
  _iMaxAdditionalGuests = iMaxAdditionalGuests;
}

int Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::getMaxAdditionalGuests() {
  return _iMaxAdditionalGuests;
}

void Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::setAmount(float fAmount) {
  _fAmount = fAmount;
}

float Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::getAmount() {
  return _fAmount;
}

void Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::setPercent(float fPercent) {
  _fPercent = fPercent;
}

float Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::getPercent() {
  return _fPercent;
}

Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount() {
  
}

Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::~Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount() {
  
}