#include "Date.h"

void Model_Date::setStart(std::string sStart) {
  _sStart = sStart;
}

std::string Model_Date::getStart() {
  return _sStart;
}

void Model_Date::setEnd(std::string sEnd) {
  _sEnd = sEnd;
}

std::string Model_Date::getEnd() {
  return _sEnd;
}

Model_Date::Model_Date() : _sStart(""), _sEnd("") {

}

Model_Date::~Model_Date() {

}
