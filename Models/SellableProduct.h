#ifndef __MODEL_OTA_HOTELRATEPLANNOTIFRQ_SELLABLEPRODUCTS_SELLABLEPRODUCT_H__
#define __MODEL_OTA_HOTELRATEPLANNOTIFRQ_SELLABLEPRODUCTS_SELLABLEPRODUCT_H__

#include "Entity.h"
#include "GuestRoom.h"
#include "Date.h"

class Model_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct : public Model_Entity, public Model_Date {
public:
  void setInvType(std::string);
  std::string getInvType();
  
  void setInvCode(std::string);
  std::string getInvCode();
  
  void setGuestRoom(Model_OTA_HotelRatePlanNotifRQ_GuestRoom&);
  Model_OTA_HotelRatePlanNotifRQ_GuestRoom* getGuestRoom();
  
  Model_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct();
  ~Model_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct();
protected:
private:
  std::string _sInvCode;
  std::string _sInvType;
  
  Model_OTA_HotelRatePlanNotifRQ_GuestRoom *_oGuestRoom = NULL;
};

#endif
