#include "AgeGroup.h"

void Model_AgeGroup::setInfantMinAge(unsigned int iInfantMinAge) {
  _iInfantMinAge = iInfantMinAge;
}

unsigned int Model_AgeGroup::getInfantMinAge(void) {
  return _iInfantMinAge;
}

void Model_AgeGroup::setInfantMaxAge(unsigned int iInfantMaxAge) {
  _iInfantMaxAge = iInfantMaxAge;
}

unsigned int Model_AgeGroup::getInfantMaxAge(void) {
  return _iInfantMaxAge;
}

void Model_AgeGroup::setChildrenMinAge(unsigned int iChildrenMinAge) {
  _iChildrenMinAge = iChildrenMinAge;
}

unsigned int Model_AgeGroup::getChildrenMinAge(void) {
  return _iChildrenMinAge;
}

void Model_AgeGroup::setChildrenMaxAge(unsigned int iChildrenMaxAge) {
  _iChildrenMaxAge = iChildrenMaxAge;
}

unsigned int Model_AgeGroup::getChildrenMaxAge(void) {
  return _iChildrenMaxAge;
}

void Model_AgeGroup::setAdultMinAge(unsigned int iAdultMinAge) {
  _iAdultMinAge = iAdultMinAge;
}

unsigned int Model_AgeGroup::getAdultMinAge(void) {
  return _iAdultMinAge;
}

void Model_AgeGroup::setAdultMaxAge(unsigned int iAdultMaxAge) {
  _iAdultMaxAge = iAdultMaxAge;
}

unsigned int Model_AgeGroup::getAdultMaxAge(void) {
  return _iAdultMaxAge;
}

Model_AgeGroup::Model_AgeGroup() {
  setAdultMinAge(0);
  setAdultMaxAge(0);
  setChildrenMinAge(0);
  setChildrenMaxAge(0);
  setInfantMinAge(0);
  setInfantMaxAge(0);
}

Model_AgeGroup::~Model_AgeGroup() {

}
