#ifndef __MODEL_OTA_HOTELAVAILNOTIFRQ_AVAILSTATUSMESSAGES_AVAILSTATUSMESSAGE_H__
#define __MODEL_OTA_HOTELAVAILNOTIFRQ_AVAILSTATUSMESSAGES_AVAILSTATUSMESSAGE_H__

#include "RestrictionStatus.h"
#include "StatusApplicationControl.h"

class Model_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage {
public:
  void setRestrictionStatus(Model_OTA_HotelAvailNotifRQ_RestrictionStatus&);
  Model_OTA_HotelAvailNotifRQ_RestrictionStatus* getRestrictionStatus();
  
  void setStatusApplicationControl(Model_OTA_HotelAvailNotifRQ_StatusApplicationControl&);
  Model_OTA_HotelAvailNotifRQ_StatusApplicationControl* getStatusApplicationControl();
  
  Model_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage();
  ~Model_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage();
protected:
private:
  Model_OTA_HotelAvailNotifRQ_RestrictionStatus* _poRestrictionStatus = NULL;
  Model_OTA_HotelAvailNotifRQ_StatusApplicationControl* _poStatusApplicationControl = NULL;
};

#endif
