#ifndef __MODEL_OTA_HOTELRATEPLANNOTIFRQ_DISCOUNT_H__
#define __MODEL_OTA_HOTELRATEPLANNOTIFRQ_DISCOUNT_H__

#include <string>

class Model_OTA_HotelRatePlanNotifRQ_Discount {
public:
  void setPercent(double);
  double getPercent();
  
  void setAmount(double);
  double getAmount();
  
  void setNightsRequired(int);
  int getNightsRequired();
  
  void setNightsDiscounted(int);
  int getNightsDiscounted();
  
  void setChargeUnitCode(int);
  int getChargeUnitCode(void);
  
  void setCurrencyCode(std::string);
  std::string getCurrencyCode(void);
  
  void setDiscountLastBookingDays(bool);
  bool getDiscountLastBookingDays(void);
  
  Model_OTA_HotelRatePlanNotifRQ_Discount();
  ~Model_OTA_HotelRatePlanNotifRQ_Discount();
protected:
private:
  double _dPercent;
  double _dAmount;
  
  bool _bDiscountLastBookingDays;
  
  std::string _sCurrencyCode;
  
  int _iChargeUnitCode;
  int _iNightsRequired;
  int _iNightsDiscounted;
};

#endif
