#ifndef __MODEL_OTA_HOTELRATEPLANNOTIFRQ_ADDITIONALGUESTAMOUNTS_ADDITIONALGUESTAMOUNT_H__
#define __MODEL_OTA_HOTELRATEPLANNOTIFRQ_ADDITIONALGUESTAMOUNTS_ADDITIONALGUESTAMOUNT_H__
#include <string>

#include "Entity.h"

class Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount : 
  public Model_Entity {
  public:
    void setMinAge(int);
    int getMinAge();
    
    void setMaxAge(int);
    int getMaxAge();
    
    void setCurrencyCode(std::string);
    std::string getCurrencyCode();
    
    void setAmountAfterTax(float);
    float getAmountAfterTax();
    
    void setAdditionalGuestsNumber(int);
    int getAdditionalGuestNumber();
    
    void setAgeQualifyingCode(int);
    int getAgeQualifyingCode();
    
    void setMaxAdditionalGuests(int);
    int getMaxAdditionalGuests();
    
    void setAmount(float);
    float getAmount();
    
    void setPercent(float);
    float getPercent();
    
    Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount();
    ~Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount();
  protected:
  private:
    std::string _sCurrencyCode;
    
    float _fAmountAfterTax;
    float _fAmount;
    float _fPercent;
    
    int _iMinAge;
    int _iMaxAge;
    int _iAdditionalGuestNumber;
    int _iAgeQualifyingCode;
    int _iMaxAdditionalGuests;
};

#endif