#include "AmountPercent.h"

void Model_OTA_HotelRatePlanNotifRQ_AmountPercent::setAmount(float fAmount) {
  _fAmount = fAmount;
}

float Model_OTA_HotelRatePlanNotifRQ_AmountPercent::getAmount() {
  return _fAmount;
}

void Model_OTA_HotelRatePlanNotifRQ_AmountPercent::setPercent(float fPercent) {
  _fPercent = fPercent;
}

float Model_OTA_HotelRatePlanNotifRQ_AmountPercent::getPercent() {
  return _fPercent;
}

void Model_OTA_HotelRatePlanNotifRQ_AmountPercent::setCurrencyCode(std::string sCurrencyCode) {
  _sCurrencyCode = sCurrencyCode;
}

std::string Model_OTA_HotelRatePlanNotifRQ_AmountPercent::getCurrencyCode() {
  return _sCurrencyCode;
}

Model_OTA_HotelRatePlanNotifRQ_AmountPercent::Model_OTA_HotelRatePlanNotifRQ_AmountPercent() {

}

Model_OTA_HotelRatePlanNotifRQ_AmountPercent::~Model_OTA_HotelRatePlanNotifRQ_AmountPercent() {

}
