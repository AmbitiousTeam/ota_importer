#ifndef __MODEL_PERSON_H__
#define __MODEL_PERSON_H__

#include <iostream>
#include <string>
#include "Entity.h"

class Model_Person : public Model_Entity {
public:
  std::string sName;
  std::string sNachName;
  
  Model_Person();
  ~Model_Person();
};

#endif