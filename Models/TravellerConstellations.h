#ifndef __MODEL_TRAVELLERCONSTELLATIONS_H__
#define __MODEL_TRAVELLERCONSTELLATIONS_H__

#include "Collection.h"
#include "TravellerConstellation.h"

class Model_TravellerConstellations : public Model_Collection<Model_TravellerConstellation> {
};

#endif
