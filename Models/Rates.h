#ifndef __MODEL_OTA_HOTELRATEPLANNOTIFRQ_RATES_H__
#define __MODEL_OTA_HOTELRATEPLANNOTIFRQ_RATES_H__

#include <string>
#include "Collection.h"
#include "Rate.h"

class Model_OTA_HotelRatePlanNotifRQ_Rates : public Model_Collection<Model_OTA_HotelRatePlanNotifRQ_Rates_Rate> {
public:
  
  Model_OTA_HotelRatePlanNotifRQ_Rates();
  ~Model_OTA_HotelRatePlanNotifRQ_Rates();
  
protected:
private:
  std::string sInvTypeCode;
};

#endif