#include "CSVOffer.h"

void Model_CSVOffer::setAdultCount(const int iAdultCount) {
  _iAdultCount = iAdultCount;
}

int Model_CSVOffer::getAdultCount(void) const {
  return _iAdultCount;
}

void Model_CSVOffer::setOfferId(const int iOfferId) {
  _iOfferId = iOfferId;
}

int Model_CSVOffer::getOfferId(void) const {
  return _iOfferId;
}

std::string Model_CSVOffer::getArrivalFrom(void) const {
  return _sArrivalDateFrom;
}

void Model_CSVOffer::setArrivalFrom(const std::string sArrivalDateFrom) {
  _sArrivalDateFrom = sArrivalDateFrom;
}

std::string Model_CSVOffer::getArrivalTo(void) const {
  return _sArrivalDateTo;
}

void Model_CSVOffer::setArrivalTo(const std::string sArrivalDateTo) {
  _sArrivalDateTo = sArrivalDateTo;
}

std::string Model_CSVOffer::getBoardingCode(void) const {
  return _sBoardingCode;
}

void Model_CSVOffer::setBoardingCode(const std::string sBoardingCode) {
  _sBoardingCode = sBoardingCode;
}

void Model_CSVOffer::setPersonCount(const int iPersonCount) {
  _iPersonCount = iPersonCount;
}

int Model_CSVOffer::getPersonCount(void) const {
  return _iPersonCount;
}

std::string Model_CSVOffer::getBoardingCodeAgency(void) const {
  return _sBoardingCodeAgency;
}

void Model_CSVOffer::setBoardingCodeAgency(const std::string sBoardingCodeAgency) {
  _sBoardingCodeAgency = sBoardingCodeAgency;
}

std::string Model_CSVOffer::getDateFrom(void) const {
  return _sOfferDateFrom;
}

void Model_CSVOffer::setDateFrom(const std::string sOfferDateFrom) {
  _sOfferDateFrom = sOfferDateFrom;
}

std::string Model_CSVOffer::getDateTo(void) const {
  return _sOfferDateTo;
}

void Model_CSVOffer::setDateTo(const std::string sOfferDateTo) {
  _sOfferDateTo = sOfferDateTo;
}

int Model_CSVOffer::getDaysOfArrival(void) const {
  return _iDaysOfArrival;
}

void Model_CSVOffer::setDaysOfArrival(const int iDaysOfArrival) {
  _iDaysOfArrival = iDaysOfArrival;
}

std::string Model_CSVOffer::getDlc(void) const {
  return _sDlc;
}

void Model_CSVOffer::setDlc(const std::string sDlc) {
  _sDlc = sDlc;
}

std::string Model_CSVOffer::getHotelCode(void) const {
  return _sHotelCode;
}

void Model_CSVOffer::setHotelCode(const std::string sHotelCode) {
  _sHotelCode = sHotelCode;
}

std::string Model_CSVOffer::getHotelCodeAgency(void) const {
  return _sHotelCodeAgency;
}

void Model_CSVOffer::setHotelCodeAgency(const std::string sHotelCodeAgency) {
  _sHotelCodeAgency = sHotelCodeAgency;
}

unsigned int Model_CSVOffer::getMaxAge(void) const {
  return _iMaxAge;
}

void Model_CSVOffer::setMaxAge(const unsigned int iMaxAge) {
  _iMaxAge = iMaxAge;
}

int Model_CSVOffer::getMaxDaysOfArrival(void) const {
  return _iMaxDaysOfArrival;
}

void Model_CSVOffer::setMaxDaysOfArrival(const int iMaxDaysOfArrival) {
  _iMaxDaysOfArrival = iMaxDaysOfArrival;
}

unsigned int Model_CSVOffer::getMinAge(void) const {
  return _iMinAge;
}

void Model_CSVOffer::setMinAge(const unsigned int iMinAge) {
  _iMinAge = iMinAge;
}

int Model_CSVOffer::getMinDaysOfArrival(void) const {
  return _iMinDaysOfArrival;
}

void Model_CSVOffer::setMinDaysOfArrival(const int iMinDaysOfArrival) {
  _iMinDaysOfArrival = iMinDaysOfArrival;
}

std::string Model_CSVOffer::getPriceType(void) const {
  return _sPriceType;
}

void Model_CSVOffer::setPriceType(const std::string sPriceType) {
  _sPriceType = sPriceType;
}

int Model_CSVOffer::getReleaseDays(void) const {
  return _iReleaseDays;
}

void Model_CSVOffer::setReleaseDays(const int iReleaseDays) {
  _iReleaseDays = iReleaseDays;
}

bool Model_CSVOffer::getRentalCar(void) const {
  return _bRentalCar;
}

void Model_CSVOffer::setRentalCar(const bool bRentalCar) {
  _bRentalCar = bRentalCar;
}

std::string Model_CSVOffer::getRoomCode(void) const {
  return _sRoomCode;
}

void Model_CSVOffer::setRoomCode(const std::string sRoomCode) {
  _sRoomCode = sRoomCode;
}

int Model_CSVOffer::getSellingDaysBeforeArrival(void) const {
  return _iSellingDaysBeforeArrival;
}

void Model_CSVOffer::setSellingDaysBeforeArrival(const int iSellingDaysBeforeArrival) {
  _iSellingDaysBeforeArrival = iSellingDaysBeforeArrival;
}

std::string Model_CSVOffer::getSellingFrom(void) const {
  return _sSellingDateFrom;
}

void Model_CSVOffer::setSellingFrom(const std::string sSellingDateFrom) {
  _sSellingDateFrom = sSellingDateFrom;
}

std::string Model_CSVOffer::getSellingTo(void) const {
  return _sSellingDateTo;
}

void Model_CSVOffer::setSellingTo(const std::string sSellingDateTo) {
  _sSellingDateTo = sSellingDateTo;
}

int Model_CSVOffer::getStatus(void) const {
  return _iStatus;
}

void Model_CSVOffer::setStatus(const int iStatus) {
  _iStatus = iStatus;
}

std::string Model_CSVOffer::getTravelType(void) const {
  return _sTravelType;
}

void Model_CSVOffer::setTravelType(const std::string sTravelType) {
  _sTravelType = sTravelType;
}

std::string Model_CSVOffer::getUniqueOfferHash(void) const {
  return _sUniqueOfferHash;
}

void Model_CSVOffer::setUniqueOfferHash(const std::string sUniqueOfferHash) {
  _sUniqueOfferHash = sUniqueOfferHash;
}

std::string Model_CSVOffer::getUpdateDate(void) const {
  return _sUpdateDate;
}

void Model_CSVOffer::setUpdateDate(const std::string sUpdateDate) {
  _sUpdateDate = sUpdateDate;
}

double Model_CSVOffer::getPrice(void) const {
  return _dPrice;
}

void Model_CSVOffer::setPrice(const double dPrice) {
  _dPrice = dPrice;
}

std::ostream& operator<< (std::ostream& out, const Model_CSVOffer& oOfferEntity) {
  return out 
      << "#" << oOfferEntity.getOfferId() << "#;"
      << oOfferEntity.getDateFrom() << ";"
      << oOfferEntity.getDateTo() << ";"
      << oOfferEntity.getPrice() << ";"
      << oOfferEntity.getPriceType() << ";"
      << oOfferEntity.getTravelType() << ";"
      << oOfferEntity.getDaysOfArrival() << ";"
      << oOfferEntity.getReleaseDays() << ";"
      << oOfferEntity.getPersonCount() << ";" 
      << oOfferEntity.getAdultCount() << ";"
      << oOfferEntity.getMinAge() << ";"
      << oOfferEntity.getMaxAge() << ";"
      << oOfferEntity.getStatus() << ";"
      << oOfferEntity.getBoardingCodeAgency() << ";"
      << oOfferEntity.getMinDaysOfArrival() << ";"
      << oOfferEntity.getMaxDaysOfArrival() << ";"
      << oOfferEntity.getSellingFrom() << ";"
      << oOfferEntity.getSellingTo() << ";"
      << oOfferEntity.getArrivalFrom() << ";"
      << oOfferEntity.getArrivalTo() << ";"
      << oOfferEntity.getSellingDaysBeforeArrival() << ";"
      << oOfferEntity.getRentalCar() << ";"
      << "0;"
      << oOfferEntity.getUpdateDate() << ";"
      << oOfferEntity.getDlc() << ";"
      << oOfferEntity.getHotelCode() << ";"
      << oOfferEntity.getBoardingCode() << ";"
      << oOfferEntity.getRoomCode() << ";"
      << oOfferEntity.getUniqueOfferHash()
      << "\n";
}

std::ostream& operator<< (std::ostream& out, const Model_CSVOffer* oOfferEntity) {
  return out
      << "#" << oOfferEntity->getOfferId() << "#;"
      << oOfferEntity->getDateFrom() << ";"
      << oOfferEntity->getDateTo() << ";"
      << oOfferEntity->getPrice() << ";"
      << oOfferEntity->getPriceType() << ";"
      << oOfferEntity->getTravelType() << ";"
      << oOfferEntity->getDaysOfArrival() << ";"
      << oOfferEntity->getReleaseDays() << ";"
      << oOfferEntity->getPersonCount() << ";" 
      << oOfferEntity->getAdultCount() << ";"
      << oOfferEntity->getMinAge() << ";"
      << oOfferEntity->getMaxAge() << ";"
      << oOfferEntity->getStatus() << ";"
      << oOfferEntity->getBoardingCodeAgency() << ";"
      << oOfferEntity->getMinDaysOfArrival() << ";"
      << oOfferEntity->getMaxDaysOfArrival() << ";"
      << oOfferEntity->getSellingFrom() << ";"
      << oOfferEntity->getSellingTo() << ";"
      << oOfferEntity->getArrivalFrom() << ";"
      << oOfferEntity->getArrivalTo() << ";"
      << oOfferEntity->getSellingDaysBeforeArrival() << ";"
      << oOfferEntity->getRentalCar() << ";"
      << "0;"
      << oOfferEntity->getUpdateDate() << ";"
      << oOfferEntity->getDlc() << ";"
      << oOfferEntity->getHotelCode() << ";"
      << oOfferEntity->getBoardingCode() << ";"
      << oOfferEntity->getRoomCode() << ";"
      << oOfferEntity->getUniqueOfferHash()
      << "\n";
}

Model_CSVOffer::operator std::string() const {
  std::stringstream oOutStream;
  oOutStream << *this;
  std::string out = oOutStream.str();
  return out;
}

Model_CSVOffer::Model_CSVOffer() {
  this->_bRentalCar = false;
  
  this->_dPrice = 0.00;
  
  this->_iAdultCount = 0;
  this->_iPersonCount = 0;
  this->_iDaysBeforeArrival = 1;
  this->_iDaysOfArrival = 1;
  this->_iMaxAge = 17;
  this->_iMaxDaysOfArrival = 99;
  this->_iMinAge = 0;
  this->_iMinDaysOfArrival = 1;
  this->_iOfferId = 0;
  this->_iReleaseDays = 3;
  this->_iSellingDaysBeforeArrival = 3;
  this->_iHotelId = 0;
  this->_iRoomId = 0;
  this->_iStatus = 0;
  
  this->_sArrivalDateFrom = "0000-00-00";
  this->_sArrivalDateTo = "0000-00-00";
  this->_sBoardingCode = "";
  this->_sBoardingCodeAgency = "";
  this->_sDlc = "";
  this->_sHotelCode = "";
  this->_sHotelCodeAgency = "";
  this->_sOfferDateFrom = "0000-00-00";
  this->_sOfferDateTo = "0000-00-00";
  this->_sPriceType = "";
  this->_sRoomCode = "";
  this->_sRoomCodeAgency = "";
  this->_sSellingDateFrom = "0000-00-00";
  this->_sSellingDateTo = "0000-00-00";
  this->_sTravelType = "";
  this->_sUniqueOfferHash = "";
  this->_sUpdateDate = "0000-00-00";
}

Model_CSVOffer::~Model_CSVOffer() {

}
