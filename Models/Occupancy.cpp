#include "Occupancy.h"

const int Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::AGE_QUALIFYING_CODE_ADULT = 10;
const int Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::AGE_QUALIFYING_CODE_CHILDREN = 8;
const int Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::AGE_QUALIFYING_CODE_INFANT = 7;
  
void Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::setAgeQualifyingCode(int iAgeQualifyingCode) {
  _iAgeQualifyingCode = iAgeQualifyingCode;
}

int Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::getAgeQualifyingCode() {
  return _iAgeQualifyingCode;
}

void Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::setAgeTimeUnit(std::string sAgeTimeUnit) {
  _sAgeTimeUnit = sAgeTimeUnit;
}

std::string Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::getAgeTimeUnit() {
  return _sAgeTimeUnit;
}

void Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::setInfantsAreCounted(bool bInfantsAreCounted) {
  _bInfantsAreCounted = bInfantsAreCounted;
}

bool Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::getInfantsAreCounted() {
  return _bInfantsAreCounted;
}

void Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::setMinAge(int iMinAge) {
  _iMinAge = iMinAge;
}

int Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::getMinAge() {
  return _iMinAge;
}

void Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::setMaxAge(int iMaxAge) {
  _iMaxAge = iMaxAge;
}

int Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::getMaxAge() {
  return _iMaxAge;
}

void Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::setMinOccupancy(int iMinOccupancy) {
  _iMinOccupancy = iMinOccupancy;
}

int Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::getMinOccupancy() {
  return _iMinOccupancy;
}

void Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::setMaxOccupancy(int iMaxOccupancy) {
  _iMaxOccupancy = iMaxOccupancy;
}

int Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::getMaxOccupancy() {
  return _iMaxOccupancy;
}

Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy() {
  _iAgeQualifyingCode = 0;
  _iMinAge = 0;
  _iMaxAge = 0;
  _iMinOccupancy = 0;
  _iMaxOccupancy = 0;
  _bInfantsAreCounted = false;
}

Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::~Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy() {

}
