#ifndef __MODEL_OTA_HOTELRATEPLANNOTIFRQ_RATEPLANS_RATEPLAN_H__
#define __MODEL_OTA_HOTELRATEPLANNOTIFRQ_RATEPLANS_RATEPLAN_H__

#include "Entity.h"
#include "Offers.h"
#include "Rates.h"
#include "Supplements.h"
#include "BookingRules.h"
#include "SellableProducts.h"

class Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan : public Model_Entity {
public:

  static const std::string RATEPLANNOTIFTYPEOVERLAY;

  void setRatePlanNotifType(const std::string);
  std::string getRatePlanNotifType(void);
  void setCurrencyCode(const std::string);
  std::string getCurrencyCode(void);
  void setRatePlanCode(const std::string);
  std::string getRatePlanCode(void);
  
  void setOffers(Model_OTA_HotelRatePlanNotifRQ_Offers&);
  Model_OTA_HotelRatePlanNotifRQ_Offers* getOffers(void);
  
  void setSupplements(Model_OTA_HotelRatePlanNotifRQ_Supplements&);
  Model_OTA_HotelRatePlanNotifRQ_Supplements* getSupplements(void);
  
  void setBookingRules(Model_OTA_HotelRatePlanNotifRQ_BookingRules&);
  Model_OTA_HotelRatePlanNotifRQ_BookingRules* getBookingRules(void);
  
  void setRates(Model_OTA_HotelRatePlanNotifRQ_Rates&);
  Model_OTA_HotelRatePlanNotifRQ_Rates* getRates(void);
  
  void setSellableProducts(Model_OTA_HotelRatePlanNotifRQ_SellableProducts&);
  Model_OTA_HotelRatePlanNotifRQ_SellableProducts* getSellableProducts(void);
  
  void setPromotionCode(std::string);
  std::string getPromotionCode(void);
  
  void setChargeType(int);
  int getChargeType(void);
  
  Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan();
  ~Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan();
protected:
private:
  std::string _sRatePlanNotifType;
  std::string _sCurrencyCode;
  std::string _sRatePlanCode;
  std::string _sPromotionCode;
  int _iChargeType;
  
  Model_OTA_HotelRatePlanNotifRQ_Rates *_oRates = NULL;
  Model_OTA_HotelRatePlanNotifRQ_Offers *_oOffers = NULL;
  Model_OTA_HotelRatePlanNotifRQ_Supplements *_oSupplements = NULL;
  Model_OTA_HotelRatePlanNotifRQ_BookingRules *_oBookingRules = NULL;
  Model_OTA_HotelRatePlanNotifRQ_SellableProducts *_oSellableProducts = NULL;
};

#endif
