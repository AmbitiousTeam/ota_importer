#ifndef __MODEL_OTA_HOTELRATEPLANNOTIFRQ_BASEBYGUESTAMTS_H__
#define __MODEL_OTA_HOTELRATEPLANNOTIFRQ_BASEBYGUESTAMTS_H__

#include "Collection.h"
#include "BaseByGuestAmt.h"

class Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts : public Model_Collection<Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt> {
public:
  Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts();
  ~Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts();
protected:
private:
};

typedef Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt BaseByGuestAmt;

#endif