#ifndef __MODEL_TRAVELLERCONSTELLATION_H__
#define __MODEL_TRAVELLERCONSTELLATION_H__

#include "Entity.h"
#include "AgeGroup.h"

class Model_TravellerConstellation : public Model_Entity {
public:
  
  void setStandardCapacity(int);
  int getStandardCapacity();
  
  void setPersonCount(int);
  int getPersonCount();
  
  void setAdultCount(int);
  int getAdultCount();
  
  void setChildCount(int);
  int getChildCount();
  
  void setInfantCount(int);
  int getInfantCount();
  
  void setAdditionalAdults(int);
  int getAdditionalAdults();
  
  void setAdditionalChildren(int);
  int getAdditionalChildren();
  
  void setAdditionalInfants(int);
  int getAdditionalInfants();
  
  void setAgeGroup(Model_AgeGroup&);
  Model_AgeGroup* getAgeGroup(void);
  
  Model_TravellerConstellation();
  ~Model_TravellerConstellation();
  
protected:
private:
  
  int _iStandardCapacity;
  
  int _iPersonCount;
  int _iAdultCount;
  int _iChildCount;
  int _iInfantCount;
  int _iAdditionalAdults;
  int _iAdditionalChildren;
  int _iAdditionalInfantCount;
  Model_AgeGroup* _oAgeGroup = NULL;
};

#endif
