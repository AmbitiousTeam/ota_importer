#ifndef __MODEL_OTA_HOTELRATEPLANNOTIFRQ_DEPARTUREDAYSOFWEEK_H__
#define __MODEL_OTA_HOTELRATEPLANNOTIFRQ_DEPARTUREDAYSOFWEEK_H__

#include "Entity.h"

class Model_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek : public Model_Entity {
public:
  void setMon(bool);
  bool getMon();
  
  void setTue(bool);
  bool getTue();
  
  void setWed(bool);
  bool getWed();
  
  void setThu(bool);
  bool getThu();
  
  void setFri(bool);
  bool getFri();
  
  void setSun(bool);
  bool getSun();
  
  void setSat(bool);
  bool getSat();
  
  Model_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek();
  ~Model_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek();
protected:
private:
  
  bool _bMon;
  bool _bTue;
  bool _bWed;
  bool _bThu;
  bool _bFri;
  bool _bSun;
  bool _bSat;
};

#endif