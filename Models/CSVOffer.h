#ifndef __MODEL_CSVOFFER_H__
#define __MODEL_CSVOFFER_H__

#include <iostream>
#include <string>
#include <sstream>

#include "Entity.h"

class Model_CSVOffer : public Model_Entity {
public:
  
  void setOfferId(const int);
  int getOfferId(void) const;
  
  void setDateFrom(const std::string);
  std::string getDateFrom(void) const;
  
  void setDateTo(const std::string);
  std::string getDateTo(void) const;
  
  void setPrice(const double);
  double getPrice(void) const;
  
  void setPriceType(const std::string);
  std::string getPriceType(void) const;
  
  void setTravelType(const std::string);
  std::string getTravelType(void) const;
  
  void setDaysOfArrival(const int);
  int getDaysOfArrival(void) const;
  
  void setReleaseDays(const int);
  int getReleaseDays(void) const;
  
  void setPersonCount(const int);
  int getPersonCount(void) const;
  
  void setAdultCount(const int);
  int getAdultCount(void) const;
  
  void setMinAge(const unsigned int);
  unsigned int getMinAge(void) const;
  
  void setMaxAge(const unsigned int);
  unsigned int getMaxAge(void) const;
  
  void setStatus(const int);
  int getStatus(void) const;
  
  void setBoardingCodeAgency(const std::string);
  std::string getBoardingCodeAgency(void) const;
  
  void setMinDaysOfArrival(const int);
  int getMinDaysOfArrival(void) const;
  
  void setMaxDaysOfArrival(const int);
  int getMaxDaysOfArrival(void) const;
  
  void setSellingFrom(const std::string);
  std::string getSellingFrom(void) const;
  
  void setSellingTo(const std::string);
  std::string getSellingTo(void) const;
  
  void setArrivalFrom(const std::string);
  std::string getArrivalFrom(void) const;
  
  void setArrivalTo(const std::string);
  std::string getArrivalTo(void) const;
  
  void setSellingDaysBeforeArrival(const int);
  int getSellingDaysBeforeArrival(void) const;
  
  void setRentalCar(const bool);
  bool getRentalCar(void) const;
  
  void setUpdateDate(const std::string);
  std::string getUpdateDate(void) const;
  
  void setDlc(const std::string);
  std::string getDlc(void) const;
  
  void setHotelCodeAgency(const std::string);
  std::string getHotelCodeAgency(void) const;
  
  void setHotelCode(const std::string);
  std::string getHotelCode(void) const;
  
  void setBoardingCode(const std::string);
  std::string getBoardingCode(void) const;
  
  void setRoomCode(const std::string);
  std::string getRoomCode(void) const;
  
  void setUniqueOfferHash(const std::string);
  std::string getUniqueOfferHash(void) const;
  
  friend std::ostream& operator<< (std::ostream& out, const Model_CSVOffer& oOfferEntity);
  friend std::ostream& operator<< (std::ostream& out, const Model_CSVOffer* oOfferEntity);
  std::string& operator= (std::string& out);
  operator std::string() const;
  
  Model_CSVOffer();
  ~Model_CSVOffer();
  
protected:
private:
  
  int _iOfferId;
  int _iDaysOfArrival;
  int _iDaysBeforeArrival;
  int _iReleaseDays;
  int _iPersonCount;
  int _iAdultCount;
  unsigned int _iMinAge;
  unsigned int _iMaxAge;
  int _iStatus;
  int _iMinDaysOfArrival;
  int _iMaxDaysOfArrival;
  int _iSellingDaysBeforeArrival;
  int _iRoomId;
  int _iHotelId;
  
  double _dPrice;
  
  bool _bRentalCar;
  
  std::string _sOfferDateFrom;
  std::string _sOfferDateTo;
  std::string _sSellingDateFrom;
  std::string _sSellingDateTo;
  std::string _sArrivalDateFrom;
  std::string _sArrivalDateTo;
  std::string _sPriceType;
  std::string _sTravelType;
  std::string _sBoardingCode;
  std::string _sBoardingCodeAgency;
  std::string _sHotelCodeAgency;
  std::string _sHotelCode;
  std::string _sRoomCode;
  std::string _sRoomCodeAgency;
  std::string _sDlc;
  std::string _sUpdateDate;
  std::string _sUniqueOfferHash;
};

// std::ostream& operator<< (std::ostream&, const Model_CSVOffer&);

// std::ostream& operator<< (std::ostream& out, const Model_CSVOffer& oOfferEntity) {
//   return out << oOfferEntity.getOfferId();
// }

#endif
