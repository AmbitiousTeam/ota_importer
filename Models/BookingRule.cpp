#include "BookingRule.h"

void Model_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule::setDOWRestrictions(Model_OTA_HotelRatePlanNotifRQ_DOW_Restrictions& oDOWRestrictions) {
  _poDOWRestrictions = &oDOWRestrictions;
}

Model_OTA_HotelRatePlanNotifRQ_DOW_Restrictions* Model_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule::getDOWRestrictions() {
  return _poDOWRestrictions;
}

void Model_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule::setLengthsOfStay(Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay& oLengthsOfStay) {
  _poLengthsOfStay = &oLengthsOfStay;
}

Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay* Model_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule::getLengthsOfStay() {
  return _poLengthsOfStay;
}

void Model_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule::setCancelPenalty(Model_OTA_HotelRatePlanNotifRQ_CancelPenalty& oCancelPenalty) {
  _poCancelPenalty = &oCancelPenalty;
}

Model_OTA_HotelRatePlanNotifRQ_CancelPenalty* Model_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule::getCancelPenalty() {
  return _poCancelPenalty;
}

void Model_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule::setRestrictionStatus(Model_OTA_HotelAvailNotifRQ_RestrictionStatus& oRestrictionStatus) {
  _poRestrictionStatus = &oRestrictionStatus;
}

Model_OTA_HotelAvailNotifRQ_RestrictionStatus* Model_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule::getRestrictionStatus() {
  return _poRestrictionStatus;
}

Model_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule::Model_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule() {

}

Model_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule::~Model_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule() {
  delete this->_poCancelPenalty;
  delete this->_poDOWRestrictions;
  delete this->_poLengthsOfStay;
  delete this->_poRestrictionStatus;
}
