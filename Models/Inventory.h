#ifndef __MODEL_OTA_HOTELRATEPLANNOTIFRQ_INVENTORIES_INVENTORY_H__
#define __MODEL_OTA_HOTELRATEPLANNOTIFRQ_INVENTORIES_INVENTORY_H__

#include <string>

class Model_OTA_HotelRatePlanNotifRQ_Inventories_Inventory {
public:
  void setInvCode(std::string);
  std::string getInvCode(void);
  
  void setInvType(std::string);
  std::string getInvType(void);
  
  void setAppliesToIndicator(bool);
  bool getAppliesToIndicator(void);
  
  Model_OTA_HotelRatePlanNotifRQ_Inventories_Inventory();
  ~Model_OTA_HotelRatePlanNotifRQ_Inventories_Inventory();
protected:
private:
  std::string _sInvCode;
  std::string _sInvType;
  bool _bAppliesToIndicator;
};

#endif
