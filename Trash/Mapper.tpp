// #include "Mapper.h"

template<typename T>void Service_Mapper<T>::addFunction(std::string funcName, memberFunction func) {
  _funcMap[funcName] = func;
}

// template<typename T>bool Service_Mapper<T>::operator()(string sFuncName, string sValue, T& oObject) {
template<typename T>bool Service_Mapper<T>::operator()(std::string sFuncName, std::string sValue) {
  typename std::map<std::string, memberFunction>::const_iterator itMap;
  
  itMap = _funcMap.find(sFuncName);
  bool bReturn = false;
  
  if (itMap != _funcMap.end()) {
    if (_oObject) {
      (_oObject->*itMap->second)(sValue);
      bReturn = true;
    } else {
      std::cout << "Object ist nicht gesetzt!" << std::endl;
    }
  } else {
    std::cout << "Kann mit Element " << sFuncName << " nichts anfangen" << std::endl;
  }
  return bReturn;
}

template<typename T>bool Service_Mapper<T>::setObject(T* oObject) {
  _oObject = oObject;
}

template<typename T>T* Service_Mapper<T>::getObject() {
  return _oObject;
}