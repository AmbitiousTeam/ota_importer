#include <iostream>
#include <string>
#include "Node.h"

template<typename T>void Model_Node<T>::setNext(Model_Node* pNextNode) {
  _pNext = pNextNode;
}

template<typename T>Model_Node<T>* Model_Node<T>::getNext() {
  return _pNext;
}

template<typename T>void Model_Node<T>::setPrev(Model_Node* pPrevNode) {
  _pPrev = pPrevNode;
}

template<typename T>Model_Node<T>* Model_Node<T>::getPrev() {
  return _pPrev;
}

template<typename T>void Model_Node<T>::setData(T& mData) {
  _mData = mData;
}

template<typename T>T& Model_Node<T>::getData() {
  return _mData;
}

template<typename T>void Model_Node<T>::setKey(std::string sKey) {
  _sKey = sKey;
}

template<typename T>void Model_Node<T>::setKey(int iKey) {
  _iKey = iKey;
}

template<typename T>std::string Model_Node<T>::getStringKey() {
  return _sKey;
}

template<typename T>int Model_Node<T>::getIntKey() {
  return _iKey;
}

template<typename T>Model_Node<T>::Model_Node() {
}

template<typename T>Model_Node<T>::~Model_Node() {
  if (DEBUG_MODE) {std::cout << "DTOR Node!" << std::endl;}
}

