#include <string>
#include <map>
#include <boost/regex.hpp>
#include <iostream>

// #include "XmlParser.h"
#include "../Tools/String.cpp"
#include "../Tools/Logger.h"

/**
 * datei wird eingelesen und dann das erste tag gemachted, dieser string wird als aData[tag] persistiert,
 * attribute des tags als $aData[tag][attribute], nachfolgende inhalte als $aData[tag][children];
 * 
 * es gibt die funktionen:
 *      - parse - parst einen string nach aData
 *      - load - liest xml content von einer datei ein
 *      - loadXmlString - parst einen xml string nach aData
 *      - validate - validiert die korrektheit des xml content, hier könnte man noch diverse schematas oder standards übergeben um auf deren validität zu überprüfen
 *      - save - speichert den aktuellen inhalt von aData als string in eine datei, ist keine datei angeben, wird der string ausgegeben
 *      - getAttributes - gibt alle attribute eines knotens zurück
 *      - getAttribut[attributName] - gibt den inhalt eines attributes zurück
 *      - setAttributes - setzt alle attribute, übergeben wird ein array attribute => values
 *      - setAttribute[attributName] - setzt den inhalt eines attributes
 *      - getNextSilbling - gibt das nächste element auf selber ebene zurück
 *      - getNextChild - gibt den nächsten unterknoten zurück
 *      - hasChildren - checkt, ob das knoten unterknoten enthält
 *      - hasAttributes - checkt ob das knoten attribute enthält
 *      - getChildrenCount - gibt die anzahl der unterknoten zurück
 *      - replaceNode - ersetzt einen knoten durch einen neuen
 *      - deleteNode - löscht einen knoten
 *      - addNode - fügt dem übergebenen knoten einen neuen knoten ein
 *      - saveXml - gibt den xmlString des aktuellen aData elements aus
 *      
 */

template<typename T>bool Service_XmlParser<T>::checkHasXmlHeader(std::string sContent) {
  boost::regex_constants::icase;
  boost::regex sRegExXmlHeader("<\\?.*?xml.*?\\?>");
  boost::match_results<std::string::const_iterator>matches;
  std::string::const_iterator start = sContent.begin();
  std::string::const_iterator end   = sContent.end();
  bool bResult = boost::regex_match(sContent, matches, sRegExXmlHeader, boost::match_partial | boost::format_perl);
  return bResult;
}

template<typename T>std::string Service_XmlParser<T>::removeXmlHeader(std::string sContent) {
  boost::regex_constants::icase;
  boost::regex sRegExXmlHeader("<\\?.*?xml.*?\\?>");
  std::string sReplaceResult = boost::regex_replace(sContent, sRegExXmlHeader, "", boost::match_partial | boost::format_perl);
  return sReplaceResult;
}

template<typename T>bool Service_XmlParser<T>::extractTags(std::string sContent) {
  boost::regex regex("<(.*?)>");
  boost::match_results<std::string::const_iterator> what;
  if(0 == boost::regex_match(sContent, what, regex, boost::match_default | boost::match_partial | boost::format_perl))
  {
    throw std::runtime_error("Invalid data entered - this could not possibly be a valid card number");
  }
  
  boost::smatch matches;
  boost::regex_constants::icase;
  boost::regex_constants::match_all;
  std::string::const_iterator start1 = sContent.begin();
  std::string::const_iterator end1   = sContent.end();

  while (boost::regex_search(start1, end1, matches, regex))
  {
    std::string sErgebnis(matches[1].first, matches[1].second);
    start1 = matches[0].second;
  }
  
  const int subs[] = {1};  // we just want to see group 1
  boost::sregex_token_iterator i(sContent.begin(), sContent.end(), regex, subs);
  boost::sregex_token_iterator j;
  
  while(i != j)
  {
    std::cout << "Ergebnis : " << *i++ << std::endl;
  }

  boost::sregex_token_iterator iter(sContent.begin(), sContent.end(), regex, 0);
  boost::sregex_token_iterator end2;
  int iTreffer = std::distance(iter, end2);
  std::cout << "Treffer : " << iTreffer << std::endl;
  
  while(iter != end2) {
    int iRest = std::distance(iter, end2);
    std::cout << "Pos : " << iRest << std::endl;
    std::cout << "Ergebnis : " << *iter << std::endl;
    iter++;
  }
  
  std::cout << "Header? " << checkHasXmlHeader(sContent) << std::endl;
  if (checkHasXmlHeader(sContent)) {
    std::cout << "Habe eine Datei mit XML Header!" << std::endl;
    removeXmlHeader(sContent);
  }
}

/**
 * Einstiegspunkt der klasse
 * 
 * übergeben wird der zu parsende string 
 * 
 * @param string sContent
 * @return bool
 * 
 */
template<typename T>bool Service_XmlParser<T>::parse(std::string sContent) {
  prepareXmlContent(sContent);
  
//   boost::regex regex("<([a-zA-Z\\_\\-]+) *([^>]*)/>|<([a-zA-Z\\_\\-]+) *([^>]*)>(.*?)</\\3>");
  boost::regex regex("<(" + getParentTag() + ") *([^>]*)/>|<(" + getParentTag() + ") *([^>]*)>(.*?)</\\3>");
  boost::smatch matches;                // container für die ergebnisse
  boost::regex_constants::icase;        // nicht casesensitive
  boost::regex_constants::match_all;    // like preg_match_all
  
  std::string::const_iterator start1 = sContent.begin();
  std::string::const_iterator end1   = sContent.end();
  
  while (boost::regex_search(start1, end1, matches, regex)) {
    std::string sEmptyTag(matches[1].first, matches[1].second);
    std::string sEmptyTagAttr(matches[2].first, matches[2].second);
    std::string sOpenTag(matches[3].first, matches[3].second);
    std::string sOpenTagAttr(matches[4].first, matches[4].second);
    std::string sInnerTagContent(matches[5].first, matches[5].second);
    std::string sCloseTag(matches[3].first, matches[3].second);
    
    bool bHashChildrens = checkXmlHasChilds(sInnerTagContent);

    if (0 < sOpenTag.length()) {
      std::cout << "OpenTag : " << sOpenTag << std::endl;
      if (0 < sOpenTagAttr.length()) {
        parseAttributes(sOpenTagAttr);
      }
//       _oMapper(sOpenTag, sInnerTagContent);
    } else if (0 < sEmptyTag.length()) {
      if (0 < sEmptyTagAttr.length()) {
        parseAttributes(sEmptyTagAttr);
      }
//       _oMapper(sEmptyTag, sInnerTagContent);
    }
    if (0 < sInnerTagContent.length()) {
      _oMapper("AvailStatusMessages", sInnerTagContent);
    }
    start1 = matches[0].second;
  }
}

template<typename T>bool Service_XmlParser<T>::prepareXmlContent(std::string sContent) {
  sContent = trim(sContent);
  
  // XML Header entfernen, ist mit der aktuellen logik aber nicht unbedingt notwendig
  if (true == checkHasXmlHeader(sContent)) {
    sContent = removeXmlHeader(sContent);
  }
}

template<typename T>bool Service_XmlParser<T>::parseAttributes(std::string sAttributes) {
  bool bReturn = false;
  boost::regex_constants::icase;        // nicht casesensitive
  boost::regex_constants::match_all;    // like preg_match_all
  boost::regex regex("([A-Za-z_\\-:]+)=['\"](.*?)['\"]");
  boost::smatch matches;                // container für die ergebnisse
  
  std::string::const_iterator start1 = sAttributes.begin();
  std::string::const_iterator end1   = sAttributes.end();
  
  while (boost::regex_search(start1, end1, matches, regex)) {
    std::string sName(matches[1].first, matches[1].second);
    std::string sValues(matches[2].first, matches[2].second);
    sName = formatTagName(sName);
    sValues = formatTagName(sValues);
    if (0 < sName.length()) {
      _oMapper(sName, sValues);
    }
    start1 = matches[0].second;
  }
  return true;
}

template<typename T>std::string Service_XmlParser<T>::formatTagName(std::string sTagName) {
  sTagName = trim(sTagName);
  sTagName = fucase(sTagName);
  return sTagName;
}

template<typename T>bool Service_XmlParser<T>::checkXmlHasChilds(std::string sXml) {
  sXml = trim(sXml);
  
  bool bReturn = false;
  boost::regex_constants::icase;
  boost::regex sRegExXmlHeader("(<.*? />)|(<.*?>.*?</.*?>)");
  boost::match_results<std::string::const_iterator>matches;
  
  std::string::const_iterator start = sXml.begin();
  std::string::const_iterator end   = sXml.end();
  bReturn = boost::regex_match(sXml, matches, sRegExXmlHeader, boost::match_partial | boost::format_perl);
  
  return bReturn;
}

template<typename T>std::string Service_XmlParser<T>::getXmlContent() const {
  return _sXmlContent;
}

template<typename T>bool Service_XmlParser<T>::setXmlContent(std::string sXmlContent) {
  _sXmlContent = sXmlContent;
  return true;
}

template<typename T>std::string Service_XmlParser<T>::getActualXmlContent() const {
  return _sActualXmlContent;
}

template<typename T>bool Service_XmlParser<T>::setActualXmlContent(std::string sActualXmlContent) {
  _sActualXmlContent = sActualXmlContent;
}

template<typename T>std::string Service_XmlParser<T>::getParentTag() const {
  return _sParentTag;
}

template<typename T>bool Service_XmlParser<T>::setParentTag(std::string sParentTag) {
  _sParentTag = sParentTag;
}

template<typename T>bool Service_XmlParser<T>::setMapper(Service_Mapper<T> const oMapper) {
  _oMapper = oMapper;
}

template<typename T>Service_Mapper<T> Service_XmlParser<T>::getMapper() {
  return _oMapper;
}
