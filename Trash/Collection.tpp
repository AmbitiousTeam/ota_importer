#include <iostream>
#include <stdexcept>
// #include "Collection.h"
#include "Entity.h"
#include "Node.h"

template<typename T>unsigned int Model_Collection<T>::add(const T& mData, const int iKey = -1) {
  Model_Node<T>* pNode = new Model_Node<T>;
  pNode->setPrev(_pNode);
  pNode->setNext(0);
  
  if (iKey >= 0) {
    pNode->setKey(iKey);
  } else {
    pNode->setKey(_iCount);
  }
  pNode->setData(mData);
  
  if (_pNode) {
    _pNode->setNext(pNode);
  }
  
  if (0 == _pFirstNode) {
    _pFirstNode = pNode;
  }
  Model_Collection::setLastNode(pNode);
  _pNode = pNode;
  _iCount++;
  return pNode->getIntKey();
}

template<typename T>std::string Model_Collection<T>::add(const T& mData, const std::string sKey = "UNDEFINED") {
  Model_Node<T>* pNode = new Model_Node<T>;
  pNode->setPrev(_pNode);
  pNode->setNext(0);
  
  pNode->setKey(sKey);
  pNode->setData(mData);
  
  if (_pNode) {
    _pNode->setNext(pNode);
  }
  
  if (0 == _pFirstNode) {
    _pFirstNode = pNode;
  }
  Model_Collection::setLastNode(pNode);
  _pNode = pNode;
  _iCount++;
  return pNode->getStringKey();
}

template<typename T>T& Model_Collection<T>::get(const unsigned int iIndex) {
  Model_Node<T>* pNode = Model_Collection::_search(iIndex);
  return pNode->getData();
}

template<typename T>T& Model_Collection<T>::get(const std::string sIndex) {
  Model_Node<T>* pNode = Model_Collection::_search(sIndex);
  if (pNode) {
    return pNode->getData();
  } else {
    T oT = T();
    return oT;
  }
}

template<typename T>Model_Node<T>* Model_Collection<T>::_search(unsigned int iKey) {
  Model_Node<T>* pNode = Model_Collection::getFirstNode();
  while (pNode) {
    unsigned int iTempKey = pNode->getIntKey();
    if (iKey == iTempKey) {
      break;
    }
    pNode = pNode->getNext();
  }
  return pNode;
}

template<typename T>Model_Node<T>* Model_Collection<T>::_search(const std::string sKey) {
  Model_Node<T>* pNode = Model_Collection::getFirstNode();
  while (pNode) {
    std::string sStringKey = pNode->getStringKey();
    if (sKey == sStringKey) {
      break;
    }
    pNode = pNode->getNext();
  }
  return pNode;
}

template<typename T>unsigned int Model_Collection<T>::set(const T& mData, unsigned int iIndex = -1) {
  if (-1 == iIndex) {
    add(mData);
  } else {
    replace(iIndex, mData);
  }
  return true;
}

template<typename T>bool Model_Collection<T>::remove(const unsigned int iIndex) {
  if (0 < iIndex 
    && iIndex <= _iCount
  ) {
    Model_Node<T>* pActualNode = _search(iIndex);
    Model_Node<T>* pPrevNode = pActualNode->getPrev();
    Model_Node<T>* pNextNode = pActualNode->getNext();
    pPrevNode->setNext(pNextNode);
    pNextNode->setPrev(pPrevNode);
    
    delete pActualNode;
  } else {
    return false;
  }
  
  return true;
}

template<typename T>bool Model_Collection<T>::replace(const unsigned int iIndex, T mData) {
  if (0 < iIndex 
    && iIndex <= _iCount
  ) {
    Model_Node<T>* pAcutalNode = _search(iIndex);
    if (pAcutalNode) {
      pAcutalNode->setData(mData);
    }
  } else {
    add(mData, iIndex);
  }
  return true;
}

template<typename T>bool Model_Collection<T>::checkKeyExists(unsigned int iKey) {
  Model_Node<T>* pNode = Model_Collection::getFirstNode();
  while (pNode) {
    if (iKey == pNode->getIntKey()
    ) {
      return true;
    }
    pNode = pNode->getNext();
  }
  return false;
}

template<typename T>bool Model_Collection<T>::checkKeyExists(std::string sKey) {
  Model_Node<T>* pNode = Model_Collection::getFirstNode();
  while (pNode) {
    if (sKey == pNode->getStringKey()) {
      return true;
    }
    pNode = pNode->getNext();
  }
  return false;
}

template<typename T>bool Model_Collection<T>::reset() {
  _pNode = _pFirstNode;
}

template<typename T>bool Model_Collection<T>::deleteNode(Model_Node<T>* pNode) {
  
}

template<typename T>void Model_Collection<T>::setFirstNode(Model_Node<T>* pFirstNode) {
  _pFirstNode = pFirstNode;
}

template<typename T>Model_Node<T>* Model_Collection<T>::getFirstNode() {
  return _pFirstNode;
}

template<typename T>void Model_Collection<T>::setLastNode(Model_Node<T>* pLastNode) {
  _pLastNode = pLastNode;
}

template<typename T>Model_Node<T>* Model_Collection<T>::getLastNode() {
  return _pLastNode;
}

template<typename T>Model_Node<T>* Model_Collection<T>::getActualNode() {
  return _pNode;
}

template<typename T>T& Model_Collection<T>::operator[] (const unsigned int iIndex)
{
  Model_Node<T>* pNode = _search(iIndex);
  
  if (pNode) {
    return pNode->getData();
  }
  
  // index wurde nicht gefunden
  // wenn der index kleiner gleich dem aktuellen maxCount, gültig, also setzen
  if (iIndex <= _iCount) {
    T mData;
    add(mData, iIndex);
    Model_Node<T>* pNode = _search(iIndex);
    return pNode->getData();
  }
  return _pFirstNode->getData();
}

template<typename T>const T& Model_Collection<T>::operator[] (const unsigned int iIndex) const
{
  Model_Node<T>* pNode = _search(iIndex);
  
  if (pNode) {
    return pNode->getData();
  }
  
  // index wurde nicht gefunden
  // wenn der index kleiner gleich dem aktuellen maxCount, gültig, also setzen
  if (iIndex <= _iCount) {
    T mData;
    add(mData, iIndex);
    Model_Node<T>* pNode = _search(iIndex);
    return pNode->getData();
  }
  return _pFirstNode->getData();
}

// ++Object
template<typename T>Model_Node<T>* Model_Collection<T>::operator++ () {
  if (_pNode) {
    _pNode = _pNode->getNext();
  }
  return _pNode;
}

// Object++
template<typename T>const Model_Node<T>* Model_Collection<T>::operator++ (int iIndex) {
  Model_Node<T>* pTmp = _pNode;
  if (_pNode) {
    _pNode = _pNode->getNext();
  }
  return pTmp;
}

template<typename T>Model_Node<T>* Model_Collection<T>::operator-- () {
  if (_pNode) {
    _pNode = _pNode->getPrev();
  }
  return _pNode;
}

template<typename T>const Model_Node<T>* Model_Collection<T>::operator-- (int iIndex) {
  Model_Node<T>* pTmp = _pNode;
  if (_pNode) {
    _pNode = _pNode->getPrev();
  }
  return pTmp;
}

template<typename T>Model_Node<T>& Model_Collection<T>::operator=(const T& mData) {
  std::cout << "Rufe operator= auf!" << std::endl;
  return *this;
}

/**
 * CTOR
 */
template<typename T>Model_Collection<T>::Model_Collection() {
  _iCount = 0;
  _pFirstNode = 0;
  _pLastNode = 0;
  _pNode = 0;
}

/**
 * DTOR
 */
// template<typename T>Model_Collection<T>::~Model_Collection() {
// //   std::cout << "DTOR von Collection!" << std::endl;
//   
// //   Model_Node<T>* pNode = getFirstNode();
// //   Model_Node<T>* pTempNode = pNode;
// //   while (pTempNode) {
// //     pNode = pTempNode;
// //     pTempNode = pNode->getNext();
// //     delete pNode;
// //   }
// //   // nur wenn es auch ein folge node gab, diese auch freigeben!
// //   if (pTempNode) {
// //     delete pTempNode;
// //   }
// }