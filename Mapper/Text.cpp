#include "Text.h"

Mapper_OTA_HotelRatePlanNotifRQ_Text::Mapper_OTA_HotelRatePlanNotifRQ_Text() {
  setModel(this);
  
  Map<Mapper_OTA_HotelRatePlanNotifRQ_Text>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ_Text>();
  oMap->setMapper(this);
  
  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_Text>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_Text>();
  oParser->setMap(*oMap);
  oParser->setRootTag("Text");
  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ_Text::~Mapper_OTA_HotelRatePlanNotifRQ_Text() {

}
