#include "BookingRules.h"

void Mapper_OTA_HotelRatePlanNotifRQ_BookingRules::collectBookingRule(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "Collect BookingRule mit " << sXmlContent << std::endl;};
  Mapper_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule* oBookingRule = new Mapper_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule();
  oBookingRule->setXmlString(sXmlContent);
  oBookingRule->processXml();
  getModel()->add(*oBookingRule->getModel());
}

Mapper_OTA_HotelRatePlanNotifRQ_BookingRules::Mapper_OTA_HotelRatePlanNotifRQ_BookingRules() {
  Model_OTA_HotelRatePlanNotifRQ_BookingRules* oBookingRules = new Model_OTA_HotelRatePlanNotifRQ_BookingRules();
  setModel(*oBookingRules);
  
  Map<Mapper_OTA_HotelRatePlanNotifRQ_BookingRules>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ_BookingRules>();
  oMap->setMapper(this);
  
  oMap->addFunction("BookingRule", &Mapper_OTA_HotelRatePlanNotifRQ_BookingRules::collectBookingRule);
  
  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_BookingRules>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_BookingRules>();
  oParser->setMap(*oMap);
  oParser->setRootTag("BookingRules");
  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ_BookingRules::~Mapper_OTA_HotelRatePlanNotifRQ_BookingRules() {

}
