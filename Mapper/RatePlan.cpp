#include "RatePlan.h"

void Mapper_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::collectRates(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "Rates Content : " << sXmlContent << std::endl;}
  Mapper_OTA_HotelRatePlanNotifRQ_Rates* oRates = new Mapper_OTA_HotelRatePlanNotifRQ_Rates();
  oRates->setXmlString(sXmlContent);
  oRates->processXml();
  getModel()->setRates(*oRates->getModel());
}

void Mapper_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::collectBookingRules(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "BookingRules Content : " << sXmlContent << std::endl;}
    Mapper_OTA_HotelRatePlanNotifRQ_BookingRules* oBookingRules = new Mapper_OTA_HotelRatePlanNotifRQ_BookingRules();
  oBookingRules->setXmlString(sXmlContent);
  oBookingRules->processXml();
  getModel()->setBookingRules(*oBookingRules->getModel());
}

void Mapper_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::collectSellableProducts(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "SellableProducts Content : " << sXmlContent << std::endl;}
    Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts* oSellableProducts = new Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts();
  oSellableProducts->setXmlString(sXmlContent);
  oSellableProducts->processXml();
  getModel()->setSellableProducts(*oSellableProducts->getModel());
}

void Mapper_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::collectSupplements(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "Supplements Content : " << sXmlContent << std::endl;}
    Mapper_OTA_HotelRatePlanNotifRQ_Supplements* oSupplements = new Mapper_OTA_HotelRatePlanNotifRQ_Supplements();
  oSupplements->setXmlString(sXmlContent);
  oSupplements->processXml();
  getModel()->setSupplements(*oSupplements->getModel());
}

void Mapper_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::collectOffers(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "Offers Content : " << sXmlContent << std::endl;}
  Mapper_OTA_HotelRatePlanNotifRQ_Offers* oOffers = new Mapper_OTA_HotelRatePlanNotifRQ_Offers();
  oOffers->setXmlString(sXmlContent);
  oOffers->processXml();
  getModel()->setOffers(*oOffers->getModel());
}

void Mapper_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::setRatePlanCode(std::string sRatePlanCode) {
  getModel()->setRatePlanCode(sRatePlanCode);
}

void Mapper_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::setChargeType(std::string sChargeType) {
  getModel()->setChargeType(Converter::convertMixedToInt(sChargeType));
}

void Mapper_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::setPromotionCode(std::string sPromotionCode) {
  getModel()->setPromotionCode(sPromotionCode);
}

Mapper_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::Mapper_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan() {
  Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan* oRatePlan = new Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan();
  setModel(*oRatePlan);
  
  Map<Mapper_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan>();
  oMap->setMapper(this);
  oMap->addFunction("PromotionCode", &Mapper_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::setPromotionCode);
  oMap->addFunction("ChargeType", &Mapper_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::setChargeType);
  oMap->addFunction("RatePlanCode", &Mapper_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::setRatePlanCode);
  oMap->addFunction("Rates", &Mapper_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::collectRates);
  oMap->addFunction("SellableProducts", &Mapper_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::collectSellableProducts);
  oMap->addFunction("Offers", &Mapper_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::collectOffers);
  oMap->addFunction("Supplements", &Mapper_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::collectSupplements);
  oMap->addFunction("BookingRules", &Mapper_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::collectBookingRules);
  
  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan>();
  oParser->setMap(*oMap);
  oParser->setRootTag("RatePlan");
  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan::~Mapper_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan() {
  
}
