#include "BookingRule.h"
#include "CancelPenalty.h"
#include "LengthsOfStay.h"
#include "DOW_Restrictions.h"

void Mapper_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule::setStart(std::string sStart) {
  getModel()->setStart(sStart);
}

std::string Mapper_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule::getStart() {
  return getModel()->getStart();
}

void Mapper_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule::setEnd(std::string sEnd) {
  getModel()->setEnd(sEnd);
}

std::string Mapper_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule::getEnd() {
  return getModel()->getEnd();
}

void Mapper_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule::processDOWRestrictions(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "Verarbeite DOW_Restrictions mit " << sXmlContent << std::endl;};
  Mapper_OTA_HotelRatePlanNotifRQ_DOW_Restrictions* oDOWRestrictions = new Mapper_OTA_HotelRatePlanNotifRQ_DOW_Restrictions();
  oDOWRestrictions->setXmlString(sXmlContent);
  oDOWRestrictions->processXml();
  getModel()->setDOWRestrictions(*oDOWRestrictions->getModel());
}

void Mapper_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule::processCancelPenalty(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "Verarbeite CancelPenalty mit " << sXmlContent << std::endl;};
  Mapper_OTA_HotelRatePlanNotifRQ_CancelPenalty* oCancelPenalty = new Mapper_OTA_HotelRatePlanNotifRQ_CancelPenalty();
  oCancelPenalty->setXmlString(sXmlContent);
  oCancelPenalty->processXml();
  getModel()->setCancelPenalty(*oCancelPenalty->getModel());
}

void Mapper_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule::processLengthsOfStay(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "Verarbeite LenghtsOfStay mit " << sXmlContent << std::endl;};
  Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay* oLenghtsOfStay = new Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay();
  oLenghtsOfStay->setXmlString(sXmlContent);
  oLenghtsOfStay->processXml();
  getModel()->setLengthsOfStay(*oLenghtsOfStay->getModel());
}

Mapper_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule::Mapper_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule() {
  Model_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule* oBookingRule = new Model_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule();
  setModel(*oBookingRule);
  
  Map<Mapper_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule>();
  oMap->setMapper(this);
  
  oMap->addFunction("CancelPenalty", &Mapper_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule::processCancelPenalty);
  oMap->addFunction("LengthsOfStay", &Mapper_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule::processLengthsOfStay);
  oMap->addFunction("DOW_Restrictions", &Mapper_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule::processDOWRestrictions);
  oMap->addFunction("Start", &Mapper_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule::setStart);
  oMap->addFunction("End", &Mapper_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule::setEnd);
  
  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule>;
  oParser->setMap(*oMap);
  oParser->setRootTag("BookingRule");
  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule::~Mapper_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule() {

}
