#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_AVAILSTATUSMESSAGES_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_AVAILSTATUSMESSAGES_H__

#include "Mapper.h"
#include "Map.h"
#include "AvailStatusMessage.h"

#include "../Models/AvailStatusMessages.h"
#include "../Models/AvailStatusMessage.h"

class Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages :
  public Mapper<Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages, Model_OTA_HotelAvailNotifRQ_AvailStatusMessages> {
public:
  
  /** verarbeitet den übergebenen XML Content für AvailStatusMessage **/
  void collectAvailStatusMessage(std::string);
  
  void setHotelCode(std::string);
  std::string getHotelCode();
  
  void setIdCon(std::string);
  std::string getIdCon();
  
  void setDelta(std::string);
  bool getDelta();
  
  Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages();
  ~Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages();
protected:
private:
};

#endif
