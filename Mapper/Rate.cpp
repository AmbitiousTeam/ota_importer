#include "Rate.h"

void Mapper_OTA_HotelRatePlanNotifRQ_Rates_Rate::setStart(std::string sStart) {
  getModel()->setStart(sStart);
}

std::string Mapper_OTA_HotelRatePlanNotifRQ_Rates_Rate::getStart() {
  return getModel()->getStart();
}

void Mapper_OTA_HotelRatePlanNotifRQ_Rates_Rate::setEnd(std::string sEnd) {
  getModel()->setEnd(sEnd);
}

std::string Mapper_OTA_HotelRatePlanNotifRQ_Rates_Rate::getEnd() {
  return getModel()->getEnd();
}

void Mapper_OTA_HotelRatePlanNotifRQ_Rates_Rate::setNoOffersAllowed(std::string sNoOffersAllowed) {
  getModel()->setNoOffersAllowed(Converter::convertMixedToBoolean(sNoOffersAllowed));
}

bool Mapper_OTA_HotelRatePlanNotifRQ_Rates_Rate::getNoOffersAllowed() {
  return getModel()->getNoOffersAllowed();
}

void Mapper_OTA_HotelRatePlanNotifRQ_Rates_Rate::processAdditionalGuestAmounts(std::string sXmlContent) {
  Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts* oRatePlans = new Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts();
  oRatePlans->setXmlString(sXmlContent);
  oRatePlans->processXml();
  getModel()->setAdditionalGuestAmounts(*oRatePlans->getModel());
}

void Mapper_OTA_HotelRatePlanNotifRQ_Rates_Rate::processBaseByGuestAmts(std::string sXmlContent) {
  Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts* oBaseByGuestAmts = new Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts();
  oBaseByGuestAmts->setXmlString(sXmlContent);
  oBaseByGuestAmts->processXml();
  getModel()->setBaseByGuestAmts(*oBaseByGuestAmts->getModel());
}

Mapper_OTA_HotelRatePlanNotifRQ_Rates_Rate::Mapper_OTA_HotelRatePlanNotifRQ_Rates_Rate() {
  Model_OTA_HotelRatePlanNotifRQ_Rates_Rate* oRate = new Model_OTA_HotelRatePlanNotifRQ_Rates_Rate();
  setModel(*oRate);
  
  Map<Mapper_OTA_HotelRatePlanNotifRQ_Rates_Rate>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ_Rates_Rate>();
  oMap->setMapper(this);
  oMap->addFunction("Start", &Mapper_OTA_HotelRatePlanNotifRQ_Rates_Rate::setStart);
  oMap->addFunction("End", &Mapper_OTA_HotelRatePlanNotifRQ_Rates_Rate::setEnd);
  oMap->addFunction("NoOffersAllowed", &Mapper_OTA_HotelRatePlanNotifRQ_Rates_Rate::setNoOffersAllowed);
  oMap->addFunction("BaseByGuestAmts", &Mapper_OTA_HotelRatePlanNotifRQ_Rates_Rate::processBaseByGuestAmts);
  oMap->addFunction("AdditionalGuestAmounts", &Mapper_OTA_HotelRatePlanNotifRQ_Rates_Rate::processAdditionalGuestAmounts);
  
  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_Rates_Rate>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_Rates_Rate>();
  oParser->setMap(*oMap);
  oParser->setRootTag("Rate");
  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ_Rates_Rate::~Mapper_OTA_HotelRatePlanNotifRQ_Rates_Rate() {

}
