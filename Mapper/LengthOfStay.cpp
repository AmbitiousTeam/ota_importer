#include "LengthOfStay.h"

void Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay::setTime(std::string sTime) {
  getModel()->setTime(sTime);
}

std::string Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay::getTime() {
  return getModel()->getTime();
}

void Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay::setTimeUnit(std::string sTimeUnit) {
  getModel()->setTimeUnit(sTimeUnit);
}

std::string Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay::getTimeUnit() {
  return getModel()->getTimeUnit();
}

void Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay::setMinMaxMessageType(std::string sMinMaxMessageType) {
  getModel()->setMinMaxMessageType(sMinMaxMessageType);
}

std::string Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay::getMinMaxMessageType() {
  return getModel()->getMinMaxMessageType();
}

Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay::Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay() {
  Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay* oLengthOfStay = new Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay();
  setModel(*oLengthOfStay);
  
  Map<Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay>();
  oMap->setMapper(this);
  oMap->addFunction("Time", &Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay::setTime);
  oMap->addFunction("TimeUnit", &Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay::setTimeUnit);
  oMap->addFunction("MinMaxMessageType", &Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay::setMinMaxMessageType);
  
  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay>();
  oParser->setEliminateRedundantRootTags(true);
  oParser->setMap(*oMap);
  oParser->setRootTag("LengthOfStay");
  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay::~Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay() {

}
