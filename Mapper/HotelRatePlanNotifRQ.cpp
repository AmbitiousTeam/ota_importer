#include "HotelRatePlanNotifRQ.h"

void Mapper_OTA_HotelRatePlanNotifRQ::collectRatePlans(std::string sXmlContent) {
  Mapper_OTA_HotelRatePlanNotifRQ_RatePlans* oRatePlans = new Mapper_OTA_HotelRatePlanNotifRQ_RatePlans();
  oRatePlans->setXmlString(sXmlContent);
  oRatePlans->processXml();
  getModel()->setRatePlans(*oRatePlans->getModel());
}

void Mapper_OTA_HotelRatePlanNotifRQ::collectOffers(std::string sXmlContent) {
  Mapper_OTA_HotelRatePlanNotifRQ_Offers* oOffers = new Mapper_OTA_HotelRatePlanNotifRQ_Offers();
  oOffers->setXmlString(sXmlContent);
  oOffers->processXml();
  getModel()->setOffers(*oOffers->getModel());
}

// void Service_OTA_HotelRatePlanNotifRQ::collectBookingRules(std::string sXmlContent) {
//   Service_OTA_HotelRatePlanNotifRQ_BookingRules oBookingRules;
//   oBookingRules.setXmlString(sXmlContent);
//   oBookingRules.processXml();
//   getModel()->setBookingRules(*oBookingRules.getModel());
// }

void Mapper_OTA_HotelRatePlanNotifRQ::collectTPAExtensions(std::string sXmlContent) {
  Mapper_OTA_HotelRatePlanNotifRQ_TPAExtensions* oTPAExtensions = new Mapper_OTA_HotelRatePlanNotifRQ_TPAExtensions();
  oTPAExtensions->setXmlString(sXmlContent);
  oTPAExtensions->processXml();
  getModel()->setTPAExtensions(*oTPAExtensions->getModel());
}

void Mapper_OTA_HotelRatePlanNotifRQ::setXmlNsXsd(std::string sXmlNsXsd) {
  getModel()->setXmlNsXsd(sXmlNsXsd);
}

std::string Mapper_OTA_HotelRatePlanNotifRQ::getXmlNsXsd() {
  return getModel()->getXmlNsXsd();
}

void Mapper_OTA_HotelRatePlanNotifRQ::setXmlNsXsi(std::string sXmlNsXsi) {
  getModel()->setXmlNsXsi(sXmlNsXsi);
}

std::string Mapper_OTA_HotelRatePlanNotifRQ::getXmlNsXsi() {
  return getModel()->getXmlNsXsi();
}

void Mapper_OTA_HotelRatePlanNotifRQ::setXmlNs(std::string sXmlNs) {
  getModel()->setXmlNs(sXmlNs);
}

std::string Mapper_OTA_HotelRatePlanNotifRQ::getXmlNs() {
  return getModel()->getXmlNs();
}

void Mapper_OTA_HotelRatePlanNotifRQ::setXsiSchemaLocation(std::string sXsiSchemaLocation) {
  getModel()->setXsiSchemaLocation(sXsiSchemaLocation);
}

std::string Mapper_OTA_HotelRatePlanNotifRQ::getXsiSchemaLocation() {
  return getModel()->getXsiSchemaLocation();
}


void Mapper_OTA_HotelRatePlanNotifRQ::setVersion(std::string sVersion) {
  getModel()->setVersion(sVersion);
}

std::string Mapper_OTA_HotelRatePlanNotifRQ::getVersion() {
  return getModel()->getVersion();
}

void Mapper_OTA_HotelRatePlanNotifRQ::setCorrelationId(std::string sCorrelationId) {
  getModel()->setCorrelationId(sCorrelationId);
}

void Mapper_OTA_HotelRatePlanNotifRQ::setEchoToken(std::string sEchoToken) {
  getModel()->setEchoToken(sEchoToken);
}

/** CTOR **/
Mapper_OTA_HotelRatePlanNotifRQ::Mapper_OTA_HotelRatePlanNotifRQ() {
  Model_OTA_HotelRatePlanNotifRQ* oHotelRatePlanNotifRQ = new Model_OTA_HotelRatePlanNotifRQ();
  setModel(*oHotelRatePlanNotifRQ);
  
  Map<Mapper_OTA_HotelRatePlanNotifRQ>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ>();
  oMap->setMapper(this);
  oMap->addFunction("Xmlns:xsd", &Mapper_OTA_HotelRatePlanNotifRQ::setXmlNsXsd);
  oMap->addFunction("Xmlns:xsi", &Mapper_OTA_HotelRatePlanNotifRQ::setXmlNsXsi);
  oMap->addFunction("Xmlns", &Mapper_OTA_HotelRatePlanNotifRQ::setXmlNs);
  oMap->addFunction("Xsi:schemaLocation", &Mapper_OTA_HotelRatePlanNotifRQ::setXsiSchemaLocation);
  oMap->addFunction("Version", &Mapper_OTA_HotelRatePlanNotifRQ::setVersion);
  oMap->addFunction("EchoToken", &Mapper_OTA_HotelRatePlanNotifRQ::setEchoToken);
  oMap->addFunction("CorrelationID", &Mapper_OTA_HotelRatePlanNotifRQ::setCorrelationId);
  oMap->addFunction("RatePlans", &Mapper_OTA_HotelRatePlanNotifRQ::collectRatePlans);
  oMap->addFunction("Offers", &Mapper_OTA_HotelRatePlanNotifRQ::collectOffers);
//   oMap->addFunction("TPA_Extensions", &Mapper_OTA_HotelRatePlanNotifRQ::collectTPAExtensions);
  
  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ>();
  oParser->setMap(*oMap);
  oParser->setRootTag("OTA_HotelRatePlanNotifRQ");
  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ::~Mapper_OTA_HotelRatePlanNotifRQ() {
}
