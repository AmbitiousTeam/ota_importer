#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_SUPPLEMENTS_SUPPLEMENT_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_SUPPLEMENTS_SUPPLEMENT_H__

#include "Mapper.h"
#include "../Models/Supplement.h"

class Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement : 
  public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement, Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement> {
public:
  
  void setAgeQualifyingCode(std::string);
  void setMinAge(std::string);
  void setMaxAge(std::string);
  void setAmount(std::string);
  void setCurrencyCode(std::string);
  void setStart(std::string);
  void setEnd(std::string);
  void setSupplementType(std::string);
  void setChargeType(std::string);
  void setAdditionalGuestNumber(std::string);
  void setAddToBasicRate(std::string);
  void setPercent(std::string);
  void setInvCode(std::string);
  void setInvType(std::string);
  void setMandatoryIndicator(std::string);
  
  Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement();
  ~Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement();
protected:
private:
};
#endif
