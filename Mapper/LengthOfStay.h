#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_LENGTHSOFSTAY_LENGTHOFSTAY_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_LENGTHSOFSTAY_LENGTHOFSTAY_H__

#include <string>
#include "Mapper.h"
#include "../Models/LengthOfStay.h"

class Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay :
  public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay, Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay> {
public:
  void setTime(std::string);
  std::string getTime();
  
  void setTimeUnit(std::string);
  std::string getTimeUnit();
  
  void setMinMaxMessageType(std::string);
  std::string getMinMaxMessageType();
  
  Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay();
  ~Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay();
protected:
private:
};

#endif
