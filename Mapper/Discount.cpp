#include "Discount.h"

void Mapper_OTA_HotelRatePlanNotifRQ_Discount::setAmount(std::string sAmount) {
  getModel()->setAmount(Converter::convertMixedToDouble(sAmount));
}

float Mapper_OTA_HotelRatePlanNotifRQ_Discount::getAmount() {
  return getModel()->getAmount();
}

void Mapper_OTA_HotelRatePlanNotifRQ_Discount::setPercent(std::string sPercent) {
  getModel()->setPercent(Converter::convertMixedToDouble(sPercent));
}

float Mapper_OTA_HotelRatePlanNotifRQ_Discount::getPercent() {
  return getModel()->getPercent();
}

void Mapper_OTA_HotelRatePlanNotifRQ_Discount::setNightsDiscounted(std::string sNightsDiscounted) {
  getModel()->setNightsDiscounted(Converter::convertMixedToInt(sNightsDiscounted));
}

int Mapper_OTA_HotelRatePlanNotifRQ_Discount::getNightsDiscounted() {
  return getModel()->getNightsDiscounted();
}

void Mapper_OTA_HotelRatePlanNotifRQ_Discount::setNightsRequired(std::string sNightsRequired) {
  getModel()->setNightsRequired(Converter::convertMixedToInt(sNightsRequired));
}

int Mapper_OTA_HotelRatePlanNotifRQ_Discount::getNightsRequired() {
  return getModel()->getNightsRequired();
}

void Mapper_OTA_HotelRatePlanNotifRQ_Discount::setChargeUnitCode(std::string sChargeUnitCode) {
  getModel()->setChargeUnitCode(Converter::convertMixedToInt(sChargeUnitCode));
}

void Mapper_OTA_HotelRatePlanNotifRQ_Discount::setCurrencyCode(std::string sCurrencyCode) {
  getModel()->setCurrencyCode(sCurrencyCode);
}

void Mapper_OTA_HotelRatePlanNotifRQ_Discount::setDiscountLastBookingDays(std::string sDiscountLastBookingDays) {
  getModel()->setDiscountLastBookingDays(Converter::convertMixedToBoolean(sDiscountLastBookingDays));
}

Mapper_OTA_HotelRatePlanNotifRQ_Discount::Mapper_OTA_HotelRatePlanNotifRQ_Discount() {
  Model_OTA_HotelRatePlanNotifRQ_Discount* oDiscount = new Model_OTA_HotelRatePlanNotifRQ_Discount();
  setModel(*oDiscount);
  
  Map<Mapper_OTA_HotelRatePlanNotifRQ_Discount>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ_Discount>();
  oMap->setMapper(this);
  oMap->addFunction("Amount", &Mapper_OTA_HotelRatePlanNotifRQ_Discount::setAmount);
  oMap->addFunction("Percent", &Mapper_OTA_HotelRatePlanNotifRQ_Discount::setPercent);
  oMap->addFunction("CurrencyCode", &Mapper_OTA_HotelRatePlanNotifRQ_Discount::setCurrencyCode);
  oMap->addFunction("NightsDiscounted", &Mapper_OTA_HotelRatePlanNotifRQ_Discount::setNightsDiscounted);
  oMap->addFunction("NightsRequired", &Mapper_OTA_HotelRatePlanNotifRQ_Discount::setNightsRequired);
  oMap->addFunction("ChargeUnitCode", &Mapper_OTA_HotelRatePlanNotifRQ_Discount::setChargeUnitCode);
  oMap->addFunction("DiscountLastBookingDays", &Mapper_OTA_HotelRatePlanNotifRQ_Discount::setDiscountLastBookingDays);
  
  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_Discount>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_Discount>();
  oParser->setMap(*oMap);
  oParser->setRootTag("Discount");
  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ_Discount::~Mapper_OTA_HotelRatePlanNotifRQ_Discount() {

}
