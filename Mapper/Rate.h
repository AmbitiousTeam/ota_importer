#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_RATES_RATE_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_RATES_RATE_H__

#include <string>
#include "Mapper.h"
#include "AdditionalGuestAmounts.h"
#include "BaseByGuestAmts.h"

#include "../Models/Rate.h"

class Mapper_OTA_HotelRatePlanNotifRQ_Rates_Rate : public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_Rates_Rate, Model_OTA_HotelRatePlanNotifRQ_Rates_Rate> {
public:
  void setStart(std::string);
  std::string getStart();
  
  void setEnd(std::string);
  std::string getEnd();
  
  void setNoOffersAllowed(std::string);
  bool getNoOffersAllowed();
  
  void processBaseByGuestAmts(std::string);
  void processAdditionalGuestAmounts(std::string);
  
  Mapper_OTA_HotelRatePlanNotifRQ_Rates_Rate();
  ~Mapper_OTA_HotelRatePlanNotifRQ_Rates_Rate();
protected:
private:
};

#endif
