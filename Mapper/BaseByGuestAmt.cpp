#include "BaseByGuestAmt.h"

void Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt::setAmountAfterTax(std::string sAmountAfterTax) {
  getModel()->setAmountAfterTax(Converter::convertMixedToDouble(sAmountAfterTax));
}

double Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt::getAmountAfterTax() {
  return getModel()->getAmountAfterTax();
}

void Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt::setCurrencyCode(std::string sCurrencyCode) {
  getModel()->setCurrencyCode(sCurrencyCode);
}

std::string Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt::getCurrencyCode() {
  return getModel()->getCurrencyCode();
}

void Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt::setNumberOfGuests(std::string sNumberOfGuests) {
  getModel()->setNumberOfGuests(Converter::convertMixedToInt(sNumberOfGuests));
}

int Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt::getNumberOfGuests() {
  return getModel()->getNumberOfGuests();
}

void Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt::setMinAge(std::string sMinAge) {
  getModel()->setMinAge(Converter::convertMixedToInt(sMinAge));
}

void Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt::setMaxAge(std::string sMaxAge) {
  getModel()->setMaxAge(Converter::convertMixedToInt(sMaxAge));
}

Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt::Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt() {
  Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt* oBaseByGuestAmt = new Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt();
  setModel(*oBaseByGuestAmt);
  
  Map<Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt>();
  oMap->setMapper(this);
  oMap->addFunction("MinAge", &Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt::setMinAge);
  oMap->addFunction("MaxAge", &Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt::setMaxAge);
  oMap->addFunction("AmountAfterTax", &Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt::setAmountAfterTax);
  oMap->addFunction("CurencyCode", &Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt::setCurrencyCode);
  oMap->addFunction("CurrencyCode", &Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt::setCurrencyCode);
  oMap->addFunction("NumberOfGuests", &Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt::setNumberOfGuests);
  
  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt>();
  oParser->setMap(*oMap);
  oParser->setRootTag("BaseByGuestAmt");
  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt::~Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt() {

}
