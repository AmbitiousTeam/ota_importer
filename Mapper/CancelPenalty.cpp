#include "CancelPenalty.h"

void Mapper_OTA_HotelRatePlanNotifRQ_CancelPenalty::processDeadline(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "Verarbeite Deadline mit " << sXmlContent << std::endl;};
  Mapper_OTA_HotelRatePlanNotifRQ_Deadline* oDeadline = new Mapper_OTA_HotelRatePlanNotifRQ_Deadline();
  oDeadline->setXmlString(sXmlContent);
  oDeadline->processXml();
  getModel()->setDeadline(*oDeadline->getModel());
}

void Mapper_OTA_HotelRatePlanNotifRQ_CancelPenalty::processAmountPercent(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "Verarbeite AmountPercent mit " << sXmlContent << std::endl;};
  Mapper_OTA_HotelRatePlanNotifRQ_AmountPercent* oAmountPercent = new Mapper_OTA_HotelRatePlanNotifRQ_AmountPercent();
  oAmountPercent->setXmlString(sXmlContent);
  oAmountPercent->processXml();
  getModel()->setAmountPercent(*oAmountPercent->getModel());
}

void Mapper_OTA_HotelRatePlanNotifRQ_CancelPenalty::setStart(std::string sStart) {
  getModel()->setStart(sStart);
}

std::string Mapper_OTA_HotelRatePlanNotifRQ_CancelPenalty::getStart() {
  return getModel()->getStart();
}

void Mapper_OTA_HotelRatePlanNotifRQ_CancelPenalty::setEnd(std::string sEnd) {
  getModel()->setEnd(sEnd);
}

std::string Mapper_OTA_HotelRatePlanNotifRQ_CancelPenalty::getEnd() {
  return getModel()->getEnd();
}

Mapper_OTA_HotelRatePlanNotifRQ_CancelPenalty::Mapper_OTA_HotelRatePlanNotifRQ_CancelPenalty() {
  Model_OTA_HotelRatePlanNotifRQ_CancelPenalty* oCancelPenalty = new Model_OTA_HotelRatePlanNotifRQ_CancelPenalty();
  setModel(*oCancelPenalty);
  
  Map<Mapper_OTA_HotelRatePlanNotifRQ_CancelPenalty>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ_CancelPenalty>();
  oMap->setMapper(this);
  
  oMap->addFunction("Deadline", &Mapper_OTA_HotelRatePlanNotifRQ_CancelPenalty::processDeadline);
  oMap->addFunction("AmountPercent", &Mapper_OTA_HotelRatePlanNotifRQ_CancelPenalty::processAmountPercent);
  oMap->addFunction("Start", &Mapper_OTA_HotelRatePlanNotifRQ_CancelPenalty::setStart);
  oMap->addFunction("End", &Mapper_OTA_HotelRatePlanNotifRQ_CancelPenalty::setEnd);
  
  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_CancelPenalty>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_CancelPenalty>();
  oParser->setMap(*oMap);
  oParser->setRootTag("CancelPenalty");
  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ_CancelPenalty::~Mapper_OTA_HotelRatePlanNotifRQ_CancelPenalty() {
}
