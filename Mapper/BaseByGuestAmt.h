#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_BASEBYGUESTAMTS_BASEBYGUESTAMT_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_BASEBYGUESTAMTS_BASEBYGUESTAMT_H__

#include "Mapper.h"
#include "../Models/BaseByGuestAmt.h"

class Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt :
  public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt, Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt> {
public:
  
  void setAmountAfterTax(std::string);
  double getAmountAfterTax(void);
  
  void setCurrencyCode(std::string);
  std::string getCurrencyCode(void);
  
  void setNumberOfGuests(std::string);
  int getNumberOfGuests(void);
  
  void setMinAge(std::string);
  
  void setMaxAge(std::string);
  
  Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt();
  ~Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt();
protected:
private:
};

#endif
