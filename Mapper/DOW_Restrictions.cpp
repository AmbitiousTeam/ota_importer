#include "DOW_Restrictions.h"

void Mapper_OTA_HotelRatePlanNotifRQ_DOW_Restrictions::processArrivalDaysOfWeek(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "Verarbeite ArrivalDaysOfWeek mit " << sXmlContent << std::endl;};
  Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek* oArrivalDaysOfWeek = new Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek();
  oArrivalDaysOfWeek->setXmlString(sXmlContent);
  oArrivalDaysOfWeek->processXml();
  getModel()->setArrivalDaysOfWeek(*oArrivalDaysOfWeek->getModel());
}

void Mapper_OTA_HotelRatePlanNotifRQ_DOW_Restrictions::processDepartureDaysOfWeek(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "Verarbeite DepartureDaysOfWeek mit " << sXmlContent << std::endl;};
  Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek* oDepartureDaysOfWeek = new Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek();
  oDepartureDaysOfWeek->setXmlString(sXmlContent);
  oDepartureDaysOfWeek->processXml();
  getModel()->setDepartureDaysOfWeek(*oDepartureDaysOfWeek->getModel());
}

Mapper_OTA_HotelRatePlanNotifRQ_DOW_Restrictions::Mapper_OTA_HotelRatePlanNotifRQ_DOW_Restrictions() {
  Model_OTA_HotelRatePlanNotifRQ_DOW_Restrictions* oDOW_Restrictions = new Model_OTA_HotelRatePlanNotifRQ_DOW_Restrictions();
  setModel(*oDOW_Restrictions);
  
  Map<Mapper_OTA_HotelRatePlanNotifRQ_DOW_Restrictions>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ_DOW_Restrictions>();
  oMap->setMapper(this);
  oMap->addFunction("ArrivalDaysOfWeek", &Mapper_OTA_HotelRatePlanNotifRQ_DOW_Restrictions::processArrivalDaysOfWeek);
  oMap->addFunction("DepartureDaysOfWeek", &Mapper_OTA_HotelRatePlanNotifRQ_DOW_Restrictions::processDepartureDaysOfWeek);
  
  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_DOW_Restrictions>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_DOW_Restrictions>();
  oParser->setMap(*oMap);
  oParser->setRootTag("DOW_Restrictions");
  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ_DOW_Restrictions::~Mapper_OTA_HotelRatePlanNotifRQ_DOW_Restrictions() {

}
