#include "SellableProducts.h"

void Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts::collectSellableProducts(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "Verarbeite SellableProduct mit " << sXmlContent << std::endl;};
  Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct* oSellableProduct = new Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct();
  oSellableProduct->setXmlString(sXmlContent);
  oSellableProduct->processXml();
  getModel()->add(*oSellableProduct->getModel());
}

Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts::Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts() {
  Model_OTA_HotelRatePlanNotifRQ_SellableProducts* oAdditionalGuestAmount = new Model_OTA_HotelRatePlanNotifRQ_SellableProducts();
  setModel(*oAdditionalGuestAmount);
  
  Map<Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts>();
  oMap->setMapper(this);
  oMap->addFunction("SellableProduct", &Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts::collectSellableProducts);
  
  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts>();
  oParser->setMap(*oMap);
  oParser->setRootTag("SellableProducts");
  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts::~Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts() {

}
