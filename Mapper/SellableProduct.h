#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_SELLABLEPRODUCTS_SELLABLEPRODUCT_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_SELLABLEPRODUCTS_SELLABLEPRODUCT_H__

#include "Mapper.h"
#include "GuestRoom.h"
#include "../Models/SellableProduct.h"

class Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct : public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct, Model_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct> {
public:
  void processGuestRoom(std::string);
  void processDescription(std::string);
  void processCategory(std::string);
  
  void setInvType(std::string);
  std::string getInvType();
  
  void setInvCode(std::string);
  std::string getInvCode();
  
  void setStart(std::string);
  void setEnd(std::string);
  
  Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct();
  ~Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct();
protected:
private:
};

#endif
