#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_RATEPLANS_RATEPLAN_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_RATEPLANS_RATEPLAN_H__

#include <string>
#include "Mapper.h"
#include "Rates.h"
#include "Offers.h"
#include "Supplements.h"
#include "BookingRules.h"
#include "SellableProducts.h"

#include "../Tools/Converter.h"
#include "../Models/RatePlan.h"

class Mapper_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan : public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan, Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan> {
public:
  void collectSellableProducts(std::string);
  void collectBookingRules(std::string);
  void collectRates(std::string);
  void collectSupplements(std::string);
  void collectOffers(std::string);
  
  void setRatePlanCode(std::string);
  void setPromotionCode(std::string);
  void setChargeType(std::string);
  
  Mapper_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan();
  ~Mapper_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan();
  
protected:
private:
};

#endif
