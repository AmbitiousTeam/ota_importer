#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_CANCELPENALTY_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_CANCELPENALTY_H__

#include "Mapper.h"
#include "Deadline.h"
#include "AmountPercent.h"

#include "../Models/CancelPenalty.h"
#include "../Models/Deadline.h"
#include "../Models/AmountPercent.h"

class Mapper_OTA_HotelRatePlanNotifRQ_CancelPenalty : public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_CancelPenalty, Model_OTA_HotelRatePlanNotifRQ_CancelPenalty> {
public:
  void processDeadline(std::string);
  void processAmountPercent(std::string);
  
  void setStart(std::string);
  std::string getStart();
  
  void setEnd(std::string);
  std::string getEnd();
  
  Mapper_OTA_HotelRatePlanNotifRQ_CancelPenalty();
  ~Mapper_OTA_HotelRatePlanNotifRQ_CancelPenalty();
protected:
private:
};

#endif
