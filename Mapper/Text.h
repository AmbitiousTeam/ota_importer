#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_TEXT_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_TEXT_H__

#include "Mapper.h"
#include "../Models/Text.h"

class Mapper_OTA_HotelRatePlanNotifRQ_Text : public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_Text, Model_OTA_HotelRatePlanNotifRQ_Text> {
public:
  
  Mapper_OTA_HotelRatePlanNotifRQ_Text();
  ~Mapper_OTA_HotelRatePlanNotifRQ_Text();
protected:
private:
};

#endif
