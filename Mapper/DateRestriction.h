#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_DATERESTRICTIONS_DATERESTRICTIONS_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_DATERESTRICTIONS_DATERESTRICTIONS_H__

#include <string>
#include "Mapper.h"
#include "../Models/DateRestriction.h"

class Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction : public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction, Model_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction> {
public:
  void setStart(std::string);
  std::string getStart();
  
  void setEnd(std::string);
  std::string getEnd();
  
  void setRestrictionType(std::string);
  std::string getRestrictionType();
  
  Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction();
  ~Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction();
protected:
private:
};

#endif
