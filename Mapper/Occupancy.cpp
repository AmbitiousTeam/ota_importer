#include "Occupancy.h"

void Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::setMinAge(std::string sMinAge) {
  getModel()->setMinAge(Converter::convertMixedToInt(sMinAge));
}

int Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::getMinAge(void) {
  return getModel()->getMinAge();
}

void Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::setMaxAge(std::string sMaxAge) {
  getModel()->setMaxAge(Converter::convertMixedToInt(sMaxAge));
}

int Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::getMaxAge(void) {
  return getModel()->getMaxAge();
}

void Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::setAgeQualifyingCode(std::string sAgeQualifyingCode) {
  getModel()->setAgeQualifyingCode(Converter::convertMixedToInt(sAgeQualifyingCode));
}

int Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::getAgeQualifyingCode() {
  return getModel()->getAgeQualifyingCode();
}

void Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::setInfantsAreCounted(std::string sInfantsAreCounted) {
  getModel()->setInfantsAreCounted(Converter::convertMixedToBoolean(sInfantsAreCounted));
}

bool Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::getInfantsAreCounted() {
  return getModel()->getInfantsAreCounted();
}

void Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::setMinOccupancy(std::string sMinOccupancy) {
  getModel()->setMinOccupancy(Converter::convertMixedToInt(sMinOccupancy));
}

int Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::getMinOccupancy() {
  return getModel()->getMinOccupancy();
}

void Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::setMaxOccupancy(std::string sMaxOccupancy) {
  getModel()->setMaxOccupancy(Converter::convertMixedToInt(sMaxOccupancy));
}

int Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::getMaxOccupancy() {
  return getModel()->getMaxOccupancy();
}

Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy() {
  Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy* oOccupancy = new Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy();
  setModel(*oOccupancy);
  
  Map<Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy>();
  oMap->setMapper(this);
  oMap->addFunction("MinOccupancy", &Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::setMinOccupancy);
  oMap->addFunction("MaxOccupancy", &Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::setMaxOccupancy);
  oMap->addFunction("MinAge", &Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::setMinAge);
  oMap->addFunction("MaxAge", &Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::setMaxAge);
  oMap->addFunction("InfantsAreCounted", &Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::setInfantsAreCounted);
  oMap->addFunction("AgeQualifyingCode", &Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::setAgeQualifyingCode);
  
  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy>();
  oParser->setMap(*oMap);
  oParser->setRootTag("Occupancy");
  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::~Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy() {

}
