#include "Inventory.h"

void Mapper_OTA_HotelRatePlanNotifRQ_Inventories_Inventory::setInvCode(std::string sInvCode) {
  getModel()->setInvCode(sInvCode);
}

std::string Mapper_OTA_HotelRatePlanNotifRQ_Inventories_Inventory::getInvCode() {
  return getModel()->getInvCode();
}

void Mapper_OTA_HotelRatePlanNotifRQ_Inventories_Inventory::setAppliesToIndicator(std::string sAppliesToIndicator) {
  getModel()->setAppliesToIndicator(Converter::convertMixedToBoolean(sAppliesToIndicator));
}

bool Mapper_OTA_HotelRatePlanNotifRQ_Inventories_Inventory::getAppliesToIndicator() {
  return getModel()->getAppliesToIndicator();
}

void Mapper_OTA_HotelRatePlanNotifRQ_Inventories_Inventory::setInvType(std::string sInvType) {
  getModel()->setInvType(sInvType);
}

Mapper_OTA_HotelRatePlanNotifRQ_Inventories_Inventory::Mapper_OTA_HotelRatePlanNotifRQ_Inventories_Inventory() {
  Model_OTA_HotelRatePlanNotifRQ_Inventories_Inventory* oInventory = new Model_OTA_HotelRatePlanNotifRQ_Inventories_Inventory();
  setModel(*oInventory);
  
  Map<Mapper_OTA_HotelRatePlanNotifRQ_Inventories_Inventory>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ_Inventories_Inventory>();
  oMap->setMapper(this);
  oMap->addFunction("InvCode", &Mapper_OTA_HotelRatePlanNotifRQ_Inventories_Inventory::setInvCode);
  oMap->addFunction("InvType", &Mapper_OTA_HotelRatePlanNotifRQ_Inventories_Inventory::setInvType);
  oMap->addFunction("AppliesToIndicator", &Mapper_OTA_HotelRatePlanNotifRQ_Inventories_Inventory::setAppliesToIndicator);
  
  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_Inventories_Inventory>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_Inventories_Inventory>();
  oParser->setMap(*oMap);
  oParser->setRootTag("Inventory");
  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ_Inventories_Inventory::~Mapper_OTA_HotelRatePlanNotifRQ_Inventories_Inventory() {

}
