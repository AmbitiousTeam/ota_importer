#include "CompatibleOffers.h"

void Mapper_OTA_HotelRatePlanNotifRQ_CompatibleOffers::collectCompatibleOffers(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "Verarbeite CompatibleOffers : " << sXmlContent << std::endl;}
}

Mapper_OTA_HotelRatePlanNotifRQ_CompatibleOffers::Mapper_OTA_HotelRatePlanNotifRQ_CompatibleOffers() {
  Model_OTA_HotelRatePlanNotifRQ_CompatibleOffers* oCompatibleOffers = new Model_OTA_HotelRatePlanNotifRQ_CompatibleOffers();
  setModel(*oCompatibleOffers);
  
  Map<Mapper_OTA_HotelRatePlanNotifRQ_CompatibleOffers>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ_CompatibleOffers>();
  oMap->setMapper(this);
  
  oMap->addFunction("CompatibleOffer", &Mapper_OTA_HotelRatePlanNotifRQ_CompatibleOffers::collectCompatibleOffers);
  
  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_CompatibleOffers>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_CompatibleOffers>();
  oParser->setMap(*oMap);
  oParser->setRootTag("CompatibleOffers");
  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ_CompatibleOffers::~Mapper_OTA_HotelRatePlanNotifRQ_CompatibleOffers() {

}
