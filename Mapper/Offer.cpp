#include "Offer.h"

void Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer::collectOfferRules(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "Verarbeite OfferRules mit Content : " << sXmlContent << std::endl;}
  Mapper_OTA_HotelRatePlanNotifRQ_OfferRules* oOfferRules = new Mapper_OTA_HotelRatePlanNotifRQ_OfferRules();
  oOfferRules->setXmlString(sXmlContent);
  oOfferRules->processXml();
  getModel()->setOfferRules(oOfferRules->getModel());
}

void Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer::collectCompatibleOffers(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "Verarbeite CompatibleOffers mit Content : " << sXmlContent << std::endl;}
  Mapper_OTA_HotelRatePlanNotifRQ_CompatibleOffers* oCompatibleOffers = new Mapper_OTA_HotelRatePlanNotifRQ_CompatibleOffers();
  oCompatibleOffers->setXmlString(sXmlContent);
  oCompatibleOffers->processXml();
  getModel()->setCompatibleOffers(oCompatibleOffers->getModel());
}

void Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer::collectInventories(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "Verarbeite Inventories mit Content : " << sXmlContent << std::endl;}
  Mapper_OTA_HotelRatePlanNotifRQ_Inventories* oInventories = new Mapper_OTA_HotelRatePlanNotifRQ_Inventories();
  oInventories->setXmlString(sXmlContent);
  oInventories->processXml();
  getModel()->setInventories(oInventories->getModel());
}

void Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer::processDiscount(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "Verarbeite Discount mit Content : " << sXmlContent << std::endl;}
  Mapper_OTA_HotelRatePlanNotifRQ_Discount* oDiscount = new Mapper_OTA_HotelRatePlanNotifRQ_Discount();
  oDiscount->setXmlString(sXmlContent);
  oDiscount->processXml();
  getModel()->setDiscount(oDiscount->getModel());
}

void Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer::processOfferDescription(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "Verarbeite OfferDescription mit Content : " << sXmlContent << std::endl;}
  Mapper_OTA_HotelRatePlanNotifRQ_OfferDescription* oOfferDescription = new Mapper_OTA_HotelRatePlanNotifRQ_OfferDescription();
  oOfferDescription->setXmlString(sXmlContent);
  oOfferDescription->processXml();
  getModel()->setOfferDescription(oOfferDescription->getModel());
}

void Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer::setRph(std::string sRph) {
  getModel()->setRph(sRph);
}

std::string Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer::getRph() {
  return getModel()->getRph();
}

void Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer::setOfferCode(std::string sOfferCode) {
  getModel()->setOfferCode(sOfferCode);
}

std::string Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer::getOfferCode() {
  return getModel()->getOfferCode();
}

void Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer::setApplicationOrder(std::string sApplicationOrder) {
  getModel()->setApplicationOrder(std::stoi(sApplicationOrder));
}

int Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer::getApplicationOrder() {
  return getModel()->getApplicationOrder();
}

Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer::Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer() {
  Model_OTA_HotelRatePlanNotifRQ_Offers_Offer* oOffer = new Model_OTA_HotelRatePlanNotifRQ_Offers_Offer();
  setModel(*oOffer);
  
  Map<Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer>();
  oMap->setMapper(this);
  oMap->addFunction("OfferCode", &Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer::setOfferCode);
  oMap->addFunction("ApplicationOrder", &Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer::setApplicationOrder);
  oMap->addFunction("OperationOrder", &Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer::setApplicationOrder);
  oMap->addFunction("RPH", &Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer::setRph);
  oMap->addFunction("OfferRules", &Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer::collectOfferRules);
  oMap->addFunction("CompatibleOffers", &Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer::collectCompatibleOffers);
  oMap->addFunction("Inventories", &Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer::collectInventories);
  oMap->addFunction("Discount", &Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer::processDiscount);
  oMap->addFunction("OfferDescription", &Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer::processOfferDescription);
  
  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer>();
  oParser->setMap(*oMap);
  oParser->setRootTag("Offer");
  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer::~Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer() {

}
