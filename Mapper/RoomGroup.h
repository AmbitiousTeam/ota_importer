#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_ROOMGROUP_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_ROOMGROUP_H__

#include "Mapper.h"
#include "../Models/RoomGroup.h"
#include "../Models/Occupancies.h"
#include "../Models/AdditionalGuestAmounts.h"

class Mapper_OTA_HotelRatePlanNotifRQ_RoomGroup : public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_RoomGroup, Model_OTA_HotelRatePlanNotifRQ_RoomGroup> {
public:
  void collectOccupancy(std::string);
  void collectAdditionalGuestAmount(std::string);
  
  Mapper_OTA_HotelRatePlanNotifRQ_RoomGroup();
  ~Mapper_OTA_HotelRatePlanNotifRQ_RoomGroup();
protected:
private:
};

#endif
