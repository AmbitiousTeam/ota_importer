#include "Supplements.h"

void Mapper_OTA_HotelRatePlanNotifRQ_Supplements::collectSupplement(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "Verarbeite Supplement mit " << sXmlContent << std::endl;};
  Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement* oSupplementMapper = new Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement();
  oSupplementMapper->setXmlString(sXmlContent);
  oSupplementMapper->processXml();
  getModel()->add(*oSupplementMapper->getModel());
}

Mapper_OTA_HotelRatePlanNotifRQ_Supplements::Mapper_OTA_HotelRatePlanNotifRQ_Supplements() {
  Model_OTA_HotelRatePlanNotifRQ_Supplements* oSupplements = new Model_OTA_HotelRatePlanNotifRQ_Supplements();
  setModel(*oSupplements);
  
  Map<Mapper_OTA_HotelRatePlanNotifRQ_Supplements>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ_Supplements>();
  oMap->setMapper(this);
  oMap->addFunction("Supplement", &Mapper_OTA_HotelRatePlanNotifRQ_Supplements::collectSupplement);
  
  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_Supplements>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_Supplements>();
  oParser->setMap(*oMap);
  oParser->setRootTag("Supplements");
  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ_Supplements::~Mapper_OTA_HotelRatePlanNotifRQ_Supplements() {

}
