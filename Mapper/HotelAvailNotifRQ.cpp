#include "HotelAvailNotifRQ.h"

#include "../Models/HotelAvailNotifRQ.h"

void Mapper_OTA_HotelAvailNotifRQ::collectAvailStatusMessages(std::string sXmlContent) {
  Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages* oAvailStatusMessages = new Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages();
  oAvailStatusMessages->setXmlString(sXmlContent);
  oAvailStatusMessages->processXml();
  getModel()->setAvailStatusMessages(*oAvailStatusMessages->getModel());
}

/** Model Functions **/
void Mapper_OTA_HotelAvailNotifRQ::setXmlNsXsd(std::string sXmlNsXsd) {
  getModel()->setXmlNsXsd(sXmlNsXsd);
}

std::string Mapper_OTA_HotelAvailNotifRQ::getXmlNsXsd() {
  return getModel()->getXmlNsXsd();
}

void Mapper_OTA_HotelAvailNotifRQ::setXmlNsXsi(std::string sXmlNsXsi) {
  getModel()->setXmlNsXsi(sXmlNsXsi);
}

std::string Mapper_OTA_HotelAvailNotifRQ::getXmlNsXsi() {
  return getModel()->getXmlNsXsi();
}

/** CTOR **/
Mapper_OTA_HotelAvailNotifRQ::Mapper_OTA_HotelAvailNotifRQ() {
  Model_OTA_HotelAvailNotifRQ* oHotelAvailNotifRQModel = new Model_OTA_HotelAvailNotifRQ();
  setModel(*oHotelAvailNotifRQModel);
  
  Map<Mapper_OTA_HotelAvailNotifRQ>* oMap = new Map<Mapper_OTA_HotelAvailNotifRQ>();
  oMap->setMapper(this);
  
  oMap->addFunction("Xmlns:xsd", &Mapper_OTA_HotelAvailNotifRQ::setXmlNsXsd);
  oMap->addFunction("Xmlns:xsi", &Mapper_OTA_HotelAvailNotifRQ::setXmlNsXsi);
  oMap->addFunction("AvailStatusMessages", &Mapper_OTA_HotelAvailNotifRQ::collectAvailStatusMessages);
  
  Service_XmlParser<Mapper_OTA_HotelAvailNotifRQ>* oParser = new Service_XmlParser<Mapper_OTA_HotelAvailNotifRQ>();
  oParser->setMap(*oMap);
  oParser->setRootTag("OTA_HotelAvailNotifRQ");
  setParser(*oParser);
}

Mapper_OTA_HotelAvailNotifRQ::~Mapper_OTA_HotelAvailNotifRQ() {

}
