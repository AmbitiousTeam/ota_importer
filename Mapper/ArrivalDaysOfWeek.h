#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_ARRIVALDAYSOFWEEK_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_ARRIVALDAYSOFWEEK_H__

#include "Mapper.h"
#include "../Models/ArrivalDaysOfWeek.h"

class Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek :
  public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek, Model_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek> {
public:
  void setMon(std::string);
  bool getMon();
  
  void setTue(std::string);
  bool getTue();
  
  void setWed(std::string);
  bool getWed();
  
  void setThu(std::string);
  bool getThu();
  
  void setFri(std::string);
  bool getFri();
  
  void setSun(std::string);
  bool getSun();
  
  void setSat(std::string);
  bool getSat();
  
  Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek();
  ~Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek();
protected:
private:
};

#endif
