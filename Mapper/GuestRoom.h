#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_GUESTROOM_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_GUESTROOM_H__

#include "Mapper.h"
#include "Occupancy.h"

#include "../Models/GuestRoom.h"
#include "../Models/Occupancies.h"
#include "../Models/AdditionalGuestAmounts.h"

class Mapper_OTA_HotelRatePlanNotifRQ_GuestRoom : public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_GuestRoom, Model_OTA_HotelRatePlanNotifRQ_GuestRoom> {
public:
  void collectOccupancy(std::string);
  void collectQuantities(std::string);
  void collectAdditionalGuestAmount(std::string);
  
  Mapper_OTA_HotelRatePlanNotifRQ_GuestRoom();
  ~Mapper_OTA_HotelRatePlanNotifRQ_GuestRoom();
protected:
private:
};

#endif
