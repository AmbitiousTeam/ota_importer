#include "StatusApplicationControl.h"

void Mapper_OTA_HotelAvailNotifRQ_StatusApplicationControl::setStart(std::string sStart) {
  getModel()->setStart(sStart);
}

std::string Mapper_OTA_HotelAvailNotifRQ_StatusApplicationControl::getStart() {
  return getModel()->getStart();
}

void Mapper_OTA_HotelAvailNotifRQ_StatusApplicationControl::setEnd(std::string sEnd) {
  getModel()->setEnd(sEnd);
}

std::string Mapper_OTA_HotelAvailNotifRQ_StatusApplicationControl::getEnd() {
  return getModel()->getEnd();
}

void Mapper_OTA_HotelAvailNotifRQ_StatusApplicationControl::setInvCode(std::string sInvCode) {
  getModel()->setInvCode(sInvCode);
}

std::string Mapper_OTA_HotelAvailNotifRQ_StatusApplicationControl::getInvCode() {
  return getModel()->getInvCode();
}

void Mapper_OTA_HotelAvailNotifRQ_StatusApplicationControl::setInvType(std::string sInvType) {
  getModel()->setInvType(sInvType);
}

std::string Mapper_OTA_HotelAvailNotifRQ_StatusApplicationControl::getInvType() {
  return getModel()->getInvType();
}

Mapper_OTA_HotelAvailNotifRQ_StatusApplicationControl::Mapper_OTA_HotelAvailNotifRQ_StatusApplicationControl() {
  Model_OTA_HotelAvailNotifRQ_StatusApplicationControl* oStatusApplicationControl = new Model_OTA_HotelAvailNotifRQ_StatusApplicationControl();
  setModel(*oStatusApplicationControl);
  
  Map<Mapper_OTA_HotelAvailNotifRQ_StatusApplicationControl>* oMap = new Map<Mapper_OTA_HotelAvailNotifRQ_StatusApplicationControl>();
  oMap->setMapper(this);
  oMap->addFunction("Start", &Mapper_OTA_HotelAvailNotifRQ_StatusApplicationControl::setStart);
  oMap->addFunction("End", &Mapper_OTA_HotelAvailNotifRQ_StatusApplicationControl::setEnd);
  oMap->addFunction("InvType", &Mapper_OTA_HotelAvailNotifRQ_StatusApplicationControl::setInvType);
  oMap->addFunction("InvCode", &Mapper_OTA_HotelAvailNotifRQ_StatusApplicationControl::setInvCode);
  
  Service_XmlParser<Mapper_OTA_HotelAvailNotifRQ_StatusApplicationControl>* oParser = new Service_XmlParser<Mapper_OTA_HotelAvailNotifRQ_StatusApplicationControl>();
  oParser->setMap(*oMap);
  oParser->setRootTag("StatusApplicationControl");
  setParser(*oParser);
}

Mapper_OTA_HotelAvailNotifRQ_StatusApplicationControl::~Mapper_OTA_HotelAvailNotifRQ_StatusApplicationControl() {

}
