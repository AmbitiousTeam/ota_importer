#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_INVENTORIES_INVENTORY_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_INVENTORIES_INVENTORY_H__

#include "Mapper.h"
#include "../Models/Inventory.h"

class Mapper_OTA_HotelRatePlanNotifRQ_Inventories_Inventory : public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_Inventories_Inventory, Model_OTA_HotelRatePlanNotifRQ_Inventories_Inventory> {
public:
  void setInvCode(std::string);
  std::string getInvCode();
  
  void setAppliesToIndicator(std::string);
  bool getAppliesToIndicator();
  
  void setInvType(std::string);
  
  Mapper_OTA_HotelRatePlanNotifRQ_Inventories_Inventory();
  ~Mapper_OTA_HotelRatePlanNotifRQ_Inventories_Inventory();
protected:
private:
};

#endif
