#include "Rates.h"

void Mapper_OTA_HotelRatePlanNotifRQ_Rates::collectRates(std::string sXmlContent) {
  Mapper_OTA_HotelRatePlanNotifRQ_Rates_Rate* oRate = new Mapper_OTA_HotelRatePlanNotifRQ_Rates_Rate();
  oRate->setXmlString(sXmlContent);
  oRate->processXml();
  std::stringstream sDateKey;
  sDateKey << oRate->getStart() << "_" << oRate->getEnd();
  getModel()->add(*oRate->getModel(), sDateKey.str());
}

Mapper_OTA_HotelRatePlanNotifRQ_Rates::Mapper_OTA_HotelRatePlanNotifRQ_Rates() {
  Model_OTA_HotelRatePlanNotifRQ_Rates* oRates = new Model_OTA_HotelRatePlanNotifRQ_Rates();
  setModel(*oRates);
  
  Map<Mapper_OTA_HotelRatePlanNotifRQ_Rates>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ_Rates>();
  oMap->setMapper(this);
  oMap->addFunction("Rate", &Mapper_OTA_HotelRatePlanNotifRQ_Rates::collectRates);
  
  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_Rates>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_Rates>();
  oParser->setMap(*oMap);
  oParser->setRootTag("Rates");
  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ_Rates::~Mapper_OTA_HotelRatePlanNotifRQ_Rates() {

}
