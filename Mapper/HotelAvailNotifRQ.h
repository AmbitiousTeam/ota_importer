#ifndef __MAPPER_OTA_HOTELAVAILNOTIFRQ_H__
#define __MAPPER_OTA_HOTELAVAILNOTIFRQ_H__

#include "Mapper.h"
#include "Map.h"
#include "AvailStatusMessages.h"
#include "../Services/XmlParser.h"

#include "../Models/HotelAvailNotifRQ.h"

class Mapper_OTA_HotelAvailNotifRQ : public Mapper<Mapper_OTA_HotelAvailNotifRQ, Model_OTA_HotelAvailNotifRQ> {
public:
  
  /** verarbeitet den übergebenen XML Content für AvailStatusMessages **/
  void collectAvailStatusMessages(std::string);
  
  /** Model Functions **/
  void setXmlNsXsd(std::string);
  std::string getXmlNsXsd();
  
  void setXmlNsXsi(std::string);
  std::string getXmlNsXsi();
  
  
  Mapper_OTA_HotelAvailNotifRQ();
  ~Mapper_OTA_HotelAvailNotifRQ();
  
protected:
private:
};

#endif
