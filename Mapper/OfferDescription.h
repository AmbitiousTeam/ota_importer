#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_OFFERDESCRIPTION_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_OFFERDESCRIPTION_H__

#include "Mapper.h"
#include "../Models/OfferDescription.h"

class Mapper_OTA_HotelRatePlanNotifRQ_OfferDescription : public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_OfferDescription, Model_OTA_HotelRatePlanNotifRQ_OfferDescription> {
public:
  void setText(std::string);
  std::string getText();
  
  Mapper_OTA_HotelRatePlanNotifRQ_OfferDescription();
  ~Mapper_OTA_HotelRatePlanNotifRQ_OfferDescription();
protected:
private:
};

#endif
