#include "SellableProduct.h"

void Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct::processCategory(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "Verarbeite Category mit " << sXmlContent << std::endl;};
  /** @todo **/
}

void Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct::processDescription(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "Verarbeite Description mit " << sXmlContent << std::endl;};
  /** @todo **/
}

void Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct::processGuestRoom(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "Verarbeite GuestRoom mit " << sXmlContent << std::endl;};
  Mapper_OTA_HotelRatePlanNotifRQ_GuestRoom* oGuestRoom = new Mapper_OTA_HotelRatePlanNotifRQ_GuestRoom();
  oGuestRoom->setXmlString(sXmlContent);
  oGuestRoom->processXml();
  getModel()->setGuestRoom(*oGuestRoom->getModel());
}

void Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct::setInvType(std::string sInvType) {
  getModel()->setInvType(sInvType);
}

std::string Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct::getInvType() {
  return getModel()->getInvType();
}

void Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct::setInvCode(std::string sInvCode) {
  getModel()->setInvCode(sInvCode);
}

std::string Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct::getInvCode() {
  return getModel()->getInvCode();
}

void Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct::setStart(std::string sStart) {
  getModel()->setStart(sStart);
}

void Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct::setEnd(std::string sEnd) {
  getModel()->setEnd(sEnd);
}

Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct::Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct() {
  Model_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct* oSellableProduct = new Model_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct();
  setModel(*oSellableProduct);
  
  Map<Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct>();
  oMap->setMapper(this);
  oMap->addFunction("Start", &Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct::setStart);
  oMap->addFunction("End", &Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct::setEnd);
  oMap->addFunction("InvType", &Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct::setInvType);
  oMap->addFunction("InvCode", &Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct::setInvCode);
  oMap->addFunction("GuestRoom", &Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct::processGuestRoom);
  oMap->addFunction("Description", &Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct::processDescription);
  oMap->addFunction("Category", &Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct::processCategory);
  
  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct>();
  oParser->setMap(*oMap);
  oParser->setRootTag("SellableProduct");
  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct::~Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct() {

}
