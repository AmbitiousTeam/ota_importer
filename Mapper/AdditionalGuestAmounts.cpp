#include "AdditionalGuestAmounts.h"

void Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts::collectAdditionalGuestAmount(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "Verarbeite AdditionalGuestAmount mit " << sXmlContent << std::endl;};
  Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount* oAdditionalGuestAmount = new Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount();
  oAdditionalGuestAmount->setXmlString(sXmlContent);
  oAdditionalGuestAmount->processXml();
  getModel()->add(*oAdditionalGuestAmount->getModel());
}

Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts::Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts() {
  Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts* oAdditionalGuestAmounts = new Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts();
  setModel(*oAdditionalGuestAmounts);

  Map<Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts>();
  oMap->setMapper(this);

  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts>();
  oMap->addFunction("AdditionalGuestAmount", &Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts::collectAdditionalGuestAmount);
  
  oParser->setMap(*oMap);
  oParser->setRootTag("AdditionalGuestAmounts");
  oParser->setEliminateRedundantTags(true);
  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts::~Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts() {

}
