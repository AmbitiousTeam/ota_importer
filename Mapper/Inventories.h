#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_INVENTORIES_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_INVENTORIES_H__

#include "Mapper.h"
#include "Inventory.h"

#include "../Models/Inventories.h"
#include "../Models/Inventory.h"

class Mapper_OTA_HotelRatePlanNotifRQ_Inventories : public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_Inventories, Model_OTA_HotelRatePlanNotifRQ_Inventories> {
public:
  void collectInventories(std::string);
  
  void setInvCode(std::string);
  std::string getInvCode();
  
  void setAppliesToIndicator(std::string);
  bool getAppliesToIndicator();
  
  Mapper_OTA_HotelRatePlanNotifRQ_Inventories();
  ~Mapper_OTA_HotelRatePlanNotifRQ_Inventories();
protected:
private:
};

#endif
