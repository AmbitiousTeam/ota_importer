#include "Deadline.h"

void Mapper_OTA_HotelRatePlanNotifRQ_Deadline::setOffsetDropTime(std::string sOffsetDropTime) {
  getModel()->setOffsetDropTime(sOffsetDropTime);
}

std::string Mapper_OTA_HotelRatePlanNotifRQ_Deadline::getOffsetDropTime() {
  return getModel()->getOffsetDropTime();
}

void Mapper_OTA_HotelRatePlanNotifRQ_Deadline::setOffsetTimeUnit(std::string sOffsetTimeUnit) {
  getModel()->setOffsetTimeUnit(sOffsetTimeUnit);
}

std::string Mapper_OTA_HotelRatePlanNotifRQ_Deadline::getOffsetTimeUnit() {
  return getModel()->getOffsetTimeUnit();
}

void Mapper_OTA_HotelRatePlanNotifRQ_Deadline::setOffsetUnitMultiplier(std::string sOffsetUnitMultiplier) {
  getModel()->setOffsetUnitMultiplier(Converter::convertMixedToInt(sOffsetUnitMultiplier));
}

int Mapper_OTA_HotelRatePlanNotifRQ_Deadline::getOffsetUnitMultiplier() {
  return getModel()->getOffsetUnitMultiplier();
}

Mapper_OTA_HotelRatePlanNotifRQ_Deadline::Mapper_OTA_HotelRatePlanNotifRQ_Deadline() {
  Model_OTA_HotelRatePlanNotifRQ_Deadline* oDeadline = new Model_OTA_HotelRatePlanNotifRQ_Deadline();
  setModel(*oDeadline);
  
  Map<Mapper_OTA_HotelRatePlanNotifRQ_Deadline>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ_Deadline>();
  oMap->setMapper(this);
  
  oMap->addFunction("OffsetUnitMultiplier", &Mapper_OTA_HotelRatePlanNotifRQ_Deadline::setOffsetUnitMultiplier);
  oMap->addFunction("OffsetTimeUnit", &Mapper_OTA_HotelRatePlanNotifRQ_Deadline::setOffsetTimeUnit);
  oMap->addFunction("OffsetDropTime", &Mapper_OTA_HotelRatePlanNotifRQ_Deadline::setOffsetDropTime);
  
  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_Deadline>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_Deadline>();
  oParser->setMap(*oMap);
  oParser->setRootTag("Deadline");
  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ_Deadline::~Mapper_OTA_HotelRatePlanNotifRQ_Deadline() {

}
