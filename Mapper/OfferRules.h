#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_OFFERRULES_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_OFFERRULES_H__

#include <string>

#include "Mapper.h"
#include "OfferRule.h"

#include "../Models/OfferRules.h"
#include "../Models/OfferRule.h"

class Mapper_OTA_HotelRatePlanNotifRQ_OfferRules : public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_OfferRules, Model_OTA_HotelRatePlanNotifRQ_OfferRules> {
public:
  void collectOfferRules(std::string);
  
  Mapper_OTA_HotelRatePlanNotifRQ_OfferRules();
  ~Mapper_OTA_HotelRatePlanNotifRQ_OfferRules();
protected:
private:
};

#endif
