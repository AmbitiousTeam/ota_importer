#include "ArrivalDaysOfWeek.h"

void Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::setMon(std::string sMon) {
  getModel()->setMon(Converter::convertMixedToBoolean(sMon));
}

bool Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::getMon() {
  return getModel()->getMon();
}

void Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::setTue(std::string sTue) {
  getModel()->setTue(Converter::convertMixedToBoolean(sTue));
}

bool Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::getTue() {
  return getModel()->getTue();
}

void Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::setWed(std::string sWed) {
  getModel()->setWed(Converter::convertMixedToBoolean(sWed));
}

bool Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::getWed() {
  return getModel()->getWed();
}

void Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::setThu(std::string sThu) {
  getModel()->setThu(Converter::convertMixedToBoolean(sThu));
}

bool Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::getThu() {
  return getModel()->getThu();
}

void Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::setFri(std::string sFri) {
  getModel()->setFri(Converter::convertMixedToBoolean(sFri));
}

bool Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::getFri() {
  return getModel()->getFri();
}

void Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::setSat(std::string sSat) {
  getModel()->setSat(Converter::convertMixedToBoolean(sSat));
}

bool Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::getSat() {
  return getModel()->getSat();
}

void Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::setSun(std::string sSun) {
  getModel()->setSun(Converter::convertMixedToBoolean(sSun));
}

bool Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::getSun() {
  return getModel()->getSun();
}


Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek() {
  Model_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek* oArrivalDaysOfWeek = new Model_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek();
  setModel(*oArrivalDaysOfWeek);
  
  Map<Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek>();
  oMap->setMapper(this);
  
  oMap->addFunction("Mon", &Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::setMon);
  oMap->addFunction("Tue", &Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::setTue);
  oMap->addFunction("Wed", &Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::setWed);
  oMap->addFunction("Thu", &Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::setThu);
  oMap->addFunction("Fri", &Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::setFri);
  oMap->addFunction("Sat", &Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::setSat);
  oMap->addFunction("Sun", &Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::setSun);
  
  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek>();
  oParser->setMap(*oMap);
  oParser->setRootTag("ArrivalDaysOfWeek");
  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek::~Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek() {
}
