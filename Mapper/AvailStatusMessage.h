#ifndef __MAPPER_OTA_HOTELAVAILNOTIFRQ_AVAILSTATUSMESSAGES_AVAILSTATUSMESSAGE_H__
#define __MAPPER_OTA_HOTELAVAILNOTIFRQ_AVAILSTATUSMESSAGES_AVAILSTATUSMESSAGE_H__

#include "Mapper.h"
#include "Map.h"

#include "../Models/AvailStatusMessage.h"

#include "../Tools/Converter.h"

#include "StatusApplicationControl.h"
#include "RestrictionStatus.h"

class Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage :
   public Mapper<Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage, Model_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage>{
public:
  
  void collectStatusApplicationControl(std::string);
  void collectRestrictionStatus(std::string);
  
  Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage();
  ~Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage();
private:
};

#endif
