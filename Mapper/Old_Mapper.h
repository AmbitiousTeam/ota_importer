#ifndef __OLD_MAPPER_H__
#define __OLD_MAPPER_H__

template<typename T_Service>
class Service_Mapper
{
public:
  typedef void(T_Service::*memberFunction)(std::string);

  /** startet die verarbeitung des XML String **/
  bool processXml();

  void setXmlString(std::string);
  std::string getXmlString();

  void setRootTag(std::string);
  std::string getRootTag();

  void setObject(T_Service*);
  T_Service* getObject();

  bool init();
  
  void addFunction(std::string, memberFunction);
  bool operator()(std::string, std::string);

  Service_Mapper();
  ~Service_Mapper();
  
protected:
private:
  std::map<std::string, memberFunction> _funcMap;
  std::string _sXmlString;
  std::string _sRootTag;
  T_Service* _oService;
};

template<typename T_Service>void Service_Mapper<T_Service>::addFunction(std::string funcName, memberFunction func) {
  _funcMap[funcName] = func;
}

// template<typename T>bool Service_Mapper<T>::operator()(string sFuncName, string sValue, T& oObject) {
template<typename T_Service>bool Service_Mapper<T_Service>::operator()(std::string sFuncName, std::string sValue) {
  typename std::map<std::string, memberFunction>::const_iterator itMap;
  
  itMap = _funcMap.find(sFuncName);
  bool bReturn = false;
  
  if (itMap != _funcMap.end()) {
    (_oService->*itMap->second)(sValue);
  } else {
//     if (DEBUG_MODE) {std::cout << "Kann mit Element " << sFuncName << " und Content (" + sValue + ") in Aktuellem Mapper (" << typeid(this->getObject()).name() << ") nichts anfangen" << std::endl;}
  }
  return bReturn;
}

template<typename T_Service>void Service_Mapper<T_Service>::setObject(T_Service* oService) {
  _oService = oService;
}

template<typename T_Service>T_Service* Service_Mapper<T_Service>::getObject() {
  return _oService;
}

template<typename T_Service>void Service_Mapper<T_Service>::setRootTag(std::string sRootTag) {
  _sRootTag = sRootTag;
}

template<typename T_Service>std::string Service_Mapper<T_Service>::getRootTag() {
  return _sRootTag;
}

template<typename T_Service>Service_Mapper<T_Service>::Service_Mapper() {
}

template<typename T_Service>Service_Mapper<T_Service>::~Service_Mapper() {

}

#endif
