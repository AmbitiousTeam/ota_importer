#include "GuestRoom.h"
#include "Quantities.h"
#include "AdditionalGuestAmounts.h"

void Mapper_OTA_HotelRatePlanNotifRQ_GuestRoom::collectOccupancy(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "Verarbeite Occupancy in GuestRoom mit " << sXmlContent << std::endl;};
  Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy* oOccupancy = new Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy();
  oOccupancy->setXmlString(sXmlContent);
  oOccupancy->processXml();
  getModel()->getOccupancies()->add(*oOccupancy->getModel());
}

void Mapper_OTA_HotelRatePlanNotifRQ_GuestRoom::collectQuantities(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "Verarbeite Quantities in GuestRoom mit " << sXmlContent << std::endl;};
  Mapper_OTA_HotelRatePlanNotifRQ_Quantities* oQuantities = new Mapper_OTA_HotelRatePlanNotifRQ_Quantities();
  oQuantities->setXmlString(sXmlContent);
  oQuantities->processXml();
  getModel()->setQuantities(*oQuantities->getModel());
}

void Mapper_OTA_HotelRatePlanNotifRQ_GuestRoom::collectAdditionalGuestAmount(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "Verarbeite AdditionalGuestAmount in GuestRoom mit " << sXmlContent << std::endl;};
  Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts* oAdditionalGuestAmounts = new Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts();
  oAdditionalGuestAmounts->setXmlString(sXmlContent);
  oAdditionalGuestAmounts->processXml();
  getModel()->setAdditionalGuestAmounts(*oAdditionalGuestAmounts->getModel());
}

Mapper_OTA_HotelRatePlanNotifRQ_GuestRoom::Mapper_OTA_HotelRatePlanNotifRQ_GuestRoom() {
  Model_OTA_HotelRatePlanNotifRQ_GuestRoom* oGuestRoom = new Model_OTA_HotelRatePlanNotifRQ_GuestRoom();
  setModel(*oGuestRoom);
  
  Model_OTA_HotelRatePlanNotifRQ_Occupancies* oOccupancies = new Model_OTA_HotelRatePlanNotifRQ_Occupancies();
  oGuestRoom->setOccupancies(*oOccupancies);
  
  Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts* oAdditionalGuestAmounts = new Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts();
  oGuestRoom->setAdditionalGuestAmounts(*oAdditionalGuestAmounts);
  
  Map<Mapper_OTA_HotelRatePlanNotifRQ_GuestRoom>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ_GuestRoom>();
  oMap->setMapper(this);
  oMap->addFunction("AdditionalGuestAmount", &Mapper_OTA_HotelRatePlanNotifRQ_GuestRoom::collectAdditionalGuestAmount);
  oMap->addFunction("Occupancy", &Mapper_OTA_HotelRatePlanNotifRQ_GuestRoom::collectOccupancy);
  oMap->addFunction("Quantities", &Mapper_OTA_HotelRatePlanNotifRQ_GuestRoom::collectQuantities);
  
  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_GuestRoom>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_GuestRoom>();
  oParser->setMap(*oMap);
  oParser->setRootTag("GuestRoom");
  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ_GuestRoom::~Mapper_OTA_HotelRatePlanNotifRQ_GuestRoom() {

}
