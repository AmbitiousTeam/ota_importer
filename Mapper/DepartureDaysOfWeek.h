#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_DEPARTUREDAYSOFWEEK_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_DEPARTUREDAYSOFWEEK_H__

#include "Mapper.h"
#include "../Models/DepartureDaysOfWeek.h"

class Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek : public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek, Model_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek> {
public:
  void setMon(std::string);
  bool getMon();
  
  void setTue(std::string);
  bool getTue();
  
  void setWed(std::string);
  bool getWed();
  
  void setThu(std::string);
  bool getThu();
  
  void setFri(std::string);
  bool getFri();
  
  void setSun(std::string);
  bool getSun();
  
  void setSat(std::string);
  bool getSat();
  
  Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek();
  ~Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek();
protected:
private:
};

#endif
