#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_DATERESTRICTIONS_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_DATERESTRICTIONS_H__

#include <string>
#include "Mapper.h"
#include "DateRestriction.h"
#include "../Models/DateRestrictions.h"

class Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions : public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions, Model_OTA_HotelRatePlanNotifRQ_DateRestrictions> {
public:
  void collectDateRestrictions(std::string);
  
  Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions();
  ~Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions();
protected:
private:
};

#endif
