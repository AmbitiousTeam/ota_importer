#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_ADDITIONALGUESTAMOUNTS_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_ADDITIONALGUESTAMOUNTS_H__

#include "Mapper.h"
#include "AdditionalGuestAmount.h"

#include "../Models/AdditionalGuestAmounts.h"
#include "../Models/AdditionalGuestAmount.h"

class Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts :
  public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts, Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts> {
public:
  
  void collectAdditionalGuestAmount(std::string);
  
  Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts();
  ~Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts();
protected:
private:
};

#endif
