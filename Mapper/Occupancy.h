#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_OCCUPANCIES_OCCUPANCY_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_OCCUPANCIES_OCCUPANCY_H__

#include "Mapper.h"
#include "../Models/Occupancy.h"

class Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy : public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy, Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy> {
public:
  void setMinAge(std::string);
  int getMinAge(void);
  
  void setMaxAge(std::string);
  int getMaxAge(void);
  
  void setMinOccupancy(std::string);
  int getMinOccupancy();
  
  void setMaxOccupancy(std::string);
  int getMaxOccupancy();
  
  void setInfantsAreCounted(std::string);
  bool getInfantsAreCounted();
  
  void setAgeQualifyingCode(std::string);
  int getAgeQualifyingCode();
  
  Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy();
  ~Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy();
protected:
private:
};

#endif
