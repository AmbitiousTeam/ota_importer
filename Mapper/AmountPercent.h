#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_AMOUNTPERCENT_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_AMOUNTPERCENT_H__

#include "Mapper.h"
#include "../Models/AmountPercent.h"

class Mapper_OTA_HotelRatePlanNotifRQ_AmountPercent :
  public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_AmountPercent, Model_OTA_HotelRatePlanNotifRQ_AmountPercent> {
public:
  void setCurrencyCode(std::string);
  std::string getCurrencyCode();
  
  void setPercent(std::string);
  float getPercent();
  
  void setAmount(std::string);
  float getAmount();
  
  Mapper_OTA_HotelRatePlanNotifRQ_AmountPercent();
  ~Mapper_OTA_HotelRatePlanNotifRQ_AmountPercent();
protected:
private:
};

#endif
