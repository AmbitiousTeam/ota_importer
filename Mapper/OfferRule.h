#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_OFFERRULES_OFFERRULE_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_OFFERRULES_OFFERRULE_H__

#include "Mapper.h"
#include "DateRestrictions.h"
#include "DateRestriction.h"
#include "LengthsOfStay.h"

#include "../Models/OfferRule.h"

class Mapper_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule : public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule, Model_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule> {
public:
  void collectDateRestrictions(std::string);
  void collectLengthsOfStay(std::string);
  
  Mapper_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule();
  ~Mapper_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule();
protected:
private:
};

#endif
