#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_BASEBYGUESTAMTS_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_BASEBYGUESTAMTS_H__

#include "Mapper.h"
#include "BaseByGuestAmt.h"

#include "../Models/BaseByGuestAmts.h"

class Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts : public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts, Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts> {
public:
  void collectBaseByGuestAmts(std::string);
  
  Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts();
  ~Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts();
protected:
private:
};

#endif
