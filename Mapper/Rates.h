#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_RATES_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_RATES_H__

#include <sstream>

#include "Mapper.h"
#include "Rate.h"

#include "../Models/Rates.h"
#include "../Models/Rate.h"

class Mapper_OTA_HotelRatePlanNotifRQ_Rates : public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_Rates, Model_OTA_HotelRatePlanNotifRQ_Rates> {
public:
  void collectRates(std::string);
  
  Mapper_OTA_HotelRatePlanNotifRQ_Rates();
  ~Mapper_OTA_HotelRatePlanNotifRQ_Rates();
protected:
private:
};

#endif
