#include "OfferDescription.h"

void Mapper_OTA_HotelRatePlanNotifRQ_OfferDescription::setText(std::string sText) {
  if (DEBUG_MODE) {std::cout << "Verarbeite Text in OfferDescription mit " << sText << std::endl;};
  getParser()->setRootTag("Text");
  getParser()->setXmlContentOnly(true);
  getParser()->parse(sText);
  getModel()->setText(getParser()->getActualXmlContent());
}

std::string Mapper_OTA_HotelRatePlanNotifRQ_OfferDescription::getText() {
  return getModel()->getText();
}

Mapper_OTA_HotelRatePlanNotifRQ_OfferDescription::Mapper_OTA_HotelRatePlanNotifRQ_OfferDescription() {
  Model_OTA_HotelRatePlanNotifRQ_OfferDescription* oOfferDescription = new Model_OTA_HotelRatePlanNotifRQ_OfferDescription();
  setModel(*oOfferDescription);
  
  Map<Mapper_OTA_HotelRatePlanNotifRQ_OfferDescription>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ_OfferDescription>();
  oMap->setMapper(this);
  oMap->addFunction("Text", &Mapper_OTA_HotelRatePlanNotifRQ_OfferDescription::setText);
  
  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_OfferDescription>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_OfferDescription>();
  oParser->setMap(*oMap);
  oParser->setRootTag("OfferDescription");
  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ_OfferDescription::~Mapper_OTA_HotelRatePlanNotifRQ_OfferDescription() {

}

