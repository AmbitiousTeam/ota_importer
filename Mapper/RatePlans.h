#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_RATEPLANS_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_RATEPLANS_H__

#include <string>
#include "Mapper.h"
#include "RatePlan.h"

#include "../Tools/Converter.h"

#include "../Models/RatePlans.h"
#include "../Models/RatePlan.h"

class Mapper_OTA_HotelRatePlanNotifRQ_RatePlans : public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_RatePlans, Model_OTA_HotelRatePlanNotifRQ_RatePlans> {
public:
  void collectOffers(std::string);
  void collectRatePlan(std::string);
  void collectSupplements(std::string);
  void collectBookingRules(std::string);
  
  void setIdCon(std::string);
  std::string getIdCon();
  
  void setDelta(std::string);
  bool getDelta();
  
  void setHotelCode(std::string);
  std::string getHotelCode();
  
  void setHotelName(std::string);
  std::string getHotelName();
  
  void setMultiMarkup(std::string);
  bool getMultiMarkup();
  
  void setMultiMarkupPriority(std::string);
  int getMultiMarkupPriority();
  
  Mapper_OTA_HotelRatePlanNotifRQ_RatePlans();
  ~Mapper_OTA_HotelRatePlanNotifRQ_RatePlans();
  
protected:
private:
};

#endif
