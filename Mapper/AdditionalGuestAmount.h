#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_ADDITIONALGUESTAMOUNTS_ADDITIONALGUESTAMOUNT_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_ADDITIONALGUESTAMOUNTS_ADDITIONALGUESTAMOUNT_H__

#include "Mapper.h"
#include "../Models/AdditionalGuestAmount.h"

class Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount :
  public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount, Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount> {
public:
  void setMinAge(const std::string);
  int getMinAge(void);
  
  void setMaxAge(const std::string);
  int getMaxAge(void);
  
  void setCurrencyCode(const std::string);
  std::string getCurrencyCode(void);
  
  void setAmountAfterTax(const std::string);
  float getAmountAfterTax(void);
  
  void setAdditionalGuestsNumber(const std::string);
  int getAdditionalGuestsNumber(void);
  
  void setAgeQualifyingCode(const std::string);
  int getAgeQualifyingCode(void);
  
  void setMaxAdditionalGuests(const std::string);
  int getMaxAdditionalGuests(void);
  
  void setAmount(const std::string);
  float getAmount(void);
  
  Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount();
  ~Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount();
protected:
private:
};

#endif
