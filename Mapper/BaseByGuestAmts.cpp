#include "BaseByGuestAmts.h"

void Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts::collectBaseByGuestAmts(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "Verarbeite BaseByGuestAmts mit " << sXmlContent << std::endl;};
  Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt* oBaseByGuestAmt = new Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt();
  oBaseByGuestAmt->setXmlString(sXmlContent);
  oBaseByGuestAmt->processXml();
  getModel()->add(*oBaseByGuestAmt->getModel());
}

Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts::Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts() {
  Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts* oBaseByGuestAmts = new Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts();
  setModel(*oBaseByGuestAmts);
  
  Map<Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts>();
  oMap->setMapper(this);
  
  oMap->addFunction("BaseByGuestAmt", &Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts::collectBaseByGuestAmts);
  
  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts>();
  oParser->setMap(*oMap);
  oParser->setRootTag("BaseByGuestAmts");
  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts::~Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts() {

}
