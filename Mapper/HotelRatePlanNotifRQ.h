#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_H__

#include "Mapper.h"
#include "Map.h"

#include "../Models/HotelRatePlanNotifRQ.h"
#include "RatePlans.h"
#include "Offers.h"
#include "TPAExtensions.h"

class Mapper_OTA_HotelRatePlanNotifRQ : public Mapper<Mapper_OTA_HotelRatePlanNotifRQ, Model_OTA_HotelRatePlanNotifRQ> {
public:
  
  /** verarbeitet den übergebenen XML Content für RatePlans **/
  void collectRatePlans(std::string);
//   void collectBookingRules(std::string);
  void collectOffers(std::string);
  void collectTPAExtensions(std::string);
  
  /** Model Functions **/
  void setXmlNsXsd(std::string);
  std::string getXmlNsXsd();
  
  /** Model Functions **/
  void setXmlNsXsi(std::string);
  std::string getXmlNsXsi();
  
  void setXmlNs(std::string);
  std::string getXmlNs();
  
  void setXsiSchemaLocation(std::string);
  std::string getXsiSchemaLocation();
  
  void setVersion(std::string);
  std::string getVersion();
  
  void setCorrelationId(std::string);
  void setEchoToken(std::string);
  
  Mapper_OTA_HotelRatePlanNotifRQ();
  ~Mapper_OTA_HotelRatePlanNotifRQ();
  
protected:
private:
};

#endif
