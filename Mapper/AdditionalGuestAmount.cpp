#include "AdditionalGuestAmount.h"

void Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::setMinAge(const std::string sMinAge) {
  getModel()->setMinAge(Converter::convertMixedToInt(sMinAge));
}

int Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::getMinAge(void) {
  return getModel()->getMinAge();
}

void Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::setMaxAge(const std::string sMaxAge) {
  getModel()->setMaxAge(Converter::convertMixedToInt(sMaxAge));
}

int Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::getMaxAge(void) {
  return getModel()->getMaxAge();
}

void Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::setAdditionalGuestsNumber(const std::string sAdditionalGuestsNumber) {
  getModel()->setAdditionalGuestsNumber(Converter::convertMixedToInt(sAdditionalGuestsNumber));
}

int Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::getAdditionalGuestsNumber(void) {
  return getModel()->getAdditionalGuestNumber();
}

void Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::setAgeQualifyingCode(const std::string sAgeQualifyingCode) {
  getModel()->setAgeQualifyingCode(Converter::convertMixedToInt(sAgeQualifyingCode));
}

int Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::getAgeQualifyingCode(void) {
  return getModel()->getAgeQualifyingCode();
}

void Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::setCurrencyCode(const std::string sCurrencyCode) {
  getModel()->setCurrencyCode(sCurrencyCode);
}

std::string Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::getCurrencyCode(void) {
  return getModel()->getCurrencyCode();
}

void Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::setAmountAfterTax(const std::string sAmountAfterTax) {
  getModel()->setAmountAfterTax(Converter::convertMixedToDouble(sAmountAfterTax));
}

float Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::getAmountAfterTax(void) {
  return getModel()->getAmountAfterTax();
}

void Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::setMaxAdditionalGuests(const std::string sMaxAdditionalGuests) {
  getModel()->setMaxAdditionalGuests(Converter::convertMixedToInt(sMaxAdditionalGuests));
}

int Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::getMaxAdditionalGuests(void) {
  return getModel()->getMaxAdditionalGuests();
}

void Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::setAmount(const std::string sAmount) {
  getModel()->setAmount(Converter::convertMixedToDouble(sAmount));
}

float Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::getAmount(void) {
  return getModel()->getAmount();
}

Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount() {
  Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount* oAdditionalGuestAmount = new Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount();
  setModel(*oAdditionalGuestAmount);
  
  Map<Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount>();
  oMap->setMapper(this);

  oMap->addFunction("MaxAdditionalGuests", &Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::setMaxAdditionalGuests);
  oMap->addFunction("MinAge", &Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::setMinAge);
  oMap->addFunction("MaxAge", &Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::setMaxAge);
  oMap->addFunction("Amount", &Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::setAmount);
  oMap->addFunction("CurencyCode", &Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::setCurrencyCode);
  oMap->addFunction("AmountAfterTax", &Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::setAmountAfterTax);
  oMap->addFunction("AdditionalGuestNumber", &Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::setAdditionalGuestsNumber);
  oMap->addFunction("AgeQualifyingCode", &Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::setAgeQualifyingCode);

  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount>();
  oParser->setRootTag("AdditionalGuestAmount");
  oParser->setEliminateRedundantRootTags(true);
  oParser->setMap(*oMap);

  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount::~Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount() {

}
