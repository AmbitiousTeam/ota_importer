#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_DISCOUNT_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_DISCOUNT_H__

#include <string>
#include "Mapper.h"
#include "../Models/Discount.h"

class Mapper_OTA_HotelRatePlanNotifRQ_Discount : public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_Discount, Model_OTA_HotelRatePlanNotifRQ_Discount> {
public:
  void setAmount(std::string);
  float getAmount();
  
  void setPercent(std::string);
  float getPercent();
  
  void setNightsRequired(std::string);
  int getNightsRequired();
  
  void setNightsDiscounted(std::string);
  int getNightsDiscounted();
  
  void setChargeUnitCode(std::string);
  void setCurrencyCode(std::string);
  
  void setDiscountLastBookingDays(std::string);
  
  Mapper_OTA_HotelRatePlanNotifRQ_Discount();
  ~Mapper_OTA_HotelRatePlanNotifRQ_Discount();
protected:
private:
};

#endif
