#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_COMPATIBLEOFFERS_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_COMPATIBLEOFFERS_H__

#include <string>

#include "Mapper.h"
#include "CompatibleOffer.h"

#include "../Models/CompatibleOffers.h"
#include "../Models/CompatibleOffer.h"

class Mapper_OTA_HotelRatePlanNotifRQ_CompatibleOffers : public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_CompatibleOffers, Model_OTA_HotelRatePlanNotifRQ_CompatibleOffers> {
public:
  void collectCompatibleOffers(std::string);
  
  Mapper_OTA_HotelRatePlanNotifRQ_CompatibleOffers();
  ~Mapper_OTA_HotelRatePlanNotifRQ_CompatibleOffers();
protected:
private:
};

#endif
