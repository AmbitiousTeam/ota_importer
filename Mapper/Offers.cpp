#include "Offers.h"

void Mapper_OTA_HotelRatePlanNotifRQ_Offers::collectOffer(std::string sXmlContent) {
  Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer* oOffer = new Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer();
  oOffer->setXmlString(sXmlContent);
  oOffer->processXml();
  getModel()->add(*oOffer->getModel());
}

Mapper_OTA_HotelRatePlanNotifRQ_Offers::Mapper_OTA_HotelRatePlanNotifRQ_Offers() {
  Model_OTA_HotelRatePlanNotifRQ_Offers* oOffers = new Model_OTA_HotelRatePlanNotifRQ_Offers();
  setModel(*oOffers);
  
  Map<Mapper_OTA_HotelRatePlanNotifRQ_Offers>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ_Offers>();
  oMap->setMapper(this);
  oMap->addFunction("Offer", &Mapper_OTA_HotelRatePlanNotifRQ_Offers::collectOffer);
  
  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_Offers>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_Offers>();
  oParser->setMap(*oMap);
  oParser->setRootTag("Offers");
  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ_Offers::~Mapper_OTA_HotelRatePlanNotifRQ_Offers() {

}
