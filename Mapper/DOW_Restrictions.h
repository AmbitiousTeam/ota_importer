#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_DOW_RESTRICTIONS_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_DOW_RESTRICTIONS_H__

#include "Mapper.h"
#include "ArrivalDaysOfWeek.h"
#include "DepartureDaysOfWeek.h"

#include "../Models/DOW_Restrictions.h"

class Mapper_OTA_HotelRatePlanNotifRQ_DOW_Restrictions : public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_DOW_Restrictions, Model_OTA_HotelRatePlanNotifRQ_DOW_Restrictions> {
public:
  
  void processArrivalDaysOfWeek(std::string);
  void processDepartureDaysOfWeek(std::string);
  
  Mapper_OTA_HotelRatePlanNotifRQ_DOW_Restrictions();
  ~Mapper_OTA_HotelRatePlanNotifRQ_DOW_Restrictions();
protected:
private:
};

#endif
