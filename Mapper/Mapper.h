#ifndef __MAPPER_H__
#define __MAPPER_H__

#include <iostream>
#include <typeinfo>
#include <string>
#include <map>

#include "../App/Defines.cpp"
#include "../Tools/Converter.h"
#include "../Services/XmlParser.h"

template<typename T_Mapper, typename T_Model>class Mapper {
public:
  bool processXml();

  void setXmlString(std::string);
  std::string getXmlString();

  void setRootTag(std::string);
  std::string getRootTag();

  bool init();

  void setParser(Service_XmlParser<T_Mapper>&);
  Service_XmlParser<T_Mapper>* getParser();

  void setModel(T_Model&);
  T_Model* getModel();

  Mapper();
  ~Mapper();
protected:
private:

  std::string _sRootTag;
  std::string _sXmlString;
  
  Service_XmlParser<T_Mapper>* _oParser = NULL;
  T_Model* _oModel = NULL;
};

template<typename T_Mapper, typename T_Model>void Mapper<T_Mapper, T_Model>::setParser(Service_XmlParser<T_Mapper>& oParser) {
  _oParser = &oParser;
}

template<typename T_Mapper, typename T_Model>Service_XmlParser<T_Mapper>* Mapper<T_Mapper, T_Model>::getParser() {
  return _oParser;
}

template<typename T_Mapper, typename T_Model>void Mapper<T_Mapper, T_Model>::setModel(T_Model& oModel) {
  _oModel = &oModel;
}

template<typename T_Mapper, typename T_Model>T_Model* Mapper<T_Mapper, T_Model>::getModel() {
  return _oModel;
}

template<typename T_Mapper, typename T_Model>void Mapper<T_Mapper, T_Model>::setXmlString(std::string sXmlString) {
  _sXmlString = sXmlString;
}

template<typename T_Mapper, typename T_Model>std::string Mapper<T_Mapper, T_Model>::getXmlString() {
  return _sXmlString;
}

template<typename T_Mapper, typename T_Model>bool Mapper<T_Mapper, T_Model>::processXml() {
  getParser()->parse(getXmlString());
}

template<typename T_Mapper, typename T_Model>void Mapper<T_Mapper, T_Model>::setRootTag(std::string sRootTag) {
  _sRootTag = sRootTag;
}

template<typename T_Mapper, typename T_Model>std::string Mapper<T_Mapper, T_Model>::getRootTag() {
  return _sRootTag;
}

template<typename T_Mapper, typename T_Model>Mapper<T_Mapper, T_Model>::Mapper() {
}

template<typename T_Mapper, typename T_Model>Mapper<T_Mapper, T_Model>::~Mapper() {
  delete this->_oModel;
  delete this->_oParser;
}

#endif
