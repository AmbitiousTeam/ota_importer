#include "LengthsOfStay.h"

void Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay::collectLenghtsOfStay(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "Verarbeite LengthOfStay mit " << sXmlContent << std::endl;}
  Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay* oLengthOfStay = new Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay();
  oLengthOfStay->setXmlString(sXmlContent);
  oLengthOfStay->processXml();
  getModel()->add(*oLengthOfStay->getModel());
}

Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay::Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay() {
  Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay* oOfferRules = new Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay();
  setModel(*oOfferRules);
  
  Map<Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay>();
  oMap->setMapper(this);
  oMap->addFunction("LengthOfStay", &Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay::collectLenghtsOfStay);
  
  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay>();
  oParser->setMap(*oMap);
  oParser->setRootTag("LengthsOfStay");
  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay::~Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay() {

}
