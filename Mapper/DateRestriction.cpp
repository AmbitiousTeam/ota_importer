#include "DateRestriction.h"

void Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction::setStart(std::string sStart) {
  getModel()->setStart(sStart);
}

std::string Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction::getStart() {
  return getModel()->getStart();
}

void Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction::setEnd(std::string sEnd) {
  getModel()->setEnd(sEnd);
}

std::string Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction::getEnd() {
  return getModel()->getEnd();
}

void Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction::setRestrictionType(std::string sRestrictionType) {
  getModel()->setRestrictionType(sRestrictionType);
}

std::string Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction::getRestrictionType() {
  return getModel()->getRestrictionType();
}

Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction::Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction() {
  Model_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction* oDateRestriction = new Model_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction();
  setModel(*oDateRestriction);
  
  Map<Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction>();
  oMap->setMapper(this);
  
  oMap->addFunction("Start", &Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction::setStart);
  oMap->addFunction("End", &Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction::setEnd);
  oMap->addFunction("RestrictionType", &Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction::setRestrictionType);
  
  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction>();
  oParser->setMap(*oMap);
  oParser->setRootTag("DateRestriction");
  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction::~Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction() {

}
