#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_SUPPLEMENTS_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_SUPPLEMENTS_H__

#include "Mapper.h"
#include "Supplement.h"
#include "../Models/Supplements.h"

class Mapper_OTA_HotelRatePlanNotifRQ_Supplements : public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_Supplements, Model_OTA_HotelRatePlanNotifRQ_Supplements> {
public:
  
  void collectSupplement(std::string);
  
  Mapper_OTA_HotelRatePlanNotifRQ_Supplements();
  ~Mapper_OTA_HotelRatePlanNotifRQ_Supplements();
protected:
private:
};

#endif
