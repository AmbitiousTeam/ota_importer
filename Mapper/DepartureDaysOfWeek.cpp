#include "DepartureDaysOfWeek.h"

void Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::setMon(std::string sMon) {
  getModel()->setMon(Converter::convertMixedToBoolean(sMon));
}

bool Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::getMon() {
  return getModel()->getMon();
}

void Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::setTue(std::string sTue) {
  getModel()->setTue(Converter::convertMixedToBoolean(sTue));
}

bool Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::getTue() {
  return getModel()->getTue();
}

void Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::setWed(std::string sWed) {
  getModel()->setWed(Converter::convertMixedToBoolean(sWed));
}

bool Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::getWed() {
  return getModel()->getWed();
}

void Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::setThu(std::string sThu) {
  getModel()->setThu(Converter::convertMixedToBoolean(sThu));
}

bool Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::getThu() {
  return getModel()->getThu();
}

void Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::setFri(std::string sFri) {
  getModel()->setFri(Converter::convertMixedToBoolean(sFri));
}

bool Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::getFri() {
  return getModel()->getFri();
}

void Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::setSat(std::string sSat) {
  getModel()->setSat(Converter::convertMixedToBoolean(sSat));
}

bool Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::getSat() {
  return getModel()->getSat();
}

void Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::setSun(std::string sSun) {
  getModel()->setSun(Converter::convertMixedToBoolean(sSun));
}

bool Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::getSun() {
  return getModel()->getSun();
}


Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek() {
  Model_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek* oDepartureDaysOfWeek = new Model_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek();
  setModel(*oDepartureDaysOfWeek);
  
  Map<Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek>();
  oMap->setMapper(this);
  oMap->addFunction("Mon", &Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::setMon);
  oMap->addFunction("Tue", &Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::setTue);
  oMap->addFunction("Wed", &Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::setWed);
  oMap->addFunction("Thu", &Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::setThu);
  oMap->addFunction("Fri", &Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::setFri);
  oMap->addFunction("Sat", &Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::setSat);
  oMap->addFunction("Sun", &Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::setSun);
  
  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek>();
  oParser->setMap(*oMap);
  oParser->setRootTag("DepartureDaysOfWeek");
  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek::~Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek() {

}
