#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_BOOKINGRULES_BOOKINGRULE_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_BOOKINGRULES_BOOKINGRULE_H__

#include "Mapper.h"
#include "../Models/BookingRule.h"

class Mapper_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule : public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule, Model_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule> {
public:
  void processCancelPenalty(std::string);
  void processLengthsOfStay(std::string);
  void processDOWRestrictions(std::string);
  
  void setStart(std::string);
  std::string getStart();
  
  void setEnd(std::string);
  std::string getEnd();
  
  Mapper_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule();
  ~Mapper_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule();
protected:
private:
};

#endif
