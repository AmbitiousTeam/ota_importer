#include "Quantities.h"

void Mapper_OTA_HotelRatePlanNotifRQ_Quantities::setMinBillableGuests(std::string sMinBillableGuests) {
  getModel()->setMinBillableGuests(Converter::convertMixedToInt(sMinBillableGuests));
}

Mapper_OTA_HotelRatePlanNotifRQ_Quantities::Mapper_OTA_HotelRatePlanNotifRQ_Quantities() {
  Model_OTA_HotelRatePlanNotifRQ_Quantities* oQuantities = new Model_OTA_HotelRatePlanNotifRQ_Quantities();
  setModel(*oQuantities);
  
  Map<Mapper_OTA_HotelRatePlanNotifRQ_Quantities>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ_Quantities>();
  oMap->setMapper(this);
  oMap->addFunction("MinBillableGuests", &Mapper_OTA_HotelRatePlanNotifRQ_Quantities::setMinBillableGuests);
  
  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_Quantities>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_Quantities>();
  oParser->setMap(*oMap);
  oParser->setRootTag("Quantities");
  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ_Quantities::~Mapper_OTA_HotelRatePlanNotifRQ_Quantities() {

}
