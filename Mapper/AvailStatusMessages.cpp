#include "AvailStatusMessages.h"

void Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages::setHotelCode(std::string sHotelCode) {
  getModel()->setHotelCode(sHotelCode);
}

std::string Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages::getHotelCode() {
  return getModel()->getHotelCode();
}

void Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages::setDelta(std::string sDelta) {
  getModel()->setDelta(Converter::convertMixedToBoolean(sDelta));
}

bool Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages::getDelta() {
  return getModel()->getDelta();
}

void Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages::setIdCon(std::string sIdCon) {
  getModel()->setIdCon(sIdCon);
}

std::string Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages::getIdCon() {
  return getModel()->getIdCon();
}

void Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages::collectAvailStatusMessage(std::string sXmlContent) {
  Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage* oAvailStatusMessage = new Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage();
  oAvailStatusMessage->setXmlString(sXmlContent);
  oAvailStatusMessage->processXml();
  getModel()->add(*oAvailStatusMessage->getModel());
}

/** CTOR **/
Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages::Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages() {
  Model_OTA_HotelAvailNotifRQ_AvailStatusMessages* oAvailStatusMessages = new Model_OTA_HotelAvailNotifRQ_AvailStatusMessages();
  setModel(*oAvailStatusMessages);
  
  Map<Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages>* oMap = new Map<Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages>();
  oMap->setMapper(this);
  
  oMap->addFunction("HotelCode", &Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages::setHotelCode);
  oMap->addFunction("IdCon", &Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages::setIdCon);
  oMap->addFunction("Delta", &Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages::setDelta);
  oMap->addFunction("AvailStatusMessage", &Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages::collectAvailStatusMessage);
  
  Service_XmlParser<Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages>* oParser = new Service_XmlParser<Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages>();
  oParser->setMap(*oMap);
  oParser->setRootTag("AvailStatusMessages");
  setParser(*oParser);
}

Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages::~Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages() {

}
