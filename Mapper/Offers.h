#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_OFFERS_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_OFFERS_H__

#include "Mapper.h"
#include "../Tools/Converter.h"

#include "Offer.h"
#include "../Models/Offers.h"
#include "../Models/Offer.h"

class Mapper_OTA_HotelRatePlanNotifRQ_Offers :
  public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_Offers, Model_OTA_HotelRatePlanNotifRQ_Offers> {
public:
  void collectOffer(std::string);
  
  Mapper_OTA_HotelRatePlanNotifRQ_Offers();
  ~Mapper_OTA_HotelRatePlanNotifRQ_Offers();
protected:
private:
};

#endif
