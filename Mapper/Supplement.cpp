#include "Supplement.h"

void Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setAdditionalGuestNumber(std::string sAdditionalGuestNumber) {
  getModel()->setAdditionalGuestNumber(Converter::convertMixedToInt(sAdditionalGuestNumber));
}

void Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setAddToBasicRate(std::string sAddToBasicRate) {
  getModel()->setAddToBasicRateIndicator(Converter::convertMixedToBoolean(sAddToBasicRate));
}

void Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setAgeQualifyingCode(std::string sAgeQualifyingCode) {
  getModel()->setAgeQualifyingCode(Converter::convertMixedToInt(sAgeQualifyingCode));
}

void Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setAmount(std::string sAmount) {
  getModel()->setAmount(Converter::convertMixedToDouble(sAmount));
}

void Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setChargeType(std::string sChargeType) {
  getModel()->setChargeType(Converter::convertMixedToInt(sChargeType));
}

void Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setCurrencyCode(std::string sCurrencyCode) {
  getModel()->setCurrencyCode(sCurrencyCode);
}

void Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setStart(std::string sStart) {
  getModel()->setStart(sStart);
}

void Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setEnd(std::string sEnd) {
  getModel()->setEnd(sEnd);
}

void Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setMinAge(std::string sMinAge) {
  getModel()->setMinAge(Converter::convertMixedToInt(sMinAge));
}

void Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setMaxAge(std::string setMaxAge) {
  getModel()->setMaxAge(Converter::convertMixedToInt(setMaxAge));
}

void Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setSupplementType(std::string sSupplementType) {
  getModel()->setSupplementType(sSupplementType);
}

void Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setPercent(std::string sPercent) {
  getModel()->setPercent(Converter::convertMixedToDouble(sPercent));
}

void Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setInvCode(std::string sInvCode) {
  getModel()->setInvCode(sInvCode);
}

void Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setInvType(std::string sInvType) {
  getModel()->setInvType(sInvType);
}

void Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setMandatoryIndicator(std::string sMandatoryIndicator) {
  getModel()->setMandatoryIndicator(Converter::convertMixedToBoolean(sMandatoryIndicator));
}

Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement() {
  Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement* oSupplement = new Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement();
  setModel(*oSupplement);
  
  Map<Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement>();
  oMap->setMapper(this);
  oMap->addFunction("MinAge", &Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setMinAge);
  oMap->addFunction("MaxAge", &Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setMaxAge);
  oMap->addFunction("Amount", &Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setAmount);
  oMap->addFunction("Percent", &Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setPercent);
  oMap->addFunction("CurencyCode", &Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setCurrencyCode);
  oMap->addFunction("CurrencyCode", &Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setCurrencyCode);
  oMap->addFunction("InvCode", &Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setInvCode);
  oMap->addFunction("InvType", &Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setInvType);
  oMap->addFunction("MandatoryIndicator", &Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setMandatoryIndicator);
  oMap->addFunction("Start", &Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setStart);
  oMap->addFunction("End", &Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setEnd);
  oMap->addFunction("SupplementType", &Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setSupplementType);
  oMap->addFunction("ChargeType", &Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setChargeType);
  oMap->addFunction("AdditionalGuestNumber", &Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setAdditionalGuestNumber);
  oMap->addFunction("AddToBasicRate", &Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setAddToBasicRate);
  oMap->addFunction("AgeQualifyingCode", &Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::setAgeQualifyingCode);
  
  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement>();
  oParser->setMap(*oMap);
  oParser->setRootTag("Supplement");
  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement::~Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement() {

}
