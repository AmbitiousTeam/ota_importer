#include "OfferRule.h"

void Mapper_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule::collectDateRestrictions(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "Verarbeite DateRestrictions mit Content : " << sXmlContent << std::endl;}
  Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions* oDateRestrictions = new Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions();
  oDateRestrictions->setXmlString(sXmlContent);
  oDateRestrictions->processXml();
  getModel()->setDateRestrictions(*oDateRestrictions->getModel());
}

void Mapper_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule::collectLengthsOfStay(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "Verarbeite LengthsOfStay mit Content : " << sXmlContent << std::endl;}
  Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay* oLengthsOfStay = new Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay();
  oLengthsOfStay->setXmlString(sXmlContent);
  oLengthsOfStay->processXml();
  getModel()->setLengthsOfStay(*oLengthsOfStay->getModel());
}

Mapper_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule::Mapper_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule() {
  Model_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule* oOfferRule = new Model_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule();
  
//   Model_OTA_HotelRatePlanNotifRQ_DateRestrictions* oDateRestrictions = new Model_OTA_HotelRatePlanNotifRQ_DateRestrictions();
//   oOfferRule->setDateRestrictions(*oDateRestrictions);

  setModel(*oOfferRule);
  
  Map<Mapper_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule>();
  oMap->setMapper(this);
  oMap->addFunction("DateRestriction", &Mapper_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule::collectDateRestrictions);
  oMap->addFunction("LengthsOfStay", &Mapper_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule::collectLengthsOfStay);
  
  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule>();
  oParser->setMap(*oMap);
  oParser->setRootTag("OfferRule");
  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule::~Mapper_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule() {

}
