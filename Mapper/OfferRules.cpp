#include "OfferRules.h"

void Mapper_OTA_HotelRatePlanNotifRQ_OfferRules::collectOfferRules(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "Verarbeite OfferRule mit " << sXmlContent << std::endl;}
  Mapper_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule* oOfferRule = new Mapper_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule();
  oOfferRule->setXmlString(sXmlContent);
  oOfferRule->processXml();
  getModel()->add(*oOfferRule->getModel());
}

Mapper_OTA_HotelRatePlanNotifRQ_OfferRules::Mapper_OTA_HotelRatePlanNotifRQ_OfferRules() {
  Model_OTA_HotelRatePlanNotifRQ_OfferRules* oOfferRules = new Model_OTA_HotelRatePlanNotifRQ_OfferRules();
  setModel(*oOfferRules);
  
  Map<Mapper_OTA_HotelRatePlanNotifRQ_OfferRules>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ_OfferRules>();
  oMap->setMapper(this);
  oMap->addFunction("OfferRule", &Mapper_OTA_HotelRatePlanNotifRQ_OfferRules::collectOfferRules);
  
  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_OfferRules>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_OfferRules>();
  oParser->setMap(*oMap);
  oParser->setRootTag("OfferRules");
  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ_OfferRules::~Mapper_OTA_HotelRatePlanNotifRQ_OfferRules() {

}
