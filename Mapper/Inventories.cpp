#include "Inventories.h"

void Mapper_OTA_HotelRatePlanNotifRQ_Inventories::collectInventories(std::string sXmlContent) {
  Mapper_OTA_HotelRatePlanNotifRQ_Inventories_Inventory* oInventory = new Mapper_OTA_HotelRatePlanNotifRQ_Inventories_Inventory();
  oInventory->setXmlString(sXmlContent);
  oInventory->processXml();
  getModel()->add(*oInventory->getModel());
}

Mapper_OTA_HotelRatePlanNotifRQ_Inventories::Mapper_OTA_HotelRatePlanNotifRQ_Inventories() {
  Model_OTA_HotelRatePlanNotifRQ_Inventories* oInventories = new Model_OTA_HotelRatePlanNotifRQ_Inventories();
  setModel(*oInventories);
  
  Map<Mapper_OTA_HotelRatePlanNotifRQ_Inventories>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ_Inventories>();
  oMap->setMapper(this);
  oMap->addFunction("Inventory", &Mapper_OTA_HotelRatePlanNotifRQ_Inventories::collectInventories);
  
  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_Inventories>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_Inventories>();
  oParser->setMap(*oMap);
  oParser->setRootTag("Inventories");
  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ_Inventories::~Mapper_OTA_HotelRatePlanNotifRQ_Inventories() {

}
