#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_OFFERS_OFFER_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_OFFERS_OFFER_H__

#include <string>

#include "Mapper.h"
#include "../Models/Offer.h"

#include "OfferRules.h"
#include "CompatibleOffers.h"
#include "OfferDescription.h"

#include "Inventories.h"
#include "Discount.h"

class Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer : public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer, Model_OTA_HotelRatePlanNotifRQ_Offers_Offer> {
public:
  void collectOfferRules(std::string);
  void collectCompatibleOffers(std::string);
  void collectInventories(std::string);
  void processDiscount(std::string);
  void processOfferDescription(std::string);
  
  void setRph(std::string);
  std::string getRph();
  
  void setApplicationOrder(std::string);
  int getApplicationOrder();
  
  void setOfferCode(std::string);
  std::string getOfferCode();
  
  Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer();
  ~Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer();
protected:
private:
};

#endif
