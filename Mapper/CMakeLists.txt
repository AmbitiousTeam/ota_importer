message("Compile Mapper!")

set(MapperSrc 
  AdditionalGuestAmounts.cpp
  AdditionalGuestAmount.cpp
  AmountPercent.cpp
  AvailStatusMessages.cpp 
  AvailStatusMessage.cpp 
  ArrivalDaysOfWeek.cpp
  BaseByGuestAmts.cpp
  BaseByGuestAmt.cpp
  BookingRules.cpp
  BookingRule.cpp
  CancelPenalty.cpp
  CompatibleOffers.cpp
  CompatibleOffer.cpp
  DateRestrictions.cpp
  DateRestriction.cpp
  Deadline.cpp
  DepartureDaysOfWeek.cpp
  Discount.cpp
  DOW_Restrictions.cpp
  GuestRoom.cpp
  HotelAvailNotifRQ.cpp
  HotelRatePlanNotifRQ.cpp 
  Quantities.cpp
  RestrictionStatus.cpp 
  RatePlans.cpp
  RatePlan.cpp
  Rates.cpp
  Rate.cpp
  RoomGroup.cpp
  Occupancy.cpp
  Occupancies.cpp
  OfferRules.cpp
  OfferRule.cpp
  OfferDescription.cpp
  StatusApplicationControl.cpp 
  Inventories.cpp
  Inventory.cpp
  LengthsOfStay.cpp
  LengthOfStay.cpp
  Offers.cpp
  SellableProducts.cpp
  Supplements.cpp
  Supplement.cpp
  Offer.cpp
  SellableProduct.cpp
  Supplements.cpp
  TPAExtensions.cpp
  TPAExtension.cpp
)

add_library(Mapper SHARED ${MapperSrc})

install(TARGETS Mapper DESTINATION lib)
