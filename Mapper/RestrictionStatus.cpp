#include "RestrictionStatus.h"

void Mapper_OTA_HotelAvailNotifRQ_RestrictionStatus::setStatus(std::string sStatus) {
  getModel()->setStatus(sStatus);
}

std::string Mapper_OTA_HotelAvailNotifRQ_RestrictionStatus::getStatus() {
  return getModel()->getStatus();
}

Mapper_OTA_HotelAvailNotifRQ_RestrictionStatus::Mapper_OTA_HotelAvailNotifRQ_RestrictionStatus() {
  Model_OTA_HotelAvailNotifRQ_RestrictionStatus* oRestrictionStatus = new Model_OTA_HotelAvailNotifRQ_RestrictionStatus();
  setModel(*oRestrictionStatus);
  
  Map<Mapper_OTA_HotelAvailNotifRQ_RestrictionStatus>* oMap = new Map<Mapper_OTA_HotelAvailNotifRQ_RestrictionStatus>();
  oMap->setMapper(this);
  oMap->addFunction("Status", &Mapper_OTA_HotelAvailNotifRQ_RestrictionStatus::setStatus);
  
  Service_XmlParser<Mapper_OTA_HotelAvailNotifRQ_RestrictionStatus>* oParser = new Service_XmlParser<Mapper_OTA_HotelAvailNotifRQ_RestrictionStatus>();
  oParser->setMap(*oMap);
  oParser->setRootTag("RestrictionStatus");
  setParser(*oParser);
}

Mapper_OTA_HotelAvailNotifRQ_RestrictionStatus::~Mapper_OTA_HotelAvailNotifRQ_RestrictionStatus() {
  
}
