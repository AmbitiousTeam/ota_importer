#include "AvailStatusMessage.h"

void Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage::collectRestrictionStatus(std::string sXmlContent) {
  Mapper_OTA_HotelAvailNotifRQ_RestrictionStatus* oRestrictionStatus = new Mapper_OTA_HotelAvailNotifRQ_RestrictionStatus();
  oRestrictionStatus->setXmlString(sXmlContent);
  oRestrictionStatus->processXml();
  getModel()->setRestrictionStatus(*oRestrictionStatus->getModel());
}

void Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage::collectStatusApplicationControl(std::string sXmlContent) {
  Mapper_OTA_HotelAvailNotifRQ_StatusApplicationControl* oStatusApplicationControl = new Mapper_OTA_HotelAvailNotifRQ_StatusApplicationControl();
  oStatusApplicationControl->setXmlString(sXmlContent);
  oStatusApplicationControl->processXml();
  getModel()->setStatusApplicationControl(*oStatusApplicationControl->getModel());
}

/** CTOR **/
Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage::Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage() {
  Model_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage* oAvailStatusMessage = new Model_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage();
  setModel(*oAvailStatusMessage);
  
  Map<Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage>* oMap = new Map<Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage>();
  oMap->setMapper(this);
  
  oMap->addFunction("RestrictionStatus", &Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage::collectRestrictionStatus);
  oMap->addFunction("StatusApplicationControl", &Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage::collectStatusApplicationControl);
  
  Service_XmlParser<Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage>* oParser = new Service_XmlParser<Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage>();
  oParser->setMap(*oMap);
  oParser->setRootTag("AvailStatusMessage");
  setParser(*oParser);
}

Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage::~Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage() {

}
