#include "RatePlans.h"

void Mapper_OTA_HotelRatePlanNotifRQ_RatePlans::setDelta(std::string sDelta) {
  getModel()->setDelta(sDelta);
}

bool Mapper_OTA_HotelRatePlanNotifRQ_RatePlans::getDelta() {
  return getModel()->getDelta();
}

void Mapper_OTA_HotelRatePlanNotifRQ_RatePlans::setHotelCode(std::string sHotelCode) {
  getModel()->setHotelCode(sHotelCode);
}

std::string Mapper_OTA_HotelRatePlanNotifRQ_RatePlans::getHotelCode() {
  return getModel()->getHotelCode();
}

void Mapper_OTA_HotelRatePlanNotifRQ_RatePlans::setHotelName(std::string sHotelName) {
  getModel()->setHotelName(sHotelName);
}

std::string Mapper_OTA_HotelRatePlanNotifRQ_RatePlans::getHotelName() {
  return getModel()->getHotelName();
}

void Mapper_OTA_HotelRatePlanNotifRQ_RatePlans::setIdCon(std::string sIdCon) {
  getModel()->setIdCon(sIdCon);
}

std::string Mapper_OTA_HotelRatePlanNotifRQ_RatePlans::getIdCon() {
  return getModel()->getIdCon();
}

void Mapper_OTA_HotelRatePlanNotifRQ_RatePlans::setMultiMarkup(std::string sMultiMarkup) {
  getModel()->setMultiMarkup(sMultiMarkup);
}

bool Mapper_OTA_HotelRatePlanNotifRQ_RatePlans::getMultiMarkup() {
  return getModel()->getMultiMarkup();
}

void Mapper_OTA_HotelRatePlanNotifRQ_RatePlans::setMultiMarkupPriority(std::string sMultiMarkupPriority) {
  getModel()->setMultiMarkupPriority(sMultiMarkupPriority);
}

int Mapper_OTA_HotelRatePlanNotifRQ_RatePlans::getMultiMarkupPriority() {
  return getModel()->getMultiMarkupPriority();
}

void Mapper_OTA_HotelRatePlanNotifRQ_RatePlans::collectRatePlan(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "RatePlansMapper - Verarbeite RatePlan mit " << sXmlContent << std::endl;};
  Mapper_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan* oRatePlan = new Mapper_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan();
  oRatePlan->setXmlString(sXmlContent);
  oRatePlan->processXml();
  getModel()->add(*oRatePlan->getModel());
  if (DEBUG_MODE) {std::cout << "RatePlansMapper - Anzahl RatePlan in der Collection : " << getModel()->getCount() << std::endl;};
}

void Mapper_OTA_HotelRatePlanNotifRQ_RatePlans::collectBookingRules(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "RatePlansMapper - Verarbeite BookingRules mit " << sXmlContent << std::endl;};
  Mapper_OTA_HotelRatePlanNotifRQ_BookingRules* oBookingRules = new Mapper_OTA_HotelRatePlanNotifRQ_BookingRules();
  oBookingRules->setXmlString(sXmlContent);
  oBookingRules->processXml();
  getModel()->setBookingRules(*oBookingRules->getModel());
}

void Mapper_OTA_HotelRatePlanNotifRQ_RatePlans::collectSupplements(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "RatePlansMapper - Verarbeite Supplements mit " << sXmlContent << std::endl;};
  Mapper_OTA_HotelRatePlanNotifRQ_Supplements* oSupplements = new Mapper_OTA_HotelRatePlanNotifRQ_Supplements();
  oSupplements->setXmlString(sXmlContent);
  oSupplements->processXml();
  getModel()->setSupplements(*oSupplements->getModel());
}

void Mapper_OTA_HotelRatePlanNotifRQ_RatePlans::collectOffers(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "RatePlansMapper - Verarbeite Offers mit " << sXmlContent << std::endl;};
  Mapper_OTA_HotelRatePlanNotifRQ_Offers* oOffers = new Mapper_OTA_HotelRatePlanNotifRQ_Offers();
  oOffers->setXmlString(sXmlContent);
  oOffers->processXml();
  getModel()->setOffers(*oOffers->getModel());
}


Mapper_OTA_HotelRatePlanNotifRQ_RatePlans::Mapper_OTA_HotelRatePlanNotifRQ_RatePlans() {
  Model_OTA_HotelRatePlanNotifRQ_RatePlans* oRatePlans = new Model_OTA_HotelRatePlanNotifRQ_RatePlans();
  setModel(*oRatePlans);
  
  Map<Mapper_OTA_HotelRatePlanNotifRQ_RatePlans>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ_RatePlans>();
  oMap->setMapper(this);

  oMap->addFunction("HotelCode", &Mapper_OTA_HotelRatePlanNotifRQ_RatePlans::setHotelCode);
  oMap->addFunction("HotelName", &Mapper_OTA_HotelRatePlanNotifRQ_RatePlans::setHotelName);
  oMap->addFunction("IdCon", &Mapper_OTA_HotelRatePlanNotifRQ_RatePlans::setIdCon);
  oMap->addFunction("Delta", &Mapper_OTA_HotelRatePlanNotifRQ_RatePlans::setDelta);
  oMap->addFunction("Multimarkup", &Mapper_OTA_HotelRatePlanNotifRQ_RatePlans::setMultiMarkup);
  oMap->addFunction("MultimarkupPriority", &Mapper_OTA_HotelRatePlanNotifRQ_RatePlans::setMultiMarkupPriority);
  oMap->addFunction("RatePlan", &Mapper_OTA_HotelRatePlanNotifRQ_RatePlans::collectRatePlan);
  oMap->addFunction("Offers", &Mapper_OTA_HotelRatePlanNotifRQ_RatePlans::collectOffers);
  oMap->addFunction("BookingRules", &Mapper_OTA_HotelRatePlanNotifRQ_RatePlans::collectBookingRules);
  oMap->addFunction("Supplements", &Mapper_OTA_HotelRatePlanNotifRQ_RatePlans::collectSupplements);
  
  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_RatePlans>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_RatePlans>();
  oParser->setMap(*oMap);
  oParser->setRootTag("RatePlans");
  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ_RatePlans::~Mapper_OTA_HotelRatePlanNotifRQ_RatePlans() {

}


