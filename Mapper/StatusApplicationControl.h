#ifndef __MAPPER_OTA_HOTELAVAILNOTIFRQ_STATUSAPPLICATIONCONTROL_H__
#define __MAPPER_OTA_HOTELAVAILNOTIFRQ_STATUSAPPLICATIONCONTROL_H__

#include "Mapper.h"
#include "../Models/StatusApplicationControl.h"

class Mapper_OTA_HotelAvailNotifRQ_StatusApplicationControl :
   public Mapper<Mapper_OTA_HotelAvailNotifRQ_StatusApplicationControl, Model_OTA_HotelAvailNotifRQ_StatusApplicationControl> {
public:
  void setStart(std::string);
  std::string getStart();
  void setEnd(std::string);
  std::string getEnd();
  void setInvType(std::string);
  std::string getInvType();
  void setInvCode(std::string);
  std::string getInvCode();
  
  Mapper_OTA_HotelAvailNotifRQ_StatusApplicationControl();
  ~Mapper_OTA_HotelAvailNotifRQ_StatusApplicationControl();
protected:
private:
};

#endif
