#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_QUANTITIES_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_QUANTITIES_H__

#include "Mapper.h"
#include "../Models/Quantities.h"

class Mapper_OTA_HotelRatePlanNotifRQ_Quantities : public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_Quantities, Model_OTA_HotelRatePlanNotifRQ_Quantities> {
public:
  void setMinBillableGuests(std::string);
  
  Mapper_OTA_HotelRatePlanNotifRQ_Quantities();
  ~Mapper_OTA_HotelRatePlanNotifRQ_Quantities();
protected:
private:
};
#endif
