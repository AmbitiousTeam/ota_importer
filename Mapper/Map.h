#ifndef __MAP_H__
#define __MAP_H__

#include "../App/Defines.cpp"

#include <iostream>
#include <typeinfo>
#include <string>
#include <map>

template<typename T_Mapper>class Map {
public:
  typedef void(T_Mapper::*memberFunction)(std::string);

  /** setzt den Mapper **/
  void setMapper(T_Mapper*);
  T_Mapper* getMapper();

  void addFunction(std::string, memberFunction);
  bool operator()(std::string, std::string);
  
  Map();
  ~Map();
protected:
private:
  std::string _sXmlString;
  T_Mapper* _oMapper = NULL;

  std::map<std::string, memberFunction> _funcMap;
};

template<typename T_Mapper>void Map<T_Mapper>::setMapper(T_Mapper* oMapper) {
  _oMapper = oMapper;
}

template<typename T_Mapper>T_Mapper* Map<T_Mapper>::getMapper() {
  return _oMapper;
}

template<typename T_Mapper>void Map<T_Mapper>::addFunction(std::string funcName, memberFunction func) {
  _funcMap[funcName] = func;
}

template<typename T_Mapper>bool Map<T_Mapper>::operator()(std::string sFuncName, std::string sValue) {
  typename std::map<std::string, memberFunction>::const_iterator itMap;

  itMap = _funcMap.find(sFuncName);
  bool bReturn = false;

  if (itMap != _funcMap.end()) {
    (_oMapper->*itMap->second)(sValue);
  } else {
    if (DEBUG_MODE) {std::cout << "Kann mit Element " << sFuncName << " und Content (" + sValue + ") in Aktueller Map (" << typeid(_oMapper).name() << ") nichts anfangen" << std::endl;}
  }
  return bReturn;
}

template<typename T_Mapper>Map<T_Mapper>::Map() {
}

template<typename T_Mapper>Map<T_Mapper>::~Map() {
  delete this->_oMapper;
}

#endif
