#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_SELLABLEPRODUCTS_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_SELLABLEPRODUCTS_H__

#include "Mapper.h"
#include "SellableProduct.h"
#include "../Models/SellableProducts.h"

class Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts : public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts, Model_OTA_HotelRatePlanNotifRQ_SellableProducts> {
public:
  void collectSellableProducts(std::string);
  
  Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts();
  ~Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts();
protected:
private:
};

#endif
