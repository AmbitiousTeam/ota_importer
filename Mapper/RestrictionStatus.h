#ifndef __MAPPER_OTA_HOTELAVAILNOTIFRQ_RESTRICTIONSTATUS_H__
#define __MAPPER_OTA_HOTELAVAILNOTIFRQ_RESTRICTIONSTATUS_H__

#include "Mapper.h"

#include "../Models/RestrictionStatus.h"

class Mapper_OTA_HotelAvailNotifRQ_RestrictionStatus :
   public Mapper<Mapper_OTA_HotelAvailNotifRQ_RestrictionStatus, Model_OTA_HotelAvailNotifRQ_RestrictionStatus> {
public:
  void setStatus(std::string);
  std::string getStatus();
  
  Mapper_OTA_HotelAvailNotifRQ_RestrictionStatus();
  ~Mapper_OTA_HotelAvailNotifRQ_RestrictionStatus();
private:
};

#endif
