#include "AmountPercent.h"

void Mapper_OTA_HotelRatePlanNotifRQ_AmountPercent::setAmount(std::string sAmount) {
  getModel()->setAmount(Converter::convertMixedToDouble(sAmount));
}

float Mapper_OTA_HotelRatePlanNotifRQ_AmountPercent::getAmount() {
  return getModel()->getAmount();
}

void Mapper_OTA_HotelRatePlanNotifRQ_AmountPercent::setCurrencyCode(std::string sCurrencyCode) {
  getModel()->setCurrencyCode(sCurrencyCode);
}

std::string Mapper_OTA_HotelRatePlanNotifRQ_AmountPercent::getCurrencyCode() {
  return getModel()->getCurrencyCode();
}

void Mapper_OTA_HotelRatePlanNotifRQ_AmountPercent::setPercent(std::string sPercent) {
  getModel()->setPercent(Converter::convertMixedToDouble(sPercent));
}

float Mapper_OTA_HotelRatePlanNotifRQ_AmountPercent::getPercent() {
  return getModel()->getPercent();
}

Mapper_OTA_HotelRatePlanNotifRQ_AmountPercent::Mapper_OTA_HotelRatePlanNotifRQ_AmountPercent() {
  Model_OTA_HotelRatePlanNotifRQ_AmountPercent* oAmountPercent = new Model_OTA_HotelRatePlanNotifRQ_AmountPercent(); 
  setModel(*oAmountPercent);
  
  Map<Mapper_OTA_HotelRatePlanNotifRQ_AmountPercent>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ_AmountPercent>();
  oMap->setMapper(this);
  
  oMap->addFunction("CurrencyCode", &Mapper_OTA_HotelRatePlanNotifRQ_AmountPercent::setCurrencyCode);
  oMap->addFunction("Amount", &Mapper_OTA_HotelRatePlanNotifRQ_AmountPercent::setAmount);
  oMap->addFunction("Percent", &Mapper_OTA_HotelRatePlanNotifRQ_AmountPercent::setPercent);
  
  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_AmountPercent>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_AmountPercent>();
  oParser->setMap(*oMap);
  oParser->setRootTag("AmountPercent");
  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ_AmountPercent::~Mapper_OTA_HotelRatePlanNotifRQ_AmountPercent() {

}
