#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_BOOKINGRULES_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_BOOKINGRULES_H__

#include "Mapper.h"
#include "BookingRule.h"
#include "../Models/BookingRules.h"

class Mapper_OTA_HotelRatePlanNotifRQ_BookingRules : public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_BookingRules, Model_OTA_HotelRatePlanNotifRQ_BookingRules> {
public:
  void collectBookingRule(std::string);
  
  Mapper_OTA_HotelRatePlanNotifRQ_BookingRules();
  ~Mapper_OTA_HotelRatePlanNotifRQ_BookingRules();
protected:
private:
};

#endif
