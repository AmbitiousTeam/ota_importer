#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_COMPATIBLEOFFERS_COMPATIBLEOFFER_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_COMPATIBLEOFFERS_COMPATIBLEOFFER_H__

#include <string>
#include "Mapper.h"
#include "../Models/CompatibleOffer.h"

class Mapper_OTA_HotelRatePlanNotifRQ_CompatibleOffers_CompatibleOffer : public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_CompatibleOffers_CompatibleOffer, Model_OTA_HotelRatePlanNotifRQ_CompatibleOffers_CompatibleOffer> {
public:
  
  Mapper_OTA_HotelRatePlanNotifRQ_CompatibleOffers_CompatibleOffer();
  ~Mapper_OTA_HotelRatePlanNotifRQ_CompatibleOffers_CompatibleOffer();
protected:
private:
};
#endif
