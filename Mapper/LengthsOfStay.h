#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_LENGTHSOFSTAY_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_LENGTHSOFSTAY_H__

#include "Mapper.h"
#include "LengthOfStay.h"

#include "../Models/LengthsOfStay.h"
#include "../Models/LengthOfStay.h"

class Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay : public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay, Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay> {
public:
  void collectLenghtsOfStay(std::string);
  
  Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay();
  ~Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay();
protected:
private:
};

#endif
