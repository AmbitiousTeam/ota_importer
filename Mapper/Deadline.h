#ifndef __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_DEADLINE_H__
#define __MAPPER_OTA_HOTELRATEPLANNOTIFRQ_DEADLINE_H__

#include "Mapper.h"
#include "../Models/Deadline.h"

class Mapper_OTA_HotelRatePlanNotifRQ_Deadline : public Mapper<Mapper_OTA_HotelRatePlanNotifRQ_Deadline, Model_OTA_HotelRatePlanNotifRQ_Deadline> {
public:
  void setOffsetUnitMultiplier(std::string);
  int getOffsetUnitMultiplier();
  
  void setOffsetTimeUnit(std::string);
  std::string getOffsetTimeUnit();
  
  void setOffsetDropTime(std::string);
  std::string getOffsetDropTime();
  
  Mapper_OTA_HotelRatePlanNotifRQ_Deadline();
  ~Mapper_OTA_HotelRatePlanNotifRQ_Deadline();
protected:
private:
};

#endif
