#ifndef __SERVICE_OTA_HOTELRATEPLANNOTIFRQ_OCCUPANCIES_H__
#define __SERVICE_OTA_HOTELRATEPLANNOTIFRQ_OCCUPANCIES_H__

#include "Service.h"
#include "../Models/Occupancies.h"

class Service_OTA_HotelRatePlanNotifRQ_Occupancies : public Service<Service_OTA_HotelRatePlanNotifRQ_Occupancies, Model_OTA_HotelRatePlanNotifRQ_Occupancies> {
public:
  
  Service_OTA_HotelRatePlanNotifRQ_Occupancies();
  ~Service_OTA_HotelRatePlanNotifRQ_Occupancies();
protected:
private:
};

#endif
