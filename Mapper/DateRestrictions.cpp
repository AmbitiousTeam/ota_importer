#include "DateRestrictions.h"

void Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions::collectDateRestrictions(std::string sXmlContent) {
  if (DEBUG_MODE) {std::cout << "Verarbeite DateRestriction mit " << sXmlContent << std::endl;}
  Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction* oDateRestriction = new Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction();
  oDateRestriction->setXmlString(sXmlContent);
  oDateRestriction->processXml();
  getModel()->add(*oDateRestriction->getModel());
}

Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions::Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions() {
  Model_OTA_HotelRatePlanNotifRQ_DateRestrictions* oDateRestrictions = new Model_OTA_HotelRatePlanNotifRQ_DateRestrictions();
  setModel(*oDateRestrictions);
  
  Map<Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions>* oMap = new Map<Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions>();
  oMap->setMapper(this);
  oMap->addFunction("DateRestriction", &Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions::collectDateRestrictions);
  
  Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions>* oParser = new Service_XmlParser<Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions>();
  oParser->setEliminateRedundantTags(true);
  oParser->setMap(*oMap);
  oParser->setRootTag("DateRestrictions");
  setParser(*oParser);
}

Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions::~Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions() {

}
