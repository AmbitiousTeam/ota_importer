#include <iostream>
#include <fstream>
#include <string>

#include "Defines.cpp"
#include "Import.h"
#include "../Tools/FileSplitter.h"
#include "../Exceptions/HotelNotFoundException.cpp"

using namespace std;

int main(int argc, char **argv) {
  
  try {
    Tool_FileSplitter* oFileSplitter = new Tool_FileSplitter();
    oFileSplitter->process();
    
    return 0;
    
    Service_Import* oImportService = new Service_Import();
    oImportService->start();
//   } catch (HotelNotFoundException& oException) {
//     std::cout << oException.what() << std::endl;
  } catch (const std::exception& oException) {
    std::cout << oException.what() << std::endl;
  } catch (...) {
    //     exception_ptr eptr = std::current_exception();
    //     std::cout << eptr.what() << std::endl;
    std::cout << "Es ist ein interner Fehler aufgetreten!" << std::endl;
  }
  
  return 0;
}
