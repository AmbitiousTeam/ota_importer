#ifndef __DEFINES_CPP__
#define __DEFINES_CPP__

static const int AGEQUALIFYINGCODEADULTS = 10;
static const int AGEQUALIFYINGCODECHILDREN = 8;
static const int AGEQUALIFYINGCODEINFANT = 7;

static const int DEBUG_MODE = 1;

#endif
