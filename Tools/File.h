#ifndef __TOOL_FILE_H__
#define __TOOL_FILE_H__

#include <string>
#include "../Models/Collection.h"

class Tool_File {
    public:
    static bool open(const std::string&);
    static bool close();
  
  bool openFile(const std::string&);
  bool closeFile() const;
  
  static std::string load(std::string&);
  bool loadContent(std::string);
  static bool save(std::string&, std::string&);
  bool saveContent(std::string);
  bool setContent(std::string);
  std::string getContent();
protected:
private:     
  
  static Model_Collection<std::ofstream>* _oFileHandleCollection;
  std::string _sFilePathName;
  std::string _sFileContent;
};

#endif
