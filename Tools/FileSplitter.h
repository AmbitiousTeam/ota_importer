#ifndef __TOOL_FILESPLITTER_H__
#define __TOOL_FILESPLITTER_H__

#include <iostream>
#include <string>

#include "File.h"

class Tool_FileSplitter {
public:
     
  bool process(void);
protected:
private:
  std::string _loadContent(std::string& sFilePathName);
  bool _split(std::string& sContent);
  std::string _generateCurrentFilePathName(std::string& sFilePathName);
  
  std::string _sInputFilePathName;
  std::string _sOutputFilePathName;
  std::string _sCurrentContent;
  std::string _sFilePathName;
  std::string _sCurrentFilePathName;
  
  Tool_File* _oOutputFile = NULL;
  Tool_File* _oInputFile = NULL;
};

#endif