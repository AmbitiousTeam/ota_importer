#include <iostream>
#include <fstream>
#include <string>

#include "File.h"

#include "../Tools/String.cpp"
#include "../Tools/Logger.h"

Model_Collection<std::ofstream>* Tool_File::_oFileHandleCollection = new Model_Collection<std::ofstream>();

bool Tool_File::loadContent(std::string sFilePathName) {
  std::ifstream hInputFile(sFilePathName.c_str());
  std::string sLine;
  
  if (hInputFile.is_open()) {
    unsigned long iCountLine = 0;
    std::string sContent = "";
    while (std::getline(hInputFile, sLine)) {
      ++iCountLine;
      sContent += trim(sLine);
    }
    setContent(sContent);
    hInputFile.close();
  } else {
    std::cout << "Konnte Datei " << sFilePathName << " nicht öffnen!" << std::endl;
  }
  return 0;
}

bool Tool_File::saveContent(std::string sFilePathName) {
  std::ofstream hOutputFile(sFilePathName.c_str());
  if (hOutputFile.is_open()) {
    hOutputFile << getContent() << std::endl;
    hOutputFile.close();
  }
  else std::cout << "Unable to open file";
}

bool Tool_File::setContent(std::string sContent) {
  _sFileContent = sContent;
}

std::string Tool_File::getContent() {
  return _sFileContent;
}

bool Tool_File::closeFile() const {

}

bool Tool_File::openFile(const std::string& sFilePathName) {

}

std::string Tool_File::load(std::string& sFilePathName) {
  std::string sContent = "";
  std::ifstream hInputFile(sFilePathName.c_str());
  std::string sLine;
  
  if (hInputFile.is_open()) {
    unsigned long iCountLine = 0;
    while (std::getline(hInputFile, sLine)) {
      ++iCountLine;
      sContent += trim(sLine);
    }
  } else {
    std::cout << "Konnte Datei " << sFilePathName << " nicht öffnen!" << std::endl;
  }
    return sContent;
}

bool Tool_File::save(std::string& sContent,std::string& sFilePathName) {
  if (0 < sContent.length()
    && 0 < sFilePathName.length()
    && Tool_File::_oFileHandleCollection->checkKeyExists(sFilePathName)
  ) {
    (*Tool_File::_oFileHandleCollection->get(sFilePathName)) << sContent;
    return true;
  }
  std::cout << "Unable to open file : " << sFilePathName << std::endl;
  return false;
}

bool Tool_File::open(const std::string& sFilePathName) {
  if (false == Tool_File::_oFileHandleCollection->checkKeyExists(sFilePathName)) {
    std::ofstream* oFileHandle = new std::ofstream(sFilePathName.c_str());
    if (oFileHandle->is_open()) {
      Tool_File::_oFileHandleCollection->set(*oFileHandle, sFilePathName);
//       Tool_File::_oFileHandleCollection.add(oFileHandle, sFilePathName);
      return true;
    } else {
      return false;
    }
  }
    return true;
}

bool Tool_File::close() {

}
