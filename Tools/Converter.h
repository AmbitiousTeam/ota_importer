#ifndef __TOOLS_CONVERTER_H__
#define __TOOLS_CONVERTER_H__

#include <QString>
#include "../Tools/String.cpp"

namespace {
  class Converter {
  public:
    static bool convertMixedToBoolean(const std::string);
    static bool convertMixedToBoolean(const QString);
    static bool convertMixedToBoolean(const int);
    static bool convertMixedToBoolean(const bool);
    
    static int convertMixedToInt(const std::string);
    static int convertMixedToInt(const QString);
    static int convertMixedToInt(const int);
    
    static float convertMixedToFloat(const std::string);
    static float convertMixedToFloat(const QString);
    static float convertMixedToFloat(const int);
    static float convertMixedToFloat(const float);
    
    static double convertMixedToDouble(const std::string);
    static double convertMixedToDouble(const QString);
    static double convertMixedToDouble(const int);
    static double convertMixedToDouble(const double);
  protected:
  private:
    
  };

  bool Converter::convertMixedToBoolean(const std::string sValue) {
    if ("FALSE" == toUpper(sValue)
      || "OFF" == toUpper(sValue)
      || "0" == sValue
    ) {
      return false;
    } else if ("TRUE" == toUpper(sValue)
      || "ON" == toUpper(sValue)
      || "1" == sValue
    ) {
      return true;
    }
  }

  bool Converter::convertMixedToBoolean(const QString sValue) {
    if ("FALSE" == sValue.toUpper()
      || "OFF" == sValue.toUpper()
      || "0" == sValue
    ) {
      return false;
    } else if ("TRUE" == sValue.toUpper()
      || "ON" == sValue.toUpper()
      || "1" == sValue
    ) {
      return true;
    }
  }

  bool Converter::convertMixedToBoolean (const int iValue) {
    if (0 == iValue) {
      return false;
    } else if (1 == iValue) {
      return true;
    }
  }

  bool Converter::convertMixedToBoolean (const bool bValue) {
    return bValue;
  }
  
  int Converter::convertMixedToInt(const std::string sValue) {
    return std::stoi(sValue);
  }
  
  int Converter::convertMixedToInt(const QString sValue) {
    return sValue.toInt();
  }

  int Converter::convertMixedToInt(const int iValue) {
    return iValue;
  }
  
  float Converter::convertMixedToFloat(const std::string sValue) {
    char* pEnd;
    return strtof (sValue.c_str(), &pEnd);
  }
  
  float Converter::convertMixedToFloat(const QString sValue) {
    return sValue.toFloat();
  }
  
  float Converter::convertMixedToFloat(const int iValue) {
    return (float)iValue;
  }
  
  float Converter::convertMixedToFloat(const float fValue) {
    return fValue;
  }
  
  double Converter::convertMixedToDouble(const std::string sValue) {
    char* pEnd;
    return strtod (sValue.c_str(), &pEnd);
  }
  
  double Converter::convertMixedToDouble(const QString sValue) {
    return sValue.toDouble();
  }
  
  double Converter::convertMixedToDouble(const int iValue) {
    return (double)iValue;
  }
  
  double Converter::convertMixedToDouble(const double dValue) {
    return dValue;
  }
}
#endif
