#ifndef __TOOL_STRING__
#define __TOOL_STRING__

#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>

// trim from start
static inline std::string &ltrim(std::string &s) {
  s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
  return s;
}

// trim from end
static inline std::string &rtrim(std::string &s) {
  s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
  return s;
}

// trim from both ends
static inline std::string &trim(std::string &s) {
  return ltrim(rtrim(s));
}

static inline std::string &fucase(std::string &s) {
  if (0 < s.length()) {
    s[0] = toupper(s[0]);
  }
  return s;
}

static inline std::string toUpper(std::string s) {
//   std::transform(s.begin(), s.end(), s.begin(), toupper);
//   for (int i = 0; i < s.length; i++) {
//     s[i] = toupper(s[i]);
//   }
  std::string::iterator i = s.begin();
  std::string::iterator end = s.end();
  
  while (i != end) {
    *i = std::toupper((unsigned char)*i);
    ++i;
  }
  return s;
}

#endif
