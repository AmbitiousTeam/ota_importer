#ifndef __TOOLS_LOGGER_H__
#define __TOOLS_LOGGER_H__

#include <iostream>
#include <fstream>
#include <string>

enum _enLogLevels {
  EMERGENCY = 0,
  ALERT = 1,
  CRIT = 2,
  ERR = 4,
  WARN = 8,
  INFO = 16,
  DEBUG = 32
};

class Logger
{
public:
  std::string convertLogLevelToName(_enLogLevels);
  void setDateTimeFormatString(std::string);
  std::string getDateTimeFormatString();
  void write(const std::string, _enLogLevels);
  std::string getFileName() const;
  void setFileName(const std::string);
  void operator()(const std::string, _enLogLevels);
  std::string formatString(std::string, _enLogLevels);
  
  Logger(const std::string, _enLogLevels);
  ~Logger();
protected:
private:
  std::string _sFileName;
  std::string _sDateTimeFormatString;
  _enLogLevels _iMaxLogLevel;
  _enLogLevels _iMinLogLevel;
  
  bool bInitialized;
};

#endif
