#include "FileSplitter.h"

#include <boost/regex.hpp>

#include "File.h"

bool Tool_FileSplitter::process(void) {
  std::string sFilePathName = "../../Input/OTA_HotelRatePlanNotifRQ.xml";
  this->_loadContent(sFilePathName);
}

std::string Tool_FileSplitter::_loadContent(std::string& sFilePathName) {
  Tool_File* oFile = new Tool_File();
  std::string sContent = oFile->load(sFilePathName);
  this->_split(sContent);
//   std::cout << sContent << std::endl;
}

bool Tool_FileSplitter::_split(std::string& sContent) {
  
//   boost::regex regex("(<([a-zA-Z\\_\\-]+) *[^>]*/>)|(<([a-zA-Z\\_\\-]+) *[^>]*>.*?</\\4>)");
  boost::regex regex("(<RatePlans .*?HotelCode=\"([A-Z0-9]*)\".*?>.*?<\/RatePlans>)");
  boost::smatch matches;                // container für die ergebnisse
  boost::regex_constants::icase;        // nicht casesensitive
  boost::regex_constants::match_all;    // like preg_match_all
  
  std::string::const_iterator start1 = sContent.begin();
  std::string::const_iterator end1   = sContent.end();
  
  while (boost::regex_search(start1, end1, matches, regex)) {
    std::string sCurrentContent(matches[1].first, matches[1].second);
    std::string sHotelCode(matches[2].first, matches[2].second);
   
    std::cout << "CurrentContent : " << sCurrentContent << std::endl;
    std::cout << "HotelCode : " << sHotelCode << std::endl;
    
    start1 = matches[0].second;
  }
}