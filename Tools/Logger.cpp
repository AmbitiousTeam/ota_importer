#include <iostream>
#include <string>
#include <ctime>
#include <map>

#include "Logger.h"

std::string Logger::convertLogLevelToName(_enLogLevels iLogLevel) {
    static std::map<int, std::string> strings;
    if (strings.size() == 0){
#define INSERT_ELEMENT(p) strings[p] = #p
        INSERT_ELEMENT(EMERGENCY);
        INSERT_ELEMENT(ALERT);
        INSERT_ELEMENT(ERR);
        INSERT_ELEMENT(WARN);
        INSERT_ELEMENT(INFO);
        INSERT_ELEMENT(DEBUG);
#undef INSERT_ELEMENT
    }   

    return strings[iLogLevel];
}

void Logger::setFileName(const std::string sFileName) {
  _sFileName = sFileName;
}

std::string Logger::getFileName() const {
  return _sFileName;
}

void Logger::write(const std::string sContent, _enLogLevels iLogLevel = WARN) {
  
  if (iLogLevel <= _iMinLogLevel
    && iLogLevel >= _iMaxLogLevel
  ) {
    std::ofstream hOutputFile(getFileName().c_str(), std::ios_base::app);
    if (hOutputFile.is_open()) {
      hOutputFile << formatString(sContent, iLogLevel) << std::endl;
      hOutputFile.close();
    } else {
      std::cout << "Konnte Datei '" << getFileName() << "' nicht öffnen!" << std::endl;
    }
  }
}

/**
 * http://en.cppreference.com/w/cpp/chrono/c/strftime
 */
std::string Logger::formatString(std::string sContent, _enLogLevels iLogLevel) {
  std::locale::global(std::locale("de_DE.utf8"));
  std::time_t t = std::time(NULL);
  char mbstr[100];
  std::strftime(mbstr, sizeof(mbstr), getDateTimeFormatString().c_str(), std::localtime(&t));
  std::string sDateTime(mbstr);
  std::string sLogLevel = convertLogLevelToName(iLogLevel);
  std::string sFormatedContent = sDateTime + " [ " + sLogLevel + " ] " + sContent;
  return sFormatedContent;
}

void Logger::setDateTimeFormatString(std::string sDateTimeFormatString) {
  _sDateTimeFormatString = sDateTimeFormatString;
}

std::string Logger::getDateTimeFormatString() {
  return _sDateTimeFormatString;
}

void Logger::operator()(const std::string sContent, _enLogLevels iLogLevel = WARN) {
  write(sContent, iLogLevel);
}

Logger::Logger(const std::string sContent, _enLogLevels iLogLevel = WARN) {
  _iMinLogLevel = WARN;
  _iMaxLogLevel = ALERT;
  setDateTimeFormatString("%Y-%m-%d %H:%M:%S");
  setFileName("../Data/Log/test.log");
  write(sContent, iLogLevel);
}

Logger::~Logger() {

}
