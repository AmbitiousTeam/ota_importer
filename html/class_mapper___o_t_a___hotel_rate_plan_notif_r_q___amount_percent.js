var class_mapper___o_t_a___hotel_rate_plan_notif_r_q___amount_percent =
[
    [ "Mapper_OTA_HotelRatePlanNotifRQ_AmountPercent", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___amount_percent.html#a62b0b19cb0bdf3b32d440fe449d65200", null ],
    [ "~Mapper_OTA_HotelRatePlanNotifRQ_AmountPercent", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___amount_percent.html#a4a0acb3ea7747666bf68978b6452f674", null ],
    [ "getAmount", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___amount_percent.html#a402a9af1f2c519078202bef79e987929", null ],
    [ "getCurrencyCode", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___amount_percent.html#ab650625e89b7802a265990164567d218", null ],
    [ "getPercent", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___amount_percent.html#af26c41238172f218e1855ca165e1eb3e", null ],
    [ "setAmount", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___amount_percent.html#a65d88d245d01026c224af1bf7e5e5bc3", null ],
    [ "setCurrencyCode", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___amount_percent.html#a10dbdbb0906491be19752a47e8591258", null ],
    [ "setPercent", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___amount_percent.html#a99c3339505fbc6ae4f2458ae1fa74bff", null ]
];