var class_mapper___o_t_a___hotel_rate_plan_notif_r_q___deadline =
[
    [ "Mapper_OTA_HotelRatePlanNotifRQ_Deadline", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___deadline.html#a8f2826c92e2ce016eae009a72081b7fd", null ],
    [ "~Mapper_OTA_HotelRatePlanNotifRQ_Deadline", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___deadline.html#a3c1915cfafe802158bba273fdfb80959", null ],
    [ "getOffsetDropTime", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___deadline.html#a943a9d333f527c7f46e69c3e9ec81d3c", null ],
    [ "getOffsetTimeUnit", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___deadline.html#a613429c8da0ba9843c4672d7912c0094", null ],
    [ "getOffsetUnitMultiplier", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___deadline.html#ab24530c9b14c28dab821c5b094cc9aca", null ],
    [ "setOffsetDropTime", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___deadline.html#a0430536bb95a9047fe3e7f3075821806", null ],
    [ "setOffsetTimeUnit", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___deadline.html#a7d3cfb23854c10c104bfeceb390e942e", null ],
    [ "setOffsetUnitMultiplier", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___deadline.html#ae275b8f4fd46cfbe459210972c1dd74f", null ]
];