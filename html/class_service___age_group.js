var class_service___age_group =
[
    [ "Service_AgeGroup", "class_service___age_group.html#aec092992002a348c3a2d7ddb7f917360", null ],
    [ "~Service_AgeGroup", "class_service___age_group.html#ac067a941ad9084824148f2b4cadc88e3", null ],
    [ "collectAgeInformationFromOccupancies", "class_service___age_group.html#ab5bb207e6956024f3acc64ab9a3931c4", null ],
    [ "collectAgeInformationFromSupplements", "class_service___age_group.html#afeca632bc3994127e177c1d1a7999ae9", null ],
    [ "generateAgeGroups", "class_service___age_group.html#a2eb97b3f29a071f54f98ac1c9a985635", null ],
    [ "getAgeGroups", "class_service___age_group.html#a1b8c412a72220bdc00575de2de559377", null ],
    [ "getDefaultAdultMaxAge", "class_service___age_group.html#afce9eec2c90c026df8c0a84cc2a79f50", null ],
    [ "getDefaultAdultMinAge", "class_service___age_group.html#a22d8797476092df003c740b1c6e8ef4f", null ],
    [ "getDefaultChildrenMaxAge", "class_service___age_group.html#adfa3415a1899b71aa59909ccbdb3b262", null ],
    [ "getDefaultChildrenMinAge", "class_service___age_group.html#a029a9b5a19f30be439918fa7ee5a9928", null ],
    [ "getDefaultInfantMaxAge", "class_service___age_group.html#a3a5dfdee43e6d118cbeae1d5e1bdc805", null ],
    [ "getDefaultInfantMinAge", "class_service___age_group.html#ad44fd52faeef543f1c30170f9c3a1e77", null ],
    [ "setAgeGroups", "class_service___age_group.html#ae26aad0fec41ff46c64c7d5c964c7406", null ],
    [ "setDefaultAdultMaxAge", "class_service___age_group.html#a1e361c3d7310061b8a617708abd36fc5", null ],
    [ "setDefaultAdultMinAge", "class_service___age_group.html#aa5c9d6e9b1a5b1d2c2b130aa8266e5da", null ],
    [ "setDefaultChildrenMaxAge", "class_service___age_group.html#a45d35c09c462ee96752937ed8a6d6750", null ],
    [ "setDefaultChildrenMinAge", "class_service___age_group.html#aa575b6c8888af804a2a5cf1bc0af893d", null ],
    [ "setDefaultInfantMaxAge", "class_service___age_group.html#a44c8106fb5e119f91d8dfa0b1b05a07a", null ],
    [ "setDefaultInfantMinAge", "class_service___age_group.html#a00249847cb40edc6174161aa61e10ab3", null ]
];