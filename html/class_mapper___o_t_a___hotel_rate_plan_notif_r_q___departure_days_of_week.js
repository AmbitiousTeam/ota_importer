var class_mapper___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week =
[
    [ "Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week.html#a73dd3dd6a45bb5000d012ab24179aabd", null ],
    [ "~Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week.html#ac4f995d4795cdb1ebcb031d890b3770c", null ],
    [ "getFri", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week.html#a7bda5a0a84c6161e570012748176128d", null ],
    [ "getMon", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week.html#a751a2a97f01918f01e0610c85fb86ef5", null ],
    [ "getSat", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week.html#ac53fa966f5955106bf778c4ee2fa3ce0", null ],
    [ "getSun", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week.html#a20c3dae87d0dfaae72b709e935bd44c1", null ],
    [ "getThu", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week.html#aa4bbd2acb2279a0ce355a7edf72d62ec", null ],
    [ "getTue", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week.html#a2dba8b311fff9ac10e66b714da9b1cae", null ],
    [ "getWed", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week.html#a3f76f278b5667f41522486d3699be03b", null ],
    [ "setFri", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week.html#ad43da34580e9ec660168316718dad694", null ],
    [ "setMon", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week.html#a91ac00deff5dbd7bf054e97b04224a09", null ],
    [ "setSat", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week.html#afaf84f1eb936e9dc7ffd43fa6a699869", null ],
    [ "setSun", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week.html#a29efa2ee8774b37b79b7a5d4dbf616ab", null ],
    [ "setThu", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week.html#aea0f440643c036cc2acfc5fe4bc6f44e", null ],
    [ "setTue", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week.html#a83b3ae9af806e8b095e492ddeff5754f", null ],
    [ "setWed", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week.html#a4d3dc6c238cad51cfb50afd26312f492", null ]
];