var class_model___o_t_a___hotel_rate_plan_notif_r_q___rate_plans =
[
    [ "Model_OTA_HotelRatePlanNotifRQ_RatePlans", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#a406eee17f1a383e0398d2dfad036ea67", null ],
    [ "~Model_OTA_HotelRatePlanNotifRQ_RatePlans", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#a770b9f12799f24cf5a7356dc0b541557", null ],
    [ "getBookingRules", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#a37c01bc356b082558a286ea0dcf138c9", null ],
    [ "getDelta", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#ab6340baa65413d4ded7986fbee582e3e", null ],
    [ "getHotelCode", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#aed3718b9bec404c4a71144a32a15aae3", null ],
    [ "getHotelName", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#a64a9cce7fff4fa860f5fbf797a741f6c", null ],
    [ "getIdCon", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#ab2082c94a58d55625d123a5048b07567", null ],
    [ "getMultiMarkup", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#af134dee404f82255258c710574574852", null ],
    [ "getMultiMarkupPriority", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#ac1a5eb5a11e3835d652f15df91a71d0c", null ],
    [ "getOffers", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#a9f1acb64c30d65229a081f4576c53e68", null ],
    [ "getSupplements", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#a7d3ee1edde83fe4d2afef013b7b582b8", null ],
    [ "setBookingRules", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#a2dfe798e7ccc54b5bc8d2c5b9d3e5a56", null ],
    [ "setDelta", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#aa07dfdaed4abc7c2abd0a9b1c82a2a0f", null ],
    [ "setDelta", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#a1887543964911328639bbf08e1611e13", null ],
    [ "setDelta", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#adb4d6fde1bc713108312b957cd00d729", null ],
    [ "setHotelCode", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#a174df20fff423c575a27c451e7b7e446", null ],
    [ "setHotelName", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#a6f8847026b79857674ed2b78c5f9c95a", null ],
    [ "setIdCon", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#a54e98d5d0332d93d7267ef3587c1868e", null ],
    [ "setMultiMarkup", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#a83765236cf7a47a51bf4326b6148979d", null ],
    [ "setMultiMarkup", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#a696a0a22c565d38ba6db76979b32cab0", null ],
    [ "setMultiMarkup", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#a203ddefe726f2b769ca317e147013fef", null ],
    [ "setMultiMarkupPriority", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#a59812e0f216d2f463d6e015d68ecaf5f", null ],
    [ "setMultiMarkupPriority", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#a73dd90c1b06062bb9427b89e3866a8e8", null ],
    [ "setOffers", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#a948c63fbf2c45c427d5418d38a542449", null ],
    [ "setSupplements", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#a8b0de7c1d44bf379b6c2f3b6b96fb021", null ]
];