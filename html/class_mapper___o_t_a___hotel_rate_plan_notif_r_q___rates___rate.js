var class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rates___rate =
[
    [ "Mapper_OTA_HotelRatePlanNotifRQ_Rates_Rate", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rates___rate.html#aece1a74990a57646a21fd7ac870a9843", null ],
    [ "~Mapper_OTA_HotelRatePlanNotifRQ_Rates_Rate", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rates___rate.html#a82dcd990c7e92f105deef232c5a2630c", null ],
    [ "getEnd", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rates___rate.html#afcb2465f454755f7e041dd0a28012330", null ],
    [ "getNoOffersAllowed", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rates___rate.html#ae725414bd268d1ae2f7ae150c0084497", null ],
    [ "getStart", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rates___rate.html#ab9adb273dabd7552083a38e54d792be6", null ],
    [ "processAdditionalGuestAmounts", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rates___rate.html#aa304c3a26d842d526e4692b3e0ad3cfc", null ],
    [ "processBaseByGuestAmts", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rates___rate.html#af3ad0d00b7bd670592e88ab7c5f86fd7", null ],
    [ "setEnd", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rates___rate.html#af69fc24dfc616763494261d3172c54c0", null ],
    [ "setNoOffersAllowed", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rates___rate.html#a55032b96cddabffe70b62d714f2ddd0b", null ],
    [ "setStart", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rates___rate.html#a3ee858e31cefc25cbeb952f7b1c2d61a", null ]
];