var class_mapper___o_t_a___hotel_rate_plan_notif_r_q___offers___offer =
[
    [ "Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___offers___offer.html#afc8c3d36354008ddb6a0285d49238ed5", null ],
    [ "~Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___offers___offer.html#a4c9b82d17dba4640002b099722c4d905", null ],
    [ "collectCompatibleOffers", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___offers___offer.html#ad25c5fd48328e31f4bcaf8fe999cd4b1", null ],
    [ "collectInventories", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___offers___offer.html#a8b25ffceb46647af19569bef078495f4", null ],
    [ "collectOfferRules", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___offers___offer.html#a3e5cb0eeff0c2b7a128779216a305adc", null ],
    [ "getApplicationOrder", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___offers___offer.html#a3c514ecc996385e84899db7e2bf9e27c", null ],
    [ "getOfferCode", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___offers___offer.html#aae39b995e58633251927189ed3a36af1", null ],
    [ "getRph", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___offers___offer.html#a1bae0ae2a69cf12c667c3c44fc11b1f0", null ],
    [ "processDiscount", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___offers___offer.html#a3535d258937ac17f336ad1882a984866", null ],
    [ "processOfferDescription", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___offers___offer.html#aa5d82c558b8ab61da5a8560f957c20b4", null ],
    [ "setApplicationOrder", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___offers___offer.html#a5fc12e379637f61db5799334e126ab2d", null ],
    [ "setOfferCode", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___offers___offer.html#a4a68ecf6a5458ad22260920e1c743331", null ],
    [ "setRph", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___offers___offer.html#a996aedb4ccef11e6f5fc70e5cda54566", null ]
];