var class_mapper___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount =
[
    [ "Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#a3f816b5899337a4d97f101967f6c7969", null ],
    [ "~Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#a15295106c3ddceff3c351b5a23281cbe", null ],
    [ "getAdditionalGuestsNumber", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#ac2b5d4a5f3b99636bab47e4d5527c643", null ],
    [ "getAgeQualifyingCode", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#a0ed0025ff764657a248b249e71c891c6", null ],
    [ "getAmount", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#aa946838677473029f7ef84ea982cd45e", null ],
    [ "getAmountAfterTax", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#ac423e1453d9cd527dee371e6da07c7a4", null ],
    [ "getCurrencyCode", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#a0b8597f103b01117c43d6f4c3d7a083e", null ],
    [ "getMaxAdditionalGuests", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#aa4d55a1de1712bb967f51b355bab2056", null ],
    [ "getMaxAge", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#a2c8ae3a95f10f7d1a7ca040fb3b42979", null ],
    [ "getMinAge", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#a3e0db1ed879dc9ef834ab0c4ee591fde", null ],
    [ "setAdditionalGuestsNumber", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#ab15bf5ddf140f33ed0a6a4a39980bd58", null ],
    [ "setAgeQualifyingCode", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#a15ed37b2d28d97a66d17b5c956fbc8c2", null ],
    [ "setAmount", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#a52c31c2575f246e927c90d1567f1ea5b", null ],
    [ "setAmountAfterTax", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#a6b2ef8fe3d1f93cfd9634962603c393d", null ],
    [ "setCurrencyCode", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#ac257d24cb2b13f84ccd25c280307bff8", null ],
    [ "setMaxAdditionalGuests", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#abe38fd0c8ab3e2ce3c9be9b6da4a94c0", null ],
    [ "setMaxAge", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#a2bfac92ec8be97fdd23e1370581848e4", null ],
    [ "setMinAge", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#a4dc40becf5a34067ac9da4a12d80388d", null ]
];