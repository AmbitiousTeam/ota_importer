var class_model___o_t_a___hotel_rate_plan_notif_r_q___guest_room =
[
    [ "Model_OTA_HotelRatePlanNotifRQ_GuestRoom", "class_model___o_t_a___hotel_rate_plan_notif_r_q___guest_room.html#abd536acf7e7599cc15857a3fbdc94315", null ],
    [ "~Model_OTA_HotelRatePlanNotifRQ_GuestRoom", "class_model___o_t_a___hotel_rate_plan_notif_r_q___guest_room.html#a8104724733ee2680674dab7a0b99d929", null ],
    [ "getAdditionalGuestAmounts", "class_model___o_t_a___hotel_rate_plan_notif_r_q___guest_room.html#a3f153f8979a6db1e36b3522e1bf78bd8", null ],
    [ "getOccupancies", "class_model___o_t_a___hotel_rate_plan_notif_r_q___guest_room.html#ab74e196bb73c268c3539c49bdae39a6d", null ],
    [ "getQuantities", "class_model___o_t_a___hotel_rate_plan_notif_r_q___guest_room.html#aa0776efe392b7c45f63e833436b61f21", null ],
    [ "setAdditionalGuestAmounts", "class_model___o_t_a___hotel_rate_plan_notif_r_q___guest_room.html#a0d6f709127bfd7fa114e4338ecfedf67", null ],
    [ "setOccupancies", "class_model___o_t_a___hotel_rate_plan_notif_r_q___guest_room.html#ad5a3a646a9d052533396a64025d0c5a2", null ],
    [ "setQuantities", "class_model___o_t_a___hotel_rate_plan_notif_r_q___guest_room.html#ad38b1c9ab7fc5cdd57a30d2f70e85e96", null ]
];