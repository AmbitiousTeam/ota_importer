var class_mapper___o_t_a___hotel_rate_plan_notif_r_q___date_restrictions___date_restriction =
[
    [ "Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___date_restrictions___date_restriction.html#a5321989e73f1aa25c0aff2f2d467969c", null ],
    [ "~Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___date_restrictions___date_restriction.html#a79860b3b1aaf09206e80fab738dc32f4", null ],
    [ "getEnd", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___date_restrictions___date_restriction.html#ae5d4d4b44be10c2a8211da30c439b936", null ],
    [ "getRestrictionType", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___date_restrictions___date_restriction.html#a1a7403a1701fb53e26a13735949dec4d", null ],
    [ "getStart", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___date_restrictions___date_restriction.html#a6d2c3d23d6423eb962f24f3be388c071", null ],
    [ "setEnd", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___date_restrictions___date_restriction.html#a4054da7be3bdd112b79ce3f00d322e70", null ],
    [ "setRestrictionType", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___date_restrictions___date_restriction.html#a364206020e78152782952a09adc392a2", null ],
    [ "setStart", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___date_restrictions___date_restriction.html#a6b8e4f69f8856bdd0136029420baf1f3", null ]
];