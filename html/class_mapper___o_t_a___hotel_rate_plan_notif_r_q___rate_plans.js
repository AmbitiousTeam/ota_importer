var class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rate_plans =
[
    [ "Mapper_OTA_HotelRatePlanNotifRQ_RatePlans", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#a37cc33d503d21e06fb6509b2f23e0818", null ],
    [ "~Mapper_OTA_HotelRatePlanNotifRQ_RatePlans", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#a4547c27b1353e77052abf3952f7bbf10", null ],
    [ "collectBookingRules", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#ab05dd3f50723d6709c3cda459e779a23", null ],
    [ "collectOffers", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#a23d14e7e71a11bbc4a004aeecdbd2065", null ],
    [ "collectRatePlan", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#af4464ad9202086dbdf4aff6a6981676e", null ],
    [ "collectSupplements", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#a20f82c2752a6bd5a8937093c6bcad4ed", null ],
    [ "getDelta", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#a1cd44de08dc099577877eaf6298f3684", null ],
    [ "getHotelCode", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#a67feebf4fbac2a22abc5c0110c71dff0", null ],
    [ "getHotelName", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#af31b34e2eac4ef5fc3556d6e74add00b", null ],
    [ "getIdCon", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#a3af24eeaa85ac66f029f85f1eabac169", null ],
    [ "getMultiMarkup", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#a2402216ddb1bcfb1f8d75d241d87e7dc", null ],
    [ "getMultiMarkupPriority", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#a54de7663eb6d27b018672191d3717334", null ],
    [ "setDelta", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#ad894afe9bd34d98f8722960f44bc5a45", null ],
    [ "setHotelCode", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#a4d3d08336d7de0365ffdb4bfc36f6d4b", null ],
    [ "setHotelName", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#a15ac17231c43d9c288183b0d5314561b", null ],
    [ "setIdCon", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#ab774e04536d5f084d559fbc1db2032a9", null ],
    [ "setMultiMarkup", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#a1191979bcb3cef1ae4a0f1a460367c49", null ],
    [ "setMultiMarkupPriority", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html#ace27b92c0ea4bf7e338b9f370875fdbd", null ]
];