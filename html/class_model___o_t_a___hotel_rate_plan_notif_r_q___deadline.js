var class_model___o_t_a___hotel_rate_plan_notif_r_q___deadline =
[
    [ "Model_OTA_HotelRatePlanNotifRQ_Deadline", "class_model___o_t_a___hotel_rate_plan_notif_r_q___deadline.html#a3a3a172976f16794a9c551a20fa1738d", null ],
    [ "~Model_OTA_HotelRatePlanNotifRQ_Deadline", "class_model___o_t_a___hotel_rate_plan_notif_r_q___deadline.html#a536f328644665ac71a3eee480bdbf415", null ],
    [ "getOffsetDropTime", "class_model___o_t_a___hotel_rate_plan_notif_r_q___deadline.html#ae0baed366d14bc987a97a24985b37a43", null ],
    [ "getOffsetTimeUnit", "class_model___o_t_a___hotel_rate_plan_notif_r_q___deadline.html#a3649aea8a1bd7367cc496f25b17ebb0c", null ],
    [ "getOffsetUnitMultiplier", "class_model___o_t_a___hotel_rate_plan_notif_r_q___deadline.html#a15d6ce614dd0e8e3b880cd30173f38cf", null ],
    [ "setOffsetDropTime", "class_model___o_t_a___hotel_rate_plan_notif_r_q___deadline.html#a123eafb096c06284ef2ee302e2067b24", null ],
    [ "setOffsetTimeUnit", "class_model___o_t_a___hotel_rate_plan_notif_r_q___deadline.html#ab1fc4cefa79af4e0c5886d93ec6ffb6f", null ],
    [ "setOffsetUnitMultiplier", "class_model___o_t_a___hotel_rate_plan_notif_r_q___deadline.html#aad6eb8b53a31c401009f5f00ae262031", null ]
];