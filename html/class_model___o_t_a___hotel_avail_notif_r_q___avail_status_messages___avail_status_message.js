var class_model___o_t_a___hotel_avail_notif_r_q___avail_status_messages___avail_status_message =
[
    [ "Model_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage", "class_model___o_t_a___hotel_avail_notif_r_q___avail_status_messages___avail_status_message.html#ab248d567b4cb683f1e2cf4074dcc256f", null ],
    [ "~Model_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage", "class_model___o_t_a___hotel_avail_notif_r_q___avail_status_messages___avail_status_message.html#a9b601c077a31a49bfbfcb1adad329fa3", null ],
    [ "getRestrictionStatus", "class_model___o_t_a___hotel_avail_notif_r_q___avail_status_messages___avail_status_message.html#ace058214f6f43658856444db1da367e7", null ],
    [ "getStatusApplicationControl", "class_model___o_t_a___hotel_avail_notif_r_q___avail_status_messages___avail_status_message.html#a6d64e2296725ebb8255ae33e0db9e91a", null ],
    [ "setRestrictionStatus", "class_model___o_t_a___hotel_avail_notif_r_q___avail_status_messages___avail_status_message.html#ae1ae3648c42bee3bad51e165550c5ab9", null ],
    [ "setStatusApplicationControl", "class_model___o_t_a___hotel_avail_notif_r_q___avail_status_messages___avail_status_message.html#a60912ffac632b7023bbe06f2e3330f2c", null ]
];