var class_model___o_t_a___hotel_avail_notif_r_q___restriction_status =
[
    [ "Model_OTA_HotelAvailNotifRQ_RestrictionStatus", "class_model___o_t_a___hotel_avail_notif_r_q___restriction_status.html#a49bbfc1e1768278af56c0347974aa8d9", null ],
    [ "~Model_OTA_HotelAvailNotifRQ_RestrictionStatus", "class_model___o_t_a___hotel_avail_notif_r_q___restriction_status.html#a4c666bebddb67ddb9edb4b2019a6f0d3", null ],
    [ "getRestriction", "class_model___o_t_a___hotel_avail_notif_r_q___restriction_status.html#a84f117940dead477f878631c9c15edd1", null ],
    [ "getStatus", "class_model___o_t_a___hotel_avail_notif_r_q___restriction_status.html#afe7bbc1b0139b056e3c3b13cba4178e2", null ],
    [ "setRestriction", "class_model___o_t_a___hotel_avail_notif_r_q___restriction_status.html#ad1df86dc1cc528bdc26e512e02344731", null ],
    [ "setStatus", "class_model___o_t_a___hotel_avail_notif_r_q___restriction_status.html#a00094845b08981ad4496e71ec93d527e", null ]
];