var class_model___o_t_a___hotel_rate_plan_notif_r_q___d_o_w___restrictions =
[
    [ "Model_OTA_HotelRatePlanNotifRQ_DOW_Restrictions", "class_model___o_t_a___hotel_rate_plan_notif_r_q___d_o_w___restrictions.html#abd9158bfe89066ef1d5a4b09f8554375", null ],
    [ "~Model_OTA_HotelRatePlanNotifRQ_DOW_Restrictions", "class_model___o_t_a___hotel_rate_plan_notif_r_q___d_o_w___restrictions.html#a832f6f001ae954ee67edd9cf406a5654", null ],
    [ "getArrivalDaysOfWeek", "class_model___o_t_a___hotel_rate_plan_notif_r_q___d_o_w___restrictions.html#af1584256088d55833ef168c0e37f4785", null ],
    [ "getDepartureDaysOfWeek", "class_model___o_t_a___hotel_rate_plan_notif_r_q___d_o_w___restrictions.html#a0e22c37b0162f78dd82ad6861b778e42", null ],
    [ "setArrivalDaysOfWeek", "class_model___o_t_a___hotel_rate_plan_notif_r_q___d_o_w___restrictions.html#a3b3709e790dfc2606905a9dadba5e584", null ],
    [ "setDepartureDaysOfWeek", "class_model___o_t_a___hotel_rate_plan_notif_r_q___d_o_w___restrictions.html#aa87eea727afa6ffb8e2ddd3cfcbf5199", null ]
];