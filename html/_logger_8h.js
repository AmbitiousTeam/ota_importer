var _logger_8h =
[
    [ "Logger", "class_logger.html", "class_logger" ],
    [ "_enLogLevels", "_logger_8h.html#aef1c25489759a3dc599b4d5a79d45441", [
      [ "EMERGENCY", "_logger_8h.html#aef1c25489759a3dc599b4d5a79d45441a0666e7ee289b420b8f016b5269fa8244", null ],
      [ "ALERT", "_logger_8h.html#aef1c25489759a3dc599b4d5a79d45441a45e0bb0f520fb3851c7d6cc5c5ddd6f9", null ],
      [ "CRIT", "_logger_8h.html#aef1c25489759a3dc599b4d5a79d45441a45a3dad92ec00cbe54224c69006160dd", null ],
      [ "ERR", "_logger_8h.html#aef1c25489759a3dc599b4d5a79d45441a0f886785b600b91048fcdc434c6b4a8e", null ],
      [ "WARN", "_logger_8h.html#aef1c25489759a3dc599b4d5a79d45441a74dac7ac23d5b810db6d4067f14e8676", null ],
      [ "INFO", "_logger_8h.html#aef1c25489759a3dc599b4d5a79d45441a748005382152808a72b1a9177d9dc806", null ],
      [ "DEBUG", "_logger_8h.html#aef1c25489759a3dc599b4d5a79d45441a0593585da9181e972974c1274d8f2b4f", null ]
    ] ]
];