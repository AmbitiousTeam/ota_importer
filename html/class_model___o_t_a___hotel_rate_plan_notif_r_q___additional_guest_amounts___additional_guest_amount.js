var class_model___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount =
[
    [ "Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount", "class_model___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#a090961bf1a2e35f063fccc9e29bf03c7", null ],
    [ "~Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount", "class_model___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#a9c25cf65c1aac77e1311eee5b147165d", null ],
    [ "getAdditionalGuestNumber", "class_model___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#a4de6e4f005226f68d7c36022c0893b60", null ],
    [ "getAgeQualifyingCode", "class_model___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#abf89503cea16acf1f2207cccb5d05916", null ],
    [ "getAmount", "class_model___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#ac65e5ab0de169c75bfdd3cdea8ca91ba", null ],
    [ "getAmountAfterTax", "class_model___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#a885348a1b08d2eb78c05e411ba709bc5", null ],
    [ "getCurrencyCode", "class_model___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#a1fee1305a7b1385ffad6625f77a44a59", null ],
    [ "getMaxAdditionalGuests", "class_model___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#a7743fc6f18e881ba3544e5d81e0d523c", null ],
    [ "getMaxAge", "class_model___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#a7301b23b89b9b0f5e2f8d5e74150ff5e", null ],
    [ "getMinAge", "class_model___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#a5cf428d930002078b969da7d1e959403", null ],
    [ "getPercent", "class_model___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#ad547bceb4f45d5ac128b11b37ddb2ff5", null ],
    [ "setAdditionalGuestsNumber", "class_model___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#aa2e241bb5b37c18b17469172b75f069d", null ],
    [ "setAgeQualifyingCode", "class_model___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#ac82fdc44c4d1f0113c34accadc702299", null ],
    [ "setAmount", "class_model___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#aac8cd54c5e52d0d96bafedcb2d8a899a", null ],
    [ "setAmountAfterTax", "class_model___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#af994afe64af11c04743b918a9aee738c", null ],
    [ "setCurrencyCode", "class_model___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#ac5039d7a9585ee433c4b66b579a97703", null ],
    [ "setMaxAdditionalGuests", "class_model___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#a1c0af5d28a7919da3c9265e6d9d43b92", null ],
    [ "setMaxAge", "class_model___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#a39730084fa232acd300d236157bc063b", null ],
    [ "setMinAge", "class_model___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#a0d9eb4724dc80c27329f80f11ae29896", null ],
    [ "setPercent", "class_model___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html#af6600d5ce9c53ac4a70bc8e26463a06c", null ]
];