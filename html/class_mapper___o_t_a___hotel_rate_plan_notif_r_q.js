var class_mapper___o_t_a___hotel_rate_plan_notif_r_q =
[
    [ "Mapper_OTA_HotelRatePlanNotifRQ", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q.html#aa30908929b593c3e270e295286d3302c", null ],
    [ "~Mapper_OTA_HotelRatePlanNotifRQ", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q.html#ae8f49a95c4a254e8bd7cc74c21826bbd", null ],
    [ "collectOffers", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q.html#a73ab97d7b38f3008055f0ce0affb1126", null ],
    [ "collectRatePlans", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q.html#a188ba5d3ae5146459154e6a943921c41", null ],
    [ "collectTPAExtensions", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q.html#afd37ae0e64d604722b5da9d87e5fc7bd", null ],
    [ "getVersion", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q.html#acd3622accc83c3b16e5cd947c2159f3d", null ],
    [ "getXmlNs", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q.html#a6cf5a6d9ca761a6778e281e03258f2e0", null ],
    [ "getXmlNsXsd", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q.html#a837c289f79427f66177441bcb1e43da0", null ],
    [ "getXmlNsXsi", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q.html#ad72a2c5c0a435a525fa3ae1365458058", null ],
    [ "getXsiSchemaLocation", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q.html#a56f1707b4ccfc6379c0f65786a65190f", null ],
    [ "setCorrelationId", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q.html#a6e2e8d98a2ad26db92be16b572635088", null ],
    [ "setEchoToken", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q.html#afc677538465c1b28773ce329258bf357", null ],
    [ "setVersion", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q.html#a164337062705bec201d3afd47e9791d8", null ],
    [ "setXmlNs", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q.html#aed2b407d64e18cfd98a5d3a2f4687b6e", null ],
    [ "setXmlNsXsd", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q.html#a4dd8d9b60d616392f4928d99c67b9a8b", null ],
    [ "setXmlNsXsi", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q.html#a9e6d9f2a98cc27bc7a0d807f099b75dd", null ],
    [ "setXsiSchemaLocation", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q.html#ae9c7a550a3a4f299923b6fcc98379bb0", null ]
];