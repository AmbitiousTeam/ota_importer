var class_model___o_t_a___hotel_rate_plan_notif_r_q___booking_rules___booking_rule =
[
    [ "Model_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule", "class_model___o_t_a___hotel_rate_plan_notif_r_q___booking_rules___booking_rule.html#a2ca2d6b833161d983a042e055cac2b27", null ],
    [ "~Model_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule", "class_model___o_t_a___hotel_rate_plan_notif_r_q___booking_rules___booking_rule.html#a481f9d417a325a5cbff60fe9cc3647aa", null ],
    [ "getCancelPenalty", "class_model___o_t_a___hotel_rate_plan_notif_r_q___booking_rules___booking_rule.html#a7e59f3346bd90b7843a3962b2035bd67", null ],
    [ "getDOWRestrictions", "class_model___o_t_a___hotel_rate_plan_notif_r_q___booking_rules___booking_rule.html#a4eb8c6b28fac06d2d3a7a6219ee08de4", null ],
    [ "getLengthsOfStay", "class_model___o_t_a___hotel_rate_plan_notif_r_q___booking_rules___booking_rule.html#a57355fb3c2bb14446d8969d4e11dfb5f", null ],
    [ "getRestrictionStatus", "class_model___o_t_a___hotel_rate_plan_notif_r_q___booking_rules___booking_rule.html#a86fd0a0918dbfb6bd5d63a3ea8e3ca7c", null ],
    [ "setCancelPenalty", "class_model___o_t_a___hotel_rate_plan_notif_r_q___booking_rules___booking_rule.html#a4f842b45d84b99e572900d7076a705a2", null ],
    [ "setDOWRestrictions", "class_model___o_t_a___hotel_rate_plan_notif_r_q___booking_rules___booking_rule.html#a98cee378c8a0d637cf12eac1e0b739b4", null ],
    [ "setLengthsOfStay", "class_model___o_t_a___hotel_rate_plan_notif_r_q___booking_rules___booking_rule.html#a5f1fb61f83e3aeb777593a450db1bf4e", null ],
    [ "setRestrictionStatus", "class_model___o_t_a___hotel_rate_plan_notif_r_q___booking_rules___booking_rule.html#af74be2f8b88ac6bf76ae61cf74861fd4", null ]
];