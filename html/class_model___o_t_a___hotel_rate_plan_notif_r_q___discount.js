var class_model___o_t_a___hotel_rate_plan_notif_r_q___discount =
[
    [ "Model_OTA_HotelRatePlanNotifRQ_Discount", "class_model___o_t_a___hotel_rate_plan_notif_r_q___discount.html#ab605dd85b97881b7d187cc1167c960c3", null ],
    [ "~Model_OTA_HotelRatePlanNotifRQ_Discount", "class_model___o_t_a___hotel_rate_plan_notif_r_q___discount.html#a6d1ef280341dff428a35e665287f4ebd", null ],
    [ "getAmount", "class_model___o_t_a___hotel_rate_plan_notif_r_q___discount.html#af8dc9b6b4a701a136ed8f1b4087e89e6", null ],
    [ "getChargeUnitCode", "class_model___o_t_a___hotel_rate_plan_notif_r_q___discount.html#a7d17475e0e514a6189adbba51967abf0", null ],
    [ "getCurrencyCode", "class_model___o_t_a___hotel_rate_plan_notif_r_q___discount.html#ac5629447d38d6bc2db5306b9032c7941", null ],
    [ "getDiscountLastBookingDays", "class_model___o_t_a___hotel_rate_plan_notif_r_q___discount.html#a538adf7fc61604e009500154f21f3a82", null ],
    [ "getNightsDiscounted", "class_model___o_t_a___hotel_rate_plan_notif_r_q___discount.html#a573cc91265473a1e6d6d5e12fffef666", null ],
    [ "getNightsRequired", "class_model___o_t_a___hotel_rate_plan_notif_r_q___discount.html#a64eba952879372c8bf40de4ae3dcd0e9", null ],
    [ "getPercent", "class_model___o_t_a___hotel_rate_plan_notif_r_q___discount.html#a5dfabfe088f5d37ea595e53e391bfdd6", null ],
    [ "setAmount", "class_model___o_t_a___hotel_rate_plan_notif_r_q___discount.html#a4d33077a2fd866693ef936bfefc1eaaf", null ],
    [ "setChargeUnitCode", "class_model___o_t_a___hotel_rate_plan_notif_r_q___discount.html#ae1b856ae8828b95a8f377cc98a6b15c3", null ],
    [ "setCurrencyCode", "class_model___o_t_a___hotel_rate_plan_notif_r_q___discount.html#a7baf5c1c74e1efcaf3621b8ec0efe595", null ],
    [ "setDiscountLastBookingDays", "class_model___o_t_a___hotel_rate_plan_notif_r_q___discount.html#ad59d6c8308e8e1795dc5bc2c80866f3f", null ],
    [ "setNightsDiscounted", "class_model___o_t_a___hotel_rate_plan_notif_r_q___discount.html#a1c74e4cc197f55d15f396084e4a10f39", null ],
    [ "setNightsRequired", "class_model___o_t_a___hotel_rate_plan_notif_r_q___discount.html#a48c67aa731f9379e0c472fcc39457b23", null ],
    [ "setPercent", "class_model___o_t_a___hotel_rate_plan_notif_r_q___discount.html#a3d62de5677cbe0c0fb0a0c658cd3f274", null ]
];