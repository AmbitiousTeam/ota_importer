var class_t_collection =
[
    [ "Add", "class_t_collection.html#ab36f5c927793fc4271e3fb2d71ff2285", null ],
    [ "Clear", "class_t_collection.html#a3de8bf0eaa3b84469ff38f6895442827", null ],
    [ "Count", "class_t_collection.html#ab66250cdd29807dd2f52a304a02d0e2b", null ],
    [ "GetAddress", "class_t_collection.html#a57f8caa5f81e8fff897e56ee1dafc8d5", null ],
    [ "operator[]", "class_t_collection.html#a7f8dc537852e31ef5d543cff6a443499", null ],
    [ "Remove", "class_t_collection.html#a6c6aaf982883e877879ce6bcb9aefd81", null ],
    [ "m_items", "class_t_collection.html#ad897b2a109ba8358e39b3cd700bc464b", null ]
];