var class_model___o_t_a___hotel_rate_plan_notif_r_q___offers___offer =
[
    [ "Model_OTA_HotelRatePlanNotifRQ_Offers_Offer", "class_model___o_t_a___hotel_rate_plan_notif_r_q___offers___offer.html#ae789126260114954fbc5764c62e7eb09", null ],
    [ "~Model_OTA_HotelRatePlanNotifRQ_Offers_Offer", "class_model___o_t_a___hotel_rate_plan_notif_r_q___offers___offer.html#a89062824386c351a3efed012a9c989a4", null ],
    [ "getApplicationOrder", "class_model___o_t_a___hotel_rate_plan_notif_r_q___offers___offer.html#a624a73a9654f4e7f59e243404b2d7164", null ],
    [ "getCompatibleOffers", "class_model___o_t_a___hotel_rate_plan_notif_r_q___offers___offer.html#aa8dde36f9ec4480dd226607493ae1e75", null ],
    [ "getDiscount", "class_model___o_t_a___hotel_rate_plan_notif_r_q___offers___offer.html#a4212c46dea78626b38100ee9be02c512", null ],
    [ "getInventories", "class_model___o_t_a___hotel_rate_plan_notif_r_q___offers___offer.html#a2526b286831419fb3e300e606bfabd18", null ],
    [ "getOfferCode", "class_model___o_t_a___hotel_rate_plan_notif_r_q___offers___offer.html#a07f26b2a1b74364ceeab4c5b7bd4a4ae", null ],
    [ "getOfferDescription", "class_model___o_t_a___hotel_rate_plan_notif_r_q___offers___offer.html#a1938058815ff39bf9ca79ab95d97b72f", null ],
    [ "getOfferRules", "class_model___o_t_a___hotel_rate_plan_notif_r_q___offers___offer.html#aa0a0241f77cb5eb50cd2fc977147c6c2", null ],
    [ "getRph", "class_model___o_t_a___hotel_rate_plan_notif_r_q___offers___offer.html#a347a10c3b32e7a33371d384e3b833027", null ],
    [ "setApplicationOrder", "class_model___o_t_a___hotel_rate_plan_notif_r_q___offers___offer.html#a666428ece3c16bf41b5e934bdf5b8c3b", null ],
    [ "setCompatibleOffers", "class_model___o_t_a___hotel_rate_plan_notif_r_q___offers___offer.html#ae14a454e945bc71e89b6f206fb5144ec", null ],
    [ "setDiscount", "class_model___o_t_a___hotel_rate_plan_notif_r_q___offers___offer.html#aca2a8a857a8949d71b33304b78a0fd62", null ],
    [ "setInventories", "class_model___o_t_a___hotel_rate_plan_notif_r_q___offers___offer.html#abdff7f649770b23ad53c96caa3dd6b60", null ],
    [ "setOfferCode", "class_model___o_t_a___hotel_rate_plan_notif_r_q___offers___offer.html#aa16f2a691f3478a25af3667e6b9f327a", null ],
    [ "setOfferDescription", "class_model___o_t_a___hotel_rate_plan_notif_r_q___offers___offer.html#a82b7ad77a33813de65b20faa05c9c64f", null ],
    [ "setOfferRules", "class_model___o_t_a___hotel_rate_plan_notif_r_q___offers___offer.html#a6521d8b5743760920e7e10c9f936dd91", null ],
    [ "setRph", "class_model___o_t_a___hotel_rate_plan_notif_r_q___offers___offer.html#a138472ef1d48210e174fd99466023d7f", null ]
];