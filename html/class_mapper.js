var class_mapper =
[
    [ "Mapper", "class_mapper.html#a190356362c2b2f32006ede35d045bf47", null ],
    [ "~Mapper", "class_mapper.html#a883069aa394d1f0a4d45b6368b961541", null ],
    [ "getModel", "class_mapper.html#a5b52286430281526349ae0e6139f5a2d", null ],
    [ "getParser", "class_mapper.html#a5c0e1ee374db1efb7e3e540bce995e52", null ],
    [ "getRootTag", "class_mapper.html#aa5b39ebb90fd2bc2dae9960d5f1d1f2d", null ],
    [ "getXmlString", "class_mapper.html#a5a23fb20afaad1226158be18cc1ffcc2", null ],
    [ "init", "class_mapper.html#a9cebad67cab92917cef98b107ccd09b2", null ],
    [ "processXml", "class_mapper.html#afc9686607d4393ea521a08a37341ad5e", null ],
    [ "setModel", "class_mapper.html#a4e838931f797b2eccef56130d5f1ea41", null ],
    [ "setParser", "class_mapper.html#a1bc72ac17e4161d49f23644770766e15", null ],
    [ "setRootTag", "class_mapper.html#a92321479a95ead094a991336d9c58d4b", null ],
    [ "setXmlString", "class_mapper.html#a5f07e3bdd9a129f72eed2417f2aa5ac2", null ]
];