var class_mapper___o_t_a___hotel_rate_plan_notif_r_q___supplements___supplement =
[
    [ "Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___supplements___supplement.html#a16c3699d2340a5677f2d3d7b71c141e6", null ],
    [ "~Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___supplements___supplement.html#a552946b7034e300c18944ed3f62a345f", null ],
    [ "setAdditionalGuestNumber", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___supplements___supplement.html#ada125f209be988e886d5f70195894466", null ],
    [ "setAddToBasicRate", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___supplements___supplement.html#af8b228b989098c6880fa71aaf9b34fb9", null ],
    [ "setAgeQualifyingCode", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___supplements___supplement.html#a60131bed46fb39c8a006c1f0731c38f6", null ],
    [ "setAmount", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___supplements___supplement.html#a19ca484eb5a88e949875b1c854b4acd9", null ],
    [ "setChargeType", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___supplements___supplement.html#aae61fe614dcf053b0a913280e73d60ab", null ],
    [ "setCurrencyCode", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___supplements___supplement.html#a750b3d6874e15b2352df19d293388228", null ],
    [ "setEnd", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___supplements___supplement.html#a75c1e71e6532967e8e536f8f8a048720", null ],
    [ "setInvCode", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___supplements___supplement.html#af24cf0d55370840681ac46dbd7b6c6b9", null ],
    [ "setInvType", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___supplements___supplement.html#a10e55fe3c518e8d73d38e2c934ca190b", null ],
    [ "setMandatoryIndicator", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___supplements___supplement.html#a3d19c272b921f6ffb87cbde5c60fe5ae", null ],
    [ "setMaxAge", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___supplements___supplement.html#aa699f5a6784fad2b378db03b8359cb25", null ],
    [ "setMinAge", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___supplements___supplement.html#a0cb9a3fa46abd0c19f0f8204776d0a3c", null ],
    [ "setPercent", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___supplements___supplement.html#aeb6d3f6e6048e8258aa5d8f80b90543c", null ],
    [ "setStart", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___supplements___supplement.html#aecc94105a8e0920b75ab131042978963", null ],
    [ "setSupplementType", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___supplements___supplement.html#a7a1f2036cd6a5557dab8c04b8b47b3f8", null ]
];