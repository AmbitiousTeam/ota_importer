var class_model___o_t_a___hotel_rate_plan_notif_r_q___occupancies___occupancy =
[
    [ "Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy", "class_model___o_t_a___hotel_rate_plan_notif_r_q___occupancies___occupancy.html#a47c9f5b5151c48a5276cc126cf408f53", null ],
    [ "~Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy", "class_model___o_t_a___hotel_rate_plan_notif_r_q___occupancies___occupancy.html#a82e49a2d94ef125ba91198e450afea2e", null ],
    [ "getAgeQualifyingCode", "class_model___o_t_a___hotel_rate_plan_notif_r_q___occupancies___occupancy.html#a458be6a6e1614c99ba17c01fcaec44da", null ],
    [ "getAgeTimeUnit", "class_model___o_t_a___hotel_rate_plan_notif_r_q___occupancies___occupancy.html#ac71b7987eb00cb3727f584fa617eef09", null ],
    [ "getInfantsAreCounted", "class_model___o_t_a___hotel_rate_plan_notif_r_q___occupancies___occupancy.html#a2421e2fa67d4131f9ff001c6dba29154", null ],
    [ "getMaxAge", "class_model___o_t_a___hotel_rate_plan_notif_r_q___occupancies___occupancy.html#a6687cfe82aaad4263997178e30ebe0c0", null ],
    [ "getMaxOccupancy", "class_model___o_t_a___hotel_rate_plan_notif_r_q___occupancies___occupancy.html#a4f8549e10075c69170e38f2eceff15ee", null ],
    [ "getMinAge", "class_model___o_t_a___hotel_rate_plan_notif_r_q___occupancies___occupancy.html#a57ce6f0c8895f816d53051c03e176c36", null ],
    [ "getMinOccupancy", "class_model___o_t_a___hotel_rate_plan_notif_r_q___occupancies___occupancy.html#a8dc42f919557921aa5692ff1f821a74b", null ],
    [ "setAgeQualifyingCode", "class_model___o_t_a___hotel_rate_plan_notif_r_q___occupancies___occupancy.html#a1c855ae617aab96c184d54a901f0ff4f", null ],
    [ "setAgeTimeUnit", "class_model___o_t_a___hotel_rate_plan_notif_r_q___occupancies___occupancy.html#a9c98194b51be13233f31299dfb929f17", null ],
    [ "setInfantsAreCounted", "class_model___o_t_a___hotel_rate_plan_notif_r_q___occupancies___occupancy.html#a4194c2e774403283385090353e3a4b74", null ],
    [ "setMaxAge", "class_model___o_t_a___hotel_rate_plan_notif_r_q___occupancies___occupancy.html#a7862ffdd86e61ab6c1a5ecd3fc9c5aca", null ],
    [ "setMaxOccupancy", "class_model___o_t_a___hotel_rate_plan_notif_r_q___occupancies___occupancy.html#a2f24d8b88e4b78ca5b19c0c0222b9115", null ],
    [ "setMinAge", "class_model___o_t_a___hotel_rate_plan_notif_r_q___occupancies___occupancy.html#a2c04041bdf3db27116a0b4e6be68f7cd", null ],
    [ "setMinOccupancy", "class_model___o_t_a___hotel_rate_plan_notif_r_q___occupancies___occupancy.html#a9730c428f71df212f86144b18975113e", null ]
];