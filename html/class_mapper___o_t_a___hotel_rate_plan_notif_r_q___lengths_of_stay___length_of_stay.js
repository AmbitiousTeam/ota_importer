var class_mapper___o_t_a___hotel_rate_plan_notif_r_q___lengths_of_stay___length_of_stay =
[
    [ "Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___lengths_of_stay___length_of_stay.html#a022aff0daf6bf419e322d783991d92ef", null ],
    [ "~Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___lengths_of_stay___length_of_stay.html#ade6f92c04166222cf07ba5647d718735", null ],
    [ "getMinMaxMessageType", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___lengths_of_stay___length_of_stay.html#a513bc4dee1341403b266d6b81868b992", null ],
    [ "getTime", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___lengths_of_stay___length_of_stay.html#ab2a0e31a253017862c97cc4c421b0d29", null ],
    [ "getTimeUnit", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___lengths_of_stay___length_of_stay.html#aecfb9ba15490c8661259a9aeccf08a1a", null ],
    [ "setMinMaxMessageType", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___lengths_of_stay___length_of_stay.html#aebc01e846a9d0a2fc0d292d1d95cf690", null ],
    [ "setTime", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___lengths_of_stay___length_of_stay.html#af9786c38ecbc8f58b8ba09b789264a69", null ],
    [ "setTimeUnit", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___lengths_of_stay___length_of_stay.html#a082810a1e73aa5cb0c92b1a75ec2d34a", null ]
];