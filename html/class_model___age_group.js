var class_model___age_group =
[
    [ "Model_AgeGroup", "class_model___age_group.html#a03b8955a08b1b30a750a0c7f58d81baf", null ],
    [ "~Model_AgeGroup", "class_model___age_group.html#a30f59043bd1594d06ba83ca1aea9b7a0", null ],
    [ "getAdultMaxAge", "class_model___age_group.html#ad0438826a41dce087bf89f8aa837fc06", null ],
    [ "getAdultMinAge", "class_model___age_group.html#ab3072544b92627a4788b730a4a5fc4e1", null ],
    [ "getChildrenMaxAge", "class_model___age_group.html#a25e353cac812a622e6ba1298afd6671e", null ],
    [ "getChildrenMinAge", "class_model___age_group.html#ada248fd5a7b61b4e856b207ee121fd9b", null ],
    [ "getInfantMaxAge", "class_model___age_group.html#ae480f38fcf9653cb8a2fcf584f836829", null ],
    [ "getInfantMinAge", "class_model___age_group.html#af806778373b9cd8588353690bd86bbe6", null ],
    [ "setAdultMaxAge", "class_model___age_group.html#aeb198fcee5eba41b782969f221a9af66", null ],
    [ "setAdultMinAge", "class_model___age_group.html#ad46d3f7f5afceee0de6724c76ccfe0ee", null ],
    [ "setChildrenMaxAge", "class_model___age_group.html#a8d1ffdc25f97e2b68051ec2165128f6a", null ],
    [ "setChildrenMinAge", "class_model___age_group.html#acba9ae332827ad403770eba2be68702a", null ],
    [ "setInfantMaxAge", "class_model___age_group.html#a8655bc8199223d370ea9519a792c1334", null ],
    [ "setInfantMinAge", "class_model___age_group.html#af221560338d76dc36f520b418fce8f13", null ]
];