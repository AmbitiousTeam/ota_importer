var class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rate_plans___rate_plan =
[
    [ "Mapper_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rate_plans___rate_plan.html#aff36cd3a720131189f83a95a6ead17c2", null ],
    [ "~Mapper_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rate_plans___rate_plan.html#ac96b5b0891c7b3a10ffcdd6f0e016dc7", null ],
    [ "collectBookingRules", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rate_plans___rate_plan.html#a10d32c8822d5480452f21dc8ccf3003f", null ],
    [ "collectOffers", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rate_plans___rate_plan.html#a0657c748e15a90ff4567f26a40b2ee84", null ],
    [ "collectRates", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rate_plans___rate_plan.html#a16fce33dae8ca56a610a90278537fcc3", null ],
    [ "collectSellableProducts", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rate_plans___rate_plan.html#a61370f7ba1e841a5e2658391b9a719ea", null ],
    [ "collectSupplements", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rate_plans___rate_plan.html#ab32d12c294d60ce5540c6aa00b78ba19", null ],
    [ "setChargeType", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rate_plans___rate_plan.html#a7dd52041562365327a6a7c93386dcc1e", null ],
    [ "setPromotionCode", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rate_plans___rate_plan.html#a408e0ac185f40890f026556be5aa9e35", null ],
    [ "setRatePlanCode", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rate_plans___rate_plan.html#a6df45c8d647661329f2f509a42886132", null ]
];