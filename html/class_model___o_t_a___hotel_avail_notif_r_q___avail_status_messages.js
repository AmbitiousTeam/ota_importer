var class_model___o_t_a___hotel_avail_notif_r_q___avail_status_messages =
[
    [ "Model_OTA_HotelAvailNotifRQ_AvailStatusMessages", "class_model___o_t_a___hotel_avail_notif_r_q___avail_status_messages.html#a413d0a6f89dce3bcf3211b380431bdd8", null ],
    [ "~Model_OTA_HotelAvailNotifRQ_AvailStatusMessages", "class_model___o_t_a___hotel_avail_notif_r_q___avail_status_messages.html#acc71d881ec2f793e78f1b16d52ff9015", null ],
    [ "getDelta", "class_model___o_t_a___hotel_avail_notif_r_q___avail_status_messages.html#a2169d1e9afdadc8ebc6eeba446d1f56b", null ],
    [ "getHotelCode", "class_model___o_t_a___hotel_avail_notif_r_q___avail_status_messages.html#a25cfe98a425b6710a430cde7068a654b", null ],
    [ "getIdCon", "class_model___o_t_a___hotel_avail_notif_r_q___avail_status_messages.html#ae497eecf7ff0bfecdad09cedfe78a0e9", null ],
    [ "setDelta", "class_model___o_t_a___hotel_avail_notif_r_q___avail_status_messages.html#a4053b8093cfa661397d0d33761baca75", null ],
    [ "setDelta", "class_model___o_t_a___hotel_avail_notif_r_q___avail_status_messages.html#ac1c3f2321c57e26569443e25dda87c14", null ],
    [ "setHotelCode", "class_model___o_t_a___hotel_avail_notif_r_q___avail_status_messages.html#a03c24ed93847cd9528c6b286ed9becba", null ],
    [ "setIdCon", "class_model___o_t_a___hotel_avail_notif_r_q___avail_status_messages.html#ac8482df86aba53d85b2026a679be33f7", null ]
];