var class_mapper___o_t_a___hotel_rate_plan_notif_r_q___base_by_guest_amts___base_by_guest_amt =
[
    [ "Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___base_by_guest_amts___base_by_guest_amt.html#a6a98064701cd61e8a1558200d7230df2", null ],
    [ "~Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___base_by_guest_amts___base_by_guest_amt.html#a71a43d815ea0e865734ea39dd4b5311d", null ],
    [ "getAmountAfterTax", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___base_by_guest_amts___base_by_guest_amt.html#a85e9109516eb02b7280703766112496e", null ],
    [ "getCurrencyCode", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___base_by_guest_amts___base_by_guest_amt.html#abb934e1431a5d6c0d15178cba61def1d", null ],
    [ "getNumberOfGuests", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___base_by_guest_amts___base_by_guest_amt.html#a56417af221a6afa190580f4e626dd357", null ],
    [ "setAmountAfterTax", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___base_by_guest_amts___base_by_guest_amt.html#a27151676bd12be5d79eda01818785ab4", null ],
    [ "setCurrencyCode", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___base_by_guest_amts___base_by_guest_amt.html#aabb516a035a0834bf9e58817aca101a1", null ],
    [ "setMaxAge", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___base_by_guest_amts___base_by_guest_amt.html#aa23a709b0c1ae93dd2b59243cbc91add", null ],
    [ "setMinAge", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___base_by_guest_amts___base_by_guest_amt.html#ad0e8aa057edbd213952442463d0e66bb", null ],
    [ "setNumberOfGuests", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___base_by_guest_amts___base_by_guest_amt.html#a733874694514765f5fec58c2132f618c", null ]
];