var class_model___o_t_a___hotel_rate_plan_notif_r_q___lengths_of_stay___length_of_stay =
[
    [ "Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay", "class_model___o_t_a___hotel_rate_plan_notif_r_q___lengths_of_stay___length_of_stay.html#aafe1a07da5fc1c4c60ab69119bb0d454", null ],
    [ "~Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay", "class_model___o_t_a___hotel_rate_plan_notif_r_q___lengths_of_stay___length_of_stay.html#ad2c884270e2b20a58209a59ef31197c8", null ],
    [ "getMinMaxMessageType", "class_model___o_t_a___hotel_rate_plan_notif_r_q___lengths_of_stay___length_of_stay.html#ac0cde413e44dcdded31e60934d475670", null ],
    [ "getTime", "class_model___o_t_a___hotel_rate_plan_notif_r_q___lengths_of_stay___length_of_stay.html#a90969c9d7ac98a301cca5c36007d9bf9", null ],
    [ "getTimeUnit", "class_model___o_t_a___hotel_rate_plan_notif_r_q___lengths_of_stay___length_of_stay.html#aff5374c8fe04a15b1571314e8e046199", null ],
    [ "setMinMaxMessageType", "class_model___o_t_a___hotel_rate_plan_notif_r_q___lengths_of_stay___length_of_stay.html#aecd36e96f0260408061ab081b7972573", null ],
    [ "setTime", "class_model___o_t_a___hotel_rate_plan_notif_r_q___lengths_of_stay___length_of_stay.html#ae6ea828942bccec5c9aa588d991bb211", null ],
    [ "setTimeUnit", "class_model___o_t_a___hotel_rate_plan_notif_r_q___lengths_of_stay___length_of_stay.html#a8468514bcc318a81afeda61a01f3e2cb", null ]
];