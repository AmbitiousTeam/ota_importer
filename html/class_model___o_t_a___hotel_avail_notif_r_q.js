var class_model___o_t_a___hotel_avail_notif_r_q =
[
    [ "Model_OTA_HotelAvailNotifRQ", "class_model___o_t_a___hotel_avail_notif_r_q.html#a5fbb4f0fb327bbf1f04f097ab3a85411", null ],
    [ "~Model_OTA_HotelAvailNotifRQ", "class_model___o_t_a___hotel_avail_notif_r_q.html#a2ca845fbeb2aaab816b7a77bf1fda118", null ],
    [ "getAvailStatusMessages", "class_model___o_t_a___hotel_avail_notif_r_q.html#a35528b705866767931fe63226f7a5203", null ],
    [ "getXmlNsXsd", "class_model___o_t_a___hotel_avail_notif_r_q.html#af59f9e3d446aea6e5770fcd2eeaf2b8e", null ],
    [ "getXmlNsXsi", "class_model___o_t_a___hotel_avail_notif_r_q.html#abaa0465f5121adbf5ae0347497ffbe6d", null ],
    [ "setAvailStatusMessages", "class_model___o_t_a___hotel_avail_notif_r_q.html#a774dd3b32ce67a256cb71697cadc6723", null ],
    [ "setXmlNsXsd", "class_model___o_t_a___hotel_avail_notif_r_q.html#ad9220ab6887ba0db7ada7aca77ce610e", null ],
    [ "setXmlNsXsi", "class_model___o_t_a___hotel_avail_notif_r_q.html#a6e71486992d12ff33d21a4916ba3f746", null ]
];