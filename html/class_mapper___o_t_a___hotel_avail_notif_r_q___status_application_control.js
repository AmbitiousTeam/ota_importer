var class_mapper___o_t_a___hotel_avail_notif_r_q___status_application_control =
[
    [ "Mapper_OTA_HotelAvailNotifRQ_StatusApplicationControl", "class_mapper___o_t_a___hotel_avail_notif_r_q___status_application_control.html#abbbd121dea83a7ede746976a66a78eeb", null ],
    [ "~Mapper_OTA_HotelAvailNotifRQ_StatusApplicationControl", "class_mapper___o_t_a___hotel_avail_notif_r_q___status_application_control.html#aa9b0f828726d30f42596d5a06fdbf6f9", null ],
    [ "getEnd", "class_mapper___o_t_a___hotel_avail_notif_r_q___status_application_control.html#a38e4f307a120501ab1ff5579aa0a16cc", null ],
    [ "getInvCode", "class_mapper___o_t_a___hotel_avail_notif_r_q___status_application_control.html#ac20a8eae5b339ef0668121a9036deb57", null ],
    [ "getInvType", "class_mapper___o_t_a___hotel_avail_notif_r_q___status_application_control.html#a26eeee435500274f6d70ef17c752656c", null ],
    [ "getStart", "class_mapper___o_t_a___hotel_avail_notif_r_q___status_application_control.html#ae70c9742726f0032d36ad509bee8d442", null ],
    [ "setEnd", "class_mapper___o_t_a___hotel_avail_notif_r_q___status_application_control.html#a9dd4ae8b679466fe4435451c2615c2fc", null ],
    [ "setInvCode", "class_mapper___o_t_a___hotel_avail_notif_r_q___status_application_control.html#aa47655d33ea6ba97bff4d28f5fb2213b", null ],
    [ "setInvType", "class_mapper___o_t_a___hotel_avail_notif_r_q___status_application_control.html#af138215e17197ec4b3aea7303cb46bfa", null ],
    [ "setStart", "class_mapper___o_t_a___hotel_avail_notif_r_q___status_application_control.html#acf3500ae44a361b1f732d3bfb2ebda29", null ]
];