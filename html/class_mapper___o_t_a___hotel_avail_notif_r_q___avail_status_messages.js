var class_mapper___o_t_a___hotel_avail_notif_r_q___avail_status_messages =
[
    [ "Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages", "class_mapper___o_t_a___hotel_avail_notif_r_q___avail_status_messages.html#a84c1fa1daf033c69e2c5cba4c0b8ca10", null ],
    [ "~Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages", "class_mapper___o_t_a___hotel_avail_notif_r_q___avail_status_messages.html#a1fbd59860035465e9990085e5d7f334f", null ],
    [ "collectAvailStatusMessage", "class_mapper___o_t_a___hotel_avail_notif_r_q___avail_status_messages.html#ae9d756d458f425bacbfc0190b86317e1", null ],
    [ "getDelta", "class_mapper___o_t_a___hotel_avail_notif_r_q___avail_status_messages.html#ad6d92c4110a149370f5be866cf27beac", null ],
    [ "getHotelCode", "class_mapper___o_t_a___hotel_avail_notif_r_q___avail_status_messages.html#a8cdffabd5e34d2e7c611f16adddb3dfe", null ],
    [ "getIdCon", "class_mapper___o_t_a___hotel_avail_notif_r_q___avail_status_messages.html#aff56be9a621ea3a2dc5f50d5ca3787e1", null ],
    [ "setDelta", "class_mapper___o_t_a___hotel_avail_notif_r_q___avail_status_messages.html#ae3a00f8488a137eda04a8c75d4d78092", null ],
    [ "setHotelCode", "class_mapper___o_t_a___hotel_avail_notif_r_q___avail_status_messages.html#a6878ec1cd8c683248d7d2180dafbdbc5", null ],
    [ "setIdCon", "class_mapper___o_t_a___hotel_avail_notif_r_q___avail_status_messages.html#ae28b9aa4e0dd0c691adbba6aee47f392", null ]
];