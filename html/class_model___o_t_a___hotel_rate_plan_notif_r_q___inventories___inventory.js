var class_model___o_t_a___hotel_rate_plan_notif_r_q___inventories___inventory =
[
    [ "Model_OTA_HotelRatePlanNotifRQ_Inventories_Inventory", "class_model___o_t_a___hotel_rate_plan_notif_r_q___inventories___inventory.html#a9c061ed4180d9645d796152e62cad846", null ],
    [ "~Model_OTA_HotelRatePlanNotifRQ_Inventories_Inventory", "class_model___o_t_a___hotel_rate_plan_notif_r_q___inventories___inventory.html#a953d4592b8aa77b1913fae3d6f70a09a", null ],
    [ "getAppliesToIndicator", "class_model___o_t_a___hotel_rate_plan_notif_r_q___inventories___inventory.html#a7ba06b0b0916be410f5ae60d000167bc", null ],
    [ "getInvCode", "class_model___o_t_a___hotel_rate_plan_notif_r_q___inventories___inventory.html#a098402b7e42f59c9f742503b100e7969", null ],
    [ "getInvType", "class_model___o_t_a___hotel_rate_plan_notif_r_q___inventories___inventory.html#a79f10390447161caa874307fe50cf134", null ],
    [ "setAppliesToIndicator", "class_model___o_t_a___hotel_rate_plan_notif_r_q___inventories___inventory.html#af87a32c04dab8db57b66d9a63f11d324", null ],
    [ "setInvCode", "class_model___o_t_a___hotel_rate_plan_notif_r_q___inventories___inventory.html#a03369330f992e792f67733469cd58f8d", null ],
    [ "setInvType", "class_model___o_t_a___hotel_rate_plan_notif_r_q___inventories___inventory.html#a14cd0c618af85d9accc13e471a966ff1", null ]
];