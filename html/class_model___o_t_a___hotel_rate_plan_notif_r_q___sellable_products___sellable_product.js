var class_model___o_t_a___hotel_rate_plan_notif_r_q___sellable_products___sellable_product =
[
    [ "Model_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct", "class_model___o_t_a___hotel_rate_plan_notif_r_q___sellable_products___sellable_product.html#a9498d855bbe07a7664cb4d7a2129b260", null ],
    [ "~Model_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct", "class_model___o_t_a___hotel_rate_plan_notif_r_q___sellable_products___sellable_product.html#a007df7906d8d4a8345d2e90652a2ac53", null ],
    [ "getGuestRoom", "class_model___o_t_a___hotel_rate_plan_notif_r_q___sellable_products___sellable_product.html#a883f0920e48418c9159c7559461c55c6", null ],
    [ "getInvCode", "class_model___o_t_a___hotel_rate_plan_notif_r_q___sellable_products___sellable_product.html#a9a32a9bc81e0543c3369f6d194bbff3a", null ],
    [ "getInvType", "class_model___o_t_a___hotel_rate_plan_notif_r_q___sellable_products___sellable_product.html#a5fcc2b89d873903ec34ef844e9e1b72c", null ],
    [ "setGuestRoom", "class_model___o_t_a___hotel_rate_plan_notif_r_q___sellable_products___sellable_product.html#a5fb48412d454d1c49f7928d4c40b2c20", null ],
    [ "setInvCode", "class_model___o_t_a___hotel_rate_plan_notif_r_q___sellable_products___sellable_product.html#a354e4cc8ff97b385665356f9e5948858", null ],
    [ "setInvType", "class_model___o_t_a___hotel_rate_plan_notif_r_q___sellable_products___sellable_product.html#a15677d2050a95edb1fabdcb0e35197ad", null ]
];