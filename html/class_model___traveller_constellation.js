var class_model___traveller_constellation =
[
    [ "Model_TravellerConstellation", "class_model___traveller_constellation.html#a7845346ca3cabb5054d1b7bef2197f17", null ],
    [ "~Model_TravellerConstellation", "class_model___traveller_constellation.html#ae96c6c7784105542cd23988094d26654", null ],
    [ "getAdditionalAdults", "class_model___traveller_constellation.html#a7108d54247a8c762264c96d26624f679", null ],
    [ "getAdditionalChildren", "class_model___traveller_constellation.html#a495e81d9a2ed55ef3192da18fd7754c6", null ],
    [ "getAdditionalInfants", "class_model___traveller_constellation.html#acef8ae64a16057dd7b8bdc06fc86d729", null ],
    [ "getAdultCount", "class_model___traveller_constellation.html#a3ecc09f6976c5b6142321f171d1d2635", null ],
    [ "getAgeGroup", "class_model___traveller_constellation.html#ab8fc1ef1392980068440a51bd4f29adc", null ],
    [ "getChildCount", "class_model___traveller_constellation.html#a47e25553a44254adc8cd7965221a7ccc", null ],
    [ "getInfantCount", "class_model___traveller_constellation.html#ab32a808cacb28bb9333268e525d164ce", null ],
    [ "getPersonCount", "class_model___traveller_constellation.html#acf4dd007eb8797e51242704ef998c09c", null ],
    [ "getStandardCapacity", "class_model___traveller_constellation.html#ad90b71a543748d710ed7eb4f9650bcea", null ],
    [ "setAdditionalAdults", "class_model___traveller_constellation.html#aa29a17c28346023f4fa7198cffaf6f4b", null ],
    [ "setAdditionalChildren", "class_model___traveller_constellation.html#ae0ff74eaa8b9b57a81ca0b15214defa5", null ],
    [ "setAdditionalInfants", "class_model___traveller_constellation.html#a092ac38ebcdc0315b8739d8a134fd0f7", null ],
    [ "setAdultCount", "class_model___traveller_constellation.html#a180997d90ca04d3732e060fc2ecc0d0f", null ],
    [ "setAgeGroup", "class_model___traveller_constellation.html#a6e702c9d8835954d0c23d016b60202d5", null ],
    [ "setChildCount", "class_model___traveller_constellation.html#a7f178e0b0c47b7dda3f912c62f6a9f21", null ],
    [ "setInfantCount", "class_model___traveller_constellation.html#a98ce3cc3e8db3cf67d3c36eafa30244e", null ],
    [ "setPersonCount", "class_model___traveller_constellation.html#adc96a288333613646a72da26896fab66", null ],
    [ "setStandardCapacity", "class_model___traveller_constellation.html#a7ae57a4e3b32fb9b5295f4af7db8dc32", null ]
];