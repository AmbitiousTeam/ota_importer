var class_service___boarding =
[
    [ "Service_Boarding", "class_service___boarding.html#a48ac68b895e3e6bfc622885c1f404b58", null ],
    [ "~Service_Boarding", "class_service___boarding.html#ad83b4d21f0d062f15d72d25f3af45a7d", null ],
    [ "collectBoardingsFromSellableProducts", "class_service___boarding.html#a6485a646f83a40df7986ed0fa8d77fe9", null ],
    [ "collectBoardingsFromSupplements", "class_service___boarding.html#ad87a2f2691453ea2b33adceb67388c62", null ],
    [ "getBoardings", "class_service___boarding.html#a7409922238446a69217ae55c96dc65f5", null ],
    [ "setBoardings", "class_service___boarding.html#a61010026004a0afb74129aa7b616aa5f", null ]
];