var class_model___o_t_a___hotel_rate_plan_notif_r_q___cancel_penalty =
[
    [ "Model_OTA_HotelRatePlanNotifRQ_CancelPenalty", "class_model___o_t_a___hotel_rate_plan_notif_r_q___cancel_penalty.html#a09519e45a21ac49dd80d9300faaf089a", null ],
    [ "~Model_OTA_HotelRatePlanNotifRQ_CancelPenalty", "class_model___o_t_a___hotel_rate_plan_notif_r_q___cancel_penalty.html#a997774e81170f3c3501016cfc7cf93ff", null ],
    [ "getAmountPercent", "class_model___o_t_a___hotel_rate_plan_notif_r_q___cancel_penalty.html#af7f38b465d18829813d82247f90a1ef0", null ],
    [ "getDeadline", "class_model___o_t_a___hotel_rate_plan_notif_r_q___cancel_penalty.html#a7e6a2f164f94361f4aa7d11fdbf03835", null ],
    [ "setAmountPercent", "class_model___o_t_a___hotel_rate_plan_notif_r_q___cancel_penalty.html#a3438f3545ab995ffe997bd5c350499aa", null ],
    [ "setDeadline", "class_model___o_t_a___hotel_rate_plan_notif_r_q___cancel_penalty.html#a7286954a4b9d81871ef57643a8fb99df", null ]
];