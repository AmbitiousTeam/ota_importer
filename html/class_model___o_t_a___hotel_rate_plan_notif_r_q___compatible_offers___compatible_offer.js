var class_model___o_t_a___hotel_rate_plan_notif_r_q___compatible_offers___compatible_offer =
[
    [ "Model_OTA_HotelRatePlanNotifRQ_CompatibleOffers_CompatibleOffer", "class_model___o_t_a___hotel_rate_plan_notif_r_q___compatible_offers___compatible_offer.html#a879203888ef779e6a3d54349a30802dd", null ],
    [ "~Model_OTA_HotelRatePlanNotifRQ_CompatibleOffers_CompatibleOffer", "class_model___o_t_a___hotel_rate_plan_notif_r_q___compatible_offers___compatible_offer.html#afff61ffdc2e6c425e8aed274e33b2624", null ],
    [ "getIncompatibleOfferIndicator", "class_model___o_t_a___hotel_rate_plan_notif_r_q___compatible_offers___compatible_offer.html#acae2b9a15cbab7a166b8d20adefe34a8", null ],
    [ "getOfferRph", "class_model___o_t_a___hotel_rate_plan_notif_r_q___compatible_offers___compatible_offer.html#a9aff136fc7dbc424cc808a53dcd348c0", null ],
    [ "setIncompatibleOfferIndicator", "class_model___o_t_a___hotel_rate_plan_notif_r_q___compatible_offers___compatible_offer.html#a7cee166f08525a914c2b06b98e687965", null ],
    [ "setOfferRph", "class_model___o_t_a___hotel_rate_plan_notif_r_q___compatible_offers___compatible_offer.html#a0eb8d735eba39193bb6ab95ee7cc4b66", null ]
];