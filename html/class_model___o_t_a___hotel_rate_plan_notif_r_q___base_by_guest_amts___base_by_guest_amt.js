var class_model___o_t_a___hotel_rate_plan_notif_r_q___base_by_guest_amts___base_by_guest_amt =
[
    [ "Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt", "class_model___o_t_a___hotel_rate_plan_notif_r_q___base_by_guest_amts___base_by_guest_amt.html#a6d6588d090f5ce6a29a18a9975156dc3", null ],
    [ "~Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt", "class_model___o_t_a___hotel_rate_plan_notif_r_q___base_by_guest_amts___base_by_guest_amt.html#abf45f1b0e2f9c1caf5fe0ec8c4da2b80", null ],
    [ "getAgeQualifyingCode", "class_model___o_t_a___hotel_rate_plan_notif_r_q___base_by_guest_amts___base_by_guest_amt.html#a4c782a7dea7128259dbb58be2fd54a35", null ],
    [ "getAmountAfterTax", "class_model___o_t_a___hotel_rate_plan_notif_r_q___base_by_guest_amts___base_by_guest_amt.html#aed79970692821506a6d77ceaff7f0307", null ],
    [ "getCurrencyCode", "class_model___o_t_a___hotel_rate_plan_notif_r_q___base_by_guest_amts___base_by_guest_amt.html#acdab93ea46f8e790a9b77aa79a269a5a", null ],
    [ "getMaxAge", "class_model___o_t_a___hotel_rate_plan_notif_r_q___base_by_guest_amts___base_by_guest_amt.html#aeaa2c3fa5b916ea555fddb611c35afbb", null ],
    [ "getMinAge", "class_model___o_t_a___hotel_rate_plan_notif_r_q___base_by_guest_amts___base_by_guest_amt.html#aa00d03eba14d65ade701e0f867be5919", null ],
    [ "getNumberOfGuests", "class_model___o_t_a___hotel_rate_plan_notif_r_q___base_by_guest_amts___base_by_guest_amt.html#a8864ae03d1a5e9e7a0c665c5ee72415d", null ],
    [ "setAgeQualifyingCode", "class_model___o_t_a___hotel_rate_plan_notif_r_q___base_by_guest_amts___base_by_guest_amt.html#ac322cd9d01318a6658c2518455f9304e", null ],
    [ "setAmountAfterTax", "class_model___o_t_a___hotel_rate_plan_notif_r_q___base_by_guest_amts___base_by_guest_amt.html#a4cdc0e08f1e289dcd566e2fc05f9cfc2", null ],
    [ "setCurrencyCode", "class_model___o_t_a___hotel_rate_plan_notif_r_q___base_by_guest_amts___base_by_guest_amt.html#a7ce9ca59282d4df6b1eb3faff8ea0ae6", null ],
    [ "setMaxAge", "class_model___o_t_a___hotel_rate_plan_notif_r_q___base_by_guest_amts___base_by_guest_amt.html#a9fa90aa7ba8d3e7b9ea50892803ca298", null ],
    [ "setMinAge", "class_model___o_t_a___hotel_rate_plan_notif_r_q___base_by_guest_amts___base_by_guest_amt.html#a7d078c4040a7dd194dc5c8c716f77d5e", null ],
    [ "setNumberOfGuests", "class_model___o_t_a___hotel_rate_plan_notif_r_q___base_by_guest_amts___base_by_guest_amt.html#a94bd5f4c623ca3dc42322e4d8a8b0f83", null ]
];