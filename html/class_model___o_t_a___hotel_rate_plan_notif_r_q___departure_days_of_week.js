var class_model___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week =
[
    [ "Model_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek", "class_model___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week.html#ac40ca60a98f862850cda569ca9d519a5", null ],
    [ "~Model_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek", "class_model___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week.html#aadcea1e34b1b304db3ab312c1f9304b6", null ],
    [ "getFri", "class_model___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week.html#a99f8022a6c5c882b927fb510175755ae", null ],
    [ "getMon", "class_model___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week.html#a33d24ba47f317d4afa1d7ebb79b9ec42", null ],
    [ "getSat", "class_model___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week.html#a871c1ce1e3ea6d7a8035a9aaba039155", null ],
    [ "getSun", "class_model___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week.html#ae34ea7aae9cc832d6b9107048fe06b7f", null ],
    [ "getThu", "class_model___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week.html#a5e9f8abfd131e8d93bcaca51435dcc96", null ],
    [ "getTue", "class_model___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week.html#ac424ede2b808b8d8b0dee060d11bc10f", null ],
    [ "getWed", "class_model___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week.html#a6cc826e8ee4faf510c4dc91cbcaa6974", null ],
    [ "setFri", "class_model___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week.html#a319c730601d8e9f78f61fcf089d14916", null ],
    [ "setMon", "class_model___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week.html#ae131e54013a2a88f20dc413b6702ac9b", null ],
    [ "setSat", "class_model___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week.html#a22e01d1c7bbd42f3ca9a9ee771f75909", null ],
    [ "setSun", "class_model___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week.html#a554214974abd3f5e581e7be7f577441e", null ],
    [ "setThu", "class_model___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week.html#a7039d8c3be7af6d923e525ed3c9e5139", null ],
    [ "setTue", "class_model___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week.html#a1e2f2b81480f16c2a7ff680c42727778", null ],
    [ "setWed", "class_model___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week.html#a513d8afec8e6e0c15b44cecae580e330", null ]
];