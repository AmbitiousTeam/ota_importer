var class_mapper___o_t_a___hotel_rate_plan_notif_r_q___inventories =
[
    [ "Mapper_OTA_HotelRatePlanNotifRQ_Inventories", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___inventories.html#ae7c49b29140b8caa30b181f09897e000", null ],
    [ "~Mapper_OTA_HotelRatePlanNotifRQ_Inventories", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___inventories.html#ad0ce3e3a2fff2796e81ae7901d53c07d", null ],
    [ "collectInventories", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___inventories.html#a5db30215dc49d069c55a9236ef521c65", null ],
    [ "getAppliesToIndicator", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___inventories.html#aaff089e98b95143704d83bcf6114d811", null ],
    [ "getInvCode", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___inventories.html#a420077b0c0b70506fc59746d7bb8487e", null ],
    [ "setAppliesToIndicator", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___inventories.html#af30e09f46aa827723d88833efd32b56f", null ],
    [ "setInvCode", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___inventories.html#aadb8be592a57822e110ada3dd50fa6f8", null ]
];