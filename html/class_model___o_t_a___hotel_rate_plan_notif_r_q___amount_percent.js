var class_model___o_t_a___hotel_rate_plan_notif_r_q___amount_percent =
[
    [ "Model_OTA_HotelRatePlanNotifRQ_AmountPercent", "class_model___o_t_a___hotel_rate_plan_notif_r_q___amount_percent.html#afde5ac7f23e1ef8b1f3aef48e2b9decc", null ],
    [ "~Model_OTA_HotelRatePlanNotifRQ_AmountPercent", "class_model___o_t_a___hotel_rate_plan_notif_r_q___amount_percent.html#a7cb97bd0bcb165a64d18993d7ba8dfab", null ],
    [ "getAmount", "class_model___o_t_a___hotel_rate_plan_notif_r_q___amount_percent.html#ab4ee781db650ea2519dcfbce129f8de0", null ],
    [ "getCurrencyCode", "class_model___o_t_a___hotel_rate_plan_notif_r_q___amount_percent.html#a73a6d74260aa2bcd0ec3e1ed84f0ebfd", null ],
    [ "getPercent", "class_model___o_t_a___hotel_rate_plan_notif_r_q___amount_percent.html#ad991fc1c597565c4ce1719bd438595ac", null ],
    [ "setAmount", "class_model___o_t_a___hotel_rate_plan_notif_r_q___amount_percent.html#a2baeac6f5116516a4e21b9a5ac6358b6", null ],
    [ "setCurrencyCode", "class_model___o_t_a___hotel_rate_plan_notif_r_q___amount_percent.html#ab91dcc48fb9c6664199c8d7e72982317", null ],
    [ "setPercent", "class_model___o_t_a___hotel_rate_plan_notif_r_q___amount_percent.html#a1d32f6db53002942bf1c9c950918cc1d", null ]
];