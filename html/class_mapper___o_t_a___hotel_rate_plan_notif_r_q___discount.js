var class_mapper___o_t_a___hotel_rate_plan_notif_r_q___discount =
[
    [ "Mapper_OTA_HotelRatePlanNotifRQ_Discount", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___discount.html#aea7a62f5859cb24bf1e4722e1f19cd64", null ],
    [ "~Mapper_OTA_HotelRatePlanNotifRQ_Discount", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___discount.html#a06e1d89c80ca56f02b4f822de1a7494a", null ],
    [ "getAmount", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___discount.html#a43642b417e9c31716ef1d8328569aec6", null ],
    [ "getNightsDiscounted", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___discount.html#ae15afb19c51ef82ddc46ae471c52f865", null ],
    [ "getNightsRequired", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___discount.html#a26aab6d33fc8be4ff4a93be78cbdc363", null ],
    [ "getPercent", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___discount.html#a66fec7966ab36e32cf587f7150fd77ee", null ],
    [ "setAmount", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___discount.html#ac5a1f711a7fa518c077716d5d3d8e632", null ],
    [ "setChargeUnitCode", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___discount.html#a82fd51879377bcdc64515992bcc8b8e5", null ],
    [ "setCurrencyCode", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___discount.html#aa98b5166805606a3e696ca856b4a8969", null ],
    [ "setDiscountLastBookingDays", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___discount.html#a33be431ef33ea872ac0788cb2c2292a0", null ],
    [ "setNightsDiscounted", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___discount.html#a03838196c9180129c6b4af6374febb7c", null ],
    [ "setNightsRequired", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___discount.html#af42ac7320245be84cbd29cfca3a6bca1", null ],
    [ "setPercent", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___discount.html#a772b3ec47668f003e3c202fc9f717d2d", null ]
];