var class_mapper___o_t_a___hotel_rate_plan_notif_r_q___occupancies___occupancy =
[
    [ "Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___occupancies___occupancy.html#ac3c66fd83679a348b8ce8a904a652dd4", null ],
    [ "~Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___occupancies___occupancy.html#a1180f19097b49eac2dee2309a0cce261", null ],
    [ "getAgeQualifyingCode", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___occupancies___occupancy.html#a44e3c73bffd00b75d70e98d35872ffd3", null ],
    [ "getInfantsAreCounted", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___occupancies___occupancy.html#a0f06d8bc7e2458476ae9d1f673f8cc5d", null ],
    [ "getMaxAge", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___occupancies___occupancy.html#aad7c876a03161b9fe07aac790a6f39f7", null ],
    [ "getMaxOccupancy", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___occupancies___occupancy.html#ab29d683f2215283845933f0e7da22fdc", null ],
    [ "getMinAge", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___occupancies___occupancy.html#ab51d3ad672cac49d587bb3eed7131df9", null ],
    [ "getMinOccupancy", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___occupancies___occupancy.html#aaa8f8eda937ec57595e341cc2493104e", null ],
    [ "setAgeQualifyingCode", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___occupancies___occupancy.html#a510e523e876b54e4c39fb7b75c13ad15", null ],
    [ "setInfantsAreCounted", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___occupancies___occupancy.html#adfbf14f4c2afa6961802e33db0e05440", null ],
    [ "setMaxAge", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___occupancies___occupancy.html#aab257a1c2db2db51339dfb072faaf09e", null ],
    [ "setMaxOccupancy", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___occupancies___occupancy.html#acb830738aa9fb8273f09b36031ffd2e0", null ],
    [ "setMinAge", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___occupancies___occupancy.html#a80fc5e0d8e7ce743d191af692fed9259", null ],
    [ "setMinOccupancy", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___occupancies___occupancy.html#a72cba270c5acd0d4488479cf8eee0428", null ]
];