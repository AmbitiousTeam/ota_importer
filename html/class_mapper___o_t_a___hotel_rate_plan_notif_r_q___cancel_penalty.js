var class_mapper___o_t_a___hotel_rate_plan_notif_r_q___cancel_penalty =
[
    [ "Mapper_OTA_HotelRatePlanNotifRQ_CancelPenalty", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___cancel_penalty.html#ab4eae5df49072ae0def581ccb2c29616", null ],
    [ "~Mapper_OTA_HotelRatePlanNotifRQ_CancelPenalty", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___cancel_penalty.html#aa0e5c976e75cf9bdeaada32def6fd178", null ],
    [ "getEnd", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___cancel_penalty.html#a98094fa242c20de42362f6bb322f9ad8", null ],
    [ "getStart", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___cancel_penalty.html#ad429da56d8fa38e278da023859557324", null ],
    [ "processAmountPercent", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___cancel_penalty.html#afe15760c416a0f0ed73012e38a065b81", null ],
    [ "processDeadline", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___cancel_penalty.html#ac38a2f7c711ad2cdd8243763da0a655a", null ],
    [ "setEnd", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___cancel_penalty.html#a16585f357417c04c49b369ebbe3d5b72", null ],
    [ "setStart", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___cancel_penalty.html#ae3008d000a7702f7ed0c29ad7296146f", null ]
];