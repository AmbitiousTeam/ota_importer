var class_model___o_t_a___hotel_rate_plan_notif_r_q___offer_rules___offer_rule =
[
    [ "Model_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule", "class_model___o_t_a___hotel_rate_plan_notif_r_q___offer_rules___offer_rule.html#aef1623164ea9f61a16dd382bdf6e2912", null ],
    [ "~Model_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule", "class_model___o_t_a___hotel_rate_plan_notif_r_q___offer_rules___offer_rule.html#aa4b3cf40a2cf2e87a5e845ad0f02713e", null ],
    [ "getDateRestrictions", "class_model___o_t_a___hotel_rate_plan_notif_r_q___offer_rules___offer_rule.html#a78f9445f8807dd557cf3a154d22e8e50", null ],
    [ "getLengthsOfStay", "class_model___o_t_a___hotel_rate_plan_notif_r_q___offer_rules___offer_rule.html#a747a9143e2aff96f3ddd1ed389be2425", null ],
    [ "setDateRestrictions", "class_model___o_t_a___hotel_rate_plan_notif_r_q___offer_rules___offer_rule.html#a0d3d31ff75d14dbccdbd531c1e807b30", null ],
    [ "setLengthsOfStay", "class_model___o_t_a___hotel_rate_plan_notif_r_q___offer_rules___offer_rule.html#a5fd94bd4bc09e8f90c214a8d89beb1b7", null ]
];