var searchData=
[
  ['remove',['remove',['../class_model___collection.html#a83b87a94dfd511b138cc47d0072ab1e8',1,'Model_Collection::remove(unsigned int)'],['../class_model___collection.html#aba6b36ce1973017ed8c26d030db8e45f',1,'Model_Collection::remove(std::string)']]],
  ['removexmlheader',['removeXmlHeader',['../class_service___xml_parser.html#a361c0114685958d5c8fc9695f78800e2',1,'Service_XmlParser']]],
  ['replace',['replace',['../class_model___collection.html#a0101c2bc360f6bcc1c7eea4f9a40b218',1,'Model_Collection::replace(unsigned int, T &amp;)'],['../class_model___collection.html#af21c12d01b28223119bc79f5e41b00df',1,'Model_Collection::replace(std::string, T &amp;)']]],
  ['reset',['reset',['../class_model___collection.html#a5f61f8e4e60c220717c4edc03b88ec47',1,'Model_Collection']]]
];
