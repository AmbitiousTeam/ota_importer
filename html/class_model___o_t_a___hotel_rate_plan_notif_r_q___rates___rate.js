var class_model___o_t_a___hotel_rate_plan_notif_r_q___rates___rate =
[
    [ "Model_OTA_HotelRatePlanNotifRQ_Rates_Rate", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rates___rate.html#a8b40a6c096ce3b053756ebcd677a3a55", null ],
    [ "~Model_OTA_HotelRatePlanNotifRQ_Rates_Rate", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rates___rate.html#ad32c35309b2fab8740dca77a6e6147bf", null ],
    [ "getAdditionalGuestAmounts", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rates___rate.html#a25ab861f34fa2046b5a567d0f7133ea3", null ],
    [ "getBaseByGuestAmts", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rates___rate.html#a03245d22b749a4e7a7ccb590fc1ed48b", null ],
    [ "getNoOffersAllowed", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rates___rate.html#a8f195f0418071ae616d2bcb8feb0c1c5", null ],
    [ "setAdditionalGuestAmounts", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rates___rate.html#ab460d9592e2c91712ad812852809e5c8", null ],
    [ "setBaseByGuestAmts", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rates___rate.html#a9414abf5e1ef655fcc4dae558582cf33", null ],
    [ "setNoOffersAllowed", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rates___rate.html#a437fb40bfc9883883e0644ac3d250b25", null ]
];