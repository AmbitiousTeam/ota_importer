var dir_4399d7fbe27e40be4c079d404537788b =
[
    [ "AdditionalGuestAmount.cpp", "_mapper_2_additional_guest_amount_8cpp.html", null ],
    [ "AdditionalGuestAmount.h", "_mapper_2_additional_guest_amount_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount" ]
    ] ],
    [ "AdditionalGuestAmounts.cpp", "_mapper_2_additional_guest_amounts_8cpp.html", null ],
    [ "AdditionalGuestAmounts.h", "_mapper_2_additional_guest_amounts_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts" ]
    ] ],
    [ "AmountPercent.cpp", "_mapper_2_amount_percent_8cpp.html", null ],
    [ "AmountPercent.h", "_mapper_2_amount_percent_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_AmountPercent", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___amount_percent.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___amount_percent" ]
    ] ],
    [ "ArrivalDaysOfWeek.cpp", "_mapper_2_arrival_days_of_week_8cpp.html", null ],
    [ "ArrivalDaysOfWeek.h", "_mapper_2_arrival_days_of_week_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___arrival_days_of_week.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___arrival_days_of_week" ]
    ] ],
    [ "AvailStatusMessage.cpp", "_mapper_2_avail_status_message_8cpp.html", null ],
    [ "AvailStatusMessage.h", "_mapper_2_avail_status_message_8h.html", [
      [ "Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage", "class_mapper___o_t_a___hotel_avail_notif_r_q___avail_status_messages___avail_status_message.html", "class_mapper___o_t_a___hotel_avail_notif_r_q___avail_status_messages___avail_status_message" ]
    ] ],
    [ "AvailStatusMessages.cpp", "_mapper_2_avail_status_messages_8cpp.html", null ],
    [ "AvailStatusMessages.h", "_mapper_2_avail_status_messages_8h.html", [
      [ "Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages", "class_mapper___o_t_a___hotel_avail_notif_r_q___avail_status_messages.html", "class_mapper___o_t_a___hotel_avail_notif_r_q___avail_status_messages" ]
    ] ],
    [ "BaseByGuestAmt.cpp", "_mapper_2_base_by_guest_amt_8cpp.html", null ],
    [ "BaseByGuestAmt.h", "_mapper_2_base_by_guest_amt_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___base_by_guest_amts___base_by_guest_amt.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___base_by_guest_amts___base_by_guest_amt" ]
    ] ],
    [ "BaseByGuestAmts.cpp", "_mapper_2_base_by_guest_amts_8cpp.html", null ],
    [ "BaseByGuestAmts.h", "_mapper_2_base_by_guest_amts_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___base_by_guest_amts.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___base_by_guest_amts" ]
    ] ],
    [ "BookingRule.cpp", "_mapper_2_booking_rule_8cpp.html", null ],
    [ "BookingRule.h", "_mapper_2_booking_rule_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___booking_rules___booking_rule.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___booking_rules___booking_rule" ]
    ] ],
    [ "BookingRules.cpp", "_mapper_2_booking_rules_8cpp.html", null ],
    [ "BookingRules.h", "_mapper_2_booking_rules_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_BookingRules", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___booking_rules.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___booking_rules" ]
    ] ],
    [ "CancelPenalty.cpp", "_mapper_2_cancel_penalty_8cpp.html", null ],
    [ "CancelPenalty.h", "_mapper_2_cancel_penalty_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_CancelPenalty", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___cancel_penalty.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___cancel_penalty" ]
    ] ],
    [ "CompatibleOffer.cpp", "_mapper_2_compatible_offer_8cpp.html", null ],
    [ "CompatibleOffer.h", "_mapper_2_compatible_offer_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_CompatibleOffers_CompatibleOffer", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___compatible_offers___compatible_offer.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___compatible_offers___compatible_offer" ]
    ] ],
    [ "CompatibleOffers.cpp", "_mapper_2_compatible_offers_8cpp.html", null ],
    [ "CompatibleOffers.h", "_mapper_2_compatible_offers_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_CompatibleOffers", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___compatible_offers.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___compatible_offers" ]
    ] ],
    [ "DateRestriction.cpp", "_mapper_2_date_restriction_8cpp.html", null ],
    [ "DateRestriction.h", "_mapper_2_date_restriction_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___date_restrictions___date_restriction.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___date_restrictions___date_restriction" ]
    ] ],
    [ "DateRestrictions.cpp", "_mapper_2_date_restrictions_8cpp.html", null ],
    [ "DateRestrictions.h", "_mapper_2_date_restrictions_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___date_restrictions.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___date_restrictions" ]
    ] ],
    [ "Deadline.cpp", "_mapper_2_deadline_8cpp.html", null ],
    [ "Deadline.h", "_mapper_2_deadline_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_Deadline", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___deadline.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___deadline" ]
    ] ],
    [ "DepartureDaysOfWeek.cpp", "_mapper_2_departure_days_of_week_8cpp.html", null ],
    [ "DepartureDaysOfWeek.h", "_mapper_2_departure_days_of_week_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week" ]
    ] ],
    [ "Discount.cpp", "_mapper_2_discount_8cpp.html", null ],
    [ "Discount.h", "_mapper_2_discount_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_Discount", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___discount.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___discount" ]
    ] ],
    [ "DOW_Restrictions.cpp", "_mapper_2_d_o_w___restrictions_8cpp.html", null ],
    [ "DOW_Restrictions.h", "_mapper_2_d_o_w___restrictions_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_DOW_Restrictions", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___d_o_w___restrictions.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___d_o_w___restrictions" ]
    ] ],
    [ "GuestRoom.cpp", "_mapper_2_guest_room_8cpp.html", null ],
    [ "GuestRoom.h", "_mapper_2_guest_room_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_GuestRoom", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___guest_room.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___guest_room" ]
    ] ],
    [ "HotelAvailNotifRQ.cpp", "_mapper_2_hotel_avail_notif_r_q_8cpp.html", null ],
    [ "HotelAvailNotifRQ.h", "_mapper_2_hotel_avail_notif_r_q_8h.html", [
      [ "Mapper_OTA_HotelAvailNotifRQ", "class_mapper___o_t_a___hotel_avail_notif_r_q.html", "class_mapper___o_t_a___hotel_avail_notif_r_q" ]
    ] ],
    [ "HotelRatePlanNotifRQ.cpp", "_mapper_2_hotel_rate_plan_notif_r_q_8cpp.html", null ],
    [ "HotelRatePlanNotifRQ.h", "_mapper_2_hotel_rate_plan_notif_r_q_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q" ]
    ] ],
    [ "Inventories.cpp", "_mapper_2_inventories_8cpp.html", null ],
    [ "Inventories.h", "_mapper_2_inventories_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_Inventories", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___inventories.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___inventories" ]
    ] ],
    [ "Inventory.cpp", "_mapper_2_inventory_8cpp.html", null ],
    [ "Inventory.h", "_mapper_2_inventory_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_Inventories_Inventory", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___inventories___inventory.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___inventories___inventory" ]
    ] ],
    [ "LengthOfStay.cpp", "_mapper_2_length_of_stay_8cpp.html", null ],
    [ "LengthOfStay.h", "_mapper_2_length_of_stay_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___lengths_of_stay___length_of_stay.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___lengths_of_stay___length_of_stay" ]
    ] ],
    [ "LengthsOfStay.cpp", "_mapper_2_lengths_of_stay_8cpp.html", null ],
    [ "LengthsOfStay.h", "_mapper_2_lengths_of_stay_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___lengths_of_stay.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___lengths_of_stay" ]
    ] ],
    [ "Map.h", "_map_8h.html", [
      [ "Map", "class_map.html", "class_map" ]
    ] ],
    [ "Mapper.h", "_mapper_8h.html", [
      [ "Mapper", "class_mapper.html", "class_mapper" ]
    ] ],
    [ "Occupancies.cpp", "_mapper_2_occupancies_8cpp.html", null ],
    [ "Occupancies.h", "_mapper_2_occupancies_8h.html", [
      [ "Service_OTA_HotelRatePlanNotifRQ_Occupancies", "class_service___o_t_a___hotel_rate_plan_notif_r_q___occupancies.html", "class_service___o_t_a___hotel_rate_plan_notif_r_q___occupancies" ]
    ] ],
    [ "Occupancy.cpp", "_mapper_2_occupancy_8cpp.html", null ],
    [ "Occupancy.h", "_mapper_2_occupancy_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___occupancies___occupancy.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___occupancies___occupancy" ]
    ] ],
    [ "Offer.cpp", "_mapper_2_offer_8cpp.html", null ],
    [ "Offer.h", "_mapper_2_offer_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___offers___offer.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___offers___offer" ]
    ] ],
    [ "OfferDescription.cpp", "_mapper_2_offer_description_8cpp.html", null ],
    [ "OfferDescription.h", "_mapper_2_offer_description_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_OfferDescription", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___offer_description.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___offer_description" ]
    ] ],
    [ "OfferRule.cpp", "_mapper_2_offer_rule_8cpp.html", null ],
    [ "OfferRule.h", "_mapper_2_offer_rule_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___offer_rules___offer_rule.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___offer_rules___offer_rule" ]
    ] ],
    [ "OfferRules.cpp", "_mapper_2_offer_rules_8cpp.html", null ],
    [ "OfferRules.h", "_mapper_2_offer_rules_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_OfferRules", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___offer_rules.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___offer_rules" ]
    ] ],
    [ "Offers.cpp", "_mapper_2_offers_8cpp.html", null ],
    [ "Offers.h", "_mapper_2_offers_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_Offers", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___offers.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___offers" ]
    ] ],
    [ "Old_Mapper.h", "_old___mapper_8h.html", [
      [ "Service_Mapper", "class_service___mapper.html", "class_service___mapper" ]
    ] ],
    [ "Quantities.cpp", "_mapper_2_quantities_8cpp.html", null ],
    [ "Quantities.h", "_mapper_2_quantities_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_Quantities", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___quantities.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___quantities" ]
    ] ],
    [ "Rate.cpp", "_mapper_2_rate_8cpp.html", null ],
    [ "Rate.h", "_mapper_2_rate_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_Rates_Rate", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rates___rate.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rates___rate" ]
    ] ],
    [ "RatePlan.cpp", "_mapper_2_rate_plan_8cpp.html", null ],
    [ "RatePlan.h", "_mapper_2_rate_plan_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rate_plans___rate_plan.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rate_plans___rate_plan" ]
    ] ],
    [ "RatePlans.cpp", "_mapper_2_rate_plans_8cpp.html", null ],
    [ "RatePlans.h", "_mapper_2_rate_plans_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_RatePlans", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rate_plans" ]
    ] ],
    [ "Rates.cpp", "_mapper_2_rates_8cpp.html", null ],
    [ "Rates.h", "_mapper_2_rates_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_Rates", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rates.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rates" ]
    ] ],
    [ "RestrictionStatus.cpp", "_mapper_2_restriction_status_8cpp.html", null ],
    [ "RestrictionStatus.h", "_mapper_2_restriction_status_8h.html", [
      [ "Mapper_OTA_HotelAvailNotifRQ_RestrictionStatus", "class_mapper___o_t_a___hotel_avail_notif_r_q___restriction_status.html", "class_mapper___o_t_a___hotel_avail_notif_r_q___restriction_status" ]
    ] ],
    [ "RoomGroup.cpp", "_mapper_2_room_group_8cpp.html", null ],
    [ "RoomGroup.h", "_mapper_2_room_group_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_RoomGroup", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___room_group.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___room_group" ]
    ] ],
    [ "SellableProduct.cpp", "_mapper_2_sellable_product_8cpp.html", null ],
    [ "SellableProduct.h", "_mapper_2_sellable_product_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___sellable_products___sellable_product.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___sellable_products___sellable_product" ]
    ] ],
    [ "SellableProducts.cpp", "_mapper_2_sellable_products_8cpp.html", null ],
    [ "SellableProducts.h", "_mapper_2_sellable_products_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___sellable_products.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___sellable_products" ]
    ] ],
    [ "StatusApplicationControl.cpp", "_mapper_2_status_application_control_8cpp.html", null ],
    [ "StatusApplicationControl.h", "_mapper_2_status_application_control_8h.html", [
      [ "Mapper_OTA_HotelAvailNotifRQ_StatusApplicationControl", "class_mapper___o_t_a___hotel_avail_notif_r_q___status_application_control.html", "class_mapper___o_t_a___hotel_avail_notif_r_q___status_application_control" ]
    ] ],
    [ "Supplement.cpp", "_mapper_2_supplement_8cpp.html", null ],
    [ "Supplement.h", "_mapper_2_supplement_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___supplements___supplement.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___supplements___supplement" ]
    ] ],
    [ "Supplements.cpp", "_mapper_2_supplements_8cpp.html", null ],
    [ "Supplements.h", "_mapper_2_supplements_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_Supplements", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___supplements.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___supplements" ]
    ] ],
    [ "Text.cpp", "_mapper_2_text_8cpp.html", null ],
    [ "Text.h", "_mapper_2_text_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_Text", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___text.html", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___text" ]
    ] ],
    [ "TPAExtension.cpp", "_mapper_2_t_p_a_extension_8cpp.html", null ],
    [ "TPAExtension.h", "_mapper_2_t_p_a_extension_8h.html", null ],
    [ "TPAExtensions.cpp", "_mapper_2_t_p_a_extensions_8cpp.html", null ],
    [ "TPAExtensions.h", "_mapper_2_t_p_a_extensions_8h.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_TPAExtensions", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___t_p_a_extensions.html", null ]
    ] ]
];