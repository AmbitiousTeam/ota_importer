var hierarchy =
[
    [ "Logger", "class_logger.html", null ],
    [ "Map< T_Mapper >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelAvailNotifRQ >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelAvailNotifRQ_RestrictionStatus >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelAvailNotifRQ_StatusApplicationControl >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_AmountPercent >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_BookingRules >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_CancelPenalty >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_CompatibleOffers >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_CompatibleOffers_CompatibleOffer >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_Deadline >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_Discount >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_DOW_Restrictions >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_GuestRoom >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_Inventories >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_Inventories_Inventory >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_OfferDescription >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_OfferRules >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_Offers >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_Quantities >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_RatePlans >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_Rates >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_Rates_Rate >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_RoomGroup >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_Supplements >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_Text >", "class_map.html", null ],
    [ "Map< Mapper_OTA_HotelRatePlanNotifRQ_TPAExtensions >", "class_map.html", null ],
    [ "Mapper< T_Mapper, T_Model >", "class_mapper.html", null ],
    [ "Mapper< Mapper_OTA_HotelAvailNotifRQ, Model_OTA_HotelAvailNotifRQ >", "class_mapper.html", [
      [ "Mapper_OTA_HotelAvailNotifRQ", "class_mapper___o_t_a___hotel_avail_notif_r_q.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages, Model_OTA_HotelAvailNotifRQ_AvailStatusMessages >", "class_mapper.html", [
      [ "Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages", "class_mapper___o_t_a___hotel_avail_notif_r_q___avail_status_messages.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage, Model_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage >", "class_mapper.html", [
      [ "Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage", "class_mapper___o_t_a___hotel_avail_notif_r_q___avail_status_messages___avail_status_message.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelAvailNotifRQ_RestrictionStatus, Model_OTA_HotelAvailNotifRQ_RestrictionStatus >", "class_mapper.html", [
      [ "Mapper_OTA_HotelAvailNotifRQ_RestrictionStatus", "class_mapper___o_t_a___hotel_avail_notif_r_q___restriction_status.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelAvailNotifRQ_StatusApplicationControl, Model_OTA_HotelAvailNotifRQ_StatusApplicationControl >", "class_mapper.html", [
      [ "Mapper_OTA_HotelAvailNotifRQ_StatusApplicationControl", "class_mapper___o_t_a___hotel_avail_notif_r_q___status_application_control.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ, Model_OTA_HotelRatePlanNotifRQ >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts, Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount, Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_AmountPercent, Model_OTA_HotelRatePlanNotifRQ_AmountPercent >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_AmountPercent", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___amount_percent.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek, Model_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___arrival_days_of_week.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts, Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___base_by_guest_amts.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt, Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___base_by_guest_amts___base_by_guest_amt.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_BookingRules, Model_OTA_HotelRatePlanNotifRQ_BookingRules >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_BookingRules", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___booking_rules.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule, Model_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___booking_rules___booking_rule.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_CancelPenalty, Model_OTA_HotelRatePlanNotifRQ_CancelPenalty >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_CancelPenalty", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___cancel_penalty.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_CompatibleOffers, Model_OTA_HotelRatePlanNotifRQ_CompatibleOffers >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_CompatibleOffers", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___compatible_offers.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_CompatibleOffers_CompatibleOffer, Model_OTA_HotelRatePlanNotifRQ_CompatibleOffers_CompatibleOffer >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_CompatibleOffers_CompatibleOffer", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___compatible_offers___compatible_offer.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions, Model_OTA_HotelRatePlanNotifRQ_DateRestrictions >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___date_restrictions.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction, Model_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___date_restrictions___date_restriction.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_Deadline, Model_OTA_HotelRatePlanNotifRQ_Deadline >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_Deadline", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___deadline.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek, Model_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_Discount, Model_OTA_HotelRatePlanNotifRQ_Discount >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_Discount", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___discount.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_DOW_Restrictions, Model_OTA_HotelRatePlanNotifRQ_DOW_Restrictions >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_DOW_Restrictions", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___d_o_w___restrictions.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_GuestRoom, Model_OTA_HotelRatePlanNotifRQ_GuestRoom >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_GuestRoom", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___guest_room.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_Inventories, Model_OTA_HotelRatePlanNotifRQ_Inventories >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_Inventories", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___inventories.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_Inventories_Inventory, Model_OTA_HotelRatePlanNotifRQ_Inventories_Inventory >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_Inventories_Inventory", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___inventories___inventory.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay, Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___lengths_of_stay.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay, Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___lengths_of_stay___length_of_stay.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy, Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___occupancies___occupancy.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_OfferDescription, Model_OTA_HotelRatePlanNotifRQ_OfferDescription >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_OfferDescription", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___offer_description.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_OfferRules, Model_OTA_HotelRatePlanNotifRQ_OfferRules >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_OfferRules", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___offer_rules.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule, Model_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___offer_rules___offer_rule.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_Offers, Model_OTA_HotelRatePlanNotifRQ_Offers >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_Offers", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___offers.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer, Model_OTA_HotelRatePlanNotifRQ_Offers_Offer >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___offers___offer.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_Quantities, Model_OTA_HotelRatePlanNotifRQ_Quantities >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_Quantities", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___quantities.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_RatePlans, Model_OTA_HotelRatePlanNotifRQ_RatePlans >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_RatePlans", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan, Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rate_plans___rate_plan.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_Rates, Model_OTA_HotelRatePlanNotifRQ_Rates >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_Rates", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rates.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_Rates_Rate, Model_OTA_HotelRatePlanNotifRQ_Rates_Rate >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_Rates_Rate", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___rates___rate.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_RoomGroup, Model_OTA_HotelRatePlanNotifRQ_RoomGroup >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_RoomGroup", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___room_group.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts, Model_OTA_HotelRatePlanNotifRQ_SellableProducts >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___sellable_products.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct, Model_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___sellable_products___sellable_product.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_Supplements, Model_OTA_HotelRatePlanNotifRQ_Supplements >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_Supplements", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___supplements.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement, Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___supplements___supplement.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_Text, Model_OTA_HotelRatePlanNotifRQ_Text >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_Text", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___text.html", null ]
    ] ],
    [ "Mapper< Mapper_OTA_HotelRatePlanNotifRQ_TPAExtensions, Model_OTA_HotelRatePlanNotifRQ_TPAExtensions >", "class_mapper.html", [
      [ "Mapper_OTA_HotelRatePlanNotifRQ_TPAExtensions", "class_mapper___o_t_a___hotel_rate_plan_notif_r_q___t_p_a_extensions.html", null ]
    ] ],
    [ "MD5", "class_m_d5.html", null ],
    [ "Model", "class_model.html", null ],
    [ "Model_Collection< T >", "class_model___collection.html", null ],
    [ "Model_Collection< double >", "class_model___collection.html", null ],
    [ "Model_Collection< float >", "class_model___collection.html", null ],
    [ "Model_Collection< Model_AgeGroup >", "class_model___collection.html", [
      [ "Model_AgeGroups", "class_model___age_groups.html", null ]
    ] ],
    [ "Model_Collection< Model_Boarding >", "class_model___collection.html", [
      [ "Model_Boardings", "class_model___boardings.html", null ]
    ] ],
    [ "Model_Collection< Model_Collection< Model_Collection< Model_Date > > >", "class_model___collection.html", null ],
    [ "Model_Collection< Model_Collection< Model_Date > >", "class_model___collection.html", null ],
    [ "Model_Collection< Model_Collection< Model_OTA_HotelRatePlanNotifRQ_Supplements > >", "class_model___collection.html", null ],
    [ "Model_Collection< Model_Date >", "class_model___collection.html", [
      [ "Model_ChangeDates", "class_model___change_dates.html", null ]
    ] ],
    [ "Model_Collection< Model_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage >", "class_model___collection.html", [
      [ "Model_OTA_HotelAvailNotifRQ_AvailStatusMessages", "class_model___o_t_a___hotel_avail_notif_r_q___avail_status_messages.html", null ]
    ] ],
    [ "Model_Collection< Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount >", "class_model___collection.html", [
      [ "Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts", "class_model___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts.html", null ]
    ] ],
    [ "Model_Collection< Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt >", "class_model___collection.html", [
      [ "Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts", "class_model___o_t_a___hotel_rate_plan_notif_r_q___base_by_guest_amts.html", null ]
    ] ],
    [ "Model_Collection< Model_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule >", "class_model___collection.html", [
      [ "Model_OTA_HotelRatePlanNotifRQ_BookingRules", "class_model___o_t_a___hotel_rate_plan_notif_r_q___booking_rules.html", null ]
    ] ],
    [ "Model_Collection< Model_OTA_HotelRatePlanNotifRQ_CompatibleOffers_CompatibleOffer >", "class_model___collection.html", [
      [ "Model_OTA_HotelRatePlanNotifRQ_CompatibleOffers", "class_model___o_t_a___hotel_rate_plan_notif_r_q___compatible_offers.html", null ]
    ] ],
    [ "Model_Collection< Model_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction >", "class_model___collection.html", [
      [ "Model_OTA_HotelRatePlanNotifRQ_DateRestrictions", "class_model___o_t_a___hotel_rate_plan_notif_r_q___date_restrictions.html", null ]
    ] ],
    [ "Model_Collection< Model_OTA_HotelRatePlanNotifRQ_Inventories_Inventory >", "class_model___collection.html", [
      [ "Model_OTA_HotelRatePlanNotifRQ_Inventories", "class_model___o_t_a___hotel_rate_plan_notif_r_q___inventories.html", null ]
    ] ],
    [ "Model_Collection< Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay >", "class_model___collection.html", [
      [ "Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay", "class_model___o_t_a___hotel_rate_plan_notif_r_q___lengths_of_stay.html", null ]
    ] ],
    [ "Model_Collection< Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy >", "class_model___collection.html", [
      [ "Model_OTA_HotelRatePlanNotifRQ_Occupancies", "class_model___o_t_a___hotel_rate_plan_notif_r_q___occupancies.html", null ]
    ] ],
    [ "Model_Collection< Model_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule >", "class_model___collection.html", [
      [ "Model_OTA_HotelRatePlanNotifRQ_OfferRules", "class_model___o_t_a___hotel_rate_plan_notif_r_q___offer_rules.html", null ]
    ] ],
    [ "Model_Collection< Model_OTA_HotelRatePlanNotifRQ_Offers_Offer >", "class_model___collection.html", [
      [ "Model_OTA_HotelRatePlanNotifRQ_Offers", "class_model___o_t_a___hotel_rate_plan_notif_r_q___offers.html", null ]
    ] ],
    [ "Model_Collection< Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan >", "class_model___collection.html", [
      [ "Model_OTA_HotelRatePlanNotifRQ_RatePlans", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rate_plans.html", null ]
    ] ],
    [ "Model_Collection< Model_OTA_HotelRatePlanNotifRQ_Rates_Rate >", "class_model___collection.html", [
      [ "Model_OTA_HotelRatePlanNotifRQ_Rates", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rates.html", null ]
    ] ],
    [ "Model_Collection< Model_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct >", "class_model___collection.html", [
      [ "Model_OTA_HotelRatePlanNotifRQ_SellableProducts", "class_model___o_t_a___hotel_rate_plan_notif_r_q___sellable_products.html", null ]
    ] ],
    [ "Model_Collection< Model_OTA_HotelRatePlanNotifRQ_Supplements >", "class_model___collection.html", null ],
    [ "Model_Collection< Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement >", "class_model___collection.html", [
      [ "Model_OTA_HotelRatePlanNotifRQ_Supplements", "class_model___o_t_a___hotel_rate_plan_notif_r_q___supplements.html", null ]
    ] ],
    [ "Model_Collection< Model_OTA_HotelRatePlanNotifRQ_TPAExtensions_TPAExtension >", "class_model___collection.html", [
      [ "Model_OTA_HotelRatePlanNotifRQ_TPAExtensions", "class_model___o_t_a___hotel_rate_plan_notif_r_q___t_p_a_extensions.html", null ],
      [ "Model_OTA_HotelRatePlanNotifRQ_TPAExtensions", "class_model___o_t_a___hotel_rate_plan_notif_r_q___t_p_a_extensions.html", null ]
    ] ],
    [ "Model_Collection< Model_TravellerConstellation >", "class_model___collection.html", [
      [ "Model_TravellerConstellations", "class_model___traveller_constellations.html", null ]
    ] ],
    [ "Model_Collection< std::ofstream >", "class_model___collection.html", null ],
    [ "Model_Collection< std::string >", "class_model___collection.html", null ],
    [ "Model_Collection< unsigned int >", "class_model___collection.html", null ],
    [ "Model_Date", "class_model___date.html", [
      [ "Model_OTA_HotelAvailNotifRQ_StatusApplicationControl", "class_model___o_t_a___hotel_avail_notif_r_q___status_application_control.html", null ],
      [ "Model_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule", "class_model___o_t_a___hotel_rate_plan_notif_r_q___booking_rules___booking_rule.html", null ],
      [ "Model_OTA_HotelRatePlanNotifRQ_CancelPenalty", "class_model___o_t_a___hotel_rate_plan_notif_r_q___cancel_penalty.html", null ],
      [ "Model_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction", "class_model___o_t_a___hotel_rate_plan_notif_r_q___date_restrictions___date_restriction.html", null ],
      [ "Model_OTA_HotelRatePlanNotifRQ_Rates_Rate", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rates___rate.html", null ],
      [ "Model_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct", "class_model___o_t_a___hotel_rate_plan_notif_r_q___sellable_products___sellable_product.html", null ],
      [ "Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement", "class_model___o_t_a___hotel_rate_plan_notif_r_q___supplements___supplement.html", null ]
    ] ],
    [ "Model_Entity", "class_model___entity.html", [
      [ "Model_AgeGroup", "class_model___age_group.html", null ],
      [ "Model_Boarding", "class_model___boarding.html", null ],
      [ "Model_CSVOffer", "class_model___c_s_v_offer.html", null ],
      [ "Model_OTA_HotelAvailNotifRQ_RestrictionStatus", "class_model___o_t_a___hotel_avail_notif_r_q___restriction_status.html", null ],
      [ "Model_OTA_HotelRatePlanNotifRQ", "class_model___o_t_a___hotel_rate_plan_notif_r_q.html", null ],
      [ "Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount", "class_model___o_t_a___hotel_rate_plan_notif_r_q___additional_guest_amounts___additional_guest_amount.html", null ],
      [ "Model_OTA_HotelRatePlanNotifRQ_AmountPercent", "class_model___o_t_a___hotel_rate_plan_notif_r_q___amount_percent.html", null ],
      [ "Model_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek", "class_model___o_t_a___hotel_rate_plan_notif_r_q___arrival_days_of_week.html", null ],
      [ "Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt", "class_model___o_t_a___hotel_rate_plan_notif_r_q___base_by_guest_amts___base_by_guest_amt.html", null ],
      [ "Model_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule", "class_model___o_t_a___hotel_rate_plan_notif_r_q___booking_rules___booking_rule.html", null ],
      [ "Model_OTA_HotelRatePlanNotifRQ_CancelPenalty", "class_model___o_t_a___hotel_rate_plan_notif_r_q___cancel_penalty.html", null ],
      [ "Model_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction", "class_model___o_t_a___hotel_rate_plan_notif_r_q___date_restrictions___date_restriction.html", null ],
      [ "Model_OTA_HotelRatePlanNotifRQ_Deadline", "class_model___o_t_a___hotel_rate_plan_notif_r_q___deadline.html", null ],
      [ "Model_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek", "class_model___o_t_a___hotel_rate_plan_notif_r_q___departure_days_of_week.html", null ],
      [ "Model_OTA_HotelRatePlanNotifRQ_DOW_Restrictions", "class_model___o_t_a___hotel_rate_plan_notif_r_q___d_o_w___restrictions.html", null ],
      [ "Model_OTA_HotelRatePlanNotifRQ_GuestRoom", "class_model___o_t_a___hotel_rate_plan_notif_r_q___guest_room.html", null ],
      [ "Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay", "class_model___o_t_a___hotel_rate_plan_notif_r_q___lengths_of_stay___length_of_stay.html", null ],
      [ "Model_OTA_HotelRatePlanNotifRQ_Offers_Offer", "class_model___o_t_a___hotel_rate_plan_notif_r_q___offers___offer.html", null ],
      [ "Model_OTA_HotelRatePlanNotifRQ_Quantities", "class_model___o_t_a___hotel_rate_plan_notif_r_q___quantities.html", null ],
      [ "Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rate_plans___rate_plan.html", null ],
      [ "Model_OTA_HotelRatePlanNotifRQ_Rates_Rate", "class_model___o_t_a___hotel_rate_plan_notif_r_q___rates___rate.html", null ],
      [ "Model_OTA_HotelRatePlanNotifRQ_RoomGroup", "class_model___o_t_a___hotel_rate_plan_notif_r_q___room_group.html", null ],
      [ "Model_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct", "class_model___o_t_a___hotel_rate_plan_notif_r_q___sellable_products___sellable_product.html", null ],
      [ "Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement", "class_model___o_t_a___hotel_rate_plan_notif_r_q___supplements___supplement.html", null ],
      [ "Model_OTA_HotelRatePlanNotifRQ_TPAExtensions_TPAExtension", "class_model___o_t_a___hotel_rate_plan_notif_r_q___t_p_a_extensions___t_p_a_extension.html", null ],
      [ "Model_Person", "class_model___person.html", null ],
      [ "Model_TravellerConstellation", "class_model___traveller_constellation.html", null ]
    ] ],
    [ "Model_Node< T >", "class_model___node.html", null ],
    [ "Model_Node< double >", "class_model___node.html", null ],
    [ "Model_Node< float >", "class_model___node.html", null ],
    [ "Model_Node< Model_AgeGroup >", "class_model___node.html", null ],
    [ "Model_Node< Model_Boarding >", "class_model___node.html", null ],
    [ "Model_Node< Model_Collection< Model_Collection< Model_Date > > >", "class_model___node.html", null ],
    [ "Model_Node< Model_Collection< Model_Date > >", "class_model___node.html", null ],
    [ "Model_Node< Model_Collection< Model_OTA_HotelRatePlanNotifRQ_Supplements > >", "class_model___node.html", null ],
    [ "Model_Node< Model_Date >", "class_model___node.html", null ],
    [ "Model_Node< Model_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage >", "class_model___node.html", null ],
    [ "Model_Node< Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount >", "class_model___node.html", null ],
    [ "Model_Node< Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt >", "class_model___node.html", null ],
    [ "Model_Node< Model_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule >", "class_model___node.html", null ],
    [ "Model_Node< Model_OTA_HotelRatePlanNotifRQ_CompatibleOffers_CompatibleOffer >", "class_model___node.html", null ],
    [ "Model_Node< Model_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction >", "class_model___node.html", null ],
    [ "Model_Node< Model_OTA_HotelRatePlanNotifRQ_Inventories_Inventory >", "class_model___node.html", null ],
    [ "Model_Node< Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay >", "class_model___node.html", null ],
    [ "Model_Node< Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy >", "class_model___node.html", null ],
    [ "Model_Node< Model_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule >", "class_model___node.html", null ],
    [ "Model_Node< Model_OTA_HotelRatePlanNotifRQ_Offers_Offer >", "class_model___node.html", null ],
    [ "Model_Node< Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan >", "class_model___node.html", null ],
    [ "Model_Node< Model_OTA_HotelRatePlanNotifRQ_Rates_Rate >", "class_model___node.html", null ],
    [ "Model_Node< Model_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct >", "class_model___node.html", null ],
    [ "Model_Node< Model_OTA_HotelRatePlanNotifRQ_Supplements >", "class_model___node.html", null ],
    [ "Model_Node< Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement >", "class_model___node.html", null ],
    [ "Model_Node< Model_OTA_HotelRatePlanNotifRQ_TPAExtensions_TPAExtension >", "class_model___node.html", null ],
    [ "Model_Node< Model_TravellerConstellation >", "class_model___node.html", null ],
    [ "Model_Node< std::ofstream >", "class_model___node.html", null ],
    [ "Model_Node< std::string >", "class_model___node.html", null ],
    [ "Model_Node< unsigned int >", "class_model___node.html", null ],
    [ "Model_OTA_HotelAvailNotifRQ", "class_model___o_t_a___hotel_avail_notif_r_q.html", null ],
    [ "Model_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage", "class_model___o_t_a___hotel_avail_notif_r_q___avail_status_messages___avail_status_message.html", null ],
    [ "Model_OTA_HotelRatePlanNotifRQ_CompatibleOffers_CompatibleOffer", "class_model___o_t_a___hotel_rate_plan_notif_r_q___compatible_offers___compatible_offer.html", null ],
    [ "Model_OTA_HotelRatePlanNotifRQ_Discount", "class_model___o_t_a___hotel_rate_plan_notif_r_q___discount.html", null ],
    [ "Model_OTA_HotelRatePlanNotifRQ_Inventories_Inventory", "class_model___o_t_a___hotel_rate_plan_notif_r_q___inventories___inventory.html", null ],
    [ "Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy", "class_model___o_t_a___hotel_rate_plan_notif_r_q___occupancies___occupancy.html", null ],
    [ "Model_OTA_HotelRatePlanNotifRQ_OfferDescription", "class_model___o_t_a___hotel_rate_plan_notif_r_q___offer_description.html", null ],
    [ "Model_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule", "class_model___o_t_a___hotel_rate_plan_notif_r_q___offer_rules___offer_rule.html", null ],
    [ "Model_OTA_HotelRatePlanNotifRQ_Text", "class_model___o_t_a___hotel_rate_plan_notif_r_q___text.html", null ],
    [ "Service", "class_service.html", [
      [ "Service_AgeGroup", "class_service___age_group.html", null ],
      [ "Service_Boarding", "class_service___boarding.html", null ],
      [ "Service_Calculator", "class_service___calculator.html", null ],
      [ "Service_ExtraCharges", "class_service___extra_charges.html", null ],
      [ "Service_Hotel", "class_service___hotel.html", null ],
      [ "Service_Import", "class_service___import.html", null ],
      [ "Service_Offer", "class_service___offer.html", null ],
      [ "Service_Period", "class_service___period.html", null ],
      [ "Service_PeriodDiscount", "class_service___period_discount.html", null ],
      [ "Service_Permutation", "class_service___permutation.html", null ],
      [ "Service_PriceCalculator", "class_service___price_calculator.html", null ],
      [ "Service_Rate", "class_service___rate.html", null ],
      [ "Service_RatePlan", "class_service___rate_plan.html", null ],
      [ "Service_SpecialOffer", "class_service___special_offer.html", null ],
      [ "Service_Stopsales", "class_service___stopsales.html", null ],
      [ "Service_TravellerConstellations", "class_service___traveller_constellations.html", null ]
    ] ],
    [ "Service< Service_OTA_HotelRatePlanNotifRQ_Occupancies, Model_OTA_HotelRatePlanNotifRQ_Occupancies >", "class_service.html", [
      [ "Service_OTA_HotelRatePlanNotifRQ_Occupancies", "class_service___o_t_a___hotel_rate_plan_notif_r_q___occupancies.html", null ]
    ] ],
    [ "Service_ChangeDates", "class_service___change_dates.html", null ],
    [ "Service_Date", "class_service___date.html", null ],
    [ "Service_Mapper< T_Service >", "class_service___mapper.html", null ],
    [ "Service_XmlParser< T_Mapper >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelAvailNotifRQ >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelAvailNotifRQ_RestrictionStatus >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelAvailNotifRQ_StatusApplicationControl >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_AmountPercent >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_ArrivalDaysOfWeek >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_BookingRules >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_CancelPenalty >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_CompatibleOffers >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_CompatibleOffers_CompatibleOffer >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_Deadline >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_DepartureDaysOfWeek >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_Discount >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_DOW_Restrictions >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_GuestRoom >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_Inventories >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_Inventories_Inventory >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_OfferDescription >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_OfferRules >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_Offers >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_Offers_Offer >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_Quantities >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_RatePlans >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_Rates >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_Rates_Rate >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_RoomGroup >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_Supplements >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_Supplements_Supplement >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_Text >", "class_service___xml_parser.html", null ],
    [ "Service_XmlParser< Mapper_OTA_HotelRatePlanNotifRQ_TPAExtensions >", "class_service___xml_parser.html", null ],
    [ "Tool_File", "class_tool___file.html", null ],
    [ "Tool_Generator", "class_tool___generator.html", null ]
];