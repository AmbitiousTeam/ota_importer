#ifndef __EXCEPTION_HOTEL_NOT_FOUND__ 
#define __EXCEPTION_HOTEL_NOT_FOUND__

#include <iostream>
#include <exception>

using namespace std;

class HotelNotFoundException: public exception {
  
  public:
    virtual const char* what() const throw() {
      return this->_sMessage;
    }
  private:
    const char* _sMessage = "Hotel in der Datenbank nicht vorhanden!";
};

#endif
