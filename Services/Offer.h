#ifndef __SERVICE_OFFER_H__
#define __SERVICE_OFFER_H__

#include "Service.h"
#include "PriceCalculator.h"
#include "SpecialOffer.h"

#include "../Tools/String.cpp"

#include "../Models/Offers.h"
#include "../Models/Supplements.h"
#include "../Models/TravellerConstellation.h"
#include "../Models/AdditionalGuestAmounts.h"
#include "../Models/BaseByGuestAmts.h"
#include "../Models/Boarding.h"
#include "../Models/CSVOffer.h"

class Service_Offer : public Service {
public:
  static int _iOfferCount;
  
  bool createPriceCalculator(void);
  
  bool processTravellerConstellationModel(void);
  
  bool preparePriceCollections(void);
  bool preparePriceCollection(Model_Collection<double>*, int);
  
  bool considerSpecialOffers(Model_OTA_HotelRatePlanNotifRQ_Offers*);
  bool considerSupplements();
  void considerRoomSupplements();
  void considerBoardSupplements();
  void considerExtraSupplements();
  void considerSupplementsForAgeQualifyingCode(Model_OTA_HotelRatePlanNotifRQ_Supplements*, Model_Collection<double>*, int, int, unsigned int, unsigned int);
  
  bool considerPricesFromBaseByGuestAmts(Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts*);
  bool considerPricesFromAdditionalGuestAmounts(Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts*);
  bool considerBoarding(Model_Boarding*);
  bool addBoardingPriceToChildren(Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement*);
  bool addBoardingPriceToAdult(Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement*);
  
  /**
   * Collection der Preise
   * Supplement
   * MaxCount der Aktuellen Personen Collection (theoretisch ist das hier auch überflüssig, da die collection bereits am anfang
   * initialisiert wird und jeder wert mit 0 belegt wird, von daher wäre das hier redundant)
   */
  bool addSupplementToCollection(Model_Collection<double>*, Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement*, int);
  
  /**
   * collection der preise
   * preis
   * person position
   * bool ist prozent?
   */
  bool addPriceToPersonPosition(Model_Collection<double>*, double, int, bool);
  
  bool addPriceToCollection(Model_Collection<double>*, double, int, int, bool);
  
  double extractBasePriceFromSupplements(Model_OTA_HotelRatePlanNotifRQ_Supplements*);
  
  /** sortiert die supplements in erwachsene und kinder supplements sowie in room / extra / board **/
  bool filterSupplements();
  
  bool generatePrice(void);
  void generateOfferEntity(void);
  
  void setAdultCount(int);
  int getAdultCount(void);
  
  void setChildrenCount(int);
  int getChildrenCount(void);
  
  void setSpecialOffers(Model_OTA_HotelRatePlanNotifRQ_Offers*);
  Model_OTA_HotelRatePlanNotifRQ_Offers* getSpecialOffers(void);
  
  void setTravellerConstellationModel(Model_TravellerConstellation*);
  Model_TravellerConstellation* getTravellerConstellationModel(void);
  
  void setBaseByGuestAmtsModel(Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts*);
  Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts* getBaseByGuestAmtsModel(void);
  
  void setAdditionalGuestAmountsModel(Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts*);
  Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts* getAdditionalGuestAmountsModel(void);
  
  void setSupplements(Model_OTA_HotelRatePlanNotifRQ_Supplements*);
  Model_OTA_HotelRatePlanNotifRQ_Supplements* getSupplements(void);
  
  void setFilteredSupplementsCollection(Model_Collection<Model_Collection<Model_OTA_HotelRatePlanNotifRQ_Supplements>>*);
  Model_Collection<Model_Collection<Model_OTA_HotelRatePlanNotifRQ_Supplements>>* getFilteredSupplementsCollection(void);
  
  void setPrice(double);
  double getPrice(void);
  
  void setBaseRoomPrice(double);
  double getBaseRoomPrice(void);
  
  void setBaseBoardPrice(double);
  double getBaseBoardPrice(void);
  
  void setBaseExtraPrice(double);
  double getBaseExtraPrice(void);
  
  void setPriceType(std::string);
  std::string getPriceType(void);
  
  void setDateFrom(const std::string);
  std::string getDateFrom(void) const;
  
  void setDateTo(const std::string);
  std::string getDateTo(void) const;
  
  void setOfferEntity(Model_CSVOffer&);
  Model_CSVOffer* getOfferEntity(void);
  
  Service_Offer();
  ~Service_Offer();
  
protected:
private:
  double _dPrice;                       /** Preis des Offers pro Person pro Tag **/
  double _dBaseRoomPrice;
  double _dBaseBoardPrice;
  double _dBaseExtraPrice;
  
  int _iOfferId;                        /** ID des Offers **/
  int _iChildrenMinAge;                 /** mindestalter der Kinder für das Offer **/
  int _iChildrenMaxAge;                 /** maximalalter der Kinder für das Offer **/
  int _iPersonCount;                    /** Anzahl der buchenden Personen für das Offer **/
  int _iAdultCount;                     /** Anzahl der buchenden Erwachsenen für das Offer **/
  int _iChildrenCount;                  /** Anzahl der buchenden Kinder für das Offer **/
  int _iInfantCount;                    /** Anzahl der buchenden Babys für das Offer **/
  int _iStandardCapacity;               /** Standard Kapazität des aktuellen Raumes **/
  
  std::string _sHotelCode;              /** der in unserem System gematchte Code für das Hotel **/
  std::string _sHotelCodeAgency;        /** der durch die Agentur gelieferte HotelCode **/
  std::string _sTravelType;             /** Traveltype hier default per CTOR ERDE / PAUS **/
  std::string _sDateFrom;               /** der Beginn des Offers als Datum **/
  std::string _sDateTo;                 /** das Ende des Offers als Datum **/
  std::string _sArrivalFrom;            /** der Beginn des Reisedatums des Offers **/
  std::string _sArrivalTo;              /** das Ende des Reisedatums des Offers **/
  std::string _sBookingFrom;            /** der Beginn des Buchungszeitraumes des Offers **/
  std::string _sBookingTo;              /** das Ende des Buchungszeitraumes des Offers **/
  std::string _sBoardingCode;           /** der in unserem System gematchte Code des Boardings **/
  std::string _sBoardingCodeAgency;     /** der durch die Agentur gelieferte Boarding Code **/
  std::string _sRoomCode;               /** der in unserem System gematchte Code für den Raum **/
  std::string _sRoomCodeAgency;         /** Room Code, der durch die Agentur geliefert wurde **/
  std::string _sTargetCode;             /** IATA Code der Destionation **/
  std::string _sOfferHash;              /** der eindeutige (MD5) Hash des Offers **/
  std::string _sPriceType;              /** der Preistyp des Offers (hier durch den CTOR auf EB oder BR beschränkt) **/
  
  Service_PriceCalculator* _oPriceCalculatorService = NULL;
  
  Model_CSVOffer* _oCsvOfferEntity = NULL;
  Model_TravellerConstellation* _oTravellerConstellationModel = NULL;
  Model_OTA_HotelRatePlanNotifRQ_Offers* _oSpecialOffersModel = NULL;
  Model_OTA_HotelRatePlanNotifRQ_Supplements* _oSupplementsModel = NULL;
  Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts* _oBaseByGuestAmtsModel = NULL;
  Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts* _oAdditionalGuestAmountsModel = NULL;
  Model_Collection<Model_Collection<Model_OTA_HotelRatePlanNotifRQ_Supplements>>* _oFilteredSupplementsCollection = NULL;
  
  Model_Collection<double>* _oAdultRoomPricesModel = NULL;
  Model_Collection<double>* _oChildrenRoomPricesModel = NULL;
  Model_Collection<double>* _oChildrenBoardPricesModel = NULL;
  Model_Collection<double>* _oAdultBoardPricesModel = NULL;
  Model_Collection<double>* _oAdultExtraPricesModel = NULL;
  Model_Collection<double>* _oChildrenExtraPricesModel = NULL;
};

#endif
