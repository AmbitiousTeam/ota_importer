#include "Offer.h"

int Service_Offer::_iOfferCount = 0;

/**
 * setzt die Anzahl der Erwachsenen
 * 
 * @param iAdultCount
 */
void Service_Offer::setAdultCount(int iAdultCount) {
  this->_iAdultCount = iAdultCount;
}

/**
 * gibt die Anzahl der Erwachsenen zurück
 * 
 * @return int 
 */ 
int Service_Offer::getAdultCount(void) {
  return this->_iAdultCount;
}

void Service_Offer::setChildrenCount(int iChildrenCount) {
  this->_iChildrenCount = iChildrenCount;
}

int Service_Offer::getChildrenCount(void) {
  return this->_iChildrenCount;
}

void Service_Offer::setTravellerConstellationModel(Model_TravellerConstellation* oTravellerConstellationModel) {
  this->_oTravellerConstellationModel = oTravellerConstellationModel;
}

Model_TravellerConstellation* Service_Offer::getTravellerConstellationModel(void) {
  return this->_oTravellerConstellationModel;
}

void Service_Offer::setBaseByGuestAmtsModel(Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts* oBaseByGuestAmtsModel) {
  this->_oBaseByGuestAmtsModel = oBaseByGuestAmtsModel;
}

Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts* Service_Offer::getBaseByGuestAmtsModel(void) {
  return this->_oBaseByGuestAmtsModel;
}

void Service_Offer::setAdditionalGuestAmountsModel(Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts* oAdditionalGuestAmountsModel) {
  this->_oAdditionalGuestAmountsModel = oAdditionalGuestAmountsModel;
}

Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts* Service_Offer::getAdditionalGuestAmountsModel(void) {
  return this->_oAdditionalGuestAmountsModel;
}

void Service_Offer::setPrice(double dPrice) {
  this->_dPrice = dPrice;
}

double Service_Offer::getPrice(void) {
  return this->_dPrice;
}

bool Service_Offer::processTravellerConstellationModel() {
  this->_iAdultCount = this->_oTravellerConstellationModel->getAdultCount();
  this->_iChildrenCount = this->_oTravellerConstellationModel->getChildCount();
  this->_iInfantCount = this->_oTravellerConstellationModel->getInfantCount();
  this->_iPersonCount = this->_oTravellerConstellationModel->getPersonCount();
  this->_iStandardCapacity = this->_oTravellerConstellationModel->getStandardCapacity();
}

bool Service_Offer::preparePriceCollections(void) {
  this->preparePriceCollection(this->_oAdultRoomPricesModel, this->_iAdultCount);
  this->preparePriceCollection(this->_oAdultBoardPricesModel, this->_iAdultCount);
  this->preparePriceCollection(this->_oAdultExtraPricesModel, this->_iAdultCount);
  
  this->preparePriceCollection(this->_oChildrenRoomPricesModel, this->_iChildrenCount);
  this->preparePriceCollection(this->_oChildrenBoardPricesModel, this->_iChildrenCount);
  this->preparePriceCollection(this->_oChildrenExtraPricesModel, this->_iChildrenCount);
}

bool Service_Offer::considerPricesFromBaseByGuestAmts(Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts* oBaseByGuestAmtsModel) {
  if (oBaseByGuestAmtsModel) {
    oBaseByGuestAmtsModel->reset();
    int iChildrenCount = this->_iChildrenCount;
    double dNewBaseRoomPrice = 0.00;
    
    while (Model_OTA_HotelRatePlanNotifRQ_BaseByGuestAmts_BaseByGuestAmt* oBaseByGuestAmt = (*oBaseByGuestAmtsModel)++) {
      double dPrice = oBaseByGuestAmt->getAmountAfterTax();
      if (0 < oBaseByGuestAmt->getNumberOfGuests()) {
        dPrice = dPrice / oBaseByGuestAmt->getNumberOfGuests();
      }
      this->addPriceToCollection(this->_oAdultRoomPricesModel, dPrice, this->_iAdultCount, 0, false);
      if (0 < iChildrenCount) {
        this->addPriceToCollection(this->_oChildrenRoomPricesModel, dPrice, iChildrenCount, 0, false);
      }
      dNewBaseRoomPrice = dPrice;
    }
    
    if (0 < dNewBaseRoomPrice) {
      this->setBaseRoomPrice(dNewBaseRoomPrice);
    }
    return true;
  }
}

bool Service_Offer::considerPricesFromAdditionalGuestAmounts(Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts* oAdditionalGuestAmountsModel) {
  if (oAdditionalGuestAmountsModel) {
    oAdditionalGuestAmountsModel->reset();
    while (Model_OTA_HotelRatePlanNotifRQ_AdditionalGuestAmounts_AdditionalGuestAmount* oAdditionalGuestAmount = (*oAdditionalGuestAmountsModel)++) {
      
    }
  }
}

bool Service_Offer::considerSpecialOffers(Model_OTA_HotelRatePlanNotifRQ_Offers* oSpecialOffersCollection) {
  if (oSpecialOffersCollection
    && "BR" != this->getPriceType()
  ) {
    oSpecialOffersCollection->reset();
    while (Model_OTA_HotelRatePlanNotifRQ_Offers_Offer* oSpecialOffer = (*oSpecialOffersCollection)++) {
      bool bSpecialOfferValidForRoom = false;
      bool bSpecialOfferValidForBoard = false;
      bool bSpecialOfferValidForExtra = false;
      Model_OTA_HotelRatePlanNotifRQ_Inventories* oInventories = oSpecialOffer->getInventories();
      
      if (oInventories) {
        oInventories->reset();
        while (Model_OTA_HotelRatePlanNotifRQ_Inventories_Inventory* oInventory = (*oInventories)++) {
          if ("BOARD" == toUpper(oInventory->getInvType())) {
            bSpecialOfferValidForBoard = oInventory->getAppliesToIndicator();
          } else if ("ROOM" == toUpper(oInventory->getInvType())) {
            bSpecialOfferValidForRoom = oInventory->getAppliesToIndicator();
          } else if ("EXTRA" == toUpper(oInventory->getInvType())) {
            bSpecialOfferValidForExtra = oInventory->getAppliesToIndicator();
          }
        }
      }
      if (Service_SpecialOffer::checkIsEarlyBooking(*oSpecialOffer)) {
        Model_OTA_HotelRatePlanNotifRQ_Discount* oDiscount = oSpecialOffer->getDiscount();
        
        if (oDiscount) {
          if (0 < oDiscount->getAmount()) {
            double dPrice = oDiscount->getAmount();
            if (dPrice > 0) {
              dPrice *= -1;
            }
            for (int iPersonCount = 1; iPersonCount <= this->_iAdultCount; iPersonCount++) {
              if (true == bSpecialOfferValidForRoom) {
                this->addPriceToPersonPosition(this->_oAdultRoomPricesModel, dPrice, iPersonCount, false);
              }
              if (true == bSpecialOfferValidForBoard) {
                this->addPriceToPersonPosition(this->_oAdultBoardPricesModel, dPrice, iPersonCount, false);
              }
              if (true == bSpecialOfferValidForExtra) {
                this->addPriceToPersonPosition(this->_oAdultExtraPricesModel, dPrice, iPersonCount, false);
              }
            }
            for (int iPersonCount = 1; iPersonCount <= this->_iChildrenCount; iPersonCount++) {
              if (true == bSpecialOfferValidForRoom) {
                this->addPriceToPersonPosition(this->_oChildrenRoomPricesModel, dPrice, iPersonCount, false);
              }
              if (true == bSpecialOfferValidForBoard) {
                this->addPriceToPersonPosition(this->_oChildrenBoardPricesModel, dPrice, iPersonCount, false);
              }
              if (true == bSpecialOfferValidForExtra) {
                this->addPriceToPersonPosition(this->_oChildrenExtraPricesModel, dPrice, iPersonCount, false);
              }
            }
          } else {
            double dDiscount = oDiscount->getPercent() / 100;
            if (dDiscount > 0) {
              dDiscount *= -1;
            }
            for (int iPersonCount = 1; iPersonCount <= this->_iAdultCount; iPersonCount++) {
              if (true == bSpecialOfferValidForRoom) {
                this->addPriceToPersonPosition(this->_oAdultRoomPricesModel, dDiscount, iPersonCount, true);
              }
              if (true == bSpecialOfferValidForBoard) {
                this->addPriceToPersonPosition(this->_oAdultBoardPricesModel, dDiscount, iPersonCount, true);
              }
              if (true == bSpecialOfferValidForExtra) {
                this->addPriceToPersonPosition(this->_oAdultExtraPricesModel, dDiscount, iPersonCount, true);
              }
            }
            for (int iPersonCount = 1; iPersonCount <= this->_iChildrenCount; iPersonCount++) {
              if (true == bSpecialOfferValidForRoom) {
                this->addPriceToPersonPosition(this->_oChildrenRoomPricesModel, dDiscount, iPersonCount, true);
              }
              if (true == bSpecialOfferValidForBoard) {
                this->addPriceToPersonPosition(this->_oChildrenBoardPricesModel, dDiscount, iPersonCount, true);
              }
              if (true == bSpecialOfferValidForExtra) {
                this->addPriceToPersonPosition(this->_oChildrenExtraPricesModel, dDiscount, iPersonCount, true);
              }
            }
          }
        }
      }
    }
  }
  return true;
}

bool Service_Offer::considerSupplements() {
  this->considerRoomSupplements();
  this->considerBoardSupplements();
  this->considerExtraSupplements();
  
  return true;
}

void Service_Offer::considerRoomSupplements() {
  Model_Collection<Model_Collection<Model_OTA_HotelRatePlanNotifRQ_Supplements>>* oFilteredSupplementsCollection = this->getFilteredSupplementsCollection();
  Model_Collection<Model_OTA_HotelRatePlanNotifRQ_Supplements>* oRoomSupplementsCollection = oFilteredSupplementsCollection->get("Pax");
  
  if (oRoomSupplementsCollection) {
    Model_OTA_HotelRatePlanNotifRQ_Supplements* oAdultsRoomSupplements = oRoomSupplementsCollection->get(10);
    if (oAdultsRoomSupplements) {
      this->considerSupplementsForAgeQualifyingCode(oAdultsRoomSupplements, this->_oAdultRoomPricesModel, this->getTravellerConstellationModel()->getAdultCount(), this->getTravellerConstellationModel()->getAdditionalAdults(), this->getTravellerConstellationModel()->getAgeGroup()->getAdultMinAge(), this->getTravellerConstellationModel()->getAgeGroup()->getAdultMaxAge());
    }
    if (0 < this->getTravellerConstellationModel()->getChildCount()) {
      Model_OTA_HotelRatePlanNotifRQ_Supplements* oChildrenRoomSupplements = oRoomSupplementsCollection->get(8);
      if (oChildrenRoomSupplements) {
        this->considerSupplementsForAgeQualifyingCode(oChildrenRoomSupplements, this->_oChildrenRoomPricesModel, this->getTravellerConstellationModel()->getChildCount(), this->getTravellerConstellationModel()->getAdditionalChildren(), this->getTravellerConstellationModel()->getAgeGroup()->getChildrenMinAge(), this->getTravellerConstellationModel()->getAgeGroup()->getChildrenMaxAge());
      }
    }
  }
}

void Service_Offer::considerBoardSupplements() {
  Model_Collection<Model_Collection<Model_OTA_HotelRatePlanNotifRQ_Supplements>>* oFiltereSupplementsCollection = this->getFilteredSupplementsCollection();
  Model_Collection<Model_OTA_HotelRatePlanNotifRQ_Supplements>* oBoardSupplementsCollection = oFiltereSupplementsCollection->get("Board");
  
  if (oBoardSupplementsCollection) {
    this->considerSupplementsForAgeQualifyingCode(oBoardSupplementsCollection->get(10), this->_oAdultBoardPricesModel, this->getTravellerConstellationModel()->getAdultCount(), this->getTravellerConstellationModel()->getAdditionalAdults(), this->getTravellerConstellationModel()->getAgeGroup()->getAdultMinAge(), this->getTravellerConstellationModel()->getAgeGroup()->getAdultMaxAge());
    if (0 < this->getTravellerConstellationModel()->getChildCount()) {
      Model_OTA_HotelRatePlanNotifRQ_Supplements* oChildrenBoardSupplements = oBoardSupplementsCollection->get(8);
      if (oChildrenBoardSupplements) {
        this->considerSupplementsForAgeQualifyingCode(oChildrenBoardSupplements, this->_oChildrenBoardPricesModel, this->getTravellerConstellationModel()->getChildCount(), this->getTravellerConstellationModel()->getAdditionalChildren(), this->getTravellerConstellationModel()->getAgeGroup()->getChildrenMinAge(), this->getTravellerConstellationModel()->getAgeGroup()->getChildrenMaxAge());
      }
    }
  }
}

void Service_Offer::considerExtraSupplements() {
  Model_Collection<Model_Collection<Model_OTA_HotelRatePlanNotifRQ_Supplements>>* oFiltereSupplementsCollection = this->getFilteredSupplementsCollection();
  Model_Collection<Model_OTA_HotelRatePlanNotifRQ_Supplements>* oExtraSupplementsCollection = oFiltereSupplementsCollection->get("Extra");
  
  if (oExtraSupplementsCollection) {
    Model_OTA_HotelRatePlanNotifRQ_Supplements* oAdultsExtraSupplements = oExtraSupplementsCollection->get(10); 
    
    if (oAdultsExtraSupplements) {
      this->_dBaseExtraPrice = extractBasePriceFromSupplements(oAdultsExtraSupplements);
      this->considerSupplementsForAgeQualifyingCode(oAdultsExtraSupplements, this->_oAdultExtraPricesModel, this->getTravellerConstellationModel()->getAdultCount(), this->getTravellerConstellationModel()->getAdditionalAdults(), this->getTravellerConstellationModel()->getAgeGroup()->getAdultMinAge(), this->getTravellerConstellationModel()->getAgeGroup()->getAdultMaxAge());
    }
    if (this->_dBaseExtraPrice
      && 0 < this->getTravellerConstellationModel()->getChildCount()
    ) {
      for (int iPersonCount = 1; iPersonCount <= this->getTravellerConstellationModel()->getChildCount(); iPersonCount++) {
        this->_oChildrenExtraPricesModel->set(this->_dBaseExtraPrice, iPersonCount);
      }
      Model_OTA_HotelRatePlanNotifRQ_Supplements* oChildrenExtraSupplements = oExtraSupplementsCollection->get(8);
      if (oChildrenExtraSupplements) {
        this->considerSupplementsForAgeQualifyingCode(oChildrenExtraSupplements, this->_oChildrenExtraPricesModel, this->getTravellerConstellationModel()->getChildCount(), this->getTravellerConstellationModel()->getAdditionalChildren(), this->getTravellerConstellationModel()->getAgeGroup()->getChildrenMinAge(), this->getTravellerConstellationModel()->getAgeGroup()->getChildrenMaxAge());
      }
    }
  }
}

void Service_Offer::considerSupplementsForAgeQualifyingCode(Model_OTA_HotelRatePlanNotifRQ_Supplements* oSupplementsCollection, Model_Collection<double>* oPricesCollection, int iPersonMaxCount, int iAdditionalMaxCount, unsigned int iMinAge, unsigned int iMaxAge) {

  if (oSupplementsCollection) {
    oSupplementsCollection->reset();
    while (Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement* oSupplement = (*oSupplementsCollection)++) {

      if ((iMinAge >= oSupplement->getMinAge()
        && iMaxAge == oSupplement->getMaxAge())
        || 0 == oSupplement->getMaxAge()
      ) {
        int iPersonPosition = oSupplement->getAdditionalGuestNumber();
        if (0 == iPersonPosition
          || iPersonPosition <= iAdditionalMaxCount
        ) {
          // festpreis
          if (0 != oSupplement->getAmount()) {
            double dPrice = oSupplement->getAmount();
            this->addPriceToCollection(oPricesCollection, dPrice, iPersonMaxCount, iPersonPosition, false);
          // prozent
          } else if (0 != oSupplement->getPercent()) {
            double dDiscount = oSupplement->getPercent() / 100;
            this->addPriceToCollection(oPricesCollection, dDiscount, iPersonMaxCount, iPersonPosition, true);
          }
        }
      }
    }
  }
}

bool Service_Offer::generatePrice(void) {
  double dPrice = 0.00;
  this->_oAdultRoomPricesModel->reset();
  while (double* oPrice = (*this->_oAdultRoomPricesModel)++) {
    dPrice += *oPrice;
  }

  this->_oAdultBoardPricesModel->reset();
  while (double* oPrice = (*this->_oAdultBoardPricesModel)++) {
    dPrice += *oPrice;
  }

  this->_oAdultExtraPricesModel->reset();
  while (double* oPrice = (*this->_oAdultExtraPricesModel)++) {
    dPrice += *oPrice;
  }

  this->_oChildrenRoomPricesModel->reset();
  while (double* oPrice = (*this->_oChildrenRoomPricesModel)++) {
    dPrice += *oPrice;
  }

  this->_oChildrenBoardPricesModel->reset();
  while (double* oPrice = (*this->_oChildrenBoardPricesModel)++) {
    dPrice += *oPrice;
  }

  this->_oChildrenExtraPricesModel->reset();
  while (double* oPrice = (*this->_oChildrenExtraPricesModel)++) {
    dPrice += *oPrice;
  }
  
  dPrice = dPrice / this->_iPersonCount;
  this->setPrice(dPrice);
}

void Service_Offer::generateOfferEntity(void) {
  this->_iOfferCount++;
  Model_CSVOffer* oOfferEntity = getOfferEntity();
  oOfferEntity->setOfferId(this->_iOfferCount);
  oOfferEntity->setDateFrom(this->getDateFrom());
  oOfferEntity->setDateTo(this->getDateTo());
  oOfferEntity->setPrice(this->getPrice());
  oOfferEntity->setPriceType(this->getPriceType());
  oOfferEntity->setMinAge(this->getTravellerConstellationModel()->getAgeGroup()->getChildrenMinAge());
  oOfferEntity->setMaxAge(this->getTravellerConstellationModel()->getAgeGroup()->getChildrenMaxAge());
//   oOfferEntity->setTravelType(this->getTravelType());
//   oOfferEntity->setArrivalFrom(this->getArr);
  oOfferEntity->setAdultCount(this->getTravellerConstellationModel()->getAdultCount());
  oOfferEntity->setPersonCount(this->getTravellerConstellationModel()->getPersonCount());
  this->setOfferEntity(*oOfferEntity);
}


bool Service_Offer::considerBoarding(Model_Boarding* oBoarding) {
  Model_OTA_HotelRatePlanNotifRQ_Supplements* oSupplements = oBoarding->getSupplements();
  oSupplements->reset();
  
  while (Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement* oSupplement = (*oSupplements)++) {
    if (8 == oSupplement->getAgeQualifyingCode()
      && 0 < getTravellerConstellationModel()->getChildCount()
    ) {
      addBoardingPriceToChildren(oSupplement);
    } else if (10 == oSupplement->getAgeQualifyingCode()) {
      addBoardingPriceToAdult(oSupplement);
    }
  }
}

bool Service_Offer::addBoardingPriceToChildren(Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement* oSupplement) {
  if (8 == oSupplement->getAgeQualifyingCode()) {
    _oChildrenBoardPricesModel->reset();
    for (unsigned int iPersonCount = 1; iPersonCount <= _iChildrenCount; iPersonCount++) {
      (*_oChildrenBoardPricesModel->get(iPersonCount)) = (*_oChildrenBoardPricesModel->get(iPersonCount)) + oSupplement->getAmount();
    }
  }
}

bool Service_Offer::addBoardingPriceToAdult(Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement* oSupplement) {
  if (10 == oSupplement->getAgeQualifyingCode()) {
    _oAdultBoardPricesModel->reset();
    for (unsigned int iPersonCount = 1; iPersonCount <= _iAdultCount; iPersonCount++) {
      (*_oAdultBoardPricesModel->get(iPersonCount)) = (*_oAdultBoardPricesModel->get(iPersonCount)) + oSupplement->getAmount();
    }
  }
}

bool Service_Offer::addPriceToCollection(Model_Collection<double>* oPriceCollection, double dPrice, int iPersonCount, int iPersonPosition = 0, bool bIstPercentage = false) {
  if (0 < iPersonPosition) {
    this->addPriceToPersonPosition(oPriceCollection, dPrice, iPersonPosition, bIstPercentage);
  } else if (0 < iPersonCount) {
    for (int iPersonPosition = 1; iPersonPosition <= iPersonCount; iPersonPosition++) {
      this->addPriceToPersonPosition(oPriceCollection, dPrice, iPersonPosition, bIstPercentage);
    }
  }
  return true;
}

bool Service_Offer::addPriceToPersonPosition(Model_Collection<double>* oPriceCollection, double dPrice, int iPersonPosition, bool bIstPercentage) {
  double dActualPrice = 0;
  double* pdActualPrice = new double(dActualPrice);
  if (oPriceCollection->get(iPersonPosition)) {
    pdActualPrice = oPriceCollection->get(iPersonPosition);
  }
  if (true == bIstPercentage) {
    *pdActualPrice += (*pdActualPrice * dPrice);
  } else {
    *pdActualPrice = *pdActualPrice + dPrice;
  }
  oPriceCollection->set(*pdActualPrice, iPersonPosition);

  return true;
}

/**
 * durchläuft die übergebene collection und befüllt alle positionen von 1 bis personcount mit 0
 */
bool Service_Offer::preparePriceCollection(Model_Collection<double>* oPriceCollection, int iPersonCount) {
  if (0 < iPersonCount) {
    for (unsigned int iActualPersonPosition = 1; iActualPersonPosition <= iPersonCount; iActualPersonPosition++) {
      double* dValue = new double(0.00);
      oPriceCollection->add(*dValue, iActualPersonPosition);
    }
  }
  oPriceCollection->reset();
  return true;
}

bool Service_Offer::createPriceCalculator(void) {

}

/**
 * filtert die übergebene supplements collection in EXTRA, ROOM, BOARD und sortiert da noch innerhalb in Adult und Children
 * eventuell kann man hier noch innerhalb von adult und children sortieren nach mit und ohne personposition
 */
bool Service_Offer::filterSupplements() {
  Model_OTA_HotelRatePlanNotifRQ_Supplements* oSupplementsCollection = getSupplements();
  Model_Collection<Model_Collection<Model_OTA_HotelRatePlanNotifRQ_Supplements>>* oNewSupplementsCollection = new Model_Collection<Model_Collection<Model_OTA_HotelRatePlanNotifRQ_Supplements>>();
  
  oSupplementsCollection->reset();
  while (Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement* oSupplement = (*oSupplementsCollection)++) {
    std::string sSupplementType = oSupplement->getSupplementType();
    unsigned int iAgeQualifyingCode = oSupplement->getAgeQualifyingCode();
    if (false == oNewSupplementsCollection->checkKeyExists(sSupplementType)) {
      Model_Collection<Model_OTA_HotelRatePlanNotifRQ_Supplements>* oInvTypeSupplementCollection = new Model_Collection<Model_OTA_HotelRatePlanNotifRQ_Supplements>();
      oNewSupplementsCollection->set(*oInvTypeSupplementCollection, sSupplementType);
    }
    if (false == oNewSupplementsCollection->get(sSupplementType)->checkKeyExists(iAgeQualifyingCode)) {
      Model_OTA_HotelRatePlanNotifRQ_Supplements* oAgeQualifyingCodeCollection = new Model_OTA_HotelRatePlanNotifRQ_Supplements();
      oNewSupplementsCollection->get(sSupplementType)->set(*oAgeQualifyingCodeCollection, iAgeQualifyingCode);
    }
        oNewSupplementsCollection->get(sSupplementType)->get(iAgeQualifyingCode)->add(*oSupplement);
  }
  this->setFilteredSupplementsCollection(oNewSupplementsCollection);
  return true;
}

double Service_Offer::extractBasePriceFromSupplements(Model_OTA_HotelRatePlanNotifRQ_Supplements* oSupplementsCollection) {
  double dBasePrice = 0;
  if (oSupplementsCollection) {
    oSupplementsCollection->reset();
    int iValidEntrysFound = 0;
    while (Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement* oSupplement = (*oSupplementsCollection)++) {
      if (10 == oSupplement->getAgeQualifyingCode()
        && 0 == oSupplement->getAdditionalGuestNumber()
        && 0 != oSupplement->getAmount()
      ) {
        dBasePrice = oSupplement->getAmount();
        iValidEntrysFound++;
      }
    }
  }
  return dBasePrice;
}

void Service_Offer::setSpecialOffers(Model_OTA_HotelRatePlanNotifRQ_Offers* oSpecialOffers) {
  this->_oSpecialOffersModel = oSpecialOffers;
}

Model_OTA_HotelRatePlanNotifRQ_Offers* Service_Offer::getSpecialOffers(void) {
  return this->_oSpecialOffersModel;
}

void Service_Offer::setPriceType(std::string sPriceType) {
  this->_sPriceType = sPriceType;
}

std::string Service_Offer::getPriceType(void) {
  return this->_sPriceType;
}

void Service_Offer::setSupplements(Model_OTA_HotelRatePlanNotifRQ_Supplements* oSupplements) {
  this->_oSupplementsModel = oSupplements;
}

Model_OTA_HotelRatePlanNotifRQ_Supplements* Service_Offer::getSupplements(void) {
  return this->_oSupplementsModel;
}

void Service_Offer::setFilteredSupplementsCollection(Model_Collection<Model_Collection<Model_OTA_HotelRatePlanNotifRQ_Supplements>>* oFilteredSupplementsCollection) {
  this->_oFilteredSupplementsCollection = oFilteredSupplementsCollection;
}

Model_Collection<Model_Collection<Model_OTA_HotelRatePlanNotifRQ_Supplements>>* Service_Offer::getFilteredSupplementsCollection() {
  return this->_oFilteredSupplementsCollection;
}

void Service_Offer::setOfferEntity(Model_CSVOffer& oOfferEntity) {
  this->_oCsvOfferEntity = &oOfferEntity;
}

Model_CSVOffer* Service_Offer::getOfferEntity(void) {
  return this->_oCsvOfferEntity;
}

void Service_Offer::setBaseRoomPrice(double dBaseRoomPrice) {
  this->_dBaseRoomPrice = dBaseRoomPrice;
}

double Service_Offer::getBaseRoomPrice(void) {
  return this->_dBaseRoomPrice;
}

void Service_Offer::setBaseBoardPrice(double dBaseBoardPrice) {
  this->_dBaseBoardPrice = dBaseBoardPrice;
}

double Service_Offer::getBaseBoardPrice(void) {
  return this->_dBaseBoardPrice;
}

void Service_Offer::setBaseExtraPrice(double dBaseExtraPrice) {
  this->_dBaseExtraPrice = dBaseExtraPrice;
}

double Service_Offer::getBaseExtraPrice(void) {
  return this->_dBaseExtraPrice;
}

std::string Service_Offer::getDateFrom(void) const {
  return _sDateFrom;
}

void Service_Offer::setDateFrom(const std::string sDateFrom) {
  _sDateFrom = sDateFrom;
}

std::string Service_Offer::getDateTo(void) const {
  return _sDateTo;
}

void Service_Offer::setDateTo(const std::string sDateTo) {
  _sDateTo = sDateTo;
}

Service_Offer::Service_Offer() {
  this->_iOfferId = 0;
  this->_iPersonCount = 0;
  this->_iAdultCount = 0;
  this->_iChildrenCount = 0;
  this->_iInfantCount = 0;
  this->_iChildrenMinAge = 0;
  this->_iChildrenMaxAge = 17;
  this->_iStandardCapacity = 0;
  this->_dPrice = 0.00;
  
  this->_dBaseRoomPrice = 0.00;
  this->_dBaseBoardPrice = 0.00;
  this->_dBaseExtraPrice = 0.00;
  
  this->_oAdultRoomPricesModel = new Model_Collection<double>();
  this->_oChildrenRoomPricesModel = new Model_Collection<double>();
  this->_oChildrenBoardPricesModel = new Model_Collection<double>();
  this->_oAdultBoardPricesModel = new Model_Collection<double>();
  this->_oAdultExtraPricesModel = new Model_Collection<double>();
  this->_oChildrenExtraPricesModel = new Model_Collection<double>();
}

Service_Offer::~Service_Offer() {

}
