#ifndef __SERVICE_BOARDING_H__
#define __SERVICE_BOARDING_H__

#include "Service.h"
#include "../Models/Supplements.h"
#include "../Models/Boardings.h"
#include "../Tools/String.cpp"

class Service_Boarding : public Service {
public:
  bool collectBoardingsFromSupplements(Model_OTA_HotelRatePlanNotifRQ_Supplements*);
  bool collectBoardingsFromSellableProducts(Model_OTA_HotelRatePlanNotifRQ_SellableProducts*);
  
  void setBoardings(Model_Boardings&);
  Model_Boardings* getBoardings();
  
  Service_Boarding();
  ~Service_Boarding();
protected:
private:
  Model_Boardings* _oBoardings = NULL;
};

#endif
