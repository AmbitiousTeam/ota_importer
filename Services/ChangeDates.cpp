#include "ChangeDates.h"

const std::string Service_ChangeDates::END = "END";
const std::string Service_ChangeDates::START = "START";

void Service_ChangeDates::setChangeDates(Model_Collection<Model_Collection<Model_Collection<Model_Date>>>& oChangeDates) {
  _oChangeDates = &oChangeDates;
}

Model_Collection<Model_Collection<Model_Collection<Model_Date>>>* Service_ChangeDates::getChangeDates() {
  return _oChangeDates;
}

/**
 * funktion sammelt alle zur verfügung stehenden datumsangaben und collectiert sie
 */
void Service_ChangeDates::collectChangeDates(Model_Date& oDate) {
  std::string* sStart = new std::string(oDate.getStart());
  std::string* sEnd = new std::string(oDate.getEnd());
  std::string* sStartEndKey = new std::string(*sStart + "_" + *sEnd);
  
  if (DEBUG_MODE) {std::cout << "Habe in CollectChangeDates Start : " << *sStart << " und Ende : " << *sEnd << "!" << std::endl;};
  
  Model_Collection<Model_Collection<Model_Collection<Model_Date>>>* oChangeDates = getChangeDates();
  Model_Collection<Model_Date>* oDatesCollectionStart = new Model_Collection<Model_Date>;
  Model_Collection<Model_Collection<Model_Date>>* oTypesCollectionStart = new Model_Collection<Model_Collection<Model_Date>>;
  
  if (0 < (*sStart).length()) {
    // START
    // wenn diese collection bereits dieses datum enthält
    if (oChangeDates->checkKeyExists(*sStart)) {
      oTypesCollectionStart = oChangeDates->get(*sStart);
      if (oTypesCollectionStart->checkKeyExists(Service_ChangeDates::START)) {
        oDatesCollectionStart = oTypesCollectionStart->get(Service_ChangeDates::START);
      }
    }
    oDatesCollectionStart->set(oDate, *sStartEndKey);
    oTypesCollectionStart->set(*oDatesCollectionStart, Service_ChangeDates::START);
    oChangeDates->set(*oTypesCollectionStart, *sStart);
  }
  
  if (0 < (*sEnd).length()) {
    Model_Collection<Model_Date>* oDatesCollectionEnd = new Model_Collection<Model_Date>;
    Model_Collection<Model_Collection<Model_Date>>* oTypesCollectionEnd = new Model_Collection<Model_Collection<Model_Date>>;
    
    // wenn diese collection bereits dieses datum enthält
    if (oChangeDates->checkKeyExists(*sEnd)) {
      oTypesCollectionEnd = oChangeDates->get(*sEnd);
      if (oTypesCollectionEnd->checkKeyExists(Service_ChangeDates::END)) {
        oDatesCollectionEnd = oTypesCollectionEnd->get(Service_ChangeDates::END);
      }
    }
    oDatesCollectionEnd->set(oDate, *sStartEndKey);
    oTypesCollectionEnd->set(*oDatesCollectionEnd, Service_ChangeDates::END);
    oChangeDates->set(*oTypesCollectionEnd, *sEnd);
  }
  setChangeDates(*oChangeDates);
}

Model_Date* Service_ChangeDates::extractDateFromAvailStatusMessage(Model_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage& oAvailStatusMessage) {
  return oAvailStatusMessage.getStatusApplicationControl();
}

Model_Date& Service_ChangeDates::extractDateFromBookingRule(Model_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule& oBookingRule) {
  return oBookingRule;
}

Model_Date& Service_ChangeDates::extractDateFromRate(Model_OTA_HotelRatePlanNotifRQ_Rates_Rate& oRate) {
  return oRate;
}

Model_Date& Service_ChangeDates::extractDateRestrictionDateFromOffer(Model_OTA_HotelRatePlanNotifRQ_Offers_Offer& oOffer) {
  
}

void Service_ChangeDates::setDate(Model_Date& oDate) {
  _oDate = &oDate;
}

Model_Date* Service_ChangeDates::getDate() {
  return _oDate;
}

Service_ChangeDates::Service_ChangeDates() {
  if (DEBUG_MODE) {std::cout << "CTOR von ChangeDatesService!" << std::endl;};
  // collection, die aus datumsangaben und einer collection besteht
  // die darunterliegende collection besteht aus dem type (START|END) und enthält die entities
  Model_Collection<Model_Collection<Model_Collection<Model_Date>>>* oChangeDates = new Model_Collection<Model_Collection<Model_Collection<Model_Date>>>();
  Model_Collection<Model_Collection<Model_Date>>* oTypesCollection = new Model_Collection<Model_Collection<Model_Date>>();
  Model_Collection<Model_Date>* oDatesCollection = new Model_Collection<Model_Date>();
  
  oTypesCollection->add(*oDatesCollection);
  oChangeDates->add(*oTypesCollection);
  setChangeDates(*oChangeDates);
}

Service_ChangeDates::~Service_ChangeDates() {
  if (DEBUG_MODE) {std::cout << "DTOR von ChangeDatesService!" << std::endl;};
}
