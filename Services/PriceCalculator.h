#ifndef __SERVICE_PRICECALCULATOR_H__
#define __SERVICE_PRICECALCULATOR_H__

#include "Service.h"
#include "../Models/Collection.h"
#include "../Models/Offers.h"
#include "../Models/Supplements.h"

class Service_PriceCalculator : public Service {
public:
protected:
private:
  float _fBaseRoomPrice;
  float _fFinalPrice;
  float _fFinalPricePerPerson;
  float _fAdultRoomPrice;
  float _fChildrenRoomPrice;
  float _fAdultBoardPrice;
  float _fChildrenBoardPrice;
  float _fAdultExtraPrice;
  float _fChildrenExtraPrice;
  
  Model_OTA_HotelRatePlanNotifRQ_Offers* _oSpecialOffersModel = NULL;
  Model_OTA_HotelRatePlanNotifRQ_Supplements* _oSupplementsModel = NULL;
  
  Model_Collection<float>* _oAdultRoomPricesModel = NULL;
  Model_Collection<float>* _oChildrenRoomPricesModel = NULL;
  Model_Collection<float>* _oAdultBoardPricesModel = NULL;
  Model_Collection<float>* _oChildrenBoardPricesModel = NULL;
  Model_Collection<float>* _oAdultExtraPricesModel = NULL;
  Model_Collection<float>* _oChildrenExtraPricesModel = NULL;
};

#endif
