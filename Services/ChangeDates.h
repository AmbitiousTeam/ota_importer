#ifndef __SERVICE_CHANGEDATES_H__
#define __SERVICE_CHANGEDATES_H__

#include "../Models/Date.h"
#include "../Models/Rate.h"
#include "../Models/Offer.h"
#include "../Models/ChangeDates.h"
#include "../Models/BookingRule.h"
#include "../Models/AvailStatusMessage.h"
/**
 * Der Service benötigt eine Klasse im Template, die Die Collection wieder spiegelt, 
 * an deren Position sich das entity mit den datumsangaben befindet
 * 
 * wichtig wäre hier noch zu entscheiden, was gesucht wird,
 * oder man übergibt direkt das entsprechende entity zum sammeln
 * oder eine 
 */

class Service_ChangeDates {
public: 

  static const std::string START;
  static const std::string END;

  void collectChangeDates(Model_Date&);
  
  Model_Date& extractDateFromRate(Model_OTA_HotelRatePlanNotifRQ_Rates_Rate&);
  Model_Date* extractDateFromAvailStatusMessage(Model_OTA_HotelAvailNotifRQ_AvailStatusMessages_AvailStatusMessage&);
  Model_Date& extractDateRestrictionDateFromOffer(Model_OTA_HotelRatePlanNotifRQ_Offers_Offer&);
  Model_Date& extractDateFromBookingRule(Model_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule&);
  
  void setDate(Model_Date&);
  Model_Date* getDate();
  
  void setChangeDates(Model_Collection<Model_Collection<Model_Collection<Model_Date>>>&);
  Model_Collection<Model_Collection<Model_Collection<Model_Date>>>* getChangeDates();
  
  Service_ChangeDates();
  ~Service_ChangeDates();
protected:
private:

  Model_Date* _oDate;
  /** Beinhaltet die gefundenen datumsangaben */
  Model_Collection<Model_Collection<Model_Collection<Model_Date>>>* _oChangeDates = NULL;
};

#endif
