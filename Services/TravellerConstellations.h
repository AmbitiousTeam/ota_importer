#ifndef __SERVICE_TRAVELLERCONSTELLATIONS_H__
#define __SERVICE_TRAVELLERCONSTELLATIONS_H__
#include "Service.h"

#include "../Models/Occupancies.h"
#include "../Models/Occupancy.h"
#include "../Models/TravellerConstellations.h"
#include "../Models/AgeGroups.h"
#include "../Models/GuestRoom.h"

class Service_TravellerConstellations : public Service {
public:
  void setTravellerConstellationsModel(Model_TravellerConstellations*);
  Model_TravellerConstellations* getTravellerConstellations(void);
  
  void setAgeGroupsModel(Model_AgeGroups*);
  Model_AgeGroups* getAgeGroupsModel(void);
  
  void generateTravellerConstellationsFromGuestRoom(Model_OTA_HotelRatePlanNotifRQ_GuestRoom*);
  void generateTravellerConstellationsFromOccupancies(Model_OTA_HotelRatePlanNotifRQ_Occupancies*);
  bool extractInformationFromOccupancies(Model_OTA_HotelRatePlanNotifRQ_Occupancies*);
  
  Service_TravellerConstellations();
  ~Service_TravellerConstellations();
protected:
private:
  int _iStandardCapacity;
  int _iPersonCount;
  int _iPersonMinCount;
  int _iPersonMaxCount;
  int _iAdultMinCount;
  int _iAdultMaxCount;
  int _iChildrenMinCount;
  int _iChildrenMaxCount;
  int _iInfantMinCount;
  int _iInfantMaxCount;
  bool _bInfantsAreCounted;
  
  Model_AgeGroups* _oAgeGroupsModel = NULL;
  Model_TravellerConstellations* _oTravellerConstellationsModel = NULL;
};

#endif
