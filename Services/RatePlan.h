#ifndef __SERVICE_RATEPLAN_H__
#define __SERVICE_RATEPLAN_H__

#include "Date.h"
#include "../Tools/Converter.h"
#include "../Tools/String.cpp"
#include "Service.h"
#include "Rate.h"
#include "../Models/RatePlan.h"
#include "../Models/AvailStatusMessages.h"
#include "../Models/BookingRules.h"
#include "../Models/Offers.h"
#include "../Models/Supplements.h"
#include "../Models/SellableProducts.h"

class Service_RatePlan : public Service {
public:
  
  bool processRatePlan();
  
  bool considerSplitDates();
  
  bool processRates();
  
  bool checkOfferValidForDate(Model_OTA_HotelRatePlanNotifRQ_Offers_Offer*, Model_Date*);
  
  bool checkSellableProductValidForDate(Model_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct*, Model_Date*);
  
  bool checkSupplementValidForDate(Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement*, Model_Date*);
  
  bool checkLengthsOfStayValidForDate(Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay*, Model_Date*);
  
  bool checkDateRestrictionValidForDate(Model_OTA_HotelRatePlanNotifRQ_DateRestrictions*, Model_Date*);
  
  Model_OTA_HotelRatePlanNotifRQ_Offers* filterOffers(Model_OTA_HotelRatePlanNotifRQ_Rates_Rate*);
  
  Model_OTA_HotelRatePlanNotifRQ_BookingRules* filterBookingRules(Model_OTA_HotelRatePlanNotifRQ_Rates_Rate*);
  
  Model_OTA_HotelRatePlanNotifRQ_SellableProducts* filterSellableProducts(Model_OTA_HotelRatePlanNotifRQ_Rates_Rate*);
  
  Model_OTA_HotelRatePlanNotifRQ_Supplements* filterSupplements(Model_OTA_HotelRatePlanNotifRQ_Rates_Rate*);
  
  bool checkBookingRuleValidForDate(Model_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule*, Model_Date*);
  
  void setRatePlanModel(Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan*);
  Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan* getRatePlanModel();
  
  void setAvailStatusMessageModel(Model_OTA_HotelAvailNotifRQ_AvailStatusMessages*);
  Model_OTA_HotelAvailNotifRQ_AvailStatusMessages* getAvailStatusMessagesModel();
  
  void setOffersModel(Model_OTA_HotelRatePlanNotifRQ_Offers*);
  Model_OTA_HotelRatePlanNotifRQ_Offers* getOffersModel();
  
  void setBookingRulesModel(Model_OTA_HotelRatePlanNotifRQ_BookingRules*);
  Model_OTA_HotelRatePlanNotifRQ_BookingRules* getBookingRulesModel();
  
  void setSupplementsModel(Model_OTA_HotelRatePlanNotifRQ_Supplements*);
  Model_OTA_HotelRatePlanNotifRQ_Supplements* getSupplementsModel();
  
  void setSellableProductsModel(Model_OTA_HotelRatePlanNotifRQ_SellableProducts*);
  Model_OTA_HotelRatePlanNotifRQ_SellableProducts* getSellableProductsModel();
  
  void setRateService(Service_Rate*);
  Service_Rate* getRateService();
  
  Service_RatePlan();
  ~Service_RatePlan();
protected:
private:
  
  Service_Rate* _oRateService = NULL;
  Model_OTA_HotelRatePlanNotifRQ_Offers* _oOffersModel = NULL;
  Model_OTA_HotelRatePlanNotifRQ_Supplements* _oSupplementsModel = NULL;
  Model_OTA_HotelRatePlanNotifRQ_SellableProducts* _oSellableProductsModel = NULL;
  Model_OTA_HotelRatePlanNotifRQ_BookingRules* _oBookingRulesModel = NULL;
  Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan* _oRatePlanModel = NULL;
  Model_OTA_HotelAvailNotifRQ_AvailStatusMessages* _oAvailStatusMessagesModel = NULL;  
};

#endif
