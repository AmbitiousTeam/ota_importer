#ifndef __SERVICE_SPECIALOFFER_H__
#define __SERVICE_SPECIALOFFER_H__

#include "Service.h"
#include "../Models/Offer.h"

class Service_SpecialOffer : public Service {
public:
  
  static bool checkIsEarlyBooking(Model_OTA_HotelRatePlanNotifRQ_Offers_Offer&);
  static bool checkIsFreeNight(Model_OTA_HotelRatePlanNotifRQ_Offers_Offer&);
  
protected:
private:
};

#endif
