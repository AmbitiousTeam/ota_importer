#include "AgeGroup.h"

Service_AgeGroup::Service_AgeGroup() :
    _iDefaultAdultMinAge(0), _iDefaultAdultMaxAge(0), _iDefaultChildrenMinAge(0), _iDefaultChildrenMaxAge(0),
    _iDefaultInfantMinAge(0), _iDefaultInfantMaxAge(0), _oAges(new Model_Collection<unsigned int>()),
    _oAgeGroups(new Model_AgeGroups()) {
    
    if (DEBUG_MODE) {std::cout << "CTOR von Service_AgeGroup!" << std::endl;};
    
//     _oAges = new Model_Collection<unsigned int>();
//     _oAgeGroups = new Model_AgeGroups();
}

bool Service_AgeGroup::collectAgeInformationFromOccupancies(Model_OTA_HotelRatePlanNotifRQ_Occupancies* oOccupancies) {
  if (oOccupancies) {
    oOccupancies->reset();
    int iAdultFoundCount = 0;
    int iChildrenFoundCount = 0;
    int iInfantFoundCount = 0;
    
    while (Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy* oOccupancy = (*oOccupancies)++) {
      if (10 == oOccupancy->getAgeQualifyingCode()) {
        this->setDefaultAdultMinAge(oOccupancy->getMinAge());
        this->setDefaultAdultMaxAge(oOccupancy->getMaxAge());
        iAdultFoundCount++;
      } else if (8 == oOccupancy->getAgeQualifyingCode()) {
        this->setDefaultChildrenMinAge(oOccupancy->getMinAge());
        this->setDefaultChildrenMaxAge(oOccupancy->getMaxAge());
        iChildrenFoundCount++;
      } else if (7 == oOccupancy->getAgeQualifyingCode()) {
        this->setDefaultInfantMinAge(oOccupancy->getMinAge());
        this->setDefaultInfantMaxAge(oOccupancy->getMaxAge());
        iInfantFoundCount++;
      }
    }
    return true;
  }
}

/*
bool Service_AgeGroup::collectAgeInformationFromOccupancies(Model_OTA_HotelRatePlanNotifRQ_Occupancies& oOccupancies) {
  oOccupancies.reset();
  int iAdultFoundCount = 0;
  int iChildrenFoundCount = 0;
  int iInfantFoundCount = 0;
  Model_AgeGroup oAgeGroupModel;
  
  while (Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy* oOccupancyNode = oOccupancies++) {
    if (10 == oOccupancyNode->getData()->getAgeQualifyingCode()) {
      oAgeGroupModel.setChildrenMinAge(oOccupancyNode->getData()->getMinAge());
      oAgeGroupModel.setChildrenMaxAge(oOccupancyNode->getData()->getMaxAge());
      iAdultFoundCount++;
    } else if (8 == oOccupancyNode->getData()->getAgeQualifyingCode()) {
      oAgeGroupModel.setChildrenMinAge(oOccupancyNode->getData()->getMinAge());
      oAgeGroupModel.setChildrenMaxAge(oOccupancyNode->getData()->getMaxAge());
      iChildrenFoundCount++;
    } else if (7 == oOccupancyNode->getData()->getAgeQualifyingCode()) {
      oAgeGroupModel.setInfantMinAge(oOccupancyNode->getData()->getMinAge());
      oAgeGroupModel.setInfantMaxAge(oOccupancyNode->getData()->getMaxAge());
      iInfantFoundCount++;
    }
  }
    
      _oAgeGroups.setDefaultAgeGroup(oAgeGroupModel);
}
*/
bool Service_AgeGroup::collectAgeInformationFromSupplements(Model_OTA_HotelRatePlanNotifRQ_Supplements* oSupplements) {
  if (oSupplements) {
    oSupplements->reset();
    
    while (Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement* oSupplement = (*oSupplements)++) {
      if (8 == oSupplement->getAgeQualifyingCode()) {
        unsigned int* iMinAge = new unsigned int(oSupplement->getMinAge());
        unsigned int* iMaxAge = new unsigned int(oSupplement->getMaxAge());
        this->_oAges->set(*iMinAge, oSupplement->getMinAge());
        this->_oAges->set(*iMaxAge, oSupplement->getMaxAge());
      }
    }
    this->_oAges->sort();
    return true;
  }
}

bool Service_AgeGroup::generateAgeGroups(void) {
    if (this->_oAges) {
    this->_oAges->reset();
    Model_AgeGroup* oDefaultAgeGroupModel = new Model_AgeGroup();
    
    oDefaultAgeGroupModel->setAdultMinAge(this->getDefaultAdultMinAge());
    oDefaultAgeGroupModel->setAdultMaxAge(this->getDefaultAdultMaxAge());
    oDefaultAgeGroupModel->setChildrenMinAge(this->getDefaultChildrenMinAge());
    oDefaultAgeGroupModel->setChildrenMaxAge(this->getDefaultChildrenMaxAge());
    oDefaultAgeGroupModel->setInfantMinAge(this->getDefaultInfantMinAge());
    oDefaultAgeGroupModel->setInfantMaxAge(this->getDefaultInfantMaxAge());
    
    while (unsigned int* iMinAge = (*this->_oAges)++) {
        unsigned int* iMaxAge = (*this->_oAges)++;
      
        if (0 < iMaxAge) {
            Model_AgeGroup* oAgeGroupModel = new Model_AgeGroup();
            oAgeGroupModel->setAdultMinAge(oDefaultAgeGroupModel->getAdultMinAge());
            oAgeGroupModel->setAdultMaxAge(oDefaultAgeGroupModel->getAdultMaxAge());
            oAgeGroupModel->setChildrenMinAge(*iMinAge);
            oAgeGroupModel->setChildrenMaxAge(*iMaxAge);
            oAgeGroupModel->setInfantMinAge(oDefaultAgeGroupModel->getInfantMinAge());
            oAgeGroupModel->setInfantMaxAge(oDefaultAgeGroupModel->getInfantMaxAge());
            this->_oAgeGroups->add(*oAgeGroupModel);
        }
    }
    this->_oAgeGroups->setDefaultAgeGroup(*oDefaultAgeGroupModel);
    return true;
  }
  return false;
}

void Service_AgeGroup::setDefaultAdultMinAge(int iDefaultAdultMinAge) {
    this->_iDefaultAdultMinAge = iDefaultAdultMinAge;
}

int Service_AgeGroup::getDefaultAdultMinAge(void) {
    return this->_iDefaultAdultMinAge;
}

void Service_AgeGroup::setDefaultAdultMaxAge(int iDefaultAdultMaxAge) {
    this->_iDefaultAdultMaxAge = iDefaultAdultMaxAge;
}

int Service_AgeGroup::getDefaultAdultMaxAge(void) {
    return this->_iDefaultAdultMaxAge;
}

void Service_AgeGroup::setDefaultChildrenMinAge(int iDefaultChildrenMinAge) {
    this->_iDefaultChildrenMinAge = iDefaultChildrenMinAge;
}

int Service_AgeGroup::getDefaultChildrenMinAge(void) {
    return this->_iDefaultChildrenMinAge;
}

void Service_AgeGroup::setDefaultChildrenMaxAge(int iDefaultChildrenMaxAge) {
    this->_iDefaultChildrenMaxAge = iDefaultChildrenMaxAge;
}

int Service_AgeGroup::getDefaultChildrenMaxAge(void) {
    return this->_iDefaultChildrenMaxAge;
}

void Service_AgeGroup::setDefaultInfantMinAge(int iDefaultInfantMinAge) {
    this->_iDefaultInfantMinAge = iDefaultInfantMinAge;
}

int Service_AgeGroup::getDefaultInfantMinAge(void) {
    return this->_iDefaultInfantMinAge;
}

void Service_AgeGroup::setDefaultInfantMaxAge(int iDefaultInfantMaxAge) {
    this->_iDefaultInfantMaxAge = iDefaultInfantMaxAge;
}

int Service_AgeGroup::getDefaultInfantMaxAge(void) {
    return this->_iDefaultInfantMaxAge;
}

void Service_AgeGroup::setAgeGroups(Model_AgeGroups* oAgeGroupsModel) {
    this->_oAgeGroups = oAgeGroupsModel;
}

Model_AgeGroups* Service_AgeGroup::getAgeGroups(void) {
    return this->_oAgeGroups;
}

Service_AgeGroup::~Service_AgeGroup() {
  if (DEBUG_MODE) {std::cout << "DTOR von Service_AgeGroup!" << std::endl;};
}
