#ifndef __SERVICE_RATE_H__
#define __SERVICE_RATE_H__

#include "Service.h"

#include "../Tools/String.cpp"

#include "../Models/CSVOffer.h"
#include "../Models/Offers.h"
#include "../Models/BookingRules.h"
#include "../Models/Supplements.h"
#include "../Models/ChangeDates.h"
#include "../Models/Collection.h"
#include "../Models/Rate.h"
#include "../Models/Rates.h"
#include "../Models/SellableProducts.h"
#include "../Models/AgeGroups.h"
#include "../Models/TravellerConstellations.h"
#include "../Models/Boardings.h"

#include "Offer.h"
#include "Date.h"
#include "TravellerConstellations.h"
#include "ChangeDates.h"
#include "Boarding.h"
#include "AgeGroup.h"

class Service_Rate : public Service {
public:
  
  bool process(void);
  
  bool filterSellableProducts(void);
  
  bool collectSplitDatesFromSpecialOffers(Model_OTA_HotelRatePlanNotifRQ_Offers*);
  bool collectSplitDatesFromSupplements(Model_OTA_HotelRatePlanNotifRQ_Supplements*);
  bool collectSplitDatesFromBookingRules(Model_OTA_HotelRatePlanNotifRQ_BookingRules*);
  void collectSplitDatesFromSellableProducts(Model_OTA_HotelRatePlanNotifRQ_SellableProducts*);
  
  bool considerSplitDatesForRates(Model_OTA_HotelRatePlanNotifRQ_Rates*);
  bool considerSplitDateForRate(Model_Node<Model_OTA_HotelRatePlanNotifRQ_Rates_Rate>*, Model_Date*, std::string, Model_OTA_HotelRatePlanNotifRQ_Rates*);
  
  bool considerChangeDatesCollectionWithRate(Model_OTA_HotelRatePlanNotifRQ_Rates*, Model_Node<Model_OTA_HotelRatePlanNotifRQ_Rates_Rate>*, Model_Collection<Model_Collection<Model_Collection<Model_Date>>>*);
  
  bool considerSellableProducts(void);
  bool considerRooms(void);
  bool considerBoardings(void);
  
  bool considerTravellerConstellations(void);
  bool considerPriceTypes(void);
  bool considerSpecialOffers(void);
  bool considerTravelTypes(void);
  
  bool combineSpecialOffers(void);
  
  bool combineOfferWithTransfer(void);
  bool combineOfferWithProvision(void);
  bool combineOfferWithHandling(void);
  
  void generateBoardings(void);
  bool generateExtraCharges(void);
  bool generatePeriodDiscountFromFreeNights(void);
  bool generateStopSales(void);
  
  bool generateOffers(void);
  
  bool generateOffer(void);
  
  bool generateTravelTypes(void);
  bool generatePriceTypes(void);
  bool checkDataValid(void);
  
  Model_OTA_HotelRatePlanNotifRQ_Offers* filterSpecialOffers(Model_OTA_HotelRatePlanNotifRQ_Offers*);
  
  Model_TravellerConstellations* generateTravellerConstellations(Model_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct*);
  
  Model_AgeGroups* generateAgeGroups(Model_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct*);
  
  void setChangeDatesService(Service_ChangeDates*);
  Service_ChangeDates* getChangeDatesService(void);
  
  void setBookingRulesModel(Model_OTA_HotelRatePlanNotifRQ_BookingRules*);
  Model_OTA_HotelRatePlanNotifRQ_BookingRules* getBookingRulesModel(void);
  
  void setOffersModel(Model_OTA_HotelRatePlanNotifRQ_Offers*);
  Model_OTA_HotelRatePlanNotifRQ_Offers* getOffersModel(void);
  
  void setSellableProductsModel(Model_OTA_HotelRatePlanNotifRQ_SellableProducts*);
  Model_OTA_HotelRatePlanNotifRQ_SellableProducts* getSellableProductsModel(void);
  
  void setSupplementsModel(Model_OTA_HotelRatePlanNotifRQ_Supplements*);
  Model_OTA_HotelRatePlanNotifRQ_Supplements* getSupplementsModel(void);
  
  void setRateModel(Model_OTA_HotelRatePlanNotifRQ_Rates_Rate*);
  Model_OTA_HotelRatePlanNotifRQ_Rates_Rate* getRateModel(void);
  
  void setAgeGroupsModel(Model_AgeGroups*);
  Model_AgeGroups* getAgeGroupsModel(void);
  
  void setActualSpecialOffersCollection(Model_OTA_HotelRatePlanNotifRQ_Offers*);
  Model_OTA_HotelRatePlanNotifRQ_Offers* getActualSpecialOffersCollection(void);
  
  void considerFileContent();
  void considerFileContentOfferCsv();
  void considerFileContentPeriodDiscountCsv();
  
  Service_Rate();
  ~Service_Rate();
  
protected:
private:
  std::string _sCsvOfferContent;
  std::string _sCsvPeriodDiscountContent;
  std::string _sCsvExtraChargeContent;
  std::string _sCsvTransferContent;
  std::string _sCsvStopSaleContent;
  
  std::string _sActualTravelType;
  std::string _sActualPriceType;
  std::string _sActualRoomCodeAgency;
  std::string _sActualRoomCode;
  std::string _sActualBoardingCode;
  std::string _sActualBoardingCodeAgency;
  
  void writeToFile(std::string&, const char*);
//   void writeToFile(std::string&, std::string&);
  
  Service_ChangeDates* _oChangeDatesService = NULL;
  Model_OTA_HotelRatePlanNotifRQ_Rates_Rate* _oRateModel = NULL;
  Model_OTA_HotelRatePlanNotifRQ_Offers* _oOffersModel = NULL;
  Model_OTA_HotelRatePlanNotifRQ_Supplements* _oSupplementsModel = NULL;
  Model_OTA_HotelRatePlanNotifRQ_BookingRules* _oBookingRulesModel = NULL;
  Model_OTA_HotelRatePlanNotifRQ_SellableProducts* _oSellableProductsModel = NULL;
  
  Model_AgeGroups* _oAgeGroupsModel = NULL;
  Model_TravellerConstellations* _oTravellerConstellationsModel = NULL;
  Model_Collection<std::string>* _oTravelTypesModel = NULL;
  Model_Collection<std::string>* _oPriceTypes = NULL;
  
  Model_Boardings* _oBoardings = NULL;
  
  /** FreeNights **/
  Model_OTA_HotelRatePlanNotifRQ_Offers* _oActualFreeNightsCollection = NULL;
  /** discounts pro person über die specialOffers */
  Model_OTA_HotelRatePlanNotifRQ_Offers* _oActualExtraPersonCollection = NULL;
  /** EB und LM **/
  Model_OTA_HotelRatePlanNotifRQ_Offers* _oActualSpecialOffersCollection = NULL;
  
  Model_TravellerConstellation* _oActualTravellerConstellationModel = NULL;
  Model_OTA_HotelRatePlanNotifRQ_SellableProducts* _oActualRoomsModel = NULL;
  Model_OTA_HotelRatePlanNotifRQ_SellableProducts* _oActualBoardingsModel = NULL;
  Model_Boarding* _oActualBoardingModel = NULL;
  Model_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct* _oActualSellableProduct = NULL;
  
  Model_CSVOffer* _oCsvOfferEntity;
};

#endif
