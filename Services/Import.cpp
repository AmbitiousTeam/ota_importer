#include "Import.h"
#include "../Models/DB/Abstract.h"

Service_Import::Service_Import() {
//   Tool_File::open("offer.csv");
    Mapper_OTA_HotelRatePlanNotifRQ* oOTAHotelRatePlanNotifRQMapper = new Mapper_OTA_HotelRatePlanNotifRQ();
    setHotelRatePlanMapper(oOTAHotelRatePlanNotifRQMapper);
    
    Mapper_OTA_HotelAvailNotifRQ* oOTAHotelAvailNotifRQMapper = new Mapper_OTA_HotelAvailNotifRQ();
    setHotelAvailMapper(oOTAHotelAvailNotifRQMapper);
    
    this->_oConfig = new Model_Collection<std::string>;
    
    this->_sInputPath = "";
    this->_sOutputPath = "";
    this->_sRatePlanFileName = "";
    this->_sAvailStatusMessagesFileName = "";
    this->_dtImportStartDateTime = QDateTime::currentDateTime();
}

bool Service_Import::start() {
  bool bReturn = false;
  
  if (true == this->_parseIniFile("../../Config/application.ini")) {
    this->_processConfig();
    
    Model_DB_Abstract* oDb = new Model_DB_Abstract(this->_oConfig);
    sql::ResultSet* oResultSet = oDb->query("SELECT 'Hello World!' AS _message");
    
    while (oResultSet->next()) {
      std::printf("\t... MySQL replies: ");
      /* Access column data by alias or column name */
      std::printf(oResultSet->getString("_message")->c_str());
      std::printf("\t... MySQL says it again: ");
      /* Access column fata by numeric offset, 1 is the first column */
      std::printf(oResultSet->getString(1)->c_str());
    }
    
    // verarbeitung starten
    Tool_File::open("offer.csv");
    
    this->_prepareRatePlans();
    this->_prepareAvailMessages();
    
    this->_useHotelService()->process();
    bReturn = true;
  } else {
    std::cout << "Konnte Ini Datei nicht öffnen! Abbruch!" << std::endl;
    return bReturn;
  }
  return bReturn;
}

bool Service_Import::_prepareRatePlans(void) {
    
    bool bReturn = false;
    // import der rateplans
    Model_OTA_HotelRatePlanNotifRQ* oHotelRatePlanNotifRQModel = this->_importRatePlans();
    this->_useHotelService()->setHotelRatePlanNotifRQModel(oHotelRatePlanNotifRQModel);
    
    return bReturn;
}

bool Service_Import::_prepareAvailMessages(void) {
    
    bool bReturn = false;
//     import der avails
    Model_OTA_HotelAvailNotifRQ* oHotelAvailNotifRQModel = this->_importAvailStatusMessages();
    this->_useHotelService()->setHotelAvailNotifRQModel(oHotelAvailNotifRQModel);
    
    return bReturn;
}

bool Service_Import::_parseIniFile(std::string sIniFilePathName) {
  
  bool bReturn = false;
  
  if (std::ifstream(sIniFilePathName)) {
    QSettings settings(sIniFilePathName.c_str(), QSettings::IniFormat);
    
    settings.beginGroup("production");
    QStringList childKeys = settings.childKeys();
    QStringList values;
    
    foreach (const QString &childKey, childKeys) {
      std::string* sValue = new std::string((settings.value(childKey).toString()).toStdString());
      this->_oConfig->set(*sValue, childKey.toStdString());
    }
    settings.endGroup();
    bReturn = true;
  }
  return bReturn;
}

bool Service_Import::_processConfig(void) {
  if (this->_oConfig) {
    this->setInputPath(*this->_oConfig->get("inputPath"));
    this->setOutputPath(*this->_oConfig->get("outputPath"));
    this->setRatePlanFileName(*this->_oConfig->get("RatePlanFileName"));
    this->setAvailStatusMessagesFileName(*this->_oConfig->get("AvailStatusMessagesFileName"));
  }
  return true;
}

Model_OTA_HotelAvailNotifRQ* Service_Import::_importAvailStatusMessages() {
  Tool_File oFile;
  
  oFile.loadContent(this->_generateFilePath(this->getInputPath(), this->getAvailStatusMessagesFileName()));
  std::string sFileContent = oFile.getContent();
  
  this->getHotelAvailMapper()->setXmlString(sFileContent);
  this->getHotelAvailMapper()->processXml();
  
  return this->getHotelAvailMapper()->getModel();
}

Model_OTA_HotelRatePlanNotifRQ* Service_Import::_importRatePlans() {
  Tool_File oFile;
  
  oFile.loadContent(this->_generateFilePath(this->getInputPath(), this->getRatePlanFileName()));
  std::string sFileContent = oFile.getContent();
  
  this->getHotelRatePlanMapper()->setXmlString(sFileContent);
  this->getHotelRatePlanMapper()->processXml();
  
  return this->getHotelRatePlanMapper()->getModel();
}

std::string Service_Import::_generateFilePath(std::string sFilePath, std::string sFileName) {
    std::stringstream sFilePathName;
    sFilePathName << sFilePath << sFileName;
    return sFilePathName.str();
}

Service_Hotel* Service_Import::_getHoteService(void) {
    return this->_oHotelService;
}

void Service_Import::_setHotelService(Service_Hotel* oHotelService) {
    this->_oHotelService = oHotelService;
}

Service_Hotel* Service_Import::_useHotelService(void) {
    if (!this->_getHoteService()) {
        this->_setHotelService(new Service_Hotel());
    }
    return this->_getHoteService();
}

void Service_Import::setImportStartDateTime(QDateTime& dtImportStartDateTime) {
  this->_dtImportStartDateTime = dtImportStartDateTime;
}

QDateTime& Service_Import::getImportStartDateTime() {
  return _dtImportStartDateTime;
}

void Service_Import::setHotelRatePlanMapper(Mapper_OTA_HotelRatePlanNotifRQ* oHotelRatePlanMapper) {
  this->_oHotelRatePlanMapper = oHotelRatePlanMapper;
}

Mapper_OTA_HotelRatePlanNotifRQ* Service_Import::getHotelRatePlanMapper(void) {
  return _oHotelRatePlanMapper;
}

void Service_Import::setHotelAvailMapper(Mapper_OTA_HotelAvailNotifRQ* oHotelAvailMapper) {
  this->_oHotelAvailMapper = oHotelAvailMapper;
}

Mapper_OTA_HotelAvailNotifRQ* Service_Import::getHotelAvailMapper(void) {
  return this->_oHotelAvailMapper;
}

void Service_Import::setRatePlanService(Service_RatePlan* oRatePlanService) {
  this->_oRatePlanService = oRatePlanService;
}

Service_RatePlan* Service_Import::getRatePlanService() {
  return _oRatePlanService;
}

void Service_Import::setInputPath(std::string sInputPath) {
  this->_sInputPath = sInputPath;
}

std::string Service_Import::getInputPath(void) {
  return _sInputPath;
}

void Service_Import::setOutputPath(std::string sOutputPath) {
  this->_sOutputPath = sOutputPath;
}

std::string Service_Import::getOutputPath(void) {
  return _sOutputPath;
}

void Service_Import::setRatePlanFileName(std::string sRatePlanFileName) {
  this->_sRatePlanFileName = sRatePlanFileName;
}

std::string Service_Import::getRatePlanFileName(void) {
  return _sRatePlanFileName;
}

void Service_Import::setAvailStatusMessagesFileName(std::string sAvailStatusMessagesFileName) {
  this->_sAvailStatusMessagesFileName = sAvailStatusMessagesFileName;
}

std::string Service_Import::getAvailStatusMessagesFileName(void) {
  return _sAvailStatusMessagesFileName;
}

Service_Import::~Service_Import() {
    delete this->_oHotelService;
    delete this->_oConfig;
    delete this->_oHotelRatePlanMapper;
    delete this->_oRatePlanService;
}
