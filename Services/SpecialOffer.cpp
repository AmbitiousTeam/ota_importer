#include "SpecialOffer.h"

bool Service_SpecialOffer::checkIsFreeNight(Model_OTA_HotelRatePlanNotifRQ_Offers_Offer& oSpecialOfferEntity) {
//   Model_OTA_HotelRatePlanNotifRQ_OfferRules oOfferRules = oSpecialOfferEntity.getOfferRules();
//   while (Model_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule* oOfferRule = oOfferRules++) {
//     Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay oLenthsOfStayCollection = oOfferRule->getLengthsOfStay();
//     
//   }
  if (oSpecialOfferEntity.getDiscount()->getNightsDiscounted() > 0
    && oSpecialOfferEntity.getDiscount()->getNightsRequired() > 0
  ) {
    return true;
  }
  return false;
}

bool Service_SpecialOffer::checkIsEarlyBooking(Model_OTA_HotelRatePlanNotifRQ_Offers_Offer& oSpecialOfferEntity) {
//   Model_OTA_HotelRatePlanNotifRQ_OfferRules oOfferRules = oSpecialOfferEntity.getOfferRules();
//   while (Model_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule* oOfferRule = oOfferRules++) {
//     
  //   }
  if (oSpecialOfferEntity.getDiscount()->getNightsDiscounted() > 0
    && oSpecialOfferEntity.getDiscount()->getNightsRequired() > 0
  ) {
    return false;
  }
  return true;
}
