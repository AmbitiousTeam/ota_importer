#include "Hotel.h"

/**
 * startet die verarbeitung der abgelegten RatePlans / AvailStatusMessages 
 */
bool Service_Hotel::process(void) {
  Model_OTA_HotelRatePlanNotifRQ_RatePlans* oRatePlansCollection = getHotelRatePlanNotifRQModel()->getRatePlans();
  oRatePlansCollection->reset();
  
  while (Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan* oRatePlan = (*oRatePlansCollection)++) {
    Service_RatePlan* oRatePlanService = new Service_RatePlan();
    oRatePlanService->setRatePlanModel(oRatePlan);

    oRatePlanService->setBookingRulesModel(oRatePlan->getBookingRules());
    oRatePlanService->setOffersModel(oRatePlan->getOffers());
    oRatePlanService->setSupplementsModel(oRatePlan->getSupplements());
    oRatePlanService->setSellableProductsModel(oRatePlan->getSellableProducts());
    
    oRatePlanService->processRatePlan();
    delete oRatePlanService;
  }
}

void Service_Hotel::setHotelAvailNotifRQModel(Model_OTA_HotelAvailNotifRQ* oHotelAvailNotifRQ) {
  _oHotelAvailNotifRQModel = oHotelAvailNotifRQ;
}

Model_OTA_HotelAvailNotifRQ* Service_Hotel::getHotelAvailNotifRQModel() {
  return _oHotelAvailNotifRQModel;
}

void Service_Hotel::setHotelRatePlanNotifRQModel(Model_OTA_HotelRatePlanNotifRQ* oHotelRatePlanNotifRQ) {
  _oHotelRatePlanNotifRQModel = oHotelRatePlanNotifRQ;
}

Model_OTA_HotelRatePlanNotifRQ* Service_Hotel::getHotelRatePlanNotifRQModel() {
  return _oHotelRatePlanNotifRQModel;
}

Service_Hotel::~Service_Hotel() {

}

Service_Hotel::Service_Hotel() {

}
