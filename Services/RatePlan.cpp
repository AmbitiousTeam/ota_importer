#include "RatePlan.h"
#include "../Tools/String.cpp"

bool Service_RatePlan::processRatePlan() {
  considerSplitDates();
  processRates();
  
  return true;
}

bool Service_RatePlan::considerSplitDates() {
  Service_Rate* oRateService = getRateService();
  
  /** sammeln aller bekannten datums angaben */
  oRateService->collectSplitDatesFromSpecialOffers(getOffersModel());
  oRateService->collectSplitDatesFromBookingRules(getBookingRulesModel());
  oRateService->collectSplitDatesFromSellableProducts(getSellableProductsModel());
  oRateService->collectSplitDatesFromSupplements(getSupplementsModel());
  
  Model_Collection<Model_Collection<Model_Collection<Model_Date>>>* oChangeDatesCollection = oRateService->getChangeDatesService()->getChangeDates();
  oChangeDatesCollection->sort(Model_Collection<Model_Date>::SORT_ASC);
  
  Model_OTA_HotelRatePlanNotifRQ_Rates* oRatesCollection = getRatePlanModel()->getRates();
  oRatesCollection->sort();
  
  /** splitten der raten durch die gesammelten datumsangaben */
  oRateService->considerSplitDatesForRates(oRatesCollection);
  getRatePlanModel()->setRates(*oRatesCollection);
  return true;
}

/**
 * durchläuft die raten und sucht zu jeder rate die aktuellen pendants raus
 * dann wird ein rate service instanziert und die verarbeitung dieser einen rate gestartet
 */
bool Service_RatePlan::processRates() {
  Model_OTA_HotelRatePlanNotifRQ_Rates* oRatesCollection = getRatePlanModel()->getRates();
  oRatesCollection->sort();
  
  while(Model_OTA_HotelRatePlanNotifRQ_Rates_Rate* oRate = (*oRatesCollection)++) {
    Model_OTA_HotelRatePlanNotifRQ_BookingRules* oBookingRulesFilteredModel = filterBookingRules(oRate);
    Model_OTA_HotelRatePlanNotifRQ_SellableProducts* oSellableProductsFilteredModel = filterSellableProducts(oRate);
    Model_OTA_HotelRatePlanNotifRQ_Supplements* oSupplementsFilteredModel = filterSupplements(oRate);
    Model_OTA_HotelRatePlanNotifRQ_Offers* oOffersFilteredModel = filterOffers(oRate);
    
    Service_Rate* oRateService = getRateService();
    oRateService->setOffersModel(oOffersFilteredModel);
    oRateService->setBookingRulesModel(oBookingRulesFilteredModel);
    oRateService->setSellableProductsModel(oSellableProductsFilteredModel);
    oRateService->setSupplementsModel(oSupplementsFilteredModel);
    oRateService->setRateModel(oRate);
    
    oRateService->generateOffers();
  }
  return true;
}

Model_OTA_HotelRatePlanNotifRQ_Offers* Service_RatePlan::filterOffers(Model_OTA_HotelRatePlanNotifRQ_Rates_Rate* oRate) {
  Model_OTA_HotelRatePlanNotifRQ_Offers* oOffersFilteredModel = new Model_OTA_HotelRatePlanNotifRQ_Offers;
  Model_OTA_HotelRatePlanNotifRQ_Offers* oOffersModel = getOffersModel();
  
  if (oOffersModel) {
    oOffersModel->reset();
    
    while (Model_OTA_HotelRatePlanNotifRQ_Offers_Offer* oOffer = (*oOffersModel)++) {
      if (checkOfferValidForDate(oOffer, oRate)) {
        oOffersFilteredModel->add(*oOffer);
      }
    }
  }
  return oOffersFilteredModel;
}

bool Service_RatePlan::checkOfferValidForDate(Model_OTA_HotelRatePlanNotifRQ_Offers_Offer* oOfferModel, Model_Date* oDateModel) {
  Model_OTA_HotelRatePlanNotifRQ_OfferRules* oOfferRulesModel = oOfferModel->getOfferRules();
  
  if (oOfferRulesModel) {
    oOfferRulesModel->reset();
    while (Model_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule* oOfferRule = (*oOfferRulesModel)++) {
      Model_OTA_HotelRatePlanNotifRQ_DateRestrictions* oDateRestrictionsModel = oOfferRule->getDateRestrictions();
      if (oDateRestrictionsModel
        && checkDateRestrictionValidForDate(oDateRestrictionsModel, oDateModel)
      ) {
        return true;
      }
    }
  }
  return false;
}

bool Service_RatePlan::checkDateRestrictionValidForDate(Model_OTA_HotelRatePlanNotifRQ_DateRestrictions* oDateRestrictionsModel, Model_Date* oDateModel) {
  oDateRestrictionsModel->reset();
  while (Model_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction* oDateRestriction = (*oDateRestrictionsModel)++) {
    std::string sRestrictionType = toUpper(oDateRestriction->getRestrictionType());
    
    if ((Model_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction::RESTRICTION_TYPE_ARRIVAL == sRestrictionType
      || Model_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction::RESTRICTION_TYPE_STAY == sRestrictionType)
      && (Service_Date::checkDateBetweenDuration(oDateModel->getStart(), oDateRestriction->getStart(), oDateRestriction->getEnd())
      || Service_Date::checkDateBetweenDuration(oDateModel->getEnd(), oDateRestriction->getStart(), oDateRestriction->getEnd()))
    ) {
      return true;
    }
  }
  return false;
}

bool Service_RatePlan::checkLengthsOfStayValidForDate(Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay* oLengthsOfStayModel, Model_Date* oDateModel) {
  oLengthsOfStayModel->reset();
//   while (Model_Node<Model_OTA_HotelRatePlanNotifRQ_LengthsOfStay_LengthOfStay>* oLengthOfStayNode = oLengthsOfStayModel++) {
//     if (Service_Date::checkDateBetweenDuration(oLengthOfStayNode->getData()->ge))
//   }
}

Model_OTA_HotelRatePlanNotifRQ_BookingRules* Service_RatePlan::filterBookingRules(Model_OTA_HotelRatePlanNotifRQ_Rates_Rate* oRateModel) {
  Model_OTA_HotelRatePlanNotifRQ_BookingRules* oBookingRulesFilteredModel = new Model_OTA_HotelRatePlanNotifRQ_BookingRules;
  Model_OTA_HotelRatePlanNotifRQ_BookingRules* oBookingRulesModel = getBookingRulesModel();
  
  if (oBookingRulesModel) {
    oBookingRulesModel->reset();
    while (Model_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule* oBookingRule = (*oBookingRulesModel)++) {
      if (checkBookingRuleValidForDate(oBookingRule, oRateModel)) {
        oBookingRulesFilteredModel->add(*oBookingRule);
      }
    }
  }
  return oBookingRulesFilteredModel;
}

Model_OTA_HotelRatePlanNotifRQ_SellableProducts* Service_RatePlan::filterSellableProducts(Model_OTA_HotelRatePlanNotifRQ_Rates_Rate* oRateModel) {
  Model_OTA_HotelRatePlanNotifRQ_SellableProducts* oSellableProductsFilteredModel = new Model_OTA_HotelRatePlanNotifRQ_SellableProducts();
  Model_OTA_HotelRatePlanNotifRQ_SellableProducts* oSellableProductsModel = getSellableProductsModel();
  
  if (oSellableProductsModel) {
    oSellableProductsModel->reset();
    
    while (Model_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct* oSellableProduct = (*oSellableProductsModel)++) {
      if (checkSellableProductValidForDate(oSellableProduct, oRateModel)) {
        oSellableProductsFilteredModel->add(*oSellableProduct);
      }
    }
  }
  return oSellableProductsFilteredModel;
}

Model_OTA_HotelRatePlanNotifRQ_Supplements* Service_RatePlan::filterSupplements(Model_OTA_HotelRatePlanNotifRQ_Rates_Rate* oRateModel) {
  Model_OTA_HotelRatePlanNotifRQ_Supplements* oSupplementsFilteredModel = new Model_OTA_HotelRatePlanNotifRQ_Supplements;
  Model_OTA_HotelRatePlanNotifRQ_Supplements* oSupplementsModel = getSupplementsModel();
  
  if (oSupplementsModel) {
    oSupplementsModel->reset();
    
    while (Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement* oSupplement = (*oSupplementsModel)++) {
      if (checkSupplementValidForDate(oSupplement, oRateModel)) {
        oSupplementsFilteredModel->add(*oSupplement);
      }
    }
  }
  return oSupplementsFilteredModel;
}

bool Service_RatePlan::checkBookingRuleValidForDate(Model_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule* oBookingRuleModel, Model_Date* oDateModel) {
  if(Service_Date::checkDateBetweenDuration(oDateModel->getStart(), oBookingRuleModel->getStart(), oBookingRuleModel->getEnd())
    || Service_Date::checkDateBetweenDuration(oDateModel->getEnd(), oBookingRuleModel->getStart(), oBookingRuleModel->getEnd())
    || Service_Date::checkDateBetweenDuration(oDateModel->getStart(), oBookingRuleModel->getCancelPenalty()->getStart(), oBookingRuleModel->getCancelPenalty()->getEnd())
    || Service_Date::checkDateBetweenDuration(oDateModel->getEnd(), oBookingRuleModel->getCancelPenalty()->getStart(), oBookingRuleModel->getCancelPenalty()->getEnd())
  ) {
    return true;
  }
  return false;
}

bool Service_RatePlan::checkSupplementValidForDate(Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement* oSupplementModel, Model_Date* oDateModel) {
  if(Service_Date::checkDateBetweenDuration(oDateModel->getStart(), oSupplementModel->getStart(), oSupplementModel->getEnd())
    || Service_Date::checkDateBetweenDuration(oDateModel->getEnd(), oSupplementModel->getStart(), oSupplementModel->getEnd())
  ) {
    return true;
  }
  return false;
}

bool Service_RatePlan::checkSellableProductValidForDate(Model_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct* oSellableProductModel, Model_Date* oDateModel) {
  std::string sSellableProductStart = oSellableProductModel->getStart();
  std::string sSellableProductEnd = oSellableProductModel->getEnd();
  
  if(0 == sSellableProductStart.length()
    || 0 == sSellableProductEnd.length()
    || Service_Date::checkDateBetweenDuration(oDateModel->getStart(), sSellableProductStart, sSellableProductEnd)
    || Service_Date::checkDateBetweenDuration(oDateModel->getEnd(), sSellableProductStart, sSellableProductEnd)
  ) {
    return true;
  }
  return false;
}

void Service_RatePlan::setAvailStatusMessageModel(Model_OTA_HotelAvailNotifRQ_AvailStatusMessages* oAvailStatusMessagesModel) {
  _oAvailStatusMessagesModel = oAvailStatusMessagesModel;
}

Model_OTA_HotelAvailNotifRQ_AvailStatusMessages* Service_RatePlan::getAvailStatusMessagesModel() {
  return _oAvailStatusMessagesModel;
}

void Service_RatePlan::setRatePlanModel(Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan* oRatePlanModel) {
  _oRatePlanModel = oRatePlanModel;
}

Model_OTA_HotelRatePlanNotifRQ_RatePlans_RatePlan* Service_RatePlan::getRatePlanModel() {
  return _oRatePlanModel;
}

void Service_RatePlan::setBookingRulesModel(Model_OTA_HotelRatePlanNotifRQ_BookingRules* oBookingRulesModel) {
  _oBookingRulesModel = oBookingRulesModel;
}

Model_OTA_HotelRatePlanNotifRQ_BookingRules* Service_RatePlan::getBookingRulesModel() {
  return _oBookingRulesModel;
}

void Service_RatePlan::setOffersModel(Model_OTA_HotelRatePlanNotifRQ_Offers* oOffersModel) {
  _oOffersModel = oOffersModel;
}

Model_OTA_HotelRatePlanNotifRQ_Offers* Service_RatePlan::getOffersModel() {
  return _oOffersModel;
}

void Service_RatePlan::setSellableProductsModel(Model_OTA_HotelRatePlanNotifRQ_SellableProducts* oSellableProductsModel) {
  _oSellableProductsModel = oSellableProductsModel;
}

Model_OTA_HotelRatePlanNotifRQ_SellableProducts* Service_RatePlan::getSellableProductsModel() {
  return _oSellableProductsModel;
}

void Service_RatePlan::setSupplementsModel(Model_OTA_HotelRatePlanNotifRQ_Supplements* oSupplementsModel) {
  _oSupplementsModel = oSupplementsModel;
}

Model_OTA_HotelRatePlanNotifRQ_Supplements* Service_RatePlan::getSupplementsModel() {
  return _oSupplementsModel;
}

void Service_RatePlan::setRateService(Service_Rate* oRateService) {
  _oRateService = oRateService;
}

Service_Rate* Service_RatePlan::getRateService() {
  return _oRateService;
}

Service_RatePlan::Service_RatePlan() {
  Service_Rate* oRateService = new Service_Rate();
  setRateService(oRateService);
}

Service_RatePlan::~Service_RatePlan() {
  delete _oRateService;
  delete _oAvailStatusMessagesModel;
  delete _oBookingRulesModel;
  delete _oOffersModel;
  delete _oRatePlanModel;
  delete _oSellableProductsModel;
  delete _oSupplementsModel;
}
