#ifndef __SERVICE_IMPORT_H__
#define __SERVICE_IMPORT_H__

#include <QDateTime>
#include <QSettings>
#include <QStringList>

#include "Service.h"
#include "Hotel.h"

#include "../Tools/File.cpp"

#include "../Mapper/HotelAvailNotifRQ.h"
#include "../Mapper/HotelRatePlanNotifRQ.h"

#include "../Models/AvailStatusMessages.h"
#include "../Models/AvailStatusMessage.h"
#include "../Models/HotelAvailNotifRQ.h"

class Service_Import : public Service {
public:

  /**
   * startet den import
   */
  bool start();
  
  /**
   * parsed die Init Datei
   */
//   bool parseIniFile(void);
  
  void setRatePlanService(Service_RatePlan*);
  Service_RatePlan* getRatePlanService();
  
  void setHotelRatePlanMapper(Mapper_OTA_HotelRatePlanNotifRQ*);
  Mapper_OTA_HotelRatePlanNotifRQ* getHotelRatePlanMapper(void);
  
  void setHotelAvailMapper(Mapper_OTA_HotelAvailNotifRQ*);
  Mapper_OTA_HotelAvailNotifRQ* getHotelAvailMapper(void);
  
  void setImportStartDateTime(QDateTime&);
  QDateTime& getImportStartDateTime();
  
  void setInputPath(std::string);
  std::string getInputPath(void);
  
  void setOutputPath(std::string);
  std::string getOutputPath(void);
  
  void setRatePlanFileName(std::string);
  std::string getRatePlanFileName(void);
  
  void setAvailStatusMessagesFileName(std::string);
  std::string getAvailStatusMessagesFileName(void);
  
  Service_Import();
  ~Service_Import();
protected:
private:
    
  Model_OTA_HotelAvailNotifRQ* _importAvailStatusMessages();
  Model_OTA_HotelRatePlanNotifRQ* _importRatePlans();
    
  bool _prepareRatePlans(void);
  bool _prepareAvailMessages(void);
  
  /**
   * generiert aus der übergabe einen Dateinamen
   * 
   * @param string pfad
   * @param string dateiname
   * 
   * @return string dateiname
   * 
   */
  std::string _generateFilePath(std::string, std::string);
  
  bool _parseIniFile(std::string);
  
  /**
   * verarbeitet die Config, die vorher in this->_oConfig geparsed wurde
   */
  bool _processConfig(void);
  
  void _setHotelService(Service_Hotel* oHotelService);
  Service_Hotel* _getHoteService(void);
  Service_Hotel* _useHotelService(void);
    
  Service_Hotel* _oHotelService = NULL;
  QDateTime _dtImportStartDateTime;
  std::string _sInputPath;
  std::string _sOutputPath;
  std::string _sRatePlanFileName;
  std::string _sAvailStatusMessagesFileName;
  
  Service_RatePlan* _oRatePlanService = NULL;
  Model_Collection<std::string>* _oConfig = NULL;
  Mapper_OTA_HotelRatePlanNotifRQ* _oHotelRatePlanMapper = NULL;
  Mapper_OTA_HotelAvailNotifRQ* _oHotelAvailMapper = NULL;
};

#endif
