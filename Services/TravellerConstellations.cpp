#include "TravellerConstellations.h"

void Service_TravellerConstellations::setTravellerConstellationsModel(Model_TravellerConstellations* oTravellerConstellationsModel) {
  _oTravellerConstellationsModel = oTravellerConstellationsModel;
}

Model_TravellerConstellations* Service_TravellerConstellations::getTravellerConstellations(void) {
  return _oTravellerConstellationsModel;
}

void Service_TravellerConstellations::generateTravellerConstellationsFromOccupancies(Model_OTA_HotelRatePlanNotifRQ_Occupancies* oOccupanciesModel) {
  if (oOccupanciesModel) {
    extractInformationFromOccupancies(oOccupanciesModel);
    for (int iAdultCount = _iAdultMinCount; iAdultCount <= _iAdultMaxCount; iAdultCount++) {
      for (int iChildrenCount = _iChildrenMinCount; iChildrenCount <= _iChildrenMaxCount; iChildrenCount++) {
        int iPersonCount = iAdultCount + iChildrenCount;
        if (iPersonCount >= _iPersonMinCount && iPersonCount <= _iPersonMaxCount) {
          if (0 < iChildrenCount) {
            Model_AgeGroups* oAgeGroupsModel = getAgeGroupsModel();
            oAgeGroupsModel->reset();
            while (Model_AgeGroup* oAgeGroup = (*oAgeGroupsModel)++) {
              // wenn keine allumfassende altersgruppe
              Model_TravellerConstellation* oTravellerConstellationModel = new Model_TravellerConstellation();
              oTravellerConstellationModel->setStandardCapacity(_iStandardCapacity);
              oTravellerConstellationModel->setPersonCount(iPersonCount);
              oTravellerConstellationModel->setAdultCount(iAdultCount);
              if (0 < _iStandardCapacity
                && iAdultCount > _iStandardCapacity
              ) {
                oTravellerConstellationModel->setAdditionalAdults(iAdultCount - _iStandardCapacity);
              }
              oTravellerConstellationModel->setChildCount(iChildrenCount);
              oTravellerConstellationModel->setAdditionalChildren(iChildrenCount);
              oTravellerConstellationModel->setAgeGroup(*oAgeGroup);
              _oTravellerConstellationsModel->add(*oTravellerConstellationModel);
            }
            // defaultrate hinzufügen
            if (1 < iChildrenCount) {
              Model_TravellerConstellation* oTravellerConstellationModel = new Model_TravellerConstellation();
              oTravellerConstellationModel->setStandardCapacity(_iStandardCapacity);
              oTravellerConstellationModel->setPersonCount(iPersonCount);
              oTravellerConstellationModel->setAdultCount(iAdultCount);
              if (0 < _iStandardCapacity
                && iAdultCount > _iStandardCapacity
              ) {
                oTravellerConstellationModel->setAdditionalAdults(iAdultCount - _iStandardCapacity);
              }
              oTravellerConstellationModel->setChildCount(iChildrenCount);
              oTravellerConstellationModel->setAdditionalChildren(iChildrenCount);
              oTravellerConstellationModel->setAgeGroup(*(oAgeGroupsModel->getDefaultAgeGroup()));
              _oTravellerConstellationsModel->add(*oTravellerConstellationModel);
            }
          } else if (1 != iChildrenCount) {
  //           Model_AgeGroups* oAgeGroupsModel = getAgeGroupsModel();
  //           oAgeGroupsModel->reset();
  //         std::cout << "Erstelle eine Rate mit " << iPersonCount << " Personen, bestehend aus " << iAdultCount << " Erwachsenen und " << iChildrenCount << " Kindern!" << std::endl;
            Model_TravellerConstellation* oTravellerConstellationModel = new Model_TravellerConstellation();
            oTravellerConstellationModel->setStandardCapacity(_iStandardCapacity);
            oTravellerConstellationModel->setPersonCount(iPersonCount);
            oTravellerConstellationModel->setAdultCount(iAdultCount);
            if (0 < _iStandardCapacity
              && iAdultCount > _iStandardCapacity
            ) {
              oTravellerConstellationModel->setAdditionalAdults(iAdultCount - _iStandardCapacity);
            }
            oTravellerConstellationModel->setChildCount(iChildrenCount);
            oTravellerConstellationModel->setAdditionalChildren(iChildrenCount);
            oTravellerConstellationModel->setAgeGroup(*getAgeGroupsModel()->getDefaultAgeGroup());
            _oTravellerConstellationsModel->add(*oTravellerConstellationModel);
          }
        }
      }
    }
  }
}

void Service_TravellerConstellations::setAgeGroupsModel(Model_AgeGroups* oAgeGroupsModel) {
  _oAgeGroupsModel = oAgeGroupsModel;
}

Model_AgeGroups* Service_TravellerConstellations::getAgeGroupsModel(void) {
  return _oAgeGroupsModel;
}

bool Service_TravellerConstellations::extractInformationFromOccupancies(Model_OTA_HotelRatePlanNotifRQ_Occupancies* oOccupanciesModel) {
  if (oOccupanciesModel) {
    oOccupanciesModel->reset();
    
    while (Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy* oOccupancyModel = (*oOccupanciesModel)++) {
  //     Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy oOccupancyModel = oOccupancyNode->getData();
      // Adults
      if (Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::AGE_QUALIFYING_CODE_ADULT == oOccupancyModel->getAgeQualifyingCode()) {
  //       std::cout << "Habe Erwachsene min : " << oOccupancyModel.getMinOccupancy() << " und max : " << oOccupancyModel.getMaxOccupancy() << " sowie einem minAge von " << oOccupancyModel.getMinAge() << " und einem maxAge von " << oOccupancyModel.getMaxAge() << std::endl;
        _iAdultMinCount = oOccupancyModel->getMinOccupancy();
        _iAdultMaxCount = oOccupancyModel->getMaxOccupancy();
      // Children
      } else if (Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::AGE_QUALIFYING_CODE_CHILDREN == oOccupancyModel->getAgeQualifyingCode()) {
  //       std::cout << "Habe Kinder min : " << oOccupancyModel.getMinOccupancy() << " und max : " << oOccupancyModel.getMaxOccupancy() << " sowie einem minAge von " << oOccupancyModel.getMinAge() << " und einem maxAge von " << oOccupancyModel.getMaxAge() << std::endl;
        _iChildrenMinCount = oOccupancyModel->getMinOccupancy();
        _iChildrenMaxCount = oOccupancyModel->getMaxOccupancy();
      // Infants
      } else if (Model_OTA_HotelRatePlanNotifRQ_Occupancies_Occupancy::AGE_QUALIFYING_CODE_INFANT == oOccupancyModel->getAgeQualifyingCode()) {
  //       std::cout << "Habe Babys min : " << oOccupancyModel.getMinOccupancy() << " und max : " << oOccupancyModel.getMaxOccupancy() << " sowie einem minAge von " << oOccupancyModel.getMinAge() << " und einem maxAge von " << oOccupancyModel.getMaxAge() << std::endl;
        _iInfantMinCount = oOccupancyModel->getMinOccupancy();
        _iInfantMaxCount = oOccupancyModel->getMaxOccupancy();
      // StandardCapacity
      } else {
  //       std::cout << "Habe StandardCapacity min : " << oOccupancyModel.getMinOccupancy() << " und max : " << oOccupancyModel.getMaxOccupancy() << " sowie einem minAge von " << oOccupancyModel.getMinAge() << " und einem maxAge von " << oOccupancyModel.getMaxAge() << std::endl;
        _iPersonMinCount = oOccupancyModel->getMinOccupancy();
        _iPersonMaxCount = oOccupancyModel->getMaxOccupancy();
        _bInfantsAreCounted = oOccupancyModel->getInfantsAreCounted();
      }
    }
    return true;
  }
}

void Service_TravellerConstellations::generateTravellerConstellationsFromGuestRoom(Model_OTA_HotelRatePlanNotifRQ_GuestRoom* oGuestRoom) {
  if (oGuestRoom) {
    Model_OTA_HotelRatePlanNotifRQ_Occupancies* oOccupanciesModel = oGuestRoom->getOccupancies();
    Model_OTA_HotelRatePlanNotifRQ_Quantities* oQuantities = oGuestRoom->getQuantities();
    if (oOccupanciesModel) {
      generateTravellerConstellationsFromOccupancies(oOccupanciesModel);
      if (oQuantities) {
        _iStandardCapacity = oQuantities->getMinBillableGuests();
      }
    }
  }
}

Service_TravellerConstellations::Service_TravellerConstellations() {
  _iStandardCapacity = 0;
  _iPersonCount = 0;
  _iPersonMinCount = 0;
  _iPersonMaxCount = 0;
  _iAdultMinCount = 0;
  _iAdultMaxCount = 0;
  _iChildrenMinCount = 0;
  _iChildrenMaxCount = 0;
  _iInfantMinCount = 0;
  _iInfantMaxCount = 0;
  _oTravellerConstellationsModel = new Model_TravellerConstellations();
}

Service_TravellerConstellations::~Service_TravellerConstellations() {

}
