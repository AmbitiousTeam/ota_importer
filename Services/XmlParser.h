#ifndef __SERVICE_XMLPARSER_H__
#define __SERVICE_XMLPARSER_H__

#include <string>

#include <map>
#include <boost/regex.hpp>
#include <iostream>

#include "../Tools/String.cpp"
#include "../App/Defines.cpp"
#include "../Tools/Logger.h"

#include "../Mapper/Map.h"

template<typename T_Mapper>class Service_XmlParser
{
    public:
        /** member **/
        /** methods **/
//      bool addChildNode();
//      bool hasChildNodes();
//      bool checkValid(std::string) const;
    
        /**
        * Einstiegspunkt der klasse
        * 
        * übergeben wird der zu parsende string 
        * 
        * @param sContent
        * 
        * @return bool
        */
        bool parse(std::string);
        
        void setMap(Map<T_Mapper>&);
        Map<T_Mapper>* getMap();
        
        std::string getRootTag() const;
        bool setRootTag(std::string);
        
        std::string getXmlContent() const;
        bool setXmlContent(std::string);
        
        void setEliminateRedundantTags(bool);
        bool isEliminateRedundantTags();
        
        void setEliminateRedundantRootTags(bool);
        bool isEliminateRedundantRootTags();
        
        void setXmlContentOnly(bool);
        bool isXmlContentOnly();
        
        void setActualXmlContent(std::string);
        std::string getActualXmlContent();
        
        Service_XmlParser();
        ~Service_XmlParser();
    
    protected:
        /** member **/
        bool _bIsValid;
        bool _bShouldValidate;
        bool _bEliminateRedundantRootTags;
        bool _bEliminateRedundantTags;
    
    /** methods **/
    private:
        
        /**
         * bereitet den XML Content für eventuelle Notwendigkeiten vor
         */
        bool _prepareXmlContent(std::string);
        
        /**
         * liest die Attribute des aktuellen XML Tags
         */
        bool _parseAttributes(std::string);
        
        /**
         * verarbeitet alle zur verfügung stehenden informationen zum aktuellen root knoten 
         * und gibt den innenliegenden content zurück
         * 
         */
        bool _processRootTag(std::string);
        
        /**
         * bereitet den übergebenen Tag Namen auf, 
         * hier wird aktuell der string getrimt und der erste Buchstabe groß geschrieben
         * 
         * @param std::string sTagName
         * 
         * @return std::string
         */
        std::string _formatTagName(std::string);
        
        /**
         * datei wird eingelesen und dann das erste tag gemachted, dieser string wird als aData[tag] persistiert,
         * attribute des tags als $aData[tag][attribute], nachfolgende inhalte als $aData[tag][children];
         * 
         * es gibt die funktionen:
         *      - parse - parst einen string nach aData
         *      - load - liest xml content von einer datei ein
         *      - loadXmlString - parst einen xml string nach aData
         *      - validate - validiert die korrektheit des xml content, hier könnte man noch diverse schematas oder standards übergeben um auf deren validität zu überprüfen
         *      - save - speichert den aktuellen inhalt von aData als string in eine datei, ist keine datei angeben, wird der string ausgegeben
         *      - getAttributes - gibt alle attribute eines knotens zurück
         *      - getAttribut[attributName] - gibt den inhalt eines attributes zurück
         *      - setAttributes - setzt alle attribute, übergeben wird ein array attribute => values
         *      - setAttribute[attributName] - setzt den inhalt eines attributes
         *      - getNextSilbling - gibt das nächste element auf selber ebene zurück
         *      - getNextChild - gibt den nächsten unterknoten zurück
         *      - hasChildren - checkt, ob das knoten unterknoten enthält
         *      - hasAttributes - checkt ob das knoten attribute enthält
         *      - getChildrenCount - gibt die anzahl der unterknoten zurück
         *      - replaceNode - ersetzt einen knoten durch einen neuen
         *      - deleteNode - löscht einen knoten
         *      - addNode - fügt dem übergebenen knoten einen neuen knoten ein
         *      - saveXml - gibt den xmlString des aktuellen aData elements aus
         *      
         */
        bool _checkHasXmlHeader(std::string);
        
        /**
         * prüft, ob das aktuelle element noch unterelemente hat
         */
        bool _checkXmlHasChilds(std::string);
        
        std::string _removeXmlHeader(std::string);
        
        /**
         * entfernt überflüssige root tag elemente aus der XML
         */
        std::string _eliminateRedundantRootTags();
        
        /**
         * entfernt überflüssige tag elemente aus der XML
         */
        std::string _eliminateRedundantTags(std::string, std::string);
        
        std::string _sXmlContent;
        std::string _sRootTag;
        std::string _sActualXmlContent;
        
        bool _bXmlContentOnly;
        Map<T_Mapper>* _oMap = NULL;
};

template<typename T_Mapper>Service_XmlParser<T_Mapper>::Service_XmlParser() : 
    _bIsValid(false), _bShouldValidate(false), _bXmlContentOnly(false), _bEliminateRedundantTags(false), _bEliminateRedundantRootTags(false) {
}

template<typename T_Mapper>bool Service_XmlParser<T_Mapper>::parse(std::string sContent) {
    this->setXmlContent(sContent);
    this->setActualXmlContent(sContent);
    this->_prepareXmlContent(sContent);
    this->_processRootTag(this->getActualXmlContent());
    
    sContent = this->getActualXmlContent();
    
    if (false == this->isXmlContentOnly()) {  
        boost::regex regex("(<([a-zA-Z\\_\\-]+) *[^>]*/>)|(<([a-zA-Z\\_\\-]+) *[^>]*>.*?</\\4>)");
        boost::smatch matches;                // container für die ergebnisse
        boost::regex_constants::icase;        // nicht casesensitive
        boost::regex_constants::match_all;    // like preg_match_all
        
        std::string::const_iterator start1 = sContent.begin();
        std::string::const_iterator end1   = sContent.end();
        
        while (boost::regex_search(start1, end1, matches, regex)) {
            std::string sEmptyTagContent(matches[1].first, matches[1].second);
            std::string sEmptyTag(matches[2].first, matches[2].second);
            std::string sOpenTagContent(matches[3].first, matches[3].second);
            std::string sOpenTag(matches[4].first, matches[4].second);
            
            sOpenTag = trim(sOpenTag);
            sEmptyTag = trim(sEmptyTag);
            
            if (true == this->isEliminateRedundantTags()
                && 0 < sOpenTagContent.length()
            ) {
                // falls redundante tags vorhanden waren, noch einmal neu mit parsen beginnen
                std::string sNewOpenTagContent = this->_eliminateRedundantTags(sOpenTagContent, sOpenTag);
                if (sOpenTagContent != sNewOpenTagContent) {
                    this->parse(sNewOpenTagContent);
                    return 0;
                }
            }
            
            sOpenTagContent = trim(sOpenTagContent);
            sEmptyTagContent = trim(sEmptyTagContent);
            
            if (0 < sOpenTag.length()) {
//                 (*_oMap)(sOpenTag, sOpenTagContent);
                this->_oMap->operator()(sOpenTag, sOpenTagContent);
            } else if (0 < sEmptyTag.length()) {
//                 (*_oMap)(sEmptyTag, sEmptyTagContent);
                this->_oMap->operator()(sEmptyTag, sEmptyTagContent);
            }
            
            start1 = matches[0].second;
        }
    }
}

template<typename T_Mapper>std::string Service_XmlParser<T_Mapper>::getRootTag() const {
    return this->_sRootTag;
}

template<typename T_Mapper>bool Service_XmlParser<T_Mapper>::setRootTag(std::string sRootTag) {
    this->_sRootTag = sRootTag;
}

template<typename T_Mapper>void Service_XmlParser<T_Mapper>::setMap(Map<T_Mapper>& oMap) {
    this->_oMap = &oMap;
}

template<typename T_Mapper>Map<T_Mapper>* Service_XmlParser<T_Mapper>::getMap() {
    return this->_oMap;
}

template<typename T_Mapper>void Service_XmlParser<T_Mapper>::setEliminateRedundantTags(bool bEliminateRedundantTags) {
    this->_bEliminateRedundantTags = bEliminateRedundantTags;
}

template<typename T_Mapper>bool Service_XmlParser<T_Mapper>::isEliminateRedundantTags() {
    return this->_bEliminateRedundantTags;
}

template<typename T_Mapper>void Service_XmlParser<T_Mapper>::setEliminateRedundantRootTags(bool bEliminateRedundantRootTags) {
    this->_bEliminateRedundantRootTags = bEliminateRedundantRootTags;
}

template<typename T_Mapper>bool Service_XmlParser<T_Mapper>::isEliminateRedundantRootTags() {
    return this->_bEliminateRedundantRootTags;
}

template<typename T_Mapper>std::string Service_XmlParser<T_Mapper>::getXmlContent() const {
    return this->_sXmlContent;
}

template<typename T_Mapper>bool Service_XmlParser<T_Mapper>::setXmlContent(std::string sXmlContent) {
    this->_sXmlContent = sXmlContent;
    return true;
}

template<typename T_Mapper>void Service_XmlParser<T_Mapper>::setXmlContentOnly(bool bXmlContentOnly) {
    this->_bXmlContentOnly = bXmlContentOnly;
}

template<typename T_Mapper>bool Service_XmlParser<T_Mapper>::isXmlContentOnly() {
    return this->_bXmlContentOnly;
}

template<typename T_Mapper>void Service_XmlParser<T_Mapper>::setActualXmlContent(std::string sActualXmlContent) {
    this->_sActualXmlContent = sActualXmlContent;
}

template<typename T_Mapper>std::string Service_XmlParser<T_Mapper>::getActualXmlContent() {
    return this->_sActualXmlContent;
}

template<typename T_Mapper>bool Service_XmlParser<T_Mapper>::_checkHasXmlHeader(std::string sContent) {
    boost::regex_constants::icase;
    boost::regex sRegExXmlHeader("<\\?.*?xml.*?\\?>");
    boost::match_results<std::string::const_iterator>matches;
    std::string::const_iterator start = sContent.begin();
    std::string::const_iterator end   = sContent.end();
    bool bResult = boost::regex_match(sContent, matches, sRegExXmlHeader, boost::match_partial | boost::format_perl);
    
    return bResult;
}

template<typename T_Mapper>std::string Service_XmlParser<T_Mapper>::_removeXmlHeader(std::string sContent) {
    boost::regex_constants::icase;
    boost::regex sRegExXmlHeader("<\\?.*?xml.*?\\?>");
    std::string sReplaceResult = boost::regex_replace(sContent, sRegExXmlHeader, "", boost::match_partial | boost::format_perl);

    return sReplaceResult;
}

template<typename T_Mapper>bool Service_XmlParser<T_Mapper>::_processRootTag(std::string sContent) {
//   boost::regex regex(".*?<" + getRootTag() + " *([^>]*)/>.*?|.*?<" + getRootTag() + " *([^>]*)>(.*?)</" + getRootTag() + ">.*?");
    boost::regex regex("<" + this->getRootTag() + " *([^>]*)/>|<" + this->getRootTag() + " *([^>]*)>(.*?)</" + this->getRootTag() + ">");
//   boost::regex regex("<" + getRootTag() + " +([^>]*)/>|<" + getRootTag() + " +([^>]*)>(.*?)</" + getRootTag() + ">");
    boost::smatch matches;                // container für die ergebnisse
    boost::regex_constants::icase;        // nicht casesensitive
    boost::regex_constants::match_all;    // like preg_match_all

    std::string::const_iterator start1 = sContent.begin();
    std::string::const_iterator end1   = sContent.end();

    bool bReturn = false;
  
    while (boost::regex_search(start1, end1, matches, regex)) {
        std::string sRootEmptyTagAttr(matches[1].first, matches[1].second);
        std::string sRootTagAttr(matches[2].first, matches[2].second);
        std::string sInnerRootTagContent(matches[3].first, matches[3].second);

        sContent = trim(sInnerRootTagContent);

        if (0 < sRootEmptyTagAttr.length()) {
            this->_parseAttributes(sRootEmptyTagAttr);
        } else if (0 < sRootTagAttr.length()) {
            this->_parseAttributes(sRootTagAttr);
        }
    
        start1 = matches[0].second;
        bReturn = true;
    }
    this->setActualXmlContent(sContent);
    return bReturn;
}

template<typename T_Mapper>bool Service_XmlParser<T_Mapper>::_prepareXmlContent(std::string sContent) {
    sContent = trim(sContent);
    
    // XML Header entfernen, ist mit der aktuellen logik aber nicht unbedingt notwendig
    if (true == this->_checkHasXmlHeader(sContent)) {
        sContent = this->_removeXmlHeader(sContent);
    }
//   if (true == getEliminateRedundantRootTags()) {
//     sContent = eliminateRedundantRootTags();
//   }
    if (0 < sContent.length()) {
        this->setActualXmlContent(sContent);
    }
}

template<typename T_Mapper>bool Service_XmlParser<T_Mapper>::_parseAttributes(std::string sAttributes) {
    bool bReturn = false;
    boost::regex_constants::icase;        // nicht casesensitive
    boost::regex_constants::match_all;    // like preg_match_all
    boost::regex regex("([A-Za-z_\\-:]+)=['\"](.*?)['\"]");
    boost::smatch matches;                // container für die ergebnisse

    std::string::const_iterator start1 = sAttributes.begin();
    std::string::const_iterator end1   = sAttributes.end();
  
    while (boost::regex_search(start1, end1, matches, regex)) {
        std::string sName(matches[1].first, matches[1].second);
        std::string sValues(matches[2].first, matches[2].second);
        sName = this->_formatTagName(sName);
        sValues = this->_formatTagName(sValues);
        
        if (0 < sName.length()) {
            (*_oMap)(sName, sValues);
        }
        start1 = matches[0].second;
    }
    return true;
}

template<typename T_Mapper>std::string Service_XmlParser<T_Mapper>::_formatTagName(std::string sTagName) {
    sTagName = trim(sTagName);
    sTagName = fucase(sTagName);
    
    return sTagName;
}

template<typename T_Mapper>bool Service_XmlParser<T_Mapper>::_checkXmlHasChilds(std::string sXml) {
    sXml = trim(sXml);

    bool bReturn = false;
    boost::regex_constants::icase;
    boost::regex sRegExXmlHeader("(<.*? />)|(<.*?>.*?</.*?>)");
    boost::match_results<std::string::const_iterator>matches;

    std::string::const_iterator start = sXml.begin();
    std::string::const_iterator end   = sXml.end();
    bReturn = boost::regex_match(sXml, matches, sRegExXmlHeader, boost::match_partial | boost::format_perl);

    return bReturn;
}

template<typename T_Mapper>std::string Service_XmlParser<T_Mapper>::_eliminateRedundantRootTags() {
    std::string sContent = this->getActualXmlContent();
  
    boost::regex_constants::icase;
//   boost::regex sRegExRedundantRootTag("<" + getRootTag() + " *[^>]*>(.*?<" + getRootTag() + " *[^>]*/>|<" + getRootTag() + " *[^>]*>.*?</" + getRootTag() + ">.*?)</" + getRootTag() + ">");
    boost::regex sRegExRedundantRootTag("<" + this->getRootTag() + " *[^<]*?>(<" + this->getRootTag() + ".*/>)</" + this->getRootTag() + ">");
    boost::match_results<std::string::const_iterator>aMatches;
  
    if (boost::regex_match(sContent, aMatches, sRegExRedundantRootTag, boost::match_partial | boost::format_perl)) {
        if (DEBUG_MODE) {std::cout << "Content vor der Bereinigung überflüssigen RootTags : " << sContent << std::endl;};
        sContent = std::string(aMatches[1].first, aMatches[1].second);
        if (DEBUG_MODE) {std::cout << "Content wurde von überflüssigen RootTags gereinigt : " << sContent << std::endl;};
    }
    return sContent;
}

template<typename T_Mapper>std::string Service_XmlParser<T_Mapper>::_eliminateRedundantTags(std::string sContent, std::string sTagName) {
  
    boost::regex_constants::icase;
    boost::regex sRegExRedundantRootTag("<" + sTagName + " *[^<]*?>(<" + sTagName + ".*/>)</" + sTagName + ">");
    boost::match_results<std::string::const_iterator>aMatches;

    if (boost::regex_match(sContent, aMatches, sRegExRedundantRootTag, boost::match_partial | boost::format_perl)) {
        if (DEBUG_MODE) {std::cout << "Content vor der Bereinigung überflüssigen Tags (" << sTagName << ") : " << sContent << std::endl;};
        sContent = std::string(aMatches[1].first, aMatches[1].second);
        if (DEBUG_MODE) {std::cout << "Content wurde von überflüssigen Tags gereinigt (" << sTagName << ") : " << sContent << std::endl;};
    }
    return sContent;
}

template<typename T_Mapper>Service_XmlParser<T_Mapper>::~Service_XmlParser() {
    delete this->_oMap;
}

#endif
