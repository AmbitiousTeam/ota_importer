#include "Boarding.h"

bool Service_Boarding::collectBoardingsFromSupplements(Model_OTA_HotelRatePlanNotifRQ_Supplements* oSupplements) {
  if (oSupplements) {
    oSupplements->reset();
    while (Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement* oSupplement = (*oSupplements)++) {
      std::string sBoarding = oSupplement->getInvCode();
      std::string sSupplementType = oSupplement->getSupplementType();
      
      if ("BOARD" == toUpper(sSupplementType)) {
        if (_oBoardings->checkKeyExists(sBoarding)) {
          _oBoardings->get(sBoarding)->getSupplements()->add(*oSupplement);
        } else {
          Model_OTA_HotelRatePlanNotifRQ_Supplements* oNewSupplements = new Model_OTA_HotelRatePlanNotifRQ_Supplements();
          oNewSupplements->add(*oSupplement);
          Model_Boarding* oNewBoarding = new Model_Boarding();
          oNewBoarding->setSupplements(*oNewSupplements);
          _oBoardings->add(*oNewBoarding, sBoarding);
        }
      }
    }
  }
}

bool Service_Boarding::collectBoardingsFromSellableProducts(Model_OTA_HotelRatePlanNotifRQ_SellableProducts* oSellableProducts) {
  if (oSellableProducts) {
    oSellableProducts->reset();
    while (Model_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct* oSellableProduct = (*oSellableProducts)++) {
      std::string sBoarding = oSellableProduct->getInvCode();
      if (_oBoardings->checkKeyExists(sBoarding)) {
        _oBoardings->get(sBoarding)->getSellableProducts()->add(*oSellableProduct);
      } else {
        Model_OTA_HotelRatePlanNotifRQ_SellableProducts* oNewSellableProducts = new Model_OTA_HotelRatePlanNotifRQ_SellableProducts();
        oNewSellableProducts->add(*oSellableProduct);
        Model_Boarding* oNewBoarding = new Model_Boarding();
        oNewBoarding->setSellableProducts(*oNewSellableProducts);
        _oBoardings->add(*oNewBoarding, sBoarding);
      }
    }
  }
}

void Service_Boarding::setBoardings(Model_Boardings& oBoardings) {
  _oBoardings = &oBoardings;
}

Model_Boardings* Service_Boarding::getBoardings() {
  return _oBoardings;
}

Service_Boarding::Service_Boarding() {
  _oBoardings = new Model_Boardings();
}
