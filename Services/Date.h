#ifndef __SERVICE_DATE_H__
#define __SERVICE_DATE_H__

#include <QDateTime>
#include <stdio.h>
#include <iostream>
#include <string>
#include <time.h>
#include <sstream>
#include <ctime>

class Service_Date {
public:
  /*
   * checkt ob die übergebenen zeiträume des datums in irgend einer art und weise mit dem übergebenen
   * zeitraum kollidieren
   */
  static bool checkDateHitsDuration(std::string sDateStart, std::string sDateEnd, std::string sDurationStart, std::string sDurationEnd) {
    if (true == checkDateBetweenDuration(sDateStart, sDurationStart, sDurationEnd)
      || true == checkDateBetweenDuration(sDateEnd, sDurationStart, sDurationEnd)
    ) {
      return true;
    }
      return false;
  }
  
  /**
   * checkt ob das übergebene datum innerhalb oder auf dem start oder dem ende des zeitraums liegt
   */
  static bool checkDateBetweenDuration(std::string sDate, std::string sDurationStart, std::string sDurationEnd) {
    if (sDate >= sDurationStart
      && sDate <= sDurationEnd
    ) {
      return true;
    }
      return false;
  }

  static std::string addDaysToDate(std::string sDate, int iDays) {
    QDate oDate;
    QString d = QString::fromStdString(sDate.substr(0, 10));
    oDate = oDate.fromString(d, "yyyy-MM-dd").addDays(iDays);
    std::string sNewDate = oDate.toString("yyyy-MM-dd").toStdString();
    
    return sNewDate;
  }

  static std::string subDaysFromDate(std::string sDate, int iDays) {
    QDate oDate;
    QString d = QString::fromStdString(sDate.substr(0, 10));
    if (iDays > 0) {
      iDays *= -1;
    }
    oDate = oDate.fromString(d, "yyyy-MM-dd").addDays(iDays);
    std::string sNewDate = oDate.toString("yyyy-MM-dd").toStdString();
    return sNewDate;
  }

  static tm convertStringToDate(std::string sDate) {
    tm oDate = convertStringToTime(sDate);
    return oDate;
  }
  
  static tm convertStringToTime(std::string sDate) {
    std::string dateFormat = "%Y-%m-%d";
    tm oDate;
//     oDate.tm_mday = 0;
//     oDate.tm_year = 0;
//     oDate.tm_mon = 0;
//     oDate.tm_sec = 0;
//     oDate.tm_min = 0;
//     oDate.tm_hour = 0;
//     oDate.tm_wday = 0;
//     oDate.tm_yday = 0;
//     oDate.tm_gmtoff = 0;
//     oDate.tm_isdst = 0;
    
    if (strptime(sDate.c_str(), &dateFormat[0], &oDate) == NULL) { 
      exit(1);
    }
//     oDate.tm_year += 1900;
    return oDate;
  }
  
  /**
   * duration überlappt periode komplett
   */
  static bool checkOverlapps(std::string sStartPeriod, std::string sEndPeriod, std::string sStartDuration, std::string sEndDuration) {
    if (sStartDuration <= sStartPeriod
      && sStartDuration < sEndPeriod
      && sEndDuration >= sEndPeriod
    ) {
      return true;
    }
    return false;
  }
  
  /**
   * duration startet vor der Periode und ende mit oder nach der Periode 
   * -> split der Duration in 2 teile
   */
  static bool checkStartsBeforeEndsInner(std::string sStartPeriod, std::string sEndPeriod, std::string sStartDuration, std::string sEndDuration) {
    if (sStartDuration < sStartPeriod
        && sEndDuration > sStartPeriod
        && sEndDuration >= sEndPeriod
    ) {
      return true;
    }
    return false;
  }
  
  /*
   * duration startet innerhalb der periode und ende mit oder nach der periode
   */
  static bool checkStartsInnerEndsWithOrAfter(std::string sStartPeriod, std::string sEndPeriod, std::string sStartDuration, std::string sEndDuration) {
    if (sStartDuration > sStartPeriod
      && sStartDuration < sEndPeriod
      && sEndDuration >= sEndPeriod
    ) {
      return true;
    }
    return false;
  }
  
  /**
   * duration startet vor der periode und ende mit oder nach der periode
   */
  static bool checkStartsBeforeOrWithEndsWithOrAfter(std::string sStartPeriod, std::string sEndPeriod, std::string sStartDuration, std::string sEndDuration) {
    if (sStartDuration <= sStartPeriod
      && sEndDuration >= sEndPeriod
    ) {
      return true;
    }
    return false;
  }
  
  /**
   * duration liegt innerhalb der periode
   */
  static bool checkStartsInnerEndsInner(std::string sStartPeriod, std::string sEndPeriod, std::string sStartDuration, std::string sEndDuration) {
    if (sStartDuration > sStartPeriod
      && sEndDuration < sEndPeriod
    ) {
      return true;
    }
    return false;
  }
  
protected:
private:
};

#endif
