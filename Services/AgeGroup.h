#ifndef __SERVICE_AGEGROUP_H__
#define __SERVICE_AGEGROUP_H__

#include "Service.h"
#include "../Models/Occupancies.h"
#include "../Models/AgeGroups.h"
#include "../Models/AgeGroup.h"
#include "../Models/Supplements.h"

class Service_AgeGroup : public Service {
public:
  
  bool collectAgeInformationFromOccupancies(Model_OTA_HotelRatePlanNotifRQ_Occupancies*);
  bool collectAgeInformationFromSupplements(Model_OTA_HotelRatePlanNotifRQ_Supplements*);
  
  bool generateAgeGroups(void);
  
  void setAgeGroups(Model_AgeGroups*);
  Model_AgeGroups* getAgeGroups(void);
  
  void setDefaultAdultMinAge(int);
  int getDefaultAdultMinAge(void);
  
  void setDefaultAdultMaxAge(int);
  int getDefaultAdultMaxAge(void);
  
  void setDefaultChildrenMinAge(int);
  int getDefaultChildrenMinAge(void);
  
  void setDefaultChildrenMaxAge(int);
  int getDefaultChildrenMaxAge(void);
  
  void setDefaultInfantMinAge(int);
  int getDefaultInfantMinAge(void);
  
  void setDefaultInfantMaxAge(int);
  int getDefaultInfantMaxAge(void);
  
  Service_AgeGroup();
  ~Service_AgeGroup();
protected:
private:
  
  int _iDefaultAdultMinAge;
  int _iDefaultAdultMaxAge;
  int _iDefaultChildrenMinAge;
  int _iDefaultChildrenMaxAge;
  int _iDefaultInfantMinAge;
  int _iDefaultInfantMaxAge;
  
  Model_AgeGroups* _oAgeGroups = NULL;
  Model_Collection<unsigned int>* _oAges = NULL;
};

#endif
