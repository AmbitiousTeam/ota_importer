#include "Rate.h"

#include <iostream>
#include "../Tools/Md5.h"
#include "../Tools/File.h"

bool Service_Rate::generateOffers(void) {
  std::cout << "#################### Beginne mit dem erstellen der Offers für Rate von " << _oRateModel->getStart() << " bis " << _oRateModel->getEnd() << " #################################" << std::endl;
  std::cout << "Habe " << _oOffersModel->getCount() << " SpecialOffers!" << std::endl;
  std::cout << "Habe " << _oBookingRulesModel->getCount() << " BookingRules!" << std::endl;
  std::cout << "Habe " << _oSellableProductsModel->getCount() << " SellableProducts!" << std::endl;
  std::cout << "Habe " << _oSupplementsModel->getCount() << " Supplements!" << std::endl;
  
  filterSellableProducts();
  generateBoardings();
  considerRooms();
  
  return true;
}

/**
 * sortiert sellable products in room und boarding
 */
bool Service_Rate::filterSellableProducts(void) {
  Model_OTA_HotelRatePlanNotifRQ_SellableProducts* oSellableProductsModel = getSellableProductsModel();
  
  if (oSellableProductsModel) {
    oSellableProductsModel->reset();
    
    while (Model_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct* oSellableProduct = (*oSellableProductsModel)++) {
      
      if ("ROOM" == toUpper(oSellableProduct->getInvType())) {
        _oActualRoomsModel->add(*oSellableProduct);
      } else if ("BOARD" == toUpper(oSellableProduct->getInvType())) {
        _oActualBoardingsModel->add(*oSellableProduct);
      }
    }
    return true;
  }
}

/** erstellt aus den sellableProductsBoardings und den SupplementBoardings eine Boarding Collection 
 * in dieser collection liegen unter jedem boarding die jeweils passenden collections
 */
void Service_Rate::generateBoardings(void) {
  Service_Boarding* oBoardingService = new Service_Boarding();
  oBoardingService->collectBoardingsFromSellableProducts(_oActualBoardingsModel);
  oBoardingService->collectBoardingsFromSupplements(_oSupplementsModel);
  _oBoardings = oBoardingService->getBoardings();
}

bool Service_Rate::considerRooms(void) {
  if (_oActualRoomsModel) {
    _oActualRoomsModel->reset();
    
    while (Model_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct* oActualRoom = (*_oActualRoomsModel)++) {
      _sActualRoomCodeAgency = oActualRoom->getInvCode();
      _oAgeGroupsModel = generateAgeGroups(oActualRoom);
      _oTravellerConstellationsModel = generateTravellerConstellations(oActualRoom);
      considerBoardings();
    }
  }
}

bool Service_Rate::considerBoardings(void) {
  if (_oActualBoardingsModel) {
    _oActualBoardingsModel->reset();
    
    while (Model_Boarding* oBoarding = (*_oBoardings)++) {
      _sActualBoardingCodeAgency = _oBoardings->getActualNode()->getStringKey();
      _oActualBoardingModel = oBoarding;
      considerTravellerConstellations();
    }
  }
}

bool Service_Rate::considerTravellerConstellations(void) {
  if (_oTravellerConstellationsModel) {
    _oTravellerConstellationsModel->reset();
    while (Model_TravellerConstellation* oTravellerConstellation = (*_oTravellerConstellationsModel)++) {
      _oActualTravellerConstellationModel = oTravellerConstellation;
      considerPriceTypes();
    }
  }
}

bool Service_Rate::considerPriceTypes(void) {
  if (_oPriceTypes) {
    _oPriceTypes->reset();
    while (std::string* sPriceType = (*_oPriceTypes)++) {
      if (sPriceType) {
        _sActualPriceType = *sPriceType;
        /** @todo hier den preis berechnen und setzen, dieser preis wird dann von jedem ab hier erstellten offer verwendet **/
        generateOffer();
      }
    }
  }
}

/**
 * erstellt aus allen bis hierher gesammelten informationen das offer, alles was hier danach kommt nutzt das bestehende offer
 */
bool Service_Rate::generateOffer(void) {
  if (this->checkDataValid()) {
    Service_Offer* oOfferService = new Service_Offer();
    
    oOfferService->setTravellerConstellationModel(this->_oActualTravellerConstellationModel);
    oOfferService->setBaseByGuestAmtsModel(this->_oRateModel->getBaseByGuestAmts());
    oOfferService->setAdditionalGuestAmountsModel(this->_oRateModel->getAdditionalGuestAmounts());
    oOfferService->setSpecialOffers(this->_oOffersModel);
    oOfferService->setPriceType(this->_sActualPriceType);
    oOfferService->setDateFrom(this->getRateModel()->getStart());
    oOfferService->setDateTo(this->getRateModel()->getEnd());
    oOfferService->setSupplements(this->_oSupplementsModel);
    
    oOfferService->processTravellerConstellationModel();
    oOfferService->preparePriceCollections();
    oOfferService->considerPricesFromBaseByGuestAmts(this->_oRateModel->getBaseByGuestAmts());
    oOfferService->considerPricesFromAdditionalGuestAmounts(this->_oRateModel->getAdditionalGuestAmounts());
    oOfferService->filterSupplements();
    oOfferService->considerSupplements();
    oOfferService->considerSpecialOffers(this->_oOffersModel);
    
    oOfferService->generatePrice();
    Model_CSVOffer* oOfferEntity = new Model_CSVOffer();
    oOfferService->setOfferEntity(*oOfferEntity);
    oOfferService->generateOfferEntity();
    oOfferEntity = oOfferService->getOfferEntity();
    
    std::stringstream sContent;
    sContent << oOfferEntity;
    //   MD5 oMd5;
    //   std::string sMd5Hash = MD5(sContent.str()).hexdigest();
    //   std::string sMd5Hash = md5(sContent.str());
    //   std::cout << "MD5 : " << md5(sContent.str()) << std::endl;
    //   std::cout << "MD5 : " << sMd5Hash << std::endl;
    this->_oCsvOfferEntity = oOfferEntity;
    this->considerFileContent();
    
//     std::string sHashString = *oOfferEntity;
//     std::cout << "Offer Hash String : " << sHashString;
//     std::cout << "Offer Entity " << *oOfferEntity;
    std::cout << "Offer Entity " << oOfferEntity;
//     std::cout << "Erstelle Offer " << oOfferService->_iOfferCount << " " << this->getRateModel()->getStart() << " bis " << this->getRateModel()->getEnd() << " mit " << this->_oActualTravellerConstellationModel->getPersonCount() << " Personen - davon sind " << this->_oActualTravellerConstellationModel->getAdultCount() << " Erwachsene und " << this->_oActualTravellerConstellationModel->getChildCount() << " Kinder im Alter von " << this->_oActualTravellerConstellationModel->getAgeGroup()->getChildrenMinAge() << " bis " << this->_oActualTravellerConstellationModel->getAgeGroup()->getChildrenMaxAge() << " Jahren, Das Offer ist eine " << this->_sActualPriceType << " - Rate für " << this->_sActualTravelType << " für den Raum " << this->_sActualRoomCodeAgency << " und Boardcode " << this->_sActualBoardingCodeAgency << " mit einem Preis von " << oOfferService->getPrice() << "€" << std::endl;
    
    return true;
  }
  return false;
}

bool Service_Rate::checkDataValid(void) {
  if (! this->_oActualTravellerConstellationModel) {
    return false;
  }
  
  if (0 >= this->_oActualTravellerConstellationModel->getPersonCount()) {
    return false;
  }
  
  if (0 >= this->_oActualTravellerConstellationModel->getAdultCount()) {
    return false;
  }
  
  if (! this->_oActualBoardingModel) {
    return false;
  }
  
  if (! this->_oActualRoomsModel) {
    return false;
  }
  return true;
}

bool Service_Rate::considerTravelTypes(void) {
  if (this->_oTravelTypesModel) {
    this->_oTravelTypesModel->reset();
    while (std::string* sTravelType = (*this->_oTravelTypesModel)++) {
      if (sTravelType) {
        this->_sActualTravelType = *sTravelType;
        this->combineOfferWithProvision();
      }
    }
  }
}

bool Service_Rate::combineOfferWithProvision(void) {
}

/**
 * durchläuft alle gesammelten changedates und splittet die aktuelle rate durch diese,
 * es ergibt sich eine collection aus perioden, die zurückgegeben wird
 */
bool Service_Rate::considerSplitDatesForRates(Model_OTA_HotelRatePlanNotifRQ_Rates* oRates) {
  if (oRates) {
    Model_Collection<Model_Collection<Model_Collection<Model_Date>>>* oChangeDatesCollection = getChangeDatesService()->getChangeDates();
    
    if (DEBUG_MODE) {
      oRates->reset();
      oChangeDatesCollection->reset();
      int iDateCount = 1;
      std::cout << "Changedates: " << std::endl;
      
      while (Model_Collection<Model_Collection<Model_Date>>* oChangeDatesSubCollection = (*oChangeDatesCollection)++) {
        std::cout << "CollectionKey : " << oChangeDatesCollection->getActualNode()->getStringKey() << std::endl;
        std::cout << "SubCollectionKey : " << oChangeDatesSubCollection->getActualNode()->getStringKey() << std::endl;
        oChangeDatesSubCollection->reset();
        
        while (Model_Collection<Model_Date>* oDatesCollection = (*oChangeDatesSubCollection)++) {
          oDatesCollection->reset();
          
          while (Model_Date* oDate = (*oDatesCollection)++) {
            std::cout << "Date " << iDateCount << " : von " << oDate->getStart().substr(0, 10) << " bis " << oDate->getEnd().substr(0, 10) << std::endl;
            iDateCount++;
          }
        }
      }
      std::cout << "Raten Zeiträume vor dem Splitten: " << std::endl;
      int iRateCount = 1;
      
      while (Model_OTA_HotelRatePlanNotifRQ_Rates_Rate* oRate = (*oRates)++) {
        std::cout << "Rate " << iRateCount << " : von " << oRate->getStart().substr(0, 10) << " bis " << oRate->getEnd().substr(0, 10) << std::endl;
        iRateCount++;
      }
    }
    oChangeDatesCollection->reset();
    oRates->reset();

    if (DEBUG_MODE) {std::cout << "Beginne mit dem splitten der Raten durch die changedates!" << std::endl;};
    while (Model_OTA_HotelRatePlanNotifRQ_Rates_Rate* oRate = (*oRates)++) {
        if (DEBUG_MODE) {std::cout << "##### Habe eine Rate von ##" << oRate->getStart().substr(0, 10) << "## bis " << oRate->getEnd().substr(0, 10) << "#####" << std::endl;};
        
        this->considerChangeDatesCollectionWithRate(oRates, oRates->getActualNode(), oChangeDatesCollection);
    }
    
    if (DEBUG_MODE) {
      oRates->reset();
      int iRateCount = 1;
      
      std::cout << "Raten Zeiträume nach dem Splitten: " << std::endl;
      
      while (Model_OTA_HotelRatePlanNotifRQ_Rates_Rate* oRate = (*oRates)++) {
        std::cout << "Rate " << iRateCount << " : von " << oRate->getStart().substr(0, 10) << " bis " << oRate->getEnd().substr(0, 10) << std::endl;
        iRateCount++;
      }
    }
  }
  return true;
}

bool Service_Rate::considerChangeDatesCollectionWithRate(Model_OTA_HotelRatePlanNotifRQ_Rates* oRates, Model_Node<Model_OTA_HotelRatePlanNotifRQ_Rates_Rate>* oRateNode, Model_Collection<Model_Collection<Model_Collection<Model_Date>>>* oChangeDatesCollection) {
  
  oChangeDatesCollection->reset();
  Model_OTA_HotelRatePlanNotifRQ_Rates_Rate* oRate = oRateNode->getData();
  std::string sRateStart = oRate->getStart();
  std::string sRateEnd = oRate->getEnd();
  
  while (Model_Collection<Model_Collection<Model_Date>>* oChangeDatesSubCollection = (*oChangeDatesCollection)++) {
    oChangeDatesSubCollection->reset();
    
    while (Model_Collection<Model_Date>* oDatesCollection = (*oChangeDatesSubCollection)++) {  
      oDatesCollection->reset();
            
      while (Model_Date* oDate = (*oDatesCollection)++) {
        if (Service_Date::checkDateBetweenDuration(oDate->getStart(), sRateStart, sRateEnd)
            || Service_Date::checkDateBetweenDuration(oDate->getEnd(), sRateStart, sRateEnd)
        ) {
          if (DEBUG_MODE) {std::cout << "Start Datum " << oDate->getStart() << " oder " << oDate->getEnd() << " liegt zwischen " << sRateStart << " und " << sRateEnd << " der Rate!" << std::endl;};
          if (true == this->considerSplitDateForRate(oRateNode, oDate, oChangeDatesSubCollection->getActualNode()->getStringKey(), oRates)) {
              oRates->reset();
              return true;
          }
        }
      }
    }
  }
  return false;
}

bool Service_Rate::considerSplitDateForRate(Model_Node<Model_OTA_HotelRatePlanNotifRQ_Rates_Rate>* oRateNode, Model_Date* oDate, std::string sChangeDateType, Model_OTA_HotelRatePlanNotifRQ_Rates* oRates) {
  Model_OTA_HotelRatePlanNotifRQ_Rates_Rate* oRate = oRateNode->getData();
  
  if (oRate) {
    std::string sRateStart = oRate->getStart();
    std::string sRateEnd = oRate->getEnd();
    std::string sDateStart = oDate->getStart();
    std::string sDateEnd = oDate->getEnd();
    
    if (true == Service_Date::checkOverlapps(sRateStart, sRateEnd, sDateStart, sDateEnd)) {
      if (DEBUG_MODE) {std::cout << "Periode " << sRateStart << " - " << sDateEnd << " umschließt Rate komplett " << sRateStart << " - " << sRateEnd << std::endl;};
      return false;
    } else if (true == Service_Date::checkStartsBeforeEndsInner(sRateStart, sRateEnd, sDateStart, sDateEnd)) {
      if (DEBUG_MODE) {std::cout << "Periode " << sRateStart << " < " << sDateStart << " startet vor der Rate und endet Innerhalb " << sDateEnd << " < " << sRateEnd << std::endl;};
        
      std::string sNewRateStart = Service_Date::addDaysToDate(sDateEnd, 1);
      
      Model_OTA_HotelRatePlanNotifRQ_Rates_Rate* oNewRate = new Model_OTA_HotelRatePlanNotifRQ_Rates_Rate();
      *oNewRate = *oRate;
      oRate->setEnd(sDateEnd);
      oNewRate->setStart(sNewRateStart);

      std::stringstream sDateKey;
      sDateKey << sRateStart << "_" << sDateEnd;
      oRateNode->setKey(sDateKey.str());
      oRateNode->setData(*oRate);
      std::stringstream sNewDateKey;
      sNewDateKey << sNewRateStart << "_" << sRateEnd;
      oRates->inject(oRateNode, *oNewRate, true, sNewDateKey.str());
      return true;
    } else if (Service_Date::checkStartsInnerEndsWithOrAfter(sRateStart, sRateEnd, sDateStart, sDateEnd)) {
      if (DEBUG_MODE) {std::cout << "Periode " << sDateStart << " > " << sRateStart << " startet in der Rate und endet damit oder danach " << sDateEnd << " >= " << sRateEnd << std::endl;};
      std::string* sNewRateEnd = new std::string(Service_Date::subDaysFromDate(sDateStart, 1));
      Model_OTA_HotelRatePlanNotifRQ_Rates_Rate* oNewRate = new Model_OTA_HotelRatePlanNotifRQ_Rates_Rate();
      *oNewRate = *oRate;
      
      oRate->setEnd(*sNewRateEnd);
      oNewRate->setStart(sDateStart);
      std::stringstream sDateKey;
      sDateKey << sRateStart << "_" << sDateEnd;
      oRateNode->setKey(sDateKey.str());
      oRateNode->setData(*oRate);
      std::stringstream sNewDateKey;
      sNewDateKey << sDateStart << "_" << sNewRateEnd;
      oRates->inject(oRateNode, *oNewRate, true, sNewDateKey.str());
      return true;
    } else if (Service_Date::checkStartsInnerEndsInner(sRateStart, sRateEnd, sDateStart, sDateEnd)) {
      if (DEBUG_MODE) {std::cout << "Periode " << sRateStart << " > " << sDateStart << " startet in der Rate und endet innerhalb " << sDateEnd << " <= " << sRateEnd << std::endl;};
      Model_OTA_HotelRatePlanNotifRQ_Rates_Rate* oRatePrev = new Model_OTA_HotelRatePlanNotifRQ_Rates_Rate();
      Model_OTA_HotelRatePlanNotifRQ_Rates_Rate* oRateAfter = new Model_OTA_HotelRatePlanNotifRQ_Rates_Rate();
      *oRatePrev = *oRate;
      *oRateAfter = *oRate;
      std::string sRatePrevEnd = Service_Date::subDaysFromDate(sDateStart, 1);
      std::string sRateAfterStart = Service_Date::addDaysToDate(sDateEnd, 1);
      
      std::stringstream sOrigRateDateKey;
      sOrigRateDateKey << sDateStart << "_" << sDateEnd;
      oRateNode->setKey(sOrigRateDateKey.str());
      oRate->setStart(sDateStart);
      oRate->setEnd(sDateEnd);
      oRateNode->setData(*oRate);
      
      std::stringstream sRatePrevDateKey;
      sRatePrevDateKey << sRateStart << "_" << sRatePrevEnd;
      oRatePrev->setStart(sRateStart);
      oRatePrev->setEnd(sRatePrevEnd);
      
      oRates->inject(oRateNode, *oRatePrev, true, sRatePrevDateKey.str());
      
      std::stringstream sRateAfterDateKey;
      sRateAfterDateKey << sRateAfterStart << "_" << sRateEnd;
      oRateAfter->setStart(sRateAfterStart);
      oRateAfter->setEnd(sRateEnd);
      
      oRates->inject(oRateNode, *oRateAfter, true, sRateAfterDateKey.str());
      return true;
    } else {
      if (DEBUG_MODE) {std::cout << "Kein Treffer für " << sRateStart << " - " << sRateEnd << " und " << sDateStart << " - " << sDateEnd << std::endl;};
    }
  }
  return false;
}

bool Service_Rate::collectSplitDatesFromSpecialOffers(Model_OTA_HotelRatePlanNotifRQ_Offers* oOffersCollection) {
  if (oOffersCollection) {
    oOffersCollection->reset();
    
    // offers durchlaufen
    while (Model_OTA_HotelRatePlanNotifRQ_Offers_Offer* oOffer = (*oOffersCollection)++) {
      Model_OTA_HotelRatePlanNotifRQ_OfferRules* oOfferRulesCollection = oOffer->getOfferRules();
      
      if (oOfferRulesCollection) {
        oOfferRulesCollection->reset();
        
        while (Model_OTA_HotelRatePlanNotifRQ_OfferRules_OfferRule* oOfferRule = (*oOfferRulesCollection)++) {
          Model_OTA_HotelRatePlanNotifRQ_DateRestrictions* oDateRestrictionsCollection = oOfferRule->getDateRestrictions();
          
          if (oDateRestrictionsCollection) {
            oDateRestrictionsCollection->reset();
            
            while (Model_OTA_HotelRatePlanNotifRQ_DateRestrictions_DateRestriction* oDateRestriction = (*oDateRestrictionsCollection)++) {
              this->getChangeDatesService()->collectChangeDates(*oDateRestriction);
            }
          }
        }
      }
    }
  }
}

bool Service_Rate::collectSplitDatesFromSupplements(Model_OTA_HotelRatePlanNotifRQ_Supplements* oSupplementsCollection) {
  if (oSupplementsCollection) {
    if (DEBUG_MODE) {std::cout << "Sammle alle Datumsangaben der Supplements!" << std::endl;};
    
    oSupplementsCollection->reset();
    
    while (Model_OTA_HotelRatePlanNotifRQ_Supplements_Supplement* oSupplementModel = (*oSupplementsCollection)++) {
      this->getChangeDatesService()->collectChangeDates(*oSupplementModel);
    }
  }
}

bool Service_Rate::collectSplitDatesFromBookingRules(Model_OTA_HotelRatePlanNotifRQ_BookingRules* oBookingRulesCollection) {
  if (oBookingRulesCollection) {
    oBookingRulesCollection->reset();
  
    // rateplans durchlaufen
    while (Model_OTA_HotelRatePlanNotifRQ_BookingRules_BookingRule* oBookingRule = (*oBookingRulesCollection)++) {
      this->getChangeDatesService()->collectChangeDates(*oBookingRule);
      this->getChangeDatesService()->collectChangeDates(*oBookingRule->getCancelPenalty());
    }
  }
}

void Service_Rate::collectSplitDatesFromSellableProducts(Model_OTA_HotelRatePlanNotifRQ_SellableProducts* oSellableProductsModel) {
  if (oSellableProductsModel) {
    oSellableProductsModel->reset();
    
    while (Model_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct* oSellableProductModel = (*oSellableProductsModel)++) {
      getChangeDatesService()->collectChangeDates(*oSellableProductModel);
    }
  }
}

void Service_Rate::setChangeDatesService(Service_ChangeDates* oChangeDatesService) {
  _oChangeDatesService = oChangeDatesService;
}

Service_ChangeDates* Service_Rate::getChangeDatesService() {
  return _oChangeDatesService;
}

void Service_Rate::setBookingRulesModel(Model_OTA_HotelRatePlanNotifRQ_BookingRules* oBookingRulesModel) {
  _oBookingRulesModel = oBookingRulesModel;
}

Model_OTA_HotelRatePlanNotifRQ_BookingRules* Service_Rate::getBookingRulesModel(void) {
  return _oBookingRulesModel;
}

void Service_Rate::setOffersModel(Model_OTA_HotelRatePlanNotifRQ_Offers* oOffersModel) {
  _oOffersModel = oOffersModel;
}

Model_OTA_HotelRatePlanNotifRQ_Offers* Service_Rate::getOffersModel(void) {
  return _oOffersModel;
}

void Service_Rate::setSellableProductsModel(Model_OTA_HotelRatePlanNotifRQ_SellableProducts* oSellableProductsModel) {
  _oSellableProductsModel = oSellableProductsModel;
}

Model_OTA_HotelRatePlanNotifRQ_SellableProducts* Service_Rate::getSellableProductsModel(void) {
  return _oSellableProductsModel;
}

void Service_Rate::setSupplementsModel(Model_OTA_HotelRatePlanNotifRQ_Supplements* oSupplementsModel) {
  _oSupplementsModel = oSupplementsModel;
}

Model_OTA_HotelRatePlanNotifRQ_Supplements* Service_Rate::getSupplementsModel(void) {
  return _oSupplementsModel;
}

void Service_Rate::setRateModel(Model_OTA_HotelRatePlanNotifRQ_Rates_Rate* oRateModel) {
  _oRateModel = oRateModel;
}

Model_OTA_HotelRatePlanNotifRQ_Rates_Rate* Service_Rate::getRateModel(void) {
  return _oRateModel;
}

bool Service_Rate::generateTravelTypes(void) {
  Model_Collection<std::string>* oTravelTypesCollection = new Model_Collection<std::string>();
  std::string* sErde = new std::string("ERDE");
  std::string* sPaus = new std::string("PAUS");
  
  oTravelTypesCollection->add(*sErde);
  oTravelTypesCollection->add(*sPaus);

  _oTravelTypesModel = oTravelTypesCollection;
  return true;
}

bool Service_Rate::generatePriceTypes(void) {
  Model_Collection<std::string>* oPriceTypesCollection = new Model_Collection<std::string>();
  std::string* sBr = new std::string("BR");
  std::string* sEb = new std::string("EB");
  
  oPriceTypesCollection->add(*sBr);
  oPriceTypesCollection->add(*sEb);
  
  _oPriceTypes = oPriceTypesCollection;
  return true;
}

Model_AgeGroups* Service_Rate::generateAgeGroups(Model_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct* oSellableProductModel) {
  Service_AgeGroup* oAgeGroupService = new Service_AgeGroup;
  oAgeGroupService->setAgeGroups(getAgeGroupsModel());
  oAgeGroupService->collectAgeInformationFromOccupancies(oSellableProductModel->getGuestRoom()->getOccupancies());
  oAgeGroupService->collectAgeInformationFromSupplements(_oSupplementsModel);
  oAgeGroupService->generateAgeGroups();
  setAgeGroupsModel(oAgeGroupService->getAgeGroups());
  
  return oAgeGroupService->getAgeGroups();
}

Model_TravellerConstellations* Service_Rate::generateTravellerConstellations(Model_OTA_HotelRatePlanNotifRQ_SellableProducts_SellableProduct* oSellableProductModel) {
  Model_OTA_HotelRatePlanNotifRQ_GuestRoom* oGuestRoom = oSellableProductModel->getGuestRoom();
  Service_TravellerConstellations* oTravellerConstellationsService = new Service_TravellerConstellations;
  
  oTravellerConstellationsService->setAgeGroupsModel(getAgeGroupsModel());
  
  oTravellerConstellationsService->generateTravellerConstellationsFromGuestRoom(oGuestRoom);
  return oTravellerConstellationsService->getTravellerConstellations();
}

void Service_Rate::setAgeGroupsModel(Model_AgeGroups* oAgeGroupsModel) {
  _oAgeGroupsModel = oAgeGroupsModel;
}

Model_AgeGroups* Service_Rate::getAgeGroupsModel() {
  return _oAgeGroupsModel;
}

void Service_Rate::setActualSpecialOffersCollection(Model_OTA_HotelRatePlanNotifRQ_Offers* oSpecialOffersCollection) {
  _oActualSpecialOffersCollection = oSpecialOffersCollection;
}

Model_OTA_HotelRatePlanNotifRQ_Offers* Service_Rate::getActualSpecialOffersCollection(void) {
  return _oActualSpecialOffersCollection;
}

void Service_Rate::considerFileContent() {
  std::stringstream sStream;
  sStream << _sCsvOfferContent << _oCsvOfferEntity;
  _sCsvOfferContent = sStream.str();
  considerFileContentOfferCsv();
}

void Service_Rate::considerFileContentOfferCsv() {
  if (10000000 < _sCsvOfferContent.length()) {
    std::cout << "####################################    schreibe offer content ! ####################################" << std::endl;
    writeToFile(_sCsvOfferContent, "offer.csv");
    _sCsvOfferContent = "";
  }
}

void Service_Rate::considerFileContentPeriodDiscountCsv() {

}

void Service_Rate::writeToFile(std::string& sCsvOfferContent, const char* cFileName) {
//   void Service_Rate::writeToFile(std::string& sCsvOfferContent, std::string& sFileName) {
//   Tool_File oFile;
  std::string sFileName = std::string(cFileName);
//   oFile.save(sCsvOfferContent, _sFileName);
  Tool_File::save(sCsvOfferContent, sFileName);
  
}

Service_Rate::Service_Rate() {
  if (DEBUG_MODE) {std::cout << "CTOR von RateService!" << std::endl;};
//   Model_AgeGroups oAgeGroupsModel;
//   setAgeGroupsModel(oAgeGroupsModel);
  this->_oActualBoardingModel = NULL;
  this->_oActualBoardingsModel = new Model_OTA_HotelRatePlanNotifRQ_SellableProducts();
  this->_oActualExtraPersonCollection = NULL;
  this->_oActualFreeNightsCollection = NULL;
  this->_oActualRoomsModel = new Model_OTA_HotelRatePlanNotifRQ_SellableProducts();
  this->_oActualSellableProduct = NULL;
  this->_oActualSpecialOffersCollection = new Model_OTA_HotelRatePlanNotifRQ_Offers();
  this->_oActualTravellerConstellationModel = NULL;
  this->_oAgeGroupsModel = new Model_AgeGroups();
  this->_oBoardings = NULL;
  this->_oBookingRulesModel = NULL;
  this->_oChangeDatesService = NULL;
  this->_oCsvOfferEntity = NULL;
  this->_oOffersModel = NULL;
  this->_oPriceTypes = NULL;
  this->_oRateModel = NULL;
  this->_oSellableProductsModel = NULL;
  this->_oSupplementsModel = NULL;
  this->_oTravellerConstellationsModel = NULL;
  this->_oTravelTypesModel = NULL;
  
  Service_ChangeDates* oChangeDatesService = new Service_ChangeDates();
  this->setChangeDatesService(oChangeDatesService);
  
  this->generateTravelTypes();
  this->generatePriceTypes();
}

/**
 * DTOR von Service_Rate
 * 
 * wenn es im Service noch CsvOfferContent gibt, wird dieser abschließend noch in die Datei geschrieben!
 */
Service_Rate::~Service_Rate() {
  if (DEBUG_MODE) {std::cout << "DTOR von RateService!" << std::endl;};
  if (0 < _sCsvOfferContent.length()) {
    std::cout << "####################################    habe restlichen offer content ! ####################################" << std::endl;
    writeToFile(_sCsvOfferContent, "offer.csv");
    _sCsvOfferContent = "";
  }
}


