#ifndef __SERVICE_HOTEL_H__
#define __SERVICE_HOTEL_H__

#include "Service.h"
#include "RatePlan.h"
#include "../Models/HotelRatePlanNotifRQ.h"
#include "../Models/HotelAvailNotifRQ.h"

class Service_Hotel : public Service {
public:
  bool process(void);
  
  void setHotelAvailNotifRQModel(Model_OTA_HotelAvailNotifRQ*);
  Model_OTA_HotelAvailNotifRQ* getHotelAvailNotifRQModel();
  
  void setHotelRatePlanNotifRQModel(Model_OTA_HotelRatePlanNotifRQ*);
  Model_OTA_HotelRatePlanNotifRQ* getHotelRatePlanNotifRQModel();
  
  Service_Hotel();
  ~Service_Hotel();
protected:
private:
  Model_OTA_HotelAvailNotifRQ* _oHotelAvailNotifRQModel = NULL;
  Model_OTA_HotelRatePlanNotifRQ* _oHotelRatePlanNotifRQModel = NULL;
};

#endif
