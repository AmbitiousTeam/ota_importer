#include "test_qstring.h"

// void TestErster::toUpper() {
//   QString oQStr = "Hello";
  
//   QVERIFY(oQStr.toUpper() == "HELLO");
// }

void ExampleTestSuite::first_test()
{
    // Will succeed since the expression evaluates to true
    TEST_ASSERT(1 + 1 == 2)
    
    // Will fail since the expression evaluates to false
    TEST_ASSERT(0 == 1);
}

void ExampleTestSuite::second_test()
{
    // Will succeed since the expression evaluates to true
    TEST_ASSERT_DELTA(0.5, 0.7, 0.3);
    
    // Will fail since the expression evaluates to false
    TEST_ASSERT_DELTA(0.5, 0.7, 0.1);
}