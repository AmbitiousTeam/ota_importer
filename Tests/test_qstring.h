// // #include <QtTest/QtTest>
#include <unittest++/UnitTest++.h>
#include <cpptest.h>

// class TestErster : public QObject {
//   Q_Object
  
//   private slots:
//     void toUpper();
// };

class ExampleTestSuite : public Test::Suite {
  
  public:
      ExampleTestSuite()
      {
	  TEST_ADD(ExampleTestSuite::first_test)
	  TEST_ADD(ExampleTestSuite::second_test)
      }
      
  private:
      void first_test();
      void second_test();
};

